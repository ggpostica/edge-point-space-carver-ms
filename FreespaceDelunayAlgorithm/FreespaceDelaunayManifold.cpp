//  Copyright 2014 Andrea Romanoni
//
//  This file is part of edgePointSpaceCarver.
//
//  edgePointSpaceCarver is free software: you can redistribute it
//  and/or modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version see
//  <http://www.gnu.org/licenses/>..
//
//  edgePointSpaceCarver is distributed in the hope that it will be
//  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
#include "FreespaceDelaunayManifold.h"

#include <sys/time.h>
#include <algorithm>

#include <sstream>
#include <iostream>
#include <fstream>
#include "../src/utilities/utilities.hpp"

#define USE_INV_CONE

//msc=manifoldSpaeCarving
namespace msc {
// Constructors

FreespaceDelaunayManifold::FreespaceDelaunayManifold() {
  shrinkEnabled_ = false;
  r_ = 40.0;
  w_1 = 1.0;
  w_2 = 0.5;
  w_3 = 0.05;
  num_ = 1;
  infiniteValue_ = 5000.0;
  deltatot = 0;

  calculateBoundsValues();
  l_ = sqrt(stepX_ * stepX_ + stepY_ * stepY_ + stepZ_ * stepZ_);
}

// Getters

const vector<Matrix> & FreespaceDelaunayManifold::getPoints() const {
  return m_points;
}

void FreespaceDelaunayManifold::setConfig(Configuration configuration) {
  configuration_ = configuration;
  fileStats_.open(configuration_.outputSpaceCarving.pathStatsManifold.c_str());
  if (!fileStats_.is_open()) {
    std::cerr << "fileStats_ not opened; file name: " << configuration_.outputSpaceCarving.pathStatsManifold.c_str() << std::endl;
  }
  r_ = configuration_.spaceCarvingConfig.maxDistanceCamFeature;
}

Matrix FreespaceDelaunayManifold::getPoint(const int index) const {
  return m_points[index];
}

int FreespaceDelaunayManifold::numPoints() const {
  return (int) m_points.size();
}

const vector<Matrix> & FreespaceDelaunayManifold::getCams() const {
  return m_cams;
}

const int & FreespaceDelaunayManifold::getCurCam() const {

  return curCam_;
}

Matrix FreespaceDelaunayManifold::getCam(const int index) const {
  return m_cams[index];
}

const vector<Matrix> & FreespaceDelaunayManifold::getCamCenters() const {
  return m_camCenters;
}

Matrix FreespaceDelaunayManifold::getCamCenter(const int index) const {
  return m_camCenters[index];
}

const vector<Matrix> & FreespaceDelaunayManifold::getPrincipleRays() const {
  return m_principleRays;
}

Matrix FreespaceDelaunayManifold::getPrincipleRay(const int index) const {
  return m_principleRays[index];
}

const vector<vector<int> > & FreespaceDelaunayManifold::getVisibilityList() const {
  return m_visibilityList;
}
const vector<vector<bool> > & FreespaceDelaunayManifold::getVisibilityNewPoint() const {
  return visibilityListNewPoint_;
}

const vector<int> & FreespaceDelaunayManifold::getVisibilityList(const int index) const {
  return m_visibilityList[index];
}
const vector<bool> & FreespaceDelaunayManifold::getVisibilityNewPoint(const int index) const {
  return visibilityListNewPoint_[index];
}

void FreespaceDelaunayManifold::setVisibilityNewPoint(const int index, const int point, bool val) {
  visibilityListNewPoint_[index].at(point) = val;
}
int FreespaceDelaunayManifold::numCams() const {
  return (int) m_camCenters.size();
}

double FreespaceDelaunayManifold::getBoundsMin() const {
  return m_nBoundsMin;
}

double FreespaceDelaunayManifold::getBoundsMax() const {
  return m_nBoundsMax;
}

// Setters

void FreespaceDelaunayManifold::setPoints(const vector<Matrix> & ref) {
  m_points = ref;
}

void FreespaceDelaunayManifold::setCams(const vector<Matrix> & ref) {
  m_cams = ref;
}

void FreespaceDelaunayManifold::setCurCam(const int & ref) {
  curCam_ = ref;
}

void FreespaceDelaunayManifold::setCamCenters(const vector<Matrix> & ref) {
  m_camCenters = ref;
}

void FreespaceDelaunayManifold::setPrincipleRays(const vector<Matrix> & ref) {
  m_principleRays = ref;
}

void FreespaceDelaunayManifold::setVisibilityList(const vector<vector<int> > & ref) {
  m_visibilityList = ref;
}

void FreespaceDelaunayManifold::addPoint(const Matrix & ref) {
  m_points.push_back(ref);
  m_visibilityListTransp.push_back(vector<int>());
}
int FreespaceDelaunayManifold::addPointWhere(const Matrix & ref) {
  m_points.push_back(ref);
  m_visibilityListTransp.push_back(vector<int>());
  return m_points.size() - 1;
}

void FreespaceDelaunayManifold::movePoint(const Matrix &ref, int idxPoint) {
  //points_moves_.push_back(std::pair<Matrix, int>(ref, idxPoint));
  m_points[idxPoint] = ref;
  points_moves_.push_back(idxPoint);
  //m_visibilityListTransp.push_back(vector<int>());
}
Matrix FreespaceDelaunayManifold::movePointGetOld(const Matrix &ref, int idxPoint) {
  //points_moves_.push_back(std::pair<Matrix, int>(ref, idxPoint));
  Matrix old = m_points[idxPoint];
  m_points[idxPoint] = ref;
  points_moves_.push_back(idxPoint);
  return old;
  //m_visibilityListTransp.push_back(vector<int>());
}

void FreespaceDelaunayManifold::addCamCenter(const Matrix & ref) {
  m_camCenters.push_back(ref);
  m_visibilityList.push_back(vector<int>());
  visibilityListNewPoint_.push_back(vector<bool>());
}

void FreespaceDelaunayManifold::addVisibilityPair(const int camIndex, const int pointIndex) {
  m_visibilityList[camIndex].push_back(pointIndex);
  m_visibilityListTransp[pointIndex].push_back(camIndex);
  visibilityListNewPoint_[camIndex].push_back(true);
}

void FreespaceDelaunayManifold::addVisibilityPair(const std::pair<int, int> visibilityPair) {
  m_visibilityList[visibilityPair.first].push_back(visibilityPair.second);
}

// Operators

FreespaceDelaunayManifold & FreespaceDelaunayManifold::operator=(const FreespaceDelaunayManifold & rhs) {
  if (this != &rhs) {
    copy(rhs.getPoints(), rhs.getCams(), rhs.getCamCenters(), rhs.getPrincipleRays(), rhs.getVisibilityList());
    m_mapPoint_VertexHandle = rhs.m_mapPoint_VertexHandle;
    calculateBoundsValues();
  }
  return *this;
}

// Public Methods

bool FreespaceDelaunayManifold::isVisible(const int pointIndex, const int viewIndex) const {
  const vector<int> & visList = getVisibilityList(viewIndex);

  for (vector<int>::const_iterator it = visList.begin(); it != visList.end(); it++) {
    if (*it == pointIndex)
      return true;
  }
  return false;
}

void FreespaceDelaunayManifold::IterateTetrahedronMethod_suboptimal(const int frameIndex) {
  // If the bounding vertices doesn't exist, create it as the first 8 vertices in dt:
  if (dt.number_of_vertices() == 0) {
    createSteinerPointGridAndBound();
  }
  setCurCam(frameIndex);

  if (shrinkEnabled_) {

    shrinkBall(curCam_);

    if (configuration_.outputSpaceCarving.enableSaveShrink) {
      std::stringstream outName;
      outName << configuration_.outputSpaceCarving.pathToSave << "afterShrinkBoundary" << curCam_ << ".obj";
      writeBoundaryToObj(outName.str());
    }
  }

  int count = 0;

  //std::cout << "Vertex to be moved: " << points_moves_.size() << std::endl;
  for (auto it : points_moves_) {
    if (moveVertex_suboptimal(vecVertexHandles_, it)) {
      count++;
    }
  }

  //moveVertex(vecVertexHandles_, points_moves_);
  fileStats_ << "Vertex moved:  " << count << "/" << points_moves_.size() << std::endl;
  points_moves_.clear();

  vector<int> localVisList;
  addNewlyObservedFeatures_suboptimal(vecVertexHandles_, localVisList, getVisibilityList(frameIndex));

#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif
  // Apply the current view's freespace constraints to the triangulation
  Matrix matO = getCamCenter(frameIndex);
  PointD3 O(matO(0), matO(1), matO(2));
  for (int j = 0; j < (int) localVisList.size(); j++) {
    // let Q be the point & O the optic center.
    Delaunay3::Vertex_handle hndlQ = vecVertexHandles_[localVisList[j]];

    Segment QO = Segment(hndlQ->point(), O);

    // Increment the voting counts of all tetrahedra that intersect the constraint QO & keep track of which constraints crossed which tetrahedra.
    rayTracing_suboptimal(vecVertexHandles_, hndlQ, QO, frameIndex, localVisList[j], false, true);
  }
  // Done marking tetrahedra; return.
#ifdef LOGGING
  gettimeofday(&endCarv, NULL);
  double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
  fileStats_ << "RayCasting " << delta << std::endl;
#endif
}
void FreespaceDelaunayManifold::IterateTetrahedronMethod(const int frameIndex) {
  // If the bounding vertices don't exist, create it as the first 8 vertices in dt:
  if (dt.number_of_vertices() == 0) {
    createSteinerPointGridAndBound();
    //createBounds();
  }
  setCurCam(frameIndex);

  if (shrinkEnabled_) {
    shrinkBall(curCam_);

    if (configuration_.outputSpaceCarving.enableSaveShrink) {
      std::stringstream outName;
      outName << configuration_.outputSpaceCarving.pathToSave << "afterShrinkBoundary" << curCam_ << ".obj";
      writeBoundaryToObj(outName.str());
    }
  }

  int count = 0;

  //std::cout << "Vertex to be moved: " << points_moves_.size() << std::endl;
  for (auto it : points_moves_) {
    if (moveVertex(vecVertexHandles_, it)) {
      count++;
    }
  }

  fileStats_ << "Vertex moved:  " << count << "/" << points_moves_.size() << std::endl;
  points_moves_.clear();
  // Add the newly observed points into the triangulation and the vertex handle list.
  // This involves, for each new point, properly deleting & staring off a connected subset of tetrahedra that violate the Delaunay constraint,
  // while marking the new tetrahedra with the deleted tetrahedra's freespace constraints.

  vector<int> localVisList;
  addNewlyObservedFeatures(vecVertexHandles_, localVisList, getVisibilityList(frameIndex));

#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif
  // Apply the current view's freespace constraints to the triangulation
  Matrix matO = getCamCenter(frameIndex);
  PointD3 O(matO(0), matO(1), matO(2));
  for (int j = 0; j < (int) localVisList.size(); j++) {
    // let Q be the point & O the optic center.
    Delaunay3::Vertex_handle hndlQ = vecVertexHandles_[localVisList[j]];

    Segment QO = Segment(hndlQ->point(), O);

    // Increment the voting counts of all tetrahedra that intersect the constraint QO & keep track of which constraints crossed which tetrahedra.
    rayTracing(vecVertexHandles_, hndlQ, QO, frameIndex, localVisList[j]);
  }
  // Done marking tetrahedra; return.
#ifdef LOGGING
  gettimeofday(&endCarv, NULL);
  double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
  fileStats_ << "RayCasting " << delta << std::endl;
#endif
}

bool FreespaceDelaunayManifold::sortTetByIntersection(Delaunay3::Cell_handle i, Delaunay3::Cell_handle j) {

#ifdef USE_INV_CONE
  return i->info().getVoteCountProb() < j->info().getVoteCountProb();
#endif
#ifndef USE_INV_CONE
  return i->info().getNumIntersection() < j->info().getNumIntersection();
#endif
}

bool FreespaceDelaunayManifold::additionTest(Delaunay3::Cell_handle i) {

  bool additionManifold;

  int numV = 0;
  int numFound = 0;
  for (int curVertexId = 0; curVertexId < 4; ++curVertexId) {
    // bool intersectionFound = false;

    /* std::vector<Delaunay3::Cell_handle> incidentCells;*/
    /*dt.incident_cells(i->vertex(curVertexId), std::back_inserter(incidentCells));*/
    /*
     std::cout<<"ISUSED "<<i->vertex(curVertexId)->info().isUsed()<<std::endl;
     std::cout<<"incidentCells.size() "<<incidentCells.size()<<std::endl;*/
    if (i->vertex(curVertexId)->info().isUsed() > 0) {
      numFound++;
    }
    /*for (std::vector<Delaunay3::Cell_handle>::iterator celIt = incidentCells.begin(); celIt != incidentCells.end(); ++celIt) {
     if ((*celIt)->info().iskeptManifold()) {
     intersectionFound = true;
     }
     }*/
    /*if (intersectionFound) {
     numV++;
     if(numFound!=numV){
     std::cout<< i->vertex(curVertexId)->point()<<std::endl;
     std::cout<<"WRONG"<<std::endl;
     }
     }*/
  }
  numV = numFound;
  int numE = 0;
  for (int curEdgeId1 = 0; curEdgeId1 < 4; ++curEdgeId1) {
    for (int curEdgeId2 = curEdgeId1 + 1; curEdgeId2 < 4; ++curEdgeId2) {
      bool intersectionFound = false;
      Delaunay3::Edge curEdge(i, curEdgeId1, curEdgeId2);

      Delaunay3::Cell_circulator cellCirc = dt.incident_cells(curEdge, i);
      Delaunay3::Cell_circulator cellCircInit = dt.incident_cells(curEdge, i);

      do {
        if (cellCirc->info().iskeptManifold()) {
          intersectionFound = true;
        }
        cellCirc++;
      } while (cellCirc != cellCircInit);

      if (intersectionFound) {
        numE++;
      }
    }
  }

  int numF = 0;
  for (int curNeighId = 0; curNeighId < 4; ++curNeighId) {
    bool intersectionFound = false;

    if (i->neighbor(curNeighId)->info().iskeptManifold()) {
      intersectionFound = true;
    }
    if (intersectionFound) {
      numF++;
    }
  }

  if ((numV == 0 && numE == 0 && numF == 0) || (numV == 3 && numE == 3 && numF == 1) || (numV == 4 && numE == 5 && numF == 2)
      || (numV == 4 && numE == 6 && numF == 3) || (numV == 4 && numE == 6 && numF == 4)) {
    additionManifold = true;
  } else {
    additionManifold = false;
  }
  return additionManifold;

}

bool FreespaceDelaunayManifold::subtractionTest(Delaunay3::Cell_handle i) {

  bool subtractionManifold;

  int numV = 0;
  int numFound = 0;
  for (int curVertexId = 0; curVertexId < 4; ++curVertexId) {
    /*bool intersectionFound = false;

     std::vector<Delaunay3::Cell_handle> incidentCells;
     dt.incident_cells(i->vertex(curVertexId), std::back_inserter(incidentCells));*/

    if (i->vertex(curVertexId)->info().isNotUsed()) {
      numFound++;
    }
    /*for (std::vector<Delaunay3::Cell_handle>::iterator celIt = incidentCells.begin(); celIt != incidentCells.end(); ++celIt) {
     if (!(*celIt)->info().iskeptManifold()) {
     intersectionFound = true;
     }
     }
     if (intersectionFound) {
     numV++;
     }
     if(numFound!=numV){

     std::vector<int> notManifoldNeigh;
     isInBoundary(i, notManifoldNeigh);

     std::cout<<notManifoldNeigh.size()<<std::endl;
     std::cout<<"WRONG"<<std::endl;
     }*/
  }
  numV = numFound;

  int numE = 0;
  for (int curEdgeId1 = 0; curEdgeId1 < 4; ++curEdgeId1) {
    for (int curEdgeId2 = curEdgeId1 + 1; curEdgeId2 < 4; ++curEdgeId2) {
      bool intersectionFound = false;
      Delaunay3::Edge curEdge(i, curEdgeId1, curEdgeId2);

      Delaunay3::Cell_circulator cellCirc = dt.incident_cells(curEdge, i);
      Delaunay3::Cell_circulator cellCircInit = dt.incident_cells(curEdge, i);

      do {
        if (!cellCirc->info().iskeptManifold()) {
          intersectionFound = true;
        }
        cellCirc++;
      } while (cellCirc != cellCircInit);

      if (intersectionFound) {
        numE++;
      }
    }
  }

  int numF = 0;
  for (int curNeighId = 0; curNeighId < 4; ++curNeighId) {
    bool intersectionFound = false;

    if (!i->neighbor(curNeighId)->info().iskeptManifold()) {
      intersectionFound = true;
    }
    if (intersectionFound) {
      numF++;
    }
  }

  if ((numV == 0 && numE == 0 && numF == 0) || (numV == 3 && numE == 3 && numF == 1) || (numV == 4 && numE == 5 && numF == 2)
      || (numV == 4 && numE == 6 && numF == 3) || (numV == 4 && numE == 6 && numF == 4)) {
    subtractionManifold = true;
  } else {
    subtractionManifold = false;
  }
  return subtractionManifold;

}

void FreespaceDelaunayManifold::regionGrowingIterative(int idxCam) {
#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif
  if (boundaryCells_.empty()) {

    // Locate the idxCam-th cam and start from this one to grow the manifold
    PointD3 firstCamPosition = PointD3(getCamCenter(idxCam)(0), getCamCenter(idxCam)(1), getCamCenter(idxCam)(2));
    Delaunay3::Cell_handle startingCell = dt.locate(firstCamPosition);

    addTetAndUpdateBoundary(startingCell);
  }
  regionGrowingProcedure();

#ifdef LOGGING
  gettimeofday(&endCarv, NULL);
  double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
  fileStats_ << "RegionGrowingIt " << delta << std::endl;
#endif
}

bool FreespaceDelaunayManifold::isInBoundary(Delaunay3::Cell_handle cellToTest) {
  std::vector<int> toThrowAway;

  return isInBoundary(cellToTest, toThrowAway);
}

bool FreespaceDelaunayManifold::isInBoundary(Delaunay3::Cell_handle cellToTest, std::vector<int> &neighNotManifold) {

  if (!cellToTest->info().iskeptManifold()) {
    return false;
  } else {
    bool neighNotManifoldFound = false;

    for (int curNeighId = 0; curNeighId < 4; ++curNeighId) {
      if (!cellToTest->neighbor(curNeighId)->info().iskeptManifold()) {
        neighNotManifold.push_back(curNeighId);

        neighNotManifoldFound = true;
      }
    }
    return neighNotManifoldFound;
  }
}

bool FreespaceDelaunayManifold::insertInBoundary(Delaunay3::Cell_handle cellToBeAdded) {

  //  if (find(boundaryCells_.begin(), boundaryCells_.end(), cellToBeAdded) == boundaryCells_.end()) {
  if (!cellToBeAdded->info().isBoundary()) {
    cellToBeAdded->info().setBoundary(true);
    vector<Delaunay3::Cell_handle>::iterator it = std::lower_bound(boundaryCells_.begin(), boundaryCells_.end(), cellToBeAdded,
        FreespaceDelaunayManifold::sortTetByIntersection);
    boundaryCells_.insert(it, cellToBeAdded);
    return true;
  } else {
    return false;
  }

}

bool FreespaceDelaunayManifold::removeFromBoundary(Delaunay3::Cell_handle cellToBeRemoved) {
  if (!cellToBeRemoved->info().isBoundary()) {
    return false;
  } else {
    //TODO make this work
    /*vector<Delaunay3::Cell_handle>::iterator it;
     it = std::lower_bound(boundaryCells_.begin(), boundaryCells_.end(), cellToBeRemoved, FreespaceDelaunayManifold::sortTetByIntersection);*/
    vector<Delaunay3::Cell_handle>::iterator it2;

    int num = 0;

    vector<Delaunay3::Cell_handle>::iterator itmin = std::lower_bound(boundaryCells_.begin(), boundaryCells_.end(), cellToBeRemoved,
        FreespaceDelaunayManifold::sortTetByIntersection);
    vector<Delaunay3::Cell_handle>::iterator itmax = std::upper_bound(boundaryCells_.begin(), boundaryCells_.end(), cellToBeRemoved,
        FreespaceDelaunayManifold::sortTetByIntersection);
    it2 = std::find(itmin, itmax, cellToBeRemoved);
    if ((*it2) == cellToBeRemoved) {
      cellToBeRemoved->info().setBoundary(false);
      boundaryCells_.erase(it2);
      ++num;
    } else {
      it2 = std::find(boundaryCells_.begin(), boundaryCells_.end(), cellToBeRemoved);
      //while (it2 != boundaryCells_.end()) {
      cellToBeRemoved->info().setBoundary(false);
      boundaryCells_.erase(it2);
      ++num;
      //it2 = std::find(boundaryCells_.begin(), boundaryCells_.end(), cellToBeRemoved);
      // }
    }

    if (num == 0) {
      std::cerr << "FreespaceDelaunayManifold::removeFromBoundary::notFound" << std::endl;
    } else if (num > 1) {
      std::cout << "FreespaceDelaunayManifold::removeFromBoundary::morethan1" << std::endl;
    }

    return true;
  }
}

void FreespaceDelaunayManifold::addTetAndUpdateBoundary(Delaunay3::Cell_handle currentTet) {

  //add the current tetrahedron in the manifold set and add it to the boundary if needed
  currentTet->info().setKeptManifold(true);
  currentTet->info().setShrinked(false);

  std::vector<int> notManifoldNeigh;
  if (isInBoundary(currentTet, notManifoldNeigh)) {
    insertInBoundary(currentTet);
  } else {
    //the tetrahedron is not inserted yet in the boundaryCells_ vector so we do not need to update it
    if (currentTet->info().isBoundary()) {
      removeFromBoundary(currentTet);
    }
    currentTet->info().setBoundary(false);

  }

  if (notManifoldNeigh.size() == 3) {
    for (int curV = 0; curV < 4; ++curV) {
      if (find(notManifoldNeigh.begin(), notManifoldNeigh.end(), curV) == notManifoldNeigh.end()) {
        currentTet->vertex(curV)->info().setUsed(1);
      }
    }
  } else if (notManifoldNeigh.size() == 4) {
    for (int curV = 0; curV < 4; ++curV) {
      currentTet->vertex(notManifoldNeigh[curV])->info().setUsed(1);
    }
  }

  if (notManifoldNeigh.size() == 1) {  //when you fill a "hole"
    currentTet->vertex(notManifoldNeigh[0])->info().setNotUsed(false);
  } else if (notManifoldNeigh.size() == 0) {
    for (int curV = 0; curV < 4; ++curV) {
      currentTet->vertex(curV)->info().setNotUsed(false);
    }
  }

  //Check if the neigh of the added tetrahedron still belongs to the boundary
  for (int curNeighId = 0; curNeighId < 4; ++curNeighId) {
    Delaunay3::Cell_handle currNeigh = currentTet->neighbor(curNeighId);
    //if needed, remove the neighbor of currentTet from the boundary if no facet belongs  to the manifold
    //note that isBoundary function returns the state flag of the tetrahedron, while
    //isInBoundary() check for the current real state of the tetrahedron inside the triangulation after
    // the new tetrahedron has been added
    if (currNeigh->info().isBoundary()) {
      //We nested this if since it is more efficient to test firstly
      // if the boundary state flag of the tetrahedron is set true
      if (!isInBoundary(currNeigh)) {
        removeFromBoundary(currNeigh);
      }
    }
  }

  /*
   //
   int numV = 0;
   int numFound = 0;
   for (int curVertexId = 0; curVertexId < 4; ++curVertexId) {
   bool intersectionFound = false;

   std::vector<Delaunay3::Cell_handle> incidentCells;
   dt.incident_cells(currentTet->vertex(curVertexId), std::back_inserter(incidentCells));

   if ( currentTet->vertex(curVertexId)->info().isNotUsed()){
   numFound++;
   }
   for (std::vector<Delaunay3::Cell_handle>::iterator celIt = incidentCells.begin(); celIt != incidentCells.end(); ++celIt) {
   if (!(*celIt)->info().iskeptManifold()) {
   intersectionFound = true;
   }
   }
   if (intersectionFound) {
   numV++;
   }

   //std::cout<<"Vertex id: "<<curVertexId<<" numV = "<<numV<<"; numFound = "<<numFound<<"; notManifoldNeigh = "<<notManifoldNeigh.size()<<std::endl;

   std::cout<<currentTet->vertex(curVertexId)->point().x()<< " ";
   std::cout<<currentTet->vertex(curVertexId)->point().y()<< " ";
   std::cout<<currentTet->vertex(curVertexId)->point().z()<<std::endl;
   if(numFound!=numV){

   std::vector<int> notManifoldNeigh;
   isInBoundary(currentTet, notManifoldNeigh);

   std::cout<<"WRONG"<<std::endl;
   }
   }*/

}

void FreespaceDelaunayManifold::subTetAndUpdateBoundary(Delaunay3::Cell_handle currentTet) {

  //remove the current tetrahedron from the manifold set and remove it from the boundary if needed

  std::vector<int> notManifoldNeigh;
  isInBoundary(currentTet, notManifoldNeigh);

  removeFromBoundary(currentTet);

  currentTet->info().setKeptManifold(false);

  /*for (int curV = 0; curV < 4; ++curV) {
   currentTet->vertex(curV)->info().decrUsed();
   }*/

  if (notManifoldNeigh.size() == 3) {
    for (int curV = 0; curV < 4; ++curV) {
      if (find(notManifoldNeigh.begin(), notManifoldNeigh.end(), curV) == notManifoldNeigh.end()) {
        currentTet->vertex(curV)->info().setUsed(0);
      }
    }
  } else if (notManifoldNeigh.size() == 4) {
    for (int curV = 0; curV < 4; ++curV) {
      currentTet->vertex(notManifoldNeigh[curV])->info().setUsed(0);
    }
  }

  if (notManifoldNeigh.size() == 1) {      //when you fill a "hole"
    currentTet->vertex(notManifoldNeigh[0])->info().setNotUsed(true);
  } else if (notManifoldNeigh.size() == 4) {
    for (int curV = 0; curV < 4; ++curV) {
      currentTet->vertex(notManifoldNeigh[curV])->info().setNotUsed(true);
    }
  }
  currentTet->info().setShrinked(true);

  //Check if the neigh of the added tetrahedron belongs to the boundary
  for (int curNeighId = 0; curNeighId < 4; ++curNeighId) {
    Delaunay3::Cell_handle currNeigh = currentTet->neighbor(curNeighId);
    //if needed, add the neighbor of currentTet from the boundary if at least one facet belongs  to the manifold
    //note that isBoundary function returns the state flag of the tetrahedron, while
    //isInBoundary() check for the current real state of the tetrahedron inside the triangulation after
    // the new tetrahedron has been added
    if (!currNeigh->info().isBoundary()) {
      //We nested this "if" since it is more efficient to test firstly
      // if the boundary state flag of the tetrahedron is set to false
      if (currNeigh->info().iskeptManifold()) {
        //if (isInBoundary(currNeigh)) {
        insertInBoundary(currNeigh);
      }
    }
  }

  /*
   int numV = 0;
   int numFound = 0;
   for (int curVertexId = 0; curVertexId < 4; ++curVertexId) {
   bool intersectionFound = false;

   std::vector<Delaunay3::Cell_handle> incidentCells;
   dt.incident_cells(currentTet->vertex(curVertexId), std::back_inserter(incidentCells));

   if ( currentTet->vertex(curVertexId)->info().isNotUsed()){
   numFound++;
   }
   for (std::vector<Delaunay3::Cell_handle>::iterator celIt = incidentCells.begin(); celIt != incidentCells.end(); ++celIt) {
   if (!(*celIt)->info().iskeptManifold()) {
   intersectionFound = true;
   }
   }
   if (intersectionFound) {
   numV++;
   }
   if(numFound!=numV){

   std::vector<int> notManifoldNeigh;
   isInBoundary(currentTet, notManifoldNeigh);

   std::cout<<notManifoldNeigh.size()<<std::endl;
   std::cout<<"WRONG"<<std::endl;
   }
   }*/
}

void FreespaceDelaunayManifold::computeVisibilityTransposed() {
  m_visibilityListTransp = std::vector<vector<int> >(m_points.size());

  for (size_t curPt = 0; curPt < m_points.size(); curPt++) {
    for (std::vector<std::vector<int> >::iterator itCam = m_visibilityList.begin(); itCam != m_visibilityList.end(); itCam++) {
      std::vector<int>::iterator itFound = std::find((*itCam).begin(), (*itCam).end(), curPt);
      if (itFound != (*itCam).end()) {
        int idxCam = std::distance(m_visibilityList.begin(), itCam);      //idx cam where the point is found
        m_visibilityListTransp[curPt].push_back(idxCam);
      }
    }
  }
}

void FreespaceDelaunayManifold::regionGrowingProcedure() {

  bool somethingIsChanged = false;
  bool singleTetGrowingMode = true;

  //populate the list with the boundary tetrahedra not belonging to the carved space
  vector<Delaunay3::Cell_handle> tetsQueue;
  for (vector<Delaunay3::Cell_handle>::iterator itBound = boundaryCells_.begin(); itBound != boundaryCells_.end(); itBound++) {
    for (int curIdx = 0; curIdx < 4; ++curIdx) {
      Delaunay3::Cell_handle curTet = (*itBound)->neighbor(curIdx);
      if (!dt.is_cell((*itBound))) {
        if (!dt.is_cell(curTet)) {
          std::cerr << "Something's wrong" << std::endl;
        }
      }

      if (!curTet->info().iskeptManifold() && !curTet->info().isToBeTested() && isFreespace(curTet)) {

        std::vector<Delaunay3::Cell_handle>::iterator it;
        it = std::lower_bound(tetsQueue.begin(), tetsQueue.end(), curTet, FreespaceDelaunayManifold::sortTetByIntersection);
        tetsQueue.insert(it, curTet);
        //std::sort(tetsQueue.begin(),tetsQueue.end(),FreespaceDelaunayManifold::sortTetByIntersection);

        curTet->info().setToBeTested(true);
      }
    }
  }

  int maxNumQueue = tetsQueue.size();
  int curIter = 0;
  //Growing process
  somethingIsChanged = true;
  vector<Delaunay3::Cell_handle> tetNotCarved;
  while (somethingIsChanged) {
    somethingIsChanged = false;
    while (!tetsQueue.empty()) {
      //if (singleTetGrowingMode) {
      //Single-tet-at-once
      Delaunay3::Cell_handle currentTet = tetsQueue.back();
      tetsQueue.pop_back();
      currentTet->info().setToBeTested(false);

      if (additionTest(currentTet) == true) {

        //somethingIsChanged = true;
        addTetAndUpdateBoundary(currentTet);

        //add the adjacent tets to the queue
        for (int curNeig = 0; curNeig < 4; ++curNeig) {

          if (!currentTet->neighbor(curNeig)->info().iskeptManifold() && isFreespace(currentTet->neighbor(curNeig))) {

            if (find(tetsQueue.begin(), tetsQueue.end(), currentTet->neighbor(curNeig)) == tetsQueue.end()) {

              //keep the list ordered
              std::vector<Delaunay3::Cell_handle>::iterator it;
              it = std::lower_bound(tetsQueue.begin(), tetsQueue.end(), currentTet->neighbor(curNeig), FreespaceDelaunayManifold::sortTetByIntersection);
              tetsQueue.insert(it, currentTet->neighbor(curNeig));

              if (tetsQueue.size() > maxNumQueue)
                maxNumQueue = tetsQueue.size();

            }

          }
        }

      } else {
        std::vector<Delaunay3::Cell_handle>::iterator it;

        it = std::lower_bound(tetNotCarved.begin(), tetNotCarved.end(), currentTet, FreespaceDelaunayManifold::sortTetByIntersection);
        tetNotCarved.insert(it, currentTet);
      }
      /*if(curIter%2 == 0){
       std::stringstream nameFile;
       std::vector<dlovi::Matrix> m_arrModelPoints;
       std::vector<dlovi::Matrix> m_lstModelTris;
       nameFile << "00ManifoldGrowing" << curIter << ".obj";
       tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
       writeObj(nameFile.str().c_str(), m_arrModelPoints, m_lstModelTris);

       std::stringstream namefilee, namefileBound;
       namefileBound << "00GrowingBoundary" << curIter << ".obj";
       writeBoundaryToObj(namefileBound.str());

       }

       */
//        if (somethingIsChanged) {
//          tetsQueue.insert(tetsQueue.begin(), tetNotCarved.begin(), tetNotCarved.end());
//          std::sort(tetsQueue.begin(), tetsQueue.end(), FreespaceDelaunayManifold::sortTetByIntersection);
//          tetNotCarved.clear();
      // } else if (!somethingIsChanged) {
      //switch to several tet at once
      // singleTetGrowingMode = false;
      //}
      /* }else {

       //Several-tet-at-once from the last tet since it has the highest intersection count
       for (std::vector<Delaunay3::Cell_handle>::iterator itCell = tetNotCarved.end(); itCell != tetNotCarved.begin(); itCell--) {
       for (int curV = 0; curV < 4; ++curV) {
       vector<Delaunay3::Cell_handle> addedCells;
       if (regionGrowingSeveralTetAtOnce((*itCell)->vertex(curV), addedCells)) {
       //reset the loop variable
       somethingIsChanged = false;
       //switch to one tet at once
       singleTetGrowingMode = true;
       tetsQueue.insert(tetsQueue.begin(), addedCells.begin(), addedCells.end());
       }
       }
       }
       std::sort(tetsQueue.begin(), tetsQueue.end(), FreespaceDelaunayManifold::sortTetByIntersection);
       }*/
      curIter++;
    }

    // std::cout<<"Freespace growing tetNotCarved: "<<tetNotCarved.size();
    // std::cout<<", tetsQueue (Before): "<<tetsQueue.size();
    tetsQueue.insert(tetsQueue.begin(), tetNotCarved.begin(), tetNotCarved.end());
    //std::sort(tetsQueue.begin(), tetsQueue.end(), FreespaceDelaunayManifold::sortTetByIntersection);
    // std::cout<<", tetsQueue (after): "<<tetsQueue.size();
    //std::cout<<", maxNumQueue "<<maxNumQueue<<std::endl;

    tetNotCarved.clear();

  }
  tetsQueue.clear();
}

void FreespaceDelaunayManifold::regionGrowingProcedureForHandle(Delaunay3::Cell_handle &cell, std::vector<Delaunay3::Cell_handle> &ladd) {

  //populate the list with the boundary tetrahedra not belonging to the carved space
  vector<Delaunay3::Cell_handle> tetsQueue;
  tetsQueue.push_back(cell);

  while (!tetsQueue.empty()) {
    Delaunay3::Cell_handle currentTet = tetsQueue.back();

    tetsQueue.pop_back();
    currentTet->info().setToBeTested(false);

    if (additionTest(currentTet) == true) {
      addTetAndUpdateBoundary(currentTet);
      ladd.push_back(currentTet);

      //add the adjacent tets to the queue
      for (int curNeig = 0; curNeig < 4; ++curNeig) {

        if (!currentTet->neighbor(curNeig)->info().iskeptManifold() && currentTet->neighbor(curNeig)->info().isGalpha()) {

          if (find(tetsQueue.begin(), tetsQueue.end(), currentTet->neighbor(curNeig)) == tetsQueue.end()) {

            //keep the list ordered
            std::vector<Delaunay3::Cell_handle>::iterator it;
            it = std::lower_bound(tetsQueue.begin(), tetsQueue.end(), currentTet->neighbor(curNeig), FreespaceDelaunayManifold::sortTetByIntersection);
            tetsQueue.insert(it, currentTet->neighbor(curNeig));
          }

        }
      }

    }
  }
}

void FreespaceDelaunayManifold::regionGrowingBatch(int idxCam) {
#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif
  for (Delaunay3::Finite_cells_iterator itCell = dt.finite_cells_begin(); itCell != dt.finite_cells_end(); itCell++) {
    itCell->info().setKeptManifold(false);
    for (int curV = 0; curV < 4; ++curV) {
      itCell->vertex(curV)->info().setUsed(0);
      itCell->vertex(curV)->info().setNotUsed(true);
    }

  }

  // Locate the idxCam-th cam and start from this one to grow the manifold
  PointD3 firstCamPosition = PointD3(getCamCenter(idxCam)(0), getCamCenter(idxCam)(1), getCamCenter(idxCam)(2));
  Delaunay3::Cell_handle startingCell = dt.locate(firstCamPosition);

  addTetAndUpdateBoundary(startingCell);

  regionGrowingProcedure();
#ifdef LOGGING
  gettimeofday(&endCarv, NULL);
  double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
  fileStats_ << "RegionGrowingBatch " << delta << std::endl;
#endif
}

void FreespaceDelaunayManifold::regionGrowingBatch() {
  for (Delaunay3::Finite_cells_iterator itCell = dt.finite_cells_begin(); itCell != dt.finite_cells_end(); itCell++) {
    itCell->info().setKeptManifold(false);
    for (int curV = 0; curV < 4; ++curV) {
      itCell->vertex(curV)->info().setUsed(0);
      itCell->vertex(curV)->info().setNotUsed(true);
    }
  }

  //sort the tetrahedra
  std::sort(freeSpaceTets_.begin(), freeSpaceTets_.end(), FreespaceDelaunayManifold::sortTetByIntersection);
  //keep the tetrahedron with maximum intersection counter
  Delaunay3::Cell_handle startingCell = freeSpaceTets_[freeSpaceTets_.size() - 1];

  addTetAndUpdateBoundary(startingCell);

  regionGrowingProcedure();

}

bool FreespaceDelaunayManifold::isRegular(Delaunay3::Vertex_handle vertexToCheck, bool classical) {

  bool regular = true;
  //all incident cells
  std::vector<Delaunay3::Cell_handle> cellToTest;
  dt.incident_cells(vertexToCheck, std::back_inserter(cellToTest));

  std::vector<Delaunay3::Vertex_handle> verticesToCheck;
  std::vector<int> counter;

  //Iterate over each cell adjacent to the current vertex
  std::vector<Delaunay3::Cell_handle>::iterator it;
  for (it = cellToTest.begin(); it != cellToTest.end(); it++) {

    //let's consider only the cells not belonging to the manifold
    if (!((*it)->info().iskeptManifold() && (classical || (!(*it)->info().isTemporaryInside())))) {
      //for each neighbouring cell check if belongs to the manifold, i.e., they form a boundary facet
      for (int curNeigh = 0; curNeigh < 4; ++curNeigh) {
        bool isOutside = ((*it)->neighbor(curNeigh)->info().iskeptManifold() && (classical || (!(*it)->neighbor(curNeigh)->info().isTemporaryInside())));
        if ((*it)->neighbor(curNeigh)->has_vertex(vertexToCheck) && isOutside) {
          //the current tet and the current neighbour made up a mesh triangle

          int vertIdx;
          (*it)->has_vertex(vertexToCheck, vertIdx);
          //iterate over the vertex of the mesh triangle
          for (int curid = 0; curid < 4; ++curid) {
            //the current vertex belong to the mesh triangle only if
            // it is not the one we are checking for regularity,
            // and it is not the vertex opposite to the mesh triangle itself
            if (curid != vertIdx && curid != curNeigh) {
              //add the vertex to the verticesToCheck vector if it is not
              // added yet, otherwise we remove the previously added occurrence
              vector<Delaunay3::Vertex_handle>::iterator c = std::find(verticesToCheck.begin(), verticesToCheck.end(), (*it)->vertex(curid));
              int idx = std::distance(verticesToCheck.begin(), c);
              if (c != verticesToCheck.end()) {
                counter[idx]++;
              } else {
                verticesToCheck.push_back((*it)->vertex(curid));
                counter.push_back(1);
              }

            }
          }

        }
      }
    }
  }
  //check if each vertex has exactly two occurrences (cycle) otherwise the vertex vertexToCheck is not regular
  regular = true;
  for (vector<int>::iterator v = counter.begin(); v != counter.end(); v++) {
    if (*v != 2) {
      regular = false;
    }
  }

  return regular;
}

bool FreespaceDelaunayManifold::regionGrowingSeveralTetAtOnce(Delaunay3::Vertex_handle vertexToGrow, std::vector<Delaunay3::Cell_handle> &cellsAdded) {

  //The tets adjacent to vertexToGrow are freespace?
  std::vector<Delaunay3::Cell_handle> cells;
  std::vector<Delaunay3::Cell_handle> cellToBeCarved, cellCarvedYet;

  dt.incident_cells(vertexToGrow, std::back_inserter(cells));

  for (std::vector<Delaunay3::Cell_handle>::iterator it = cells.begin(); it != cells.end(); it++) {

    if (!isFreespace(*it)) {
      if (!(*it)->info().iskeptManifold()) {
        cellToBeCarved.push_back((*it));
      } else {
        cellCarvedYet.push_back((*it));
      }
    } else {
      return false;
    }
  }

  //try to add the new tetrahedra
  std::vector<Delaunay3::Cell_handle>::iterator itCellToBeCarved;
  for (itCellToBeCarved = cellToBeCarved.begin(); itCellToBeCarved != cellToBeCarved.end(); itCellToBeCarved++) {
    (*itCellToBeCarved)->info().setKeptManifold(true);
    for (int curV = 0; curV < 4; ++curV) {
      (*itCellToBeCarved)->vertex(curV)->info().incrUsed();
    }

  }

  //check manifoldness
  std::vector<Delaunay3::Vertex_handle> vertices;
  dt.incident_vertices(vertexToGrow, std::back_inserter(vertices));

  //iterate over new added vertices to check manifoldness
  std::vector<Delaunay3::Vertex_handle>::iterator incidentVertices;
  bool isManifold = true;
  incidentVertices = vertices.begin();
  while (incidentVertices != vertices.end() && isManifold) {
    Delaunay3::Vertex_handle vertexToCheck = *incidentVertices;
    if (!isRegular(vertexToCheck, true)) {
      isManifold = false;
    }
    incidentVertices++;
  }

  if (!isManifold) {
    //restore tetrahedra state
    std::vector<Delaunay3::Cell_handle>::iterator itCellToBeCarved;
    for (itCellToBeCarved = cellToBeCarved.begin(); itCellToBeCarved != cellToBeCarved.end(); itCellToBeCarved++) {
      (*itCellToBeCarved)->info().setKeptManifold(false);
      for (int curV = 0; curV < 4; ++curV) {
        (*itCellToBeCarved)->vertex(curV)->info().decrUsed();
      }
    }
    return false;
  }

  //actually add the new tetrahedra (remember: we set the tetrahedra to be manifold yet)
  std::vector<Delaunay3::Cell_handle>::iterator itCellToBeAdded;
  for (itCellToBeAdded = cellToBeCarved.begin(); itCellToBeAdded != cellToBeCarved.end(); itCellToBeAdded++) {
    addTetAndUpdateBoundary(*itCellToBeAdded);
    std::vector<Delaunay3::Cell_handle>::iterator it = lower_bound(cellsAdded.begin(), cellsAdded.end(), *itCellToBeAdded);

    cellsAdded.insert(it, *itCellToBeAdded);
  }

  //Remove the old boundary tetrahedra
  /*std::vector<Delaunay3::Cell_handle>::iterator itCellCarved;
   for (itCellCarved = cellCarvedYet.begin(); itCellCarved != cellCarvedYet.end(); itCellCarved++) {

   vector<Delaunay3::Cell_handle>::iterator itCell = boundaryCells_.begin();
   bool found = false;
   while (found == false && itCell != boundaryCells_.end()) {
   if ((*itCell) == (*itCellCarved)) {
   boundaryCells_.erase(itCell);
   found = true;
   } else {
   ++itCell;
   }
   }
   (*itCellCarved)->info().setBoundary(false);
   }*/

  return true;

}
void FreespaceDelaunayManifold::tetsToTris(vector<Matrix> & points, vector<Matrix> & tris) {
  // NEW Version, graph cut isosurf extraction with maxflow (builds the graph from scratch every time):
  {
    // Timing output for graphcuts:
    // cerr << "Running Graph Cut Isosurface Extraction..." << endl;
    //double t = timestamp();

    tetsToTris_maxFlowSimple(points, tris, 1);

    // cerr << "Time Taken (Isosurface): " << (timestamp() - t) << " s" << endl;
  }

}

void FreespaceDelaunayManifold::tetsToTris(vector<Matrix> & points, vector<Matrix> & tris, const int nVoteThresh) {

  // OLD Version, simple isosurf extraction:
  tetsToTris_naive(points, tris, nVoteThresh);
}

void FreespaceDelaunayManifold::tetsToTrisSure(vector<Matrix> & points, vector<Matrix> & tris) {

  // OLD Version, simple isosurf extraction:
  tetsToTris_naiveOrig(points, tris, 1);
}
void FreespaceDelaunayManifold::tetsToTris(vector<Matrix> & points, vector<Matrix> & tris, const float nVoteProbThresh) {

  // OLD Version, simple isosurf extraction:
  tetsToTris_naive(points, tris, nVoteProbThresh);
}

int FreespaceDelaunayManifold::writeObj(const string filename, const vector<Matrix> & points, const vector<Matrix> & tris) const {
  // TODO: handle better for invalid files (e.g. throw exception).
  ofstream outfile;

  // Open file
  outfile.open(filename.c_str());
  if (!outfile.is_open()) {
    cerr << "Unable to open file: " << filename << endl;
    return -1;
  }

  // Write out lines one by one.
  for (vector<Matrix>::const_iterator itPoints = points.begin(); itPoints != points.end(); itPoints++)
    outfile << "v " << static_cast<float>(itPoints->at(0)) << " " << static_cast<float>(itPoints->at(1)) << " " << static_cast<float>(itPoints->at(2)) << endl;
  for (vector<Matrix>::const_iterator itTris = tris.begin(); itTris != tris.end(); itTris++)
    outfile << "f " << static_cast<int>(round(itTris->at(0)) + 1) << "//" << static_cast<int>(round(itTris->at(0)) + 1) << " "
        << static_cast<int>(round(itTris->at(1)) + 1) << "//" << static_cast<int>(round(itTris->at(1)) + 1) << " " << static_cast<int>(round(itTris->at(2)) + 1)
        << "//" << static_cast<int>(round(itTris->at(2)) + 1) << endl;
  // Close the file and return
  outfile.close();
  return 0;
}

void FreespaceDelaunayManifold::writeObj(ostream & outfile, const vector<Matrix> & points, const vector<Matrix> & tris) const {
  // Write out lines one by one.
  for (vector<Matrix>::const_iterator itPoints = points.begin(); itPoints != points.end(); itPoints++)
    outfile << "v " << itPoints->at(0) << " " << itPoints->at(1) << " " << itPoints->at(2) << endl;
  for (vector<Matrix>::const_iterator itTris = tris.begin(); itTris != tris.end(); itTris++)
    outfile << "f " << (round(itTris->at(0)) + 1) << " " << (round(itTris->at(1)) + 1) << " " << (round(itTris->at(2)) + 1) << endl;
}

void FreespaceDelaunayManifold::writeBoundaryToObj(const string outfile) {
  vector<Matrix> points;
  vector<Matrix> tris;

  int countNotManifold = 0;
  int countRealBound = 0;
  int countFakeBound = 0;
  for (vector<Delaunay3::Cell_handle>::iterator itCellBoundary = boundaryCells_.begin(); itCellBoundary != boundaryCells_.end(); ++itCellBoundary) {

    if (!(*itCellBoundary)->info().iskeptManifold())
      ++countNotManifold;
    if (isInBoundary(*itCellBoundary)) {
      ++countRealBound;
    } else {
      if ((*itCellBoundary)->info().isBoundary()) {
        ++countFakeBound;
      }
    }

    for (int curNeigh = 0; curNeigh < 4; ++curNeigh) {
      if (!(*itCellBoundary)->neighbor(curNeigh)->info().iskeptManifold()) {
        Matrix tmptris(3, 1);
        Matrix tmpPoint(3, 1);

        for (int curVertex = 0; curVertex < 4; ++curVertex) {
          if (curVertex != curNeigh) {
            tmpPoint(0) = (*itCellBoundary)->vertex(curVertex)->point().x();
            tmpPoint(1) = (*itCellBoundary)->vertex(curVertex)->point().y();
            tmpPoint(2) = (*itCellBoundary)->vertex(curVertex)->point().z();
            points.push_back(tmpPoint);

          }
        }
        tmptris(0) = points.size() - 3;
        tmptris(1) = points.size() - 2;
        tmptris(2) = points.size() - 1;
        tris.push_back(tmptris);
      }

    }

  }
  std::cout << "number of boundary cells: " << boundaryCells_.size() << std::endl;
  std::cout << "number of boundary cells not manifold (it must be 0): " << countNotManifold << std::endl;
  std::cout << "number of boundary cells that are really boundary: " << countRealBound << std::endl;
  std::cout << "number of boundary cells that are fake boundary: " << countFakeBound << std::endl;

  writeObj(outfile, points, tris);
}

void FreespaceDelaunayManifold::writeShrinkedToObj(const string outfile) {
  vector<Matrix> points;
  vector<Matrix> tris;

  for (vector<Delaunay3::Cell_handle>::iterator itCellBoundary = boundaryCells_.begin(); itCellBoundary != boundaryCells_.end(); ++itCellBoundary) {

    for (int curNeigh = 0; curNeigh < 4; ++curNeigh) {
      if (!(*itCellBoundary)->neighbor(curNeigh)->info().isShrinked()) {
        Matrix tmptris(3, 1);
        Matrix tmpPoint(3, 1);

        for (int curVertex = 0; curVertex < 4; ++curVertex) {
          if (curVertex != curNeigh) {
            tmpPoint(0) = (*itCellBoundary)->vertex(curVertex)->point().x();
            tmpPoint(1) = (*itCellBoundary)->vertex(curVertex)->point().y();
            tmpPoint(2) = (*itCellBoundary)->vertex(curVertex)->point().z();
            points.push_back(tmpPoint);

          }
        }
        tmptris(0) = points.size() - 3;
        tmptris(1) = points.size() - 2;
        tmptris(2) = points.size() - 1;
        tris.push_back(tmptris);
      }

    }

  }

  writeObj(outfile, points, tris);
}
void FreespaceDelaunayManifold::writeBoundaryToObj2(const string outfile) {
  vector<Matrix> points;
  vector<Matrix> tris;
  vector<Delaunay3::Cell_handle> boundaryCells;

  for (Delaunay3::Cell_iterator itCel = dt.cells_begin(); itCel != dt.cells_end(); itCel++) {
    if (isInBoundary(itCel))
      boundaryCells.push_back(itCel);
  }

  std::cout << "writeBoundaryToObj2: size of isInBoundary(itCel) vector: " << boundaryCells.size() << ", size of boundaryCells_: " << boundaryCells_.size()
      << std::endl;
  for (vector<Delaunay3::Cell_handle>::iterator itCellBoundary = boundaryCells.begin(); itCellBoundary != boundaryCells.end(); ++itCellBoundary) {

    for (int curNeigh = 0; curNeigh < 4; ++curNeigh) {
      if (!(*itCellBoundary)->neighbor(curNeigh)->info().iskeptManifold()) {
        Matrix tmptris(3, 1);
        Matrix tmpPoint(3, 1);

        for (int curVertex = 0; curVertex < 4; ++curVertex) {
          if (curVertex != curNeigh) {
            tmpPoint(0) = (*itCellBoundary)->vertex(curVertex)->point().x();
            tmpPoint(1) = (*itCellBoundary)->vertex(curVertex)->point().y();
            tmpPoint(2) = (*itCellBoundary)->vertex(curVertex)->point().z();
            points.push_back(tmpPoint);

          }
        }
        tmptris(0) = points.size() - 3;
        tmptris(1) = points.size() - 2;
        tmptris(2) = points.size() - 1;
        tris.push_back(tmptris);
      }

    }

  }
  writeObj(outfile, points, tris);
}

// Private Methods

void FreespaceDelaunayManifold::copy(const vector<Matrix> & points, const vector<Matrix> & cams, const vector<Matrix> & camCenters,
    const vector<Matrix> & principleRays, const vector<vector<int> > & visibilityList) {
  m_points = points;
  m_cams = cams;
  m_camCenters = camCenters;
  m_principleRays = principleRays;
  m_visibilityList = visibilityList;
}

void FreespaceDelaunayManifold::calculateBoundsValues() {
  //    m_nBoundsMin = -100;
  //    m_nBoundsMax = 100;
  m_nBoundsMin = -1000000;
  m_nBoundsMax = 1000000;
  for (int i = 0; i < numPoints(); i++) {
    if (getPoint(i)(0) > m_nBoundsMax)
      m_nBoundsMax = getPoint(i)(0);
    if (getPoint(i)(0) < m_nBoundsMin)
      m_nBoundsMin = getPoint(i)(0);
    if (getPoint(i)(1) > m_nBoundsMax)
      m_nBoundsMax = getPoint(i)(1);
    if (getPoint(i)(1) < m_nBoundsMin)
      m_nBoundsMin = getPoint(i)(1);
    if (getPoint(i)(2) > m_nBoundsMax)
      m_nBoundsMax = getPoint(i)(2);
    if (getPoint(i)(2) < m_nBoundsMin)
      m_nBoundsMin = getPoint(i)(2);
  }
  for (int i = 0; i < numCams(); i++) {
    if (getCamCenter(i)(0) > m_nBoundsMax)
      m_nBoundsMax = getCamCenter(i)(0);
    if (getCamCenter(i)(0) < m_nBoundsMin)
      m_nBoundsMin = getCamCenter(i)(0);
    if (getCamCenter(i)(1) > m_nBoundsMax)
      m_nBoundsMax = getCamCenter(i)(1);
    if (getCamCenter(i)(1) < m_nBoundsMin)
      m_nBoundsMin = getCamCenter(i)(1);
    if (getCamCenter(i)(2) > m_nBoundsMax)
      m_nBoundsMax = getCamCenter(i)(2);
    if (getCamCenter(i)(2) < m_nBoundsMin)
      m_nBoundsMin = getCamCenter(i)(2);
  }
  m_nBoundsMin *= 10.0;
  m_nBoundsMax *= 10.0;
  m_nBoundsMin = -1000;
  m_nBoundsMax = 1000;
}

void FreespaceDelaunayManifold::createSteinerPointGridAndBound() {
  const double nLargePosDouble = getBoundsMax();
  const double nLargeNegDouble = getBoundsMin();
  std::vector<PointD3> vecPoint;
  /*
   stepX_ = nLargePosDouble / 60.0;
   stepY_ = nLargePosDouble / 40.0;
   stepZ_ = nLargePosDouble / 150.0;

   stepX_ = 5.0;
   stepY_ = 5.0;
   stepZ_ = 5.0;
   //  stepX_ = 100.0;
   //   stepY_ = 100.0;
   //   stepZ_ = 100.0;
   //kitti00955
   double denX = 3.0;
   double denY = 5.0;
   double denZ = 10.0;
   //
   //   double x = nLargeNegDouble/ denX;
   //   do {
   //   double y = nLargeNegDouble/ denY;
   //   do {
   //   double z = nLargeNegDouble / denZ;
   //   do {
   //   vecPoint.push_back(PointD3(x, y, z));
   //   z = z + stepZ_;
   //   } while (z <= nLargePosDouble / denZ);
   //   y = y + stepY_;
   //   } while (y <= nLargePosDouble / denY);
   //   x = x + stepX_;
   //   } while (x <= nLargePosDouble/ denX);

   double x = -60.0;
   do {
   double y = -30.0;
   do {
   double z = -50.0;
   do {
   vecPoint.push_back(PointD3(x, y, z));
   z = z + stepZ_;
   } while (z <= 50.0);
   y = y + stepY_;
   } while (y <= 30.0);
   x = x + stepX_;
   } while (x <= 60.0);
   dt.insert(vecPoint.begin(), vecPoint.end());



   */

  double denX = 2.5 * (17.0 + 30.0 / 2.0);
  double denY = 2.5 * (8.4 + 18.0 / 2.0);
  double denZ = 2.5 * (8.0 + 20 / 2.0);
  stepX_ = 5.9;
  stepY_ = 5.9;
  stepZ_ = 5.9;

  double x = -denX;
  do {
    double y = -denY;
    do {
      double z = -denZ;
      do {
        vecPoint.push_back(PointD3(x, y, z));
        z = z + stepZ_;
      } while (z <= denZ);
      y = y + stepY_;
    } while (y <= denY);
    x = x + stepX_;
  } while (x <= denX);
  dt.insert(vecPoint.begin(), vecPoint.end());

  l_ = sqrt(stepX_ * stepX_ + stepY_ * stepY_ + stepZ_ * stepZ_);
}
void FreespaceDelaunayManifold::createBounds() {
  // Note: The two values below worked with arbitrary precision arithmetic (though slowed down the execution),
  // but caused numerical difficulties in some computational geometry algorithms when using double-precision
  // const double nLargePosDouble = (double) numeric_limits<float>::max();
  // const double nLargeNegDouble = - (double) numeric_limits<float>::max();
  const double nLargePosDouble = 10000000;  //getBoundsMax();
  const double nLargeNegDouble = -10000000;  //getBoundsMin();

  // Insert 8 points forming an axis-aligned bounding box around our real data points.  Comments of + or - on the following lines indicates the limits of x y z, resp.,
  // that the corresponding corner point is at.
  dt.insert(PointD3(nLargePosDouble, nLargePosDouble, nLargePosDouble)); // + + +
  dt.insert(PointD3(nLargePosDouble, nLargePosDouble, nLargeNegDouble)); // + + -
  dt.insert(PointD3(nLargePosDouble, nLargeNegDouble, nLargePosDouble)); // + - +
  dt.insert(PointD3(nLargePosDouble, nLargeNegDouble, nLargeNegDouble)); // + - -
  dt.insert(PointD3(nLargeNegDouble, nLargePosDouble, nLargePosDouble)); // - + +
  dt.insert(PointD3(nLargeNegDouble, nLargePosDouble, nLargeNegDouble)); // - + -
  dt.insert(PointD3(nLargeNegDouble, nLargeNegDouble, nLargePosDouble)); // - - +
  dt.insert(PointD3(nLargeNegDouble, nLargeNegDouble, nLargeNegDouble)); // - - -
}

void FreespaceDelaunayManifold::markTetraedron(Delaunay3::Cell_handle cell, const vector<Delaunay3::Vertex_handle> & vecVertexHandles, const int camIndex,
    const int featureIndex, const bool incrementCount) {
  if (incrementCount) {
    cell->info().incrementVoteCount(num_); // t.n++

    //TODO manage in a smarter way this vector usage and pay attention
    //to"how tet removal affects this value?
    freeSpaceTets_.push_back(cell);
    if (cell->info().getVoteCountProb() < infiniteValue_) {
      cell->info().incrementVoteCountProb(w_1); // t.n++
      cell->info().addIntersection(camIndex, featureIndex, w_1, vecVertexHandles, getCamCenters());
    } else {
      cell->info().addIntersection(camIndex, featureIndex, 0.0, vecVertexHandles, getCamCenters());
    }
  } else {
    cell->info().decrementVoteCount(num_); // t.n++
    //TODO manage in a smarter way freeSpaceTets_ usage
    if (cell->info().getVoteCountProb() < infiniteValue_) {
      cell->info().decrementVoteCountProb(w_1); // t.n++
      cell->info().removeIntersection(camIndex, featureIndex, vecVertexHandles, getCamCenters());
    } else {
      cell->info().removeIntersection(camIndex, featureIndex, vecVertexHandles, getCamCenters());
    }
  }
}
void FreespaceDelaunayManifold::markTetraedron_suboptimal(Delaunay3::Cell_handle cell, const vector<Delaunay3::Vertex_handle> & vecVertexHandles,
    const int camIndex, const int featureIndex, const bool incrementCount) {
  if (incrementCount) {

    //TODO manage in a smarter way this vector usage and pay attention
    //to"how tet removal affects this value?
    freeSpaceTets_.push_back(cell);
    cell->info().incrementVoteCount(num_); // t.n++
    if (cell->info().getVoteCountProb() < infiniteValue_) {
      cell->info().incrementVoteCountProb(w_1); // t.n++
    }
  } else {
    cell->info().decrementVoteCount(num_); // t.n++
    //TODO manage in a smarter way freeSpaceTets_ usage
    if (cell->info().getVoteCountProb() < infiniteValue_) {
      cell->info().decrementVoteCountProb(w_1); // t.n++
    }
  }
}

void FreespaceDelaunayManifold::rayTracing(const vector<Delaunay3::Vertex_handle> & vecVertexHandles, const Delaunay3::Vertex_handle hndlQ,
    const Segment & constraint, const int camIndex, const int featureIndex, const bool bOnlyMarkNew, const bool incrementCount) {
  Delaunay3::Cell_handle tetPrev;
  Delaunay3::Cell_handle tetCur;
  Delaunay3::Locate_type lt;
  int li, lj;

  Matrix matQ(3, 1);
  Matrix matO(3, 1);
  matQ(0) = constraint.source().x();
  matQ(1) = constraint.source().y();
  matQ(2) = constraint.source().z();
  matO(0) = constraint.target().x();
  matO(1) = constraint.target().y();
  matO(2) = constraint.target().z();

  //std::cout<<"Ray Tracing :from ("<<matQ(0)<<", "<<matQ(1)<<", "<<matQ(2)<<") to ("<<matO(0)<<", "<<matO(1)<<", "<<matO(2)<<std::endl;

  /********************************************************************************************************
   **************************The ray (=constraint) goes from the camera O to the 3D point Q
   **************************Let's start to look for the tetrahedron incident to Q
   **************************intersected by the ray
   ******************************************************************************************************/

  vector<Delaunay3::Cell_handle> vecQCells;
  //store in the vector vecQCells the handle to the cells (=tetrahedra) incident to Q
  dt.incident_cells(hndlQ, std::back_inserter(vecQCells));

  vector<Delaunay3::Cell_handle>::iterator itQCells;
  for (itQCells = vecQCells.begin(); itQCells != vecQCells.end(); itQCells++) {

    // If the tetrahedron t contains O (NB:constraint.target()=O),
    if (dt.side_of_cell(constraint.target(), *itQCells, lt, li, lj) != CGAL::ON_UNBOUNDED_SIDE) {

      //if the cell
      if (!bOnlyMarkNew || (*itQCells)->info().isNew()) {
        markTetraedron((*itQCells), vecVertexHandles, camIndex, featureIndex, incrementCount);
      }
      // The ending of the ray is reached yet, so return
      return;
    }

    // Let f be the facet of t opposite to Q
    int f = (*itQCells)->index(hndlQ); // this with the Cell_handle *itQCells defines the facet
    // If f intersects QO
    if (CGAL::do_intersect(dt.triangle(*itQCells, f), constraint)) {
      //if (triangleConstraintIntersectionTest(Delaunay3::Facet(*itQCells, f), matQ, matO)) {
      tetPrev = *itQCells; // t.precedent = t
      tetCur = (*itQCells)->neighbor(f); // t.actual = neighbour of t incident to facet f
      if (!bOnlyMarkNew || (*itQCells)->info().isNew()) {
        markTetraedron((*itQCells), vecVertexHandles, camIndex, featureIndex, incrementCount);

      }
      if (!bOnlyMarkNew || tetCur->info().isNew()) {
        markTetraedron(tetCur, vecVertexHandles, camIndex, featureIndex, incrementCount);

      }
      break;
    }
  }
  /********************************************************************************************************/

  /********************************************************************************************************/
  /****************WALK THROUG THE TRIANGULATION FOLLOWING THE RAY         ********************************/
  /********************************************************************************************************/

  int f, fOld;

  // While t.actual doesn't contain O
  while (cellTraversalExitTest(f, fOld, tetCur, tetPrev, matQ, matO)) {
    // f is now the facet of t.actual that intersects QO and that isn't incident to t.precedent

    if (configuration_.spaceCarvingConfig.inverseConicEnabled) {
      for (int curidFac = 0; curidFac < 4; ++curidFac) {
        //increment weight of the neighbors that are not traversed by the ray
        if ((curidFac != f) && (curidFac != fOld)) {

          Delaunay3::Cell_handle nearCellNotIntersected = tetCur->neighbor(curidFac);
          if (nearCellNotIntersected->info().getVoteCountProb() < infiniteValue_) {
            nearCellNotIntersected->info().incrementVoteCountProb(w_2);

            nearCellNotIntersected->info().addIntersection(camIndex, featureIndex, w_2, vecVertexHandles, getCamCenters());
          } else {

            nearCellNotIntersected->info().addIntersection(camIndex, featureIndex, 0.0, vecVertexHandles, getCamCenters());
          }
          for (int curidFacNear = 0; curidFacNear < 4; ++curidFacNear) {
            //increment weight of the neighbors of the neighbors
            if (curidFacNear != nearCellNotIntersected->index(tetCur)) {

              if (nearCellNotIntersected->info().getVoteCountProb() < infiniteValue_) {
                nearCellNotIntersected->neighbor(curidFacNear)->info().incrementVoteCountProb(w_3);
                nearCellNotIntersected->neighbor(curidFacNear)->info().addIntersection(camIndex, featureIndex, w_3, vecVertexHandles, getCamCenters());
              } else {

                nearCellNotIntersected->info().addIntersection(camIndex, featureIndex, 0.0, vecVertexHandles, getCamCenters());
              }
            }

          }
        }
      }
    }

    tetPrev = tetCur; // t.precedent = t.actual
    tetCur = tetCur->neighbor(f); // t.actual = neighbour of t.precedent(==t.actual) incident to facet f
    if (!bOnlyMarkNew || tetCur->info().isNew()) {
      markTetraedron(tetCur, vecVertexHandles, camIndex, featureIndex);
    }
  }

  //   We find intersecting tetrahedra by Pau's algorithm:
  //   let Q be the point & O the optic center.
  //   For all tetrahedra t incident to Q:
  //    if t contains O:
  //      t.n++
  //      break maybe?  bad translation.  In this case should be ~done~ here anyways.
  //    let f be the facet   of t opposite to Q
  //    if f intersects QO:
  //      t.precedent = t
  //      t.actual = neighbour of t incident to facet f
  //      t.n++;
  //      t.actual.n++;
  //      break from for loop
  //   while t.actual doesn't contain O
  //    let f be the facet of t.actual that intersects QO and which isn't incident to t.precedent
  //    t.precedent = t.actual
  //    t.actual = neighbour of t.precedent(==t.actual) incident to facet f
  //    t.actual.n++;

  // Basically Pau's algorithm says:
  // Traverse the tetrahedrization.  For all traversed tetrahedra, t the count.
  // Start with the tetrahedron that contains Q.  Do this by the algorithm of localization of a point in a delaunay triangulization.
  // (Here, this is modified by use of a triangulation hierarchy from CGAL for fast point localization.)
  // Loop over all neighbouring tetrahedra and find the one whose common facet intersects the segment QO.  Traverse to this.
  // Continue traversing adjacent tetrahedra by the constraint-intersecting facets.
  // Stop traversal when O is contained in the current tetrahedron.
}
void FreespaceDelaunayManifold::resetReconstruction() {
  dt.clear();
  for (Delaunay3::Cell_iterator c = dt.cells_begin(); c != dt.cells_end(); c++) {
    c->info().setBoundary(false);
    c->info().setKeptManifold(false);
    c->info().setToBeTested(false);
    c->info().setShrinked(true);
    c->info().setGalpha(false);
    c->info().setTemporaryInside(false);
    c->info().markNew();
  }

  for (Delaunay3::Vertex_iterator v = dt.vertices_begin(); v != dt.vertices_end(); v++) {
    v->info().setUsed(0);
    v->info().setNotUsed(true);

  }
  boundaryCells_.clear();
  /*while(!boundaryCells_.empty()){
   removeFromBoundary(boundaryCells_.back());
   }
   std::stringstream outName;
   outName << configuration_.outputSpaceCarving.pathToSave << "afterResetkBoundary" << curCam_ << ".obj";
   writeBoundaryToObj(outName.str());*/
}
void FreespaceDelaunayManifold::rayTracing_suboptimal(const vector<Delaunay3::Vertex_handle> & vecVertexHandles, const Delaunay3::Vertex_handle hndlQ,
    const Segment & constraint, const int camIndex, const int featureIndex, const bool bOnlyMarkNew, const bool incrementCount) {
  Delaunay3::Cell_handle tetPrev;
  Delaunay3::Cell_handle tetCur;
  Delaunay3::Locate_type lt;
  int li, lj;

  Matrix matQ(3, 1);
  Matrix matO(3, 1);
  matQ(0) = constraint.source().x();
  matQ(1) = constraint.source().y();
  matQ(2) = constraint.source().z();
  matO(0) = constraint.target().x();
  matO(1) = constraint.target().y();
  matO(2) = constraint.target().z();

  /********************************************************************************************************
   **************************The ray (=constraint) goes from the camera O to the 3D point Q
   **************************Let's start to look for the tetrahedron incident to Q
   **************************intersected by the ray
   ******************************************************************************************************/

  vector<Delaunay3::Cell_handle> vecQCells;
  //store in the vector vecQCells the handle to the cells (=tetrahedra) incident to Q
  dt.incident_cells(hndlQ, std::back_inserter(vecQCells));

  vector<Delaunay3::Cell_handle>::iterator itQCells;
  for (itQCells = vecQCells.begin(); itQCells != vecQCells.end(); itQCells++) {

    // If the tetrahedron t contains O (NB:constraint.target()=O),
    if (dt.side_of_cell(constraint.target(), *itQCells, lt, li, lj) != CGAL::ON_UNBOUNDED_SIDE) {

      //if the cell
      markTetraedron_suboptimal((*itQCells), vecVertexHandles, camIndex, featureIndex, incrementCount);

      // The ending of the ray is reached yet, so return
      return;
    }

    // Let f be the facet of t opposite to Q
    int f = (*itQCells)->index(hndlQ); // this with the Cell_handle *itQCells defines the facet
    // If f intersects QO
    if (CGAL::do_intersect(dt.triangle(*itQCells, f), constraint)) {
      //if (triangleConstraintIntersectionTest(Delaunay3::Facet(*itQCells, f), matQ, matO)) {
      tetPrev = *itQCells; // t.precedent = t
      tetCur = (*itQCells)->neighbor(f); // t.actual = neighbour of t incident to facet f
      markTetraedron_suboptimal((*itQCells), vecVertexHandles, camIndex, featureIndex, incrementCount);
      markTetraedron_suboptimal(tetCur, vecVertexHandles, camIndex, featureIndex, incrementCount);

      break;
    }
  }
  /********************************************************************************************************/

  /********************************************************************************************************/
  /****************WALK THROUG THE TRIANGULATION FOLLOWING THE RAY         ********************************/
  /********************************************************************************************************/

  int f, fOld;

  // While t.actual doesn't contain O
  while (cellTraversalExitTest(f, fOld, tetCur, tetPrev, matQ, matO)) {
    // f is now the facet of t.actual that intersects QO and that isn't incident to t.precedent

    if (configuration_.spaceCarvingConfig.inverseConicEnabled) {
      for (int curidFac = 0; curidFac < 4; ++curidFac) {
        //increment weight of the neighbors that are not traversed by the ray
        if ((curidFac != f) && (curidFac != fOld)) {

          Delaunay3::Cell_handle nearCellNotIntersected = tetCur->neighbor(curidFac);
          if (incrementCount == true) {
            nearCellNotIntersected->info().incrementVoteCountProb(w_2);
          } else {
            nearCellNotIntersected->info().decrementVoteCountProb(w_2);
          }

          for (int curidFacNear = 0; curidFacNear < 4; ++curidFacNear) {
            //increment weight of the neighbors of the neighbors
            if (curidFacNear != nearCellNotIntersected->index(tetCur)) {
              if (incrementCount == true) {
                nearCellNotIntersected->neighbor(curidFacNear)->info().incrementVoteCountProb(w_3);
              } else {
                nearCellNotIntersected->neighbor(curidFacNear)->info().decrementVoteCountProb(w_3);
                //nearCellNotIntersected->info().decrementVoteCountProb(w_3);
              }
            }

          }
        }
      }
    }

    tetPrev = tetCur; // t.precedent = t.actual
    tetCur = tetCur->neighbor(f); // t.actual = neighbour of t.precedent(==t.actual) incident to facet f
    markTetraedron_suboptimal(tetCur, vecVertexHandles, camIndex, featureIndex, incrementCount);
  }
}

void FreespaceDelaunayManifold::shrinkBall(int idxCam) {
#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif
  bool somethingHasChanged = false;
  bool singleTetGrowingMode = true;

  //populate the list with the boundary tetrahedra  belonging to the carved space
  deque<Delaunay3::Cell_handle> tetsQueue;
  tetsQueue.insert(tetsQueue.begin(), boundaryCells_.begin(), boundaryCells_.end());

  int curIter = 0;
  //Shrink process
  vector<Delaunay3::Cell_handle> tetNotCarved;
  while (!tetsQueue.empty()) {
    if (singleTetGrowingMode) {
      //Single-tet-at-once
      Delaunay3::Cell_handle currentTet = tetsQueue.front();
      tetsQueue.pop_front();

      double distance;
      bool testTest = false;
      PointD3 firstCamPosition = PointD3(getCamCenter(idxCam)(0), getCamCenter(idxCam)(1), getCamCenter(idxCam)(2));

      if (currentTet->info().getVoteCountProb() >= infiniteValue_) {
        testTest = false;
      } else {
        for (int curVertex = 0; curVertex < 4; ++curVertex) {
          PointD3 curPt = currentTet->vertex(curVertex)->point();

          double distx = curPt.x() - firstCamPosition.x();
          double disty = curPt.y() - firstCamPosition.y();
          double distz = curPt.z() - firstCamPosition.z();
          distance = sqrt(distx * distx + disty * disty + distz * distz);
          if (distance < l_ + r_) {
            //  if (distance < 45.0) {
            testTest = true;
          }
        }
      }

      if (testTest) {
        if (subtractionTest(currentTet) == true) {
          subTetAndUpdateBoundary(currentTet);

          //add the adjacent tets to the queue
          for (int curNeig = 0; curNeig < 4; ++curNeig) {
            if (currentTet->neighbor(curNeig)->info().iskeptManifold()) {
              //keep the list ordered
              std::deque<Delaunay3::Cell_handle>::iterator it;

              it = std::lower_bound(tetsQueue.begin(), tetsQueue.end(), currentTet->neighbor(curNeig), FreespaceDelaunayManifold::sortTetByIntersection);
              tetsQueue.insert(it, currentTet->neighbor(curNeig));

              somethingHasChanged = true;

            }
          }

        } else {
          std::vector<Delaunay3::Cell_handle>::iterator it;

          it = std::lower_bound(tetNotCarved.begin(), tetNotCarved.end(), currentTet, FreespaceDelaunayManifold::sortTetByIntersection);
          tetNotCarved.insert(it, currentTet);

        }
      }
      /* if (curIter % 10000 == 0 ) {
       std::stringstream nameFile;
       std::vector<dlovi::Matrix> m_arrModelPoints;
       std::vector<dlovi::Matrix> m_lstModelTris;
       nameFile << "ManifoldShrinking" << curIter << ".obj";
       tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
       writeObj(nameFile.str().c_str(), m_arrModelPoints, m_lstModelTris);

       std::stringstream namefilee, namefileBound;
       namefileBound << "GrowingShrinking" << curIter << ".obj";
       writeBoundaryToObj(namefileBound.str());
       namefilee << "GrowingShrinkingNew" << curIter << ".obj";
       writeBoundaryToObj2(namefilee.str());
       }*/

      if (tetsQueue.empty() && somethingHasChanged) {
        somethingHasChanged = false;
        tetsQueue.insert(tetsQueue.begin(), tetNotCarved.begin(), tetNotCarved.end());
        tetNotCarved.clear();
      } else if (tetsQueue.empty() && !somethingHasChanged) {
        //switch to several tet at once
        singleTetGrowingMode = false;
      }
    } else {
      /*
       //Several-tet-at-once from the last tet since it has the lowest intersection count
       for(std::vector<Delaunay3::Cell_handle >::iterator itCell = tetNotCarved.end();itCell != tetNotCarved.begin(); itCell--){
       for (int curV = 0; curV < 4; ++curV) {
       vector<Delaunay3::Cell_handle > addedCells;
       if(regionGrowingSeveralTetAtOnce((*itCell)->vertex(curV),addedCells)){
       //reset the loop variable
       somethingIsChanged=false;
       //switch to one tet at once
       singleTetGrowingMode = true;
       tetsQueue.insert(tetsQueue.begin(),addedCells.begin(),addedCells.end());
       }
       }
       }*/
    }
    curIter++;
  }

#ifdef LOGGING
  gettimeofday(&endCarv, NULL);
  double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
  fileStats_ << "Shrink " << delta << std::endl;
#endif
}

void FreespaceDelaunayManifold::addNewlyObservedFeatures(vector<Delaunay3::Vertex_handle> & vecVertexHandles, vector<int> & localVisList,
    const vector<int> & originalLocalVisList) {

  set<pair<int, int>, Delaunay3CellInfo::LtConstraint> setUnionedConstraints;
  int oldNumVertices = (int) vecVertexHandles.size();

#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif
  // Add new features to the global model that weren't observed before, and construct the local visibility list:
  vector<int>::const_iterator itOriginalLocalVisList;
  int i;
  int curvis = 0;
  PointD3 camPosition = PointD3(getCamCenter(curCam_)(0), getCamCenter(curCam_)(1), getCamCenter(curCam_)(2));
  for (itOriginalLocalVisList = originalLocalVisList.begin(); itOriginalLocalVisList != originalLocalVisList.end(); itOriginalLocalVisList++) {
    int originalIndex = *itOriginalLocalVisList;

    map<int, int>::iterator itVertHandleIndex = m_mapPoint_VertexHandle.find(originalIndex);

    if (getVisibilityNewPoint(curCam_)[curvis]) {
      //if(true){
      setVisibilityNewPoint(curCam_, curvis, false);
      if (itVertHandleIndex != m_mapPoint_VertexHandle.end()) {
        // We found a match: this feature is already in the point set.  So add it to the visibility list and break.
        localVisList.push_back(itVertHandleIndex->second);
      } else {
        // We didn't find a match, so this is a new feature.  Add it to the point set and the visibility list.
        // This involves properly deleting & staring off a connected subset of tetrahedra that violate the Delaunay constraint
        // after the addition, while marking the new tetrahedra with the deleted tetrahedra's freespace constraints.
        Matrix matTmpPoint = getPoint(originalIndex);
        PointD3 pd3TmpPoint(matTmpPoint(0), matTmpPoint(1), matTmpPoint(2));

        double distance, distance2;
        double distx = pd3TmpPoint.x() - camPosition.x();
        double disty = pd3TmpPoint.y() - camPosition.y();
        double distz = pd3TmpPoint.z() - camPosition.z();
        distance = sqrt(distx * distx + disty * disty + distz * distz);
        Delaunay3::Vertex_handle vNear = dt.nearest_vertex(pd3TmpPoint);
         distx = pd3TmpPoint.x() - vNear->point().x();
         disty = pd3TmpPoint.y() - vNear->point().y();
         distz = pd3TmpPoint.z() - vNear->point().z();
        distance2 = sqrt(distx * distx + disty * disty + distz * distz);

        if (distance < r_ && distance2 > 0.0001) {
          if (addNewlyObservedFeature(vecVertexHandles, setUnionedConstraints, pd3TmpPoint, originalIndex)) {      //&& 2*rand()>RAND_MAX/3) {
            localVisList.push_back((int) vecVertexHandles.size() - 1);
          }
        }
      }
    }
    curvis++;
  }

  // Apply the unioned constraint set to all the new cells adjacent to all the new points Q.  Increment the vote counts.
  set<Delaunay3::Cell_handle> setNewCells;
  for (i = oldNumVertices; i < (int) vecVertexHandles.size(); i++)
    dt.incident_cells(vecVertexHandles[i], std::inserter(setNewCells, setNewCells.begin()));
  for (set<pair<int, int>, Delaunay3CellInfo::LtConstraint>::iterator itConstraint = setUnionedConstraints.begin(); itConstraint != setUnionedConstraints.end();
      itConstraint++) {
    PointD3 tmp = vecVertexHandles[itConstraint->second]->point();
    PointD3 tmp2 = PointD3(getCamCenter(itConstraint->first)(0), getCamCenter(itConstraint->first)(1), getCamCenter(itConstraint->first)(2));
    Segment QO = Segment(tmp, tmp2);
    rayTracing(vecVertexHandles, vecVertexHandles[itConstraint->second], QO, itConstraint->first, itConstraint->second, true);
  }

  for (set<Delaunay3::Cell_handle>::iterator itCell = setNewCells.begin(); itCell != setNewCells.end(); itCell++)
    (*itCell)->info().markOld();

  // Runtime Analysis Output
  //    cerr << endl;
  //    cerr << "# of vertices added: " << ((int)vecVertexHandles.size() - oldNumVertices) << endl;
  //    cerr << "# of new tetrahedra after retriangulation: " << setNewCells.size() << endl;
  //    cerr << "# of all tetrahedra after retriangulation: " << dt.number_of_finite_cells() << endl;
  //    cerr << "# of constraints from deleted tetrahedra (doesn't include this view's new observations): " << setUnionedConstraints.size() << endl;
  //    cerr << "# of constraints in this triangulation (doesn't include this view's new observations): ";
  //    set<pair<int, int>, Delaunay3CellInfo::LtConstraint> setAllConstraints;
  //    for(Delaunay3::Finite_cells_iterator itAllCells = dt.finite_cells_begin(); itAllCells != dt.finite_cells_end(); itAllCells++)
  //      setAllConstraints.insert(itAllCells->info().getIntersections().begin(), itAllCells->info().getIntersections().end());
  //    cerr << setAllConstraints.size() << endl;

#ifdef LOGGING
  gettimeofday(&endCarv, NULL);
  double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
  fileStats_ << "AddFeatures " << delta << std::endl;
#endif
}

bool FreespaceDelaunayManifold::addNewlyObservedFeature(vector<Delaunay3::Vertex_handle> & vecVertexHandles,
    set<pair<int, int>, Delaunay3CellInfo::LtConstraint> & setUnionedConstraints, const PointD3 & Q, const int nPointIndex) {
  // Add the point Q to the triangulated point set dt.
  // This involves properly deleting & staring off a connected subset of tetrahedra that violate the Delaunay constraint
  // after the addition, while marking the new tetrahedra with the deleted tetrahedra's freespace constraints.

  // Locate the point
  Delaunay3::Locate_type lt;
  int li, lj;
  Delaunay3::Cell_handle c = dt.locate(Q, lt, li, lj);
  if (lt == Delaunay3::VERTEX) {
    //cerr << "Error in FreespaceDelaunayManifold::addNewlyObservedFeature(): Attempted to add a duplicate vertex to the triangulation" << endl;
    return false;
  }

  // Get the cells that conflict with Q in a vector vecConflictCells, and a facet on the boundary of this hole in f.
  vector<Delaunay3::Cell_handle> vecConflictCells;
  Delaunay3::Facet f;
  dt.find_conflicts(Q, c, CGAL::Oneset_iterator<Delaunay3::Facet>(f), std::back_inserter(vecConflictCells));

  //vector<Point> pointForConvexHull;
  bool toBeAdded = true;


  if (shrinkEnabled_) {
    for (vector<Delaunay3::Cell_handle>::const_iterator it = vecConflictCells.begin(); it != vecConflictCells.end(); it++) {

      if ((*it)->info().isBoundary()) {
        //std::cerr << "Error in FreespaceDelaunayManifold::addNewlyObservedFeature: one of the conflicting cell is in the boundary" << std::endl;
        toBeAdded = false;
      }

      if ((*it)->info().iskeptManifold()) {
        //std::cerr << "Error in FreespaceDelaunayManifold::addNewlyObservedFeature: one of the conflicting cell is kept in the manifold" << std::endl;
        toBeAdded = false;
      }
    }
  }
  if (!toBeAdded) {
    return false;

  }

  // Get the unioned constraint set of all the cells in vecConflictCells.
  for (vector<Delaunay3::Cell_handle>::const_iterator it = vecConflictCells.begin(); it != vecConflictCells.end(); it++) {
    setUnionedConstraints.insert((*it)->info().getIntersections().begin(), (*it)->info().getIntersections().end());

  }

  // Delete the cells in conflict, insert the point, and star off the hole.
  Delaunay3::Vertex_handle hndlQ = dt.insert_in_hole(Q, vecConflictCells.begin(), vecConflictCells.end(), f.first, f.second);

  // Add Q's Vertex_handle to vecVertexHandles
  vecVertexHandles.push_back(hndlQ);
  m_mapPoint_VertexHandle[nPointIndex] = (int) vecVertexHandles.size() - 1;
  return true;
}

void FreespaceDelaunayManifold::updateDistanceAndWeights(vector<Delaunay3::Cell_handle> &cellsToBeUpdated,
    const vector<pair<PointD3, double> > &vecDistanceWeight) {
  for (auto itNewCell : cellsToBeUpdated) {
    PointD3 curCenter = CGAL::barycenter(itNewCell->vertex(0)->point(), 1.0, itNewCell->vertex(1)->point(), 1.0, itNewCell->vertex(2)->point(), 1.0,
        itNewCell->vertex(3)->point(), 1.0);
    double voteCount = 0;

    if (configuration_.spaceCarvingConfig.suboptimalMethod == 0) {
      //mindist policy
      double minDist = 10000000000000.0;
      for (auto itWeight : vecDistanceWeight) {
        double dist = utilities::distanceEucl(curCenter.x(), itWeight.first.x(), curCenter.y(), itWeight.first.y(), curCenter.z(), itWeight.first.z());
        if (minDist > dist) {
          minDist = dist;
          voteCount = itWeight.second;
        }
      }
    } else if (configuration_.spaceCarvingConfig.suboptimalMethod == 1) {
      //Mean policy
      for (auto itWeight : vecDistanceWeight) {
        voteCount = voteCount + itWeight.second;
      }
      voteCount = voteCount / vecDistanceWeight.size();

    } else if (configuration_.spaceCarvingConfig.suboptimalMethod == 2) {
      //Weighted mean policy
      double totDist = 0;
      for (auto itWeight : vecDistanceWeight) {
        double dist = utilities::distanceEucl(curCenter.x(), itWeight.first.x(), curCenter.y(), itWeight.first.y(), curCenter.z(), itWeight.first.z());

        voteCount = voteCount + (1 / dist) * itWeight.second;
        totDist = totDist + (1 / dist);
      }
      voteCount = voteCount / totDist;

    } else {
      std::cerr << "updateDistanceAndWeight: you choose a wrong policy num" << std::endl;
    }

    if (itNewCell->info().getVoteCountProb() < infiniteValue_) {
      itNewCell->info().incrementVoteCountProb(voteCount); // t.n++
    }
    itNewCell->info().markOld();
  }

}

void FreespaceDelaunayManifold::addNewlyObservedFeatures_suboptimal(vector<Delaunay3::Vertex_handle> & vecVertexHandles, vector<int> & localVisList,
    const vector<int> & originalLocalVisList) {

#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif
  // Add new features to the global model that weren't observed before, and construct the local visibility list:
  vector<int>::const_iterator itOriginalLocalVisList;

  PointD3 camPosition = PointD3(getCamCenter(curCam_)(0), getCamCenter(curCam_)(1), getCamCenter(curCam_)(2));
  int curvis = 0;
  for (itOriginalLocalVisList = originalLocalVisList.begin(); itOriginalLocalVisList != originalLocalVisList.end(); itOriginalLocalVisList++) {
    int originalIndex = *itOriginalLocalVisList;
    if (getVisibilityNewPoint(curCam_)[curvis]) {
      setVisibilityNewPoint(curCam_, curvis, false);
      map<int, int>::iterator itVertHandleIndex = m_mapPoint_VertexHandle.find(originalIndex);
      if (itVertHandleIndex != m_mapPoint_VertexHandle.end()) {
        // We found a match: this feature is already in the point set.  So add it to the visibility list and break.
        localVisList.push_back(itVertHandleIndex->second);
      } else {
        // We didn't find a match, so this is a new feature.  Add it to the point set and the visibility list.
        // This involves properly deleting & staring off a connected subset of tetrahedra that violate the Delaunay constraint
        // after the addition, while marking the new tetrahedra with the deleted tetrahedra's freespace constraints.
        Matrix matTmpPoint = getPoint(originalIndex);
        PointD3 pd3TmpPoint(matTmpPoint(0), matTmpPoint(1), matTmpPoint(2));

        double distance;
        double distx = pd3TmpPoint.x() - camPosition.x();
        double disty = pd3TmpPoint.y() - camPosition.y();
        double distz = pd3TmpPoint.z() - camPosition.z();
        distance = sqrt(distx * distx + disty * disty + distz * distz);

        if (distance < r_) {

          vector<pair<PointD3, double> > vecDistanceWeight;
          if (addNewlyObservedFeature_suboptimal(vecVertexHandles, vecDistanceWeight, pd3TmpPoint, originalIndex)) {      //&& 2*rand()>RAND_MAX/3) {
            localVisList.push_back((int) vecVertexHandles.size() - 1);

            vector<Delaunay3::Cell_handle> newCells;
            dt.incident_cells(vecVertexHandles.back(), std::inserter(newCells, newCells.begin()));
            updateDistanceAndWeights(newCells, vecDistanceWeight);
          }
        }
      }
    }
    curvis++;
  }

#ifdef LOGGING
  gettimeofday(&endCarv, NULL);
  double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
  fileStats_ << "AddFeatures " << delta << std::endl;
#endif
}

int FreespaceDelaunayManifold::findVisuallyCriticalEdges(vector<pair<int, int> > &visualEdges, int idCam) {

  int numOfCriticalEdges = 0;

  Matrix center = getCamCenter(idCam);
  for (auto vis : getVisibilityList(idCam)) {
    Matrix curPt1 = getPoint(vis);
    for (auto visInt : getVisibilityList(idCam)) {
      if (visInt > vis) {      //avoid double comparison
        if (isVisuallyCriticalEdge(curPt1, getPoint(visInt), center)) {
          visualEdges.push_back(std::pair<int, int>(vis, visInt));
          numOfCriticalEdges++;
        }
      }
    }
  }

  return numOfCriticalEdges;
}

bool FreespaceDelaunayManifold::isVisuallyCriticalEdge(Matrix p1, Matrix p2, Matrix center) {
  Matrix camPt1;
  camPt1 = p1 - center;
  Matrix camPt2;
  camPt2 = p2 - center;
  double angle = camPt1.angleBetween(camPt2);
  if (angle > 5.0 * 3.14159265359 / 180) {
    return true;
  } else {
    return false;
  }
}

bool FreespaceDelaunayManifold::isVisuallyCriticalEdge(PointD3 p1, PointD3 p2, Matrix center) {

  Matrix curPt1(3, 1);
  curPt1[0] = p1.x();
  curPt1[1] = p1.y();
  curPt1[2] = p1.z();

  Matrix curPt2(3, 1);
  curPt2[0] = p2.x();
  curPt2[1] = p2.y();
  curPt2[2] = p2.z();
  return isVisuallyCriticalEdge(curPt1, curPt2, center);
}

int FreespaceDelaunayManifold::findCriticalArtifacts(int idCam) {
  //reset the setGalpha flag
  //for(Delaunay3::Finite_cells_iterator it = dt.finite_cells_begin(); it!=dt.finite_cells_end(); ++it ){
  //   it->info().setGalpha(false);
  // }
  Matrix center = getCamCenter(idCam);
  vector<int> visibleVertices = getVisibilityList(idCam);
  float s = 0, sOld = 0;
  do {
    sOld = s;
    for (auto vert : visibleVertices) {
      Delaunay3::Vertex_handle curVert = vecVertexHandles_[m_mapPoint_VertexHandle[vert]];

      std::vector<Delaunay3::Cell_handle> cellsIncidentToV, lsub;
      dt.incident_cells(curVert, std::back_inserter(cellsIncidentToV));

      std::vector<Delaunay3::Cell_handle> cellsInGalpha;
      //TODO avoid multiple visits for the same vertex
      bool existManifoldTet = false;
      bool existNonManifoldTet = false;
      //look for cells in G_alpha
      for (auto cell : cellsIncidentToV) {

        if (!cell->info().iskeptManifold()) {
          //one vertex belonging to boundary cells, made up the boundary iff at least
          //one of the incident cells is in the inside set,i.e., roughly the matter
          existNonManifoldTet = true;
        }
        if (cell->info().iskeptManifold()) {
          //one vertex belonging to boundary cells, made up the boundary iff at least
          //one of the incident cells is in the outside set,i.e., roughly the freespace
          existManifoldTet = true;
        }

        if (isFreespace(cell) && !cell->info().iskeptManifold()) { //To be in Freespace but not in the outside set is a necessary condition to belong to G_alpha set
          bool isInGalpha = false;
          //Check for the second necessary condition to be in G_alpha set (containing a visual critical edge)
          for (int curv1 = 0; curv1 < 4; ++curv1) {
            for (int curv2 = curv1 + 1; curv2 < 4; ++curv2) {
              if (isVisuallyCriticalEdge(cell->vertex(curv1)->point(), cell->vertex(curv2)->point(), center)) {
                isInGalpha = true;
              }
            }
          }

          if (isInGalpha) {
            cellsInGalpha.push_back(cell);
            cell->info().setGalpha(true);
          }
        }
      }

      if (existNonManifoldTet && existManifoldTet && cellsInGalpha.size() > 0) {

        double rsub, radd;
        rsub = 0;
        writeBoundaryToObj("risultBeforeEverything.obj");
        /*  std::vector<msc::Matrix> m_arrModelPoints;
         std::vector<msc::Matrix> m_lstModelTris;
         tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
         writeObj("risultBeforeEverythingManif.obj", m_arrModelPoints, m_lstModelTris);*/
        //populate lsub set & local shrinking
        for (auto cell : cellsIncidentToV) {
          if (cell->info().iskeptManifold()) {
            rsub += cell->info().getVoteCount();
            subTetAndUpdateBoundary(cell);
            lsub.push_back(cell);
          }
        }
        /* writeBoundaryToObj("risultAftersubTetAndUpdateBoundary.obj");
         std::vector<msc::Matrix> m_arrModelPoints2;
         std::vector<msc::Matrix> m_lstModelTris2;
         tetsToTrisSure(m_arrModelPoints2, m_lstModelTris2);
         writeObj("risultAftersubTetAndUpdateBoundaryManif.obj", m_arrModelPoints2, m_lstModelTris2);*/

        //find if the removal of previous tet, keeps the manifold property valid
        std::vector<Delaunay3::Vertex_handle> v;
        dt.incident_vertices(curVert, back_inserter(v));
        bool isManifold = true;
        for (auto vIt : v) {
          if (!isRegular(vIt, false)) {
            isManifold = false;
          }
        }
        if (isManifold) {
          writeBoundaryToObj("risultAfterMAnifoldCheckBoundary.obj");
          std::vector<msc::Matrix> m_arrModelPoints;
          std::vector<msc::Matrix> m_lstModelTris;
          tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
          writeObj("risultAfterMAnifoldCheckBoundaryManif.obj", m_arrModelPoints, m_lstModelTris);
          std::vector<Delaunay3::Cell_handle> vec;
          for (auto c : cellsInGalpha) {
            if (!c->info().iskeptManifold()) {
              regionGrowingProcedureForHandle(c, vec);
            }
          }
          radd = 0;
          for (auto vecAdd : vec) {

            radd += vecAdd->info().getVoteCount();
          }

          if (rsub > radd) {
            /* writeBoundaryToObj("risultBefore.obj");

             std::vector<msc::Matrix> m_arrModelPoints;
             std::vector<msc::Matrix> m_lstModelTris;
             tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
             writeObj("risultBeforeManif.obj", m_arrModelPoints, m_lstModelTris);*/

            for (auto vecAdd : vec) {
              subTetAndUpdateBoundary(vecAdd);
            }
            for (auto cell : lsub) {
              addTetAndUpdateBoundary(cell);
            }
            /* writeBoundaryToObj("risultAfter.obj");
             std::vector<msc::Matrix> m_arrModelPoints2;
             std::vector<msc::Matrix> m_lstModelTris2;
             tetsToTrisSure(m_arrModelPoints2, m_lstModelTris2);
             writeObj("risultAfterManif.obj", m_arrModelPoints2, m_lstModelTris2);*/

          } else {
            // writeBoundaryToObj("risult.obj");
            s = s + radd - rsub;
            /*std::vector<msc::Matrix> m_arrModelPoints2;
             std::vector<msc::Matrix> m_lstModelTris2;
             tetsToTrisSure(m_arrModelPoints2, m_lstModelTris2);
             writeObj("risultManif.obj", m_arrModelPoints2, m_lstModelTris2);*/

          }

        } else {
          for (auto cell : lsub) {
            addTetAndUpdateBoundary(cell);
          }
          /* writeBoundaryToObj("risultAfterEverBoundary.obj");
           std::vector<msc::Matrix> m_arrModelPoints2;
           std::vector<msc::Matrix> m_lstModelTris2;
           tetsToTrisSure(m_arrModelPoints2, m_lstModelTris2);
           writeObj("risultAfterEverManif.obj", m_arrModelPoints2, m_lstModelTris2);*/
        }

      }

      //reset galpha flag
      for (auto c : cellsInGalpha) {
        c->info().setGalpha(false);
      }

    }
  } while (sOld != s);
  return 0;
}

bool FreespaceDelaunayManifold::addNewlyObservedFeature_suboptimal(vector<Delaunay3::Vertex_handle> & vecVertexHandles,
    vector<pair<PointD3, double> > & vecDistanceWeight, const PointD3 & Q, const int nPointIndex) {
  // Add the point Q to the triangulated point set dt.
  // This involves properly deleting & staring off a connected subset of tetrahedra that violate the Delaunay constraint
  // after the addition, while marking the new tetrahedra with the deleted tetrahedra's freespace constraints.

  // Locate the point
  Delaunay3::Locate_type lt;
  int li, lj;
  bool toBeAdded = true;
  Delaunay3::Cell_handle c = dt.locate(Q, lt, li, lj);
  if (lt == Delaunay3::VERTEX) {
    //cerr << "Error in FreespaceDelaunayManifold::addNewlyObservedFeature(): Attempted to add a duplicate vertex to the triangulation" << endl;

    toBeAdded = false;
  }

  // Get the cells that conflict with Q in a vector vecConflictCells, and a facet on the boundary of this hole in f.
  vector<Delaunay3::Cell_handle> vecConflictCells;
  Delaunay3::Facet f;
  dt.find_conflicts(Q, c, CGAL::Oneset_iterator<Delaunay3::Facet>(f), std::back_inserter(vecConflictCells));

  //vector<Point> pointForConvexHull;
  if (shrinkEnabled_) {
    for (vector<Delaunay3::Cell_handle>::const_iterator it = vecConflictCells.begin(); it != vecConflictCells.end(); it++) {

      if ((*it)->info().isBoundary()) {
        //std::cerr << "Error in FreespaceDelaunayManifold::addNewlyObservedFeature: one of the conflicting cell is in the boundary" << std::endl;
        toBeAdded = false;
      }

      if ((*it)->info().iskeptManifold()) {
        //std::cerr << "Error in FreespaceDelaunayManifold::addNewlyObservedFeature: one of the conflicting cell is kept in the manifold" << std::endl;
        toBeAdded = false;
      }
    }
  }
  if (!toBeAdded) {
    return false;

  }

  for (auto itConflCell : vecConflictCells) {
    PointD3 temp = CGAL::barycenter(itConflCell->vertex(0)->point(), 1.0, itConflCell->vertex(1)->point(), 1.0, itConflCell->vertex(2)->point(), 1.0,
        itConflCell->vertex(3)->point(), 1.0);
    vecDistanceWeight.push_back(pair<PointD3, double>(temp, (double) itConflCell->info().getVoteCountProb()));
  }

  // Delete the cells in conflict, insert the point, and star off the hole.
  Delaunay3::Vertex_handle hndlQ = dt.insert_in_hole(Q, vecConflictCells.begin(), vecConflictCells.end(), f.first, f.second);

  // Add Q's Vertex_handle to vecVertexHandles
  vecVertexHandles.push_back(hndlQ);
  m_mapPoint_VertexHandle[nPointIndex] = (int) vecVertexHandles.size() - 1;
  return true;
}

bool FreespaceDelaunayManifold::triangleConstraintIntersectionTest(const Delaunay3::Facet & tri, const Matrix & segSrc, const Matrix & segDest) {
  // A custom implementation of the ray-triangle intersection test at http://jgt.akpeters.com/papers/MollerTrumbore97/code.html
  // Follows the back-face culling branch (ie: a triangle won't intersect the ray if the ray pierces the backside of it.)
  Matrix edge1, edge2, tvec, pvec, qvec;
  double det, inv_det;
  double t, u, v;

  // Get the 3 triangle vertices
  Matrix v0(3, 1);
  Matrix v1(3, 1);
  Matrix v2(3, 1);

  // Note:
  // tri.first = the Cell_handle containing the triangle
  // tri.second = f, the face index for tri.first
  // We want all normals pointing inward, ie positive halfspace of a triangle contains the 4th point of the tetrahedron.  So:
  // f == 0 -> (1, 3, 2)
  // f == 1 -> (0, 2, 3)
  // f == 2 -> (3, 1, 0)
  // f == 3 -> (0, 1, 2)
  if (tri.second == 3) {
    v0(0) = tri.first->vertex(0)->point().x();
    v0(1) = tri.first->vertex(0)->point().y();
    v0(2) = tri.first->vertex(0)->point().z();
    v1(0) = tri.first->vertex(1)->point().x();
    v1(1) = tri.first->vertex(1)->point().y();
    v1(2) = tri.first->vertex(1)->point().z();
    v2(0) = tri.first->vertex(2)->point().x();
    v2(1) = tri.first->vertex(2)->point().y();
    v2(2) = tri.first->vertex(2)->point().z();
  } else if (tri.second == 2) {
    v0(0) = tri.first->vertex(3)->point().x();
    v0(1) = tri.first->vertex(3)->point().y();
    v0(2) = tri.first->vertex(3)->point().z();
    v1(0) = tri.first->vertex(1)->point().x();
    v1(1) = tri.first->vertex(1)->point().y();
    v1(2) = tri.first->vertex(1)->point().z();
    v2(0) = tri.first->vertex(0)->point().x();
    v2(1) = tri.first->vertex(0)->point().y();
    v2(2) = tri.first->vertex(0)->point().z();
  } else if (tri.second == 1) {
    v0(0) = tri.first->vertex(0)->point().x();
    v0(1) = tri.first->vertex(0)->point().y();
    v0(2) = tri.first->vertex(0)->point().z();
    v1(0) = tri.first->vertex(2)->point().x();
    v1(1) = tri.first->vertex(2)->point().y();
    v1(2) = tri.first->vertex(2)->point().z();
    v2(0) = tri.first->vertex(3)->point().x();
    v2(1) = tri.first->vertex(3)->point().y();
    v2(2) = tri.first->vertex(3)->point().z();
  } else if (tri.second == 0) { // f == 0
    v0(0) = tri.first->vertex(1)->point().x();
    v0(1) = tri.first->vertex(1)->point().y();
    v0(2) = tri.first->vertex(1)->point().z();
    v1(0) = tri.first->vertex(3)->point().x();
    v1(1) = tri.first->vertex(3)->point().y();
    v1(2) = tri.first->vertex(3)->point().z();
    v2(0) = tri.first->vertex(2)->point().x();
    v2(1) = tri.first->vertex(2)->point().y();
    v2(2) = tri.first->vertex(2)->point().z();
  } else {
    cerr << "whaomg" << endl;
  }

  // Get the constraint ray's normalized direction vector
  Matrix dir = segDest - segSrc;
  double dirNorm = dir.norm();
  dir /= dirNorm;

  // Find vectors for two edges sharing v0:
  edge1 = v1 - v0;
  edge2 = v2 - v0;

  // Begin calculating determinant - also used to calculate U parameter
  pvec = dir.cross(edge2);

  // If determinant is near zero, ray lies in plane of triangle.  We do backface culling for the intersection test,
  // so only need to check 1 halfspace
  det = edge1.dot(pvec);
  if (det < sqrt_eps_d)
    return false;

  // Calculate distance from v0 to ray origin
  tvec = segSrc - v0;

  // Calculate U parameter and test bounds
  u = tvec.dot(pvec);
  if (u < 0.0 || u > det)
    return false;

  // Prepare to test V parameter
  qvec = tvec.cross(edge1);

  // Calculate V parameter and test bounds
  v = dir.dot(qvec);
  if (v < 0.0 || (u + v) > det)
    return false;

  // Calculate t, scale parameters, ray intersects triangle
  t = edge2.dot(qvec);
  inv_det = 1.0 / det;
  t *= inv_det;
  // u *= inv_det;
  // v *= inv_det;

  // Test if distance to plane t is too large and return false if so
  if (t < 0.0 || t > dirNorm)
    return false;

  return true;
}

bool FreespaceDelaunayManifold::triangleConstraintIntersectionTest(bool & bCrossesInteriorOfConstraint, const vector<Matrix> & points, const Matrix & tri,
    const pair<Matrix, Matrix> & constraint) {
  // A custom implementation of the ray-triangle intersection test at http://jgt.akpeters.com/papers/MollerTrumbore97/code.html
  // Follows the back-face culling branch (ie: a triangle won't intersect the ray if the ray pierces the backside of it.)
  Matrix edge1, edge2, tvec, pvec, qvec;
  double det, inv_det;
  double t, u, v;

  // Set default for interiorOfConstraint = false return value (in the case that there is no intersection for quick returns)
  bCrossesInteriorOfConstraint = false;

  // Get the two segment points:
  const Matrix & segSrc = constraint.first;
  const Matrix & segDest = constraint.second;

  // Get the 3 triangle vertices
  const Matrix & v0 = points[round(tri(0))];
  const Matrix & v1 = points[round(tri(1))];
  const Matrix & v2 = points[round(tri(2))];

  // Get the constraint ray's normalized direction vector
  Matrix dir = segDest - segSrc;
  double dirNorm = dir.norm();
  dir /= dirNorm;

  // Find vectors for two edges sharing v0:
  edge1 = v1 - v0;
  edge2 = v2 - v0;

  // Begin calculating determinant - also used to calculate U parameter
  pvec = dir.cross(edge2);

  // If determinant is near zero, ray lies in plane of triangle.  We do backface culling for the intersection test,
  // so only need to check 1 halfspace
  det = edge1.dot(pvec);
  if (det < sqrt_eps_d)
    return false;

  // Calculate distance from v0 to ray origin
  tvec = segSrc - v0;

  // Calculate U parameter and test bounds
  u = tvec.dot(pvec);
  if (u < 0.0 || u > det)
    return false;

  // Prepare to test V parameter
  qvec = tvec.cross(edge1);

  // Calculate V parameter and test bounds
  v = dir.dot(qvec);
  if (v < 0.0 || (u + v) > det)
    return false;

  // Calculate t, scale parameters, ray intersects triangle
  t = edge2.dot(qvec);
  inv_det = 1.0 / det;
  t *= inv_det;
  // u *= inv_det;
  // v *= inv_det;

  // Test if distance to plane t is too large and return false if so
  if (t < 0.0 || t > dirNorm)
    return false;
  if (t != 0.0 && t != dirNorm)
    bCrossesInteriorOfConstraint = true;

  return true;
}

bool FreespaceDelaunayManifold::cellTraversalExitTest(int & f, const Delaunay3::Cell_handle tetCur, const Delaunay3::Cell_handle tetPrev, const Matrix & matQ,
    const Matrix & matO) {
  // TODO: See if we can optimize this by reuse: we use the same constraint QO in all 3 face tests.  Faces also share edges and points.
  vector<Matrix> points;
  Matrix tri(3, 1);
  pair<Matrix, Matrix> constraint(matQ, matO);
  bool bCrossesInteriorOfConstraint;
  Matrix tmpMat(3, 1);

  // Let f be the entry face's index.
  if (tetCur->neighbor(0) == tetPrev)
    f = 0;
  else if (tetCur->neighbor(1) == tetPrev)
    f = 1;
  else if (tetCur->neighbor(2) == tetPrev)
    f = 2;
  else if (tetCur->neighbor(3) == tetPrev)
    f = 3;

  // Collect the tetrahedra's 4 vertices into the variable points
  tmpMat(0) = tetCur->vertex(0)->point().x();
  tmpMat(1) = tetCur->vertex(0)->point().y();
  tmpMat(2) = tetCur->vertex(0)->point().z();
  points.push_back(tmpMat);
  tmpMat(0) = tetCur->vertex(1)->point().x();
  tmpMat(1) = tetCur->vertex(1)->point().y();
  tmpMat(2) = tetCur->vertex(1)->point().z();
  points.push_back(tmpMat);
  tmpMat(0) = tetCur->vertex(2)->point().x();
  tmpMat(1) = tetCur->vertex(2)->point().y();
  tmpMat(2) = tetCur->vertex(2)->point().z();
  points.push_back(tmpMat);
  tmpMat(0) = tetCur->vertex(3)->point().x();
  tmpMat(1) = tetCur->vertex(3)->point().y();
  tmpMat(2) = tetCur->vertex(3)->point().z();
  points.push_back(tmpMat);

  // Construct the indices for the triangles to test, and test them as needed.
  // We want all normals pointing inward, ie positive halfspace of a triangle contains the 4th point of the tetrahedron.  So:
  // f == 0 -> (1, 3, 2)
  // f == 1 -> (0, 2, 3)
  // f == 2 -> (3, 1, 0)
  // f == 3 -> (0, 1, 2)
  if (f == 3) {
    // Test face 0
    tri(0) = 1;
    tri(1) = 3;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 0;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 1
    tri(0) = 0;
    tri(1) = 2;
    tri(2) = 3;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 1;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 2
    tri(0) = 3;
    tri(1) = 1;
    tri(2) = 0;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 2;
      return bCrossesInteriorOfConstraint;
    }
  } else if (f == 2) {
    // Test face 0
    tri(0) = 1;
    tri(1) = 3;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 0;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 1
    tri(0) = 0;
    tri(1) = 2;
    tri(2) = 3;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 1;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 3
    tri(0) = 0;
    tri(1) = 1;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 3;
      return bCrossesInteriorOfConstraint;
    }
  } else if (f == 1) {
    // Test face 0
    tri(0) = 1;
    tri(1) = 3;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 0;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 2
    tri(0) = 3;
    tri(1) = 1;
    tri(2) = 0;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 2;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 3
    tri(0) = 0;
    tri(1) = 1;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 3;
      return bCrossesInteriorOfConstraint;
    }
  } else { // f == 0
    // Test face 1
    tri(0) = 0;
    tri(1) = 2;
    tri(2) = 3;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 1;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 2
    tri(0) = 3;
    tri(1) = 1;
    tri(2) = 0;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 2;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 3
    tri(0) = 0;
    tri(1) = 1;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 3;
      return bCrossesInteriorOfConstraint;
    }
  }
  // If no triangle intersects the constraint, then the optic center lies in the interior (or on the entry face) of the tetrahedron.  So return false.
  return false;
}

bool FreespaceDelaunayManifold::cellTraversalExitTest(int & f, int & fold, const Delaunay3::Cell_handle tetCur, const Delaunay3::Cell_handle tetPrev,
    const Matrix & matQ, const Matrix & matO) {
  // TODO: See if we can optimize this by reuse: we use the same constraint QO in all 3 face tests.  Faces also share edges and points.
  vector<Matrix> points;
  Matrix tri(3, 1);
  pair<Matrix, Matrix> constraint(matQ, matO);
  bool bCrossesInteriorOfConstraint;
  Matrix tmpMat(3, 1);

  // Let f be the entry face's index.
  if (tetCur->neighbor(0) == tetPrev)
    fold = 0;
  else if (tetCur->neighbor(1) == tetPrev)
    fold = 1;
  else if (tetCur->neighbor(2) == tetPrev)
    fold = 2;
  else if (tetCur->neighbor(3) == tetPrev)
    fold = 3;

  // Collect the tetrahedra's 4 vertices into the variable points
  tmpMat(0) = tetCur->vertex(0)->point().x();
  tmpMat(1) = tetCur->vertex(0)->point().y();
  tmpMat(2) = tetCur->vertex(0)->point().z();
  points.push_back(tmpMat);
  tmpMat(0) = tetCur->vertex(1)->point().x();
  tmpMat(1) = tetCur->vertex(1)->point().y();
  tmpMat(2) = tetCur->vertex(1)->point().z();
  points.push_back(tmpMat);
  tmpMat(0) = tetCur->vertex(2)->point().x();
  tmpMat(1) = tetCur->vertex(2)->point().y();
  tmpMat(2) = tetCur->vertex(2)->point().z();
  points.push_back(tmpMat);
  tmpMat(0) = tetCur->vertex(3)->point().x();
  tmpMat(1) = tetCur->vertex(3)->point().y();
  tmpMat(2) = tetCur->vertex(3)->point().z();
  points.push_back(tmpMat);

  // Construct the indices for the triangles to test, and test them as needed.
  // We want all normals pointing inward, ie positive halfspace of a triangle contains the 4th point of the tetrahedron.  So:
  // f == 0 -> (1, 3, 2)
  // f == 1 -> (0, 2, 3)
  // f == 2 -> (3, 1, 0)
  // f == 3 -> (0, 1, 2)
  if (fold == 3) {
    // Test face 0
    tri(0) = 1;
    tri(1) = 3;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 0;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 1
    tri(0) = 0;
    tri(1) = 2;
    tri(2) = 3;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 1;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 2
    tri(0) = 3;
    tri(1) = 1;
    tri(2) = 0;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 2;
      return bCrossesInteriorOfConstraint;
    }
  } else if (fold == 2) {
    // Test face 0
    tri(0) = 1;
    tri(1) = 3;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 0;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 1
    tri(0) = 0;
    tri(1) = 2;
    tri(2) = 3;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 1;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 3
    tri(0) = 0;
    tri(1) = 1;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 3;
      return bCrossesInteriorOfConstraint;
    }
  } else if (fold == 1) {
    // Test face 0
    tri(0) = 1;
    tri(1) = 3;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 0;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 2
    tri(0) = 3;
    tri(1) = 1;
    tri(2) = 0;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 2;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 3
    tri(0) = 0;
    tri(1) = 1;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 3;
      return bCrossesInteriorOfConstraint;
    }
  } else { // f == 0
    // Test face 1
    tri(0) = 0;
    tri(1) = 2;
    tri(2) = 3;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 1;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 2
    tri(0) = 3;
    tri(1) = 1;
    tri(2) = 0;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 2;
      return bCrossesInteriorOfConstraint;
    }
    // Test face 3
    tri(0) = 0;
    tri(1) = 1;
    tri(2) = 2;
    if (triangleConstraintIntersectionTest(bCrossesInteriorOfConstraint, points, tri, constraint)) {
      f = 3;
      return bCrossesInteriorOfConstraint;
    }
  }
  // If no triangle intersects the constraint, then the optic center lies in the interior (or on the entry face) of the tetrahedron.  So return false.
  return false;
}
void FreespaceDelaunayManifold::facetToTri(const Delaunay3::Facet & f, vector<Delaunay3::Vertex_handle> & vecTri) const {
  if (f.second == 0) {
    // Vertex handle order: 3 2 1
    vecTri.push_back(f.first->vertex(3));
    vecTri.push_back(f.first->vertex(2));
    vecTri.push_back(f.first->vertex(1));
  } else if (f.second == 1) {
    // Vertex handle order: 0 2 3
    vecTri.push_back(f.first->vertex(0));
    vecTri.push_back(f.first->vertex(2));
    vecTri.push_back(f.first->vertex(3));
  } else if (f.second == 2) {
    // Vertex handle order: 3 1 0
    vecTri.push_back(f.first->vertex(3));
    vecTri.push_back(f.first->vertex(1));
    vecTri.push_back(f.first->vertex(0));
  } else { // f->second == 3
    // Vertex handle order: 0 1 2
    vecTri.push_back(f.first->vertex(0));
    vecTri.push_back(f.first->vertex(1));
    vecTri.push_back(f.first->vertex(2));
  }
}

double FreespaceDelaunayManifold::timestamp() const {
  timeval t;
  gettimeofday(&t, 0);
  return (double) (t.tv_sec + (t.tv_usec / 1000000.0));
}

bool FreespaceDelaunayManifold::isFreespace(Delaunay3::Cell_handle cell) {
  bool value;
  if (!configuration_.spaceCarvingConfig.inverseConicEnabled) {
    value = !cell->info().isKeptByVoteCount(configuration_.spaceCarvingConfig.probOrVoteThreshold);
  } else {
    value = !cell->info().isKeptByVoteCountProb(configuration_.spaceCarvingConfig.probOrVoteThreshold);    //&&
    //value = cell->info().getVoteCount()>=1;
  }
  return value;
}

void FreespaceDelaunayManifold::tetsToTris_naiveOrig(vector<Matrix> & points, vector<Matrix> & tris, const int nVoteThresh) {
  vector<Delaunay3::Vertex_handle> vecBoundsHandles;
  vector<Delaunay3::Vertex_handle> vecVertexHandles;
  hash_map<Delaunay3::Vertex_handle, int, HashVertHandle, EqVertHandle> hmapVertexHandleToIndex;
  Matrix matTmpPoint(3, 1);
  int numKept = 0, numNotKept = 0;
  int numManifold = 0;

  for (Delaunay3::Cell_iterator itCell = dt.cells_begin(); itCell != dt.cells_end(); itCell++) {
    if ((*itCell).info().iskeptManifold()) {
      ++numManifold;
    }
  }
  std::cout << "numManifold: " << numManifold << ", tets" << std::endl;

  fileStats_ << "ManifoldTets " << numManifold;
  fileStats_ << std::endl;
  fileStats_ << "RegionGrowingBatch " << std::endl;

  // Initialize points and tris as empty:
  if (!points.empty())
    points.clear();
  if (!tris.empty())
    tris.clear();

  // Create a list of vertex handles to the bounding vertices (they'll be the vertices connected to the infinite vertex):
  dt.incident_vertices(dt.infinite_vertex(), std::back_inserter(vecBoundsHandles));

  // Populate the model's point list, create a list of finite non-bounding vertex handles, and
  // create a useful associative maps (handle->point list index).
  for (Delaunay3::Finite_vertices_iterator itVert = dt.finite_vertices_begin(); itVert != dt.finite_vertices_end(); itVert++) {
    vector<Delaunay3::Vertex_handle>::iterator itBounds;
    for (itBounds = vecBoundsHandles.begin(); itBounds != vecBoundsHandles.end(); itBounds++) {
      if ((*itBounds) == ((Delaunay3::Vertex_handle) itVert))
        break;
    }
    if (itBounds == vecBoundsHandles.end()) { // the vertex is not a bounding vertex, so add it
      matTmpPoint(0) = itVert->point().x();
      matTmpPoint(1) = itVert->point().y();
      matTmpPoint(2) = itVert->point().z();
      points.push_back(matTmpPoint);
      vecVertexHandles.push_back(itVert);
      hmapVertexHandleToIndex[itVert] = points.size() - 1;
    }
  }

  // Iterate over finite facets
  for (Delaunay3::Finite_facets_iterator itFacet = dt.finite_facets_begin(); itFacet != dt.finite_facets_end(); itFacet++) {
    // If one adjacent cell is empty, and the other is not, and if the facet contains no vertex from the bounding vertices, then add a triangle to the mesh (w/ correct orientation).
    bool bFacetCellKept = itFacet->first->info().iskeptManifold();

    bool bMirrorCellKept = itFacet->first->neighbor(itFacet->second)->info().iskeptManifold();
    if ((bFacetCellKept != bMirrorCellKept)) {
      numKept++;
    } else {
      numNotKept++;
    }
    if (!bFacetCellKept && bMirrorCellKept) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;

        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = dt.mirror_facet(*itFacet); // The normal points inward so mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    } else if (!bMirrorCellKept && bFacetCellKept) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = *itFacet; // The normal points outward so no need to mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    }
  }
  std::cout << "num facets kept: " << numKept << ", num facets not kept: " << numNotKept << std::endl;
  std::cout << "tris created: " << tris.size() << ", points : " << points.size() << std::endl;
}

void FreespaceDelaunayManifold::tetsToTris_Basic(Delaunay3 temp, vector<Matrix> & points, vector<Matrix> & tris) const {
  vector<Delaunay3::Vertex_handle> vecBoundsHandles;
  vector<Delaunay3::Vertex_handle> vecVertexHandles;
  hash_map<Delaunay3::Vertex_handle, int, HashVertHandle, EqVertHandle> hmapVertexHandleToIndex;
  Matrix matTmpPoint(3, 1);
  int numKept = 0, numNotKept = 0;

  // Initialize points and tris as empty:
  if (!points.empty())
    points.clear();
  if (!tris.empty())
    tris.clear();

  // Create a list of vertex handles to the bounding vertices (they'll be the vertices connected to the infinite vertex):
  dt.incident_vertices(dt.infinite_vertex(), std::back_inserter(vecBoundsHandles));

  // Populate the model's point list, create a list of finite non-bounding vertex handles, and
  // create a useful associative maps (handle->point list index).
  for (Delaunay3::Finite_vertices_iterator itVert = dt.finite_vertices_begin(); itVert != dt.finite_vertices_end(); itVert++) {
    vector<Delaunay3::Vertex_handle>::iterator itBounds;
    for (itBounds = vecBoundsHandles.begin(); itBounds != vecBoundsHandles.end(); itBounds++) {
      if ((*itBounds) == ((Delaunay3::Vertex_handle) itVert))
        break;
    }
    if (itBounds == vecBoundsHandles.end()) { // the vertex is not a bounding vertex, so add it
      matTmpPoint(0) = itVert->point().x();
      matTmpPoint(1) = itVert->point().y();
      matTmpPoint(2) = itVert->point().z();
      points.push_back(matTmpPoint);
      vecVertexHandles.push_back(itVert);
      hmapVertexHandleToIndex[itVert] = points.size() - 1;
    }
  }

  // Iterate over finite facets
  for (Delaunay3::Finite_facets_iterator itFacet = dt.finite_facets_begin(); itFacet != dt.finite_facets_end(); itFacet++) {
    // If one adjacent cell is empty, and the other is not, and if the facet contains no vertex from the bounding vertices, then add a triangle to the mesh (w/ correct orientation).
    bool bFacetCellKept = true;

    bool bMirrorCellKept = false;
    if ((bFacetCellKept != bMirrorCellKept)) {
      numKept++;
    } else {
      numNotKept++;
    }
    if (bFacetCellKept && !bMirrorCellKept) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;

        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = dt.mirror_facet(*itFacet); // The normal points inward so mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    } else if (bMirrorCellKept && !bFacetCellKept) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = *itFacet; // The normal points outward so no need to mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    }
  }
}
void FreespaceDelaunayManifold::tetsToTris_naive(vector<Matrix> & points, vector<Matrix> & tris, const int nVoteThresh) {
  vector<Delaunay3::Vertex_handle> vecBoundsHandles;
  vector<Delaunay3::Vertex_handle> vecVertexHandles;
  hash_map<Delaunay3::Vertex_handle, int, HashVertHandle, EqVertHandle> hmapVertexHandleToIndex;
  Matrix matTmpPoint(3, 1);

  int numFreeSpaceTets = 0;
  for (Delaunay3::Cell_iterator itCell = dt.cells_begin(); itCell != dt.cells_end(); itCell++) {
    bool value;
    if (!configuration_.spaceCarvingConfig.inverseConicEnabled) {
      value = !itCell->info().isKeptByVoteCount(configuration_.spaceCarvingConfig.probOrVoteThreshold);
    } else {
      value = !itCell->info().isKeptByVoteCountProb(configuration_.spaceCarvingConfig.probOrVoteThreshold);
    }
    if (value) {
      ++numFreeSpaceTets;
    }
  }
  std::cout << "numFreeSpaceTets: " << numFreeSpaceTets << ", tets" << std::endl;

  fileStats_ << "FreespaceTets " << numFreeSpaceTets;
  fileStats_ << std::endl;

  // Initialize points and tris as empty:
  if (!points.empty())
    points.clear();
  if (!tris.empty())
    tris.clear();

  // Create a list of vertex handles to the bounding vertices (they'll be the vertices connected to the infinite vertex):
  dt.incident_vertices(dt.infinite_vertex(), std::back_inserter(vecBoundsHandles));

  // Populate the model's point list, create a list of finite non-bounding vertex handles, and
  // create a useful associative maps (handle->point list index).
  for (Delaunay3::Finite_vertices_iterator itVert = dt.finite_vertices_begin(); itVert != dt.finite_vertices_end(); itVert++) {
    vector<Delaunay3::Vertex_handle>::iterator itBounds;
    for (itBounds = vecBoundsHandles.begin(); itBounds != vecBoundsHandles.end(); itBounds++) {
      if ((*itBounds) == ((Delaunay3::Vertex_handle) itVert))
        break;
    }
    if (itBounds == vecBoundsHandles.end()) { // the vertex is not a bounding vertex, so add it
      matTmpPoint(0) = itVert->point().x();
      matTmpPoint(1) = itVert->point().y();
      matTmpPoint(2) = itVert->point().z();
      points.push_back(matTmpPoint);
      vecVertexHandles.push_back(itVert);
      hmapVertexHandleToIndex[itVert] = points.size() - 1;
    }
  }

  // Iterate over finite facets
  for (Delaunay3::Finite_facets_iterator itFacet = dt.finite_facets_begin(); itFacet != dt.finite_facets_end(); itFacet++) {
    // If one adjacent cell is empty, and the other is not, and if the facet contains no vertex from the bounding vertices, then add a triangle to the mesh (w/ correct orientation).
    bool bFacetCellKept = itFacet->first->info().isKeptByVoteCount(nVoteThresh);
    bool bMirrorCellKept = itFacet->first->neighbor(itFacet->second)->info().isKeptByVoteCount(nVoteThresh);
    bool toCheck2Manifold = false;
    if (bFacetCellKept && !bMirrorCellKept) {
      toCheck2Manifold = true;
    }
    for (int cur = 0; cur < 4; cur++) {
      if (bFacetCellKept && !bMirrorCellKept) {
        if (cur != itFacet->second) {
          if (itFacet->first->neighbor(cur)->info().isKeptByVoteCount(nVoteThresh)) {
            toCheck2Manifold = false;
          }
        }
      }

    }
    bool is2Manifold = true;
    if (toCheck2Manifold == true) {
      is2Manifold = !itFacet->first->vertex(itFacet->second)->info().isUsed();
    }

    if (bFacetCellKept && !bMirrorCellKept && is2Manifold) {

      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = dt.mirror_facet(*itFacet); // The normal points inward so mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    } else if (bMirrorCellKept && !bFacetCellKept && is2Manifold) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = *itFacet; // The normal points outward so no need to mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    }
  }
}

void FreespaceDelaunayManifold::tetsToTrisSure_naive(vector<Matrix> & points, vector<Matrix> & tris) const {
  vector<Delaunay3::Vertex_handle> vecBoundsHandles;
  vector<Delaunay3::Vertex_handle> vecVertexHandles;
  hash_map<Delaunay3::Vertex_handle, int, HashVertHandle, EqVertHandle> hmapVertexHandleToIndex;
  Matrix matTmpPoint(3, 1);

  // Initialize points and tris as empty:
  if (!points.empty())
    points.clear();
  if (!tris.empty())
    tris.clear();

  // Create a list of vertex handles to the bounding vertices (they'll be the vertices connected to the infinite vertex):
  dt.incident_vertices(dt.infinite_vertex(), std::back_inserter(vecBoundsHandles));

  // Populate the model's point list, create a list of finite non-bounding vertex handles, and
  // create a useful associative maps (handle->point list index).
  for (Delaunay3::Finite_vertices_iterator itVert = dt.finite_vertices_begin(); itVert != dt.finite_vertices_end(); itVert++) {
    vector<Delaunay3::Vertex_handle>::iterator itBounds;
    for (itBounds = vecBoundsHandles.begin(); itBounds != vecBoundsHandles.end(); itBounds++) {
      if ((*itBounds) == ((Delaunay3::Vertex_handle) itVert))
        break;
    }
    if (itBounds == vecBoundsHandles.end()) { // the vertex is not a bounding vertex, so add it
      matTmpPoint(0) = itVert->point().x();
      matTmpPoint(1) = itVert->point().y();
      matTmpPoint(2) = itVert->point().z();
      points.push_back(matTmpPoint);
      vecVertexHandles.push_back(itVert);
      hmapVertexHandleToIndex[itVert] = points.size() - 1;
    }
  }

  // Iterate over finite facets
  for (Delaunay3::Finite_facets_iterator itFacet = dt.finite_facets_begin(); itFacet != dt.finite_facets_end(); itFacet++) {
    // If one adjacent cell is empty, and the other is not, and if the facet contains no vertex from the bounding vertices, then add a triangle to the mesh (w/ correct orientation).
    bool bFacetCellKept = itFacet->first->info().iskeptManifold();
    bool bMirrorCellKept = itFacet->first->neighbor(itFacet->second)->info().iskeptManifold();
    bool toCheck2Manifold = false;
    if (bFacetCellKept && !bMirrorCellKept) {
      toCheck2Manifold = true;
    }
    for (int cur = 0; cur < 4; cur++) {
      if (bFacetCellKept && !bMirrorCellKept) {
        if (cur != itFacet->second) {
          if (itFacet->first->neighbor(cur)->info().iskeptManifold()) {
            toCheck2Manifold = false;
          }
        }
      }

    }
    bool is2Manifold = true;
    if (toCheck2Manifold == true) {
      is2Manifold = !itFacet->first->vertex(itFacet->second)->info().isUsed();
    }

    if (bFacetCellKept && !bMirrorCellKept && is2Manifold) {

      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = dt.mirror_facet(*itFacet); // The normal points inward so mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    } else if (bMirrorCellKept && !bFacetCellKept && is2Manifold) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = *itFacet; // The normal points outward so no need to mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    }
  }
}
void FreespaceDelaunayManifold::tetsToTris_naive(vector<Matrix> & points, vector<Matrix> & tris, const float nVoteThresh) {
  vector<Delaunay3::Vertex_handle> vecBoundsHandles;
  vector<Delaunay3::Vertex_handle> vecVertexHandles;
  hash_map<Delaunay3::Vertex_handle, int, HashVertHandle, EqVertHandle> hmapVertexHandleToIndex;
  Matrix matTmpPoint(3, 1);

  // Initialize points and tris as empty:
  if (!points.empty())
    points.clear();
  if (!tris.empty())
    tris.clear();

  // Create a list of vertex handles to the bounding vertices (they'll be the vertices connected to the infinite vertex):
  dt.incident_vertices(dt.infinite_vertex(), std::back_inserter(vecBoundsHandles));

  // Populate the model's point list, create a list of finite non-bounding vertex handles, and
  // create a useful associative maps (handle->point list index).
  for (Delaunay3::Finite_vertices_iterator itVert = dt.finite_vertices_begin(); itVert != dt.finite_vertices_end(); itVert++) {
    vector<Delaunay3::Vertex_handle>::iterator itBounds;
    for (itBounds = vecBoundsHandles.begin(); itBounds != vecBoundsHandles.end(); itBounds++) {
      if ((*itBounds) == ((Delaunay3::Vertex_handle) itVert))
        break;
    }
    if (itBounds == vecBoundsHandles.end()) { // the vertex is not a bounding vertex, so add it
      matTmpPoint(0) = itVert->point().x();
      matTmpPoint(1) = itVert->point().y();
      matTmpPoint(2) = itVert->point().z();
      points.push_back(matTmpPoint);
      vecVertexHandles.push_back(itVert);
      hmapVertexHandleToIndex[itVert] = points.size() - 1;
    }
  }

  // Iterate over finite facets
  for (Delaunay3::Finite_facets_iterator itFacet = dt.finite_facets_begin(); itFacet != dt.finite_facets_end(); itFacet++) {
    // If one adjacent cell is empty, and the other is not, and if the facet contains no vertex from the bounding vertices, then add a triangle to the mesh (w/ correct orientation).
    bool bFacetCellKept = itFacet->first->info().isKeptByVoteCountProb(nVoteThresh);
    bool bMirrorCellKept = itFacet->first->neighbor(itFacet->second)->info().isKeptByVoteCountProb(nVoteThresh);
    bool toCheck2Manifold = false;
    if (bFacetCellKept && !bMirrorCellKept) {
      toCheck2Manifold = true;
    }
    for (int cur = 0; cur < 4; cur++) {
      if (bFacetCellKept && !bMirrorCellKept) {
        if (cur != itFacet->second) {
          if (itFacet->first->neighbor(cur)->info().isKeptByVoteCountProb(nVoteThresh)) {
            toCheck2Manifold = false;
          }
        }
      }

    }
    bool is2Manifold = true;
    if (toCheck2Manifold == true) {
      is2Manifold = !itFacet->first->vertex(itFacet->second)->info().isUsed();
    }

    if (bFacetCellKept && !bMirrorCellKept && is2Manifold) {

      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = dt.mirror_facet(*itFacet); // The normal points inward so mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    } else if (bMirrorCellKept && !bFacetCellKept && is2Manifold) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = *itFacet; // The normal points outward so no need to mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    }
  }
}
void FreespaceDelaunayManifold::tetsToTris_maxFlowSimple(vector<Matrix> & points, vector<Matrix> & tris, const int nVoteThresh) const {
  vector<Delaunay3::Vertex_handle> vecBoundsHandles;
  vector<Delaunay3::Vertex_handle> vecVertexHandles;
  hash_map<Delaunay3::Vertex_handle, int, HashVertHandle, EqVertHandle> hmapVertexHandleToIndex;
  map<Delaunay3::Cell_handle, int> mapCellHandleToIndex;
  Matrix matTmpPoint(3, 1);
  int loop;

  // Get some size-properties from the triangulation (non-constant-time access functions in the triangulation class)
  int numFiniteTets = dt.number_of_finite_cells();
  int numFiniteFacets = dt.number_of_finite_facets();

  // Initialize the maxflow graph
  Graph_t graph(numFiniteTets, numFiniteFacets);

  // Initialize points and tris as empty:
  if (!points.empty())
    points.clear();
  if (!tris.empty())
    tris.clear();

  // Create a list of vertex handles to the bounding vertices (they'll be the vertices connected to the infinite vertex):
  dt.incident_vertices(dt.infinite_vertex(), std::back_inserter(vecBoundsHandles));

  // Populate the model's point list, create a list of finite non-bounding vertex handles, and
  // create a useful associative map (handle->point list index).
  for (Delaunay3::Finite_vertices_iterator itVert = dt.finite_vertices_begin(); itVert != dt.finite_vertices_end(); itVert++) {
    vector<Delaunay3::Vertex_handle>::iterator itBounds;
    for (itBounds = vecBoundsHandles.begin(); itBounds != vecBoundsHandles.end(); itBounds++) {
      if ((*itBounds) == ((Delaunay3::Vertex_handle) itVert))
        break;
    }
    if (itBounds == vecBoundsHandles.end()) { // the vertex is not a bounding vertex, so add it
      matTmpPoint(0) = itVert->point().x();
      matTmpPoint(1) = itVert->point().y();
      matTmpPoint(2) = itVert->point().z();
      points.push_back(matTmpPoint);
      vecVertexHandles.push_back(itVert);
      hmapVertexHandleToIndex[itVert] = points.size() - 1;
    }
  }

  // Create useful associative maps (tet list index->handle & handle->tet list index).
  Delaunay3::Finite_cells_iterator it;
  for (loop = 0, it = dt.finite_cells_begin(); it != dt.finite_cells_end(); it++, loop++)
    mapCellHandleToIndex[it] = loop;

  // Add the source and sink to the graph
  graph.addSource();
  graph.addSink();

  // Construct the graph's edge costs to minimize an engergy E = data + lambda_smooth * smoothness:
  // Labels:
  //   source s (0) = outside
  //   sink t (1) = inside
  // Data term (TODO: tune these bogus params):
  //   P(constraint in x | x = outside) = 0.6
  //   P(no constraint in x | x = outside) = 0.4
  //   P(constraint in x | x = inside) = 0.2
  //   P(no constraint in x | x = inside) = 0.8
  const double P_constr_X0 = 0.8;
  const double P_no_constr_X0 = 0.2;
  const double P_constr_X1 = 0.2;
  const double P_no_constr_X1 = 0.9;
  const double lambda_smooth = 0.001; //0.75;  // Good values approx. < 0.5 to 1

  // Construct the graph's data terms
  for (it = dt.finite_cells_begin(); it != dt.finite_cells_end(); it++) {
    int node = mapCellHandleToIndex.find(it)->second;

    double tetVolume = fabs(dt.tetrahedron(it).volume());

    if (it->info().getVoteCount()) {
      // node X has constraints
      graph.addTWeights(node, P_constr_X0 * tetVolume, P_constr_X1 * tetVolume);
    } else {
      // node X has no constraint
      graph.addTWeights(node, P_no_constr_X0 * tetVolume, P_no_constr_X1 * tetVolume);
    }
  }

  // Iterate over finite facets to construct the graph's regularization
  for (Delaunay3::Finite_facets_iterator it = dt.finite_facets_begin(); it != dt.finite_facets_end(); it++) {
    // If the facet contains a bounding vert, it won't be added to the isosurface, so don't penalize it with a smoothness cost.
    bool bContainsBoundsVert = false;
    for (int i = 0; i < 4; i++) {
      if (i == it->second)
        continue;
      if (std::find(vecBoundsHandles.begin(), vecBoundsHandles.end(), it->first->vertex(i)) != vecBoundsHandles.end()) {
        bContainsBoundsVert = true;
        break;
      }
    }
    if (!bContainsBoundsVert) {
      double smoothness_cost = lambda_smooth * sqrt(dt.triangle(*it).squared_area());
      graph.addEdge(mapCellHandleToIndex[it->first], mapCellHandleToIndex[it->first->neighbor(it->second)], smoothness_cost, smoothness_cost);
    }
  }

  // Run the maxflow algorithm to determine the labeling
  graph.maxflow();
  //double flow = graph.maxflow();
  //cerr << "Max Flow: " << flow << endl;

  // Iterate over finite facets to extract the mesh's triangles from the graph's mincut labeling
  for (Delaunay3::Finite_facets_iterator itFacet = dt.finite_facets_begin(); itFacet != dt.finite_facets_end(); itFacet++) {
    // If one adjacent cell is empty, and the other is not, and if the facet contains no vertex from the bounding vertices,
    // then add a triangle to the mesh (w/ correct orientation).
    bool bFacetCellKept = !graph.whatSegment(mapCellHandleToIndex[itFacet->first]);
    bool bMirrorCellKept = !graph.whatSegment(mapCellHandleToIndex[itFacet->first->neighbor(itFacet->second)]);
    if (bFacetCellKept && !bMirrorCellKept) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = dt.mirror_facet(*itFacet); // The normal points inward so mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    } else if (bMirrorCellKept && !bFacetCellKept) {
      bool bContainsBoundsVert = false;
      for (int i = 0; i < 4; i++) {
        if (i == itFacet->second)
          continue;
        if (hmapVertexHandleToIndex.count(itFacet->first->vertex(i)) == 0) {
          bContainsBoundsVert = true;
          break;
        }
      }
      if (!bContainsBoundsVert) {
        Delaunay3::Facet fTmp = *itFacet; // The normal points outward so no need to mirror the facet
        Matrix tmpTri(1, 3);
        vector<Delaunay3::Vertex_handle> vecTri;

        facetToTri(fTmp, vecTri);
        tmpTri(0) = hmapVertexHandleToIndex[vecTri[0]];
        tmpTri(1) = hmapVertexHandleToIndex[vecTri[1]];
        tmpTri(2) = hmapVertexHandleToIndex[vecTri[2]];
        tris.push_back(tmpTri);
      }
    }
  }
}

bool FreespaceDelaunayManifold::moveVertex(vector<Delaunay3::Vertex_handle> & vecVertexHandles, const int pointIndex) {
  // Vertex Moving Algorithm:
  // ~~~~~~~~~~~~~~~~~~~~
  // Step 1: Collect FS constraints into two unioned sets from incident cells.
  //         FS constraints containing the vertex to be moved go to their own set.
  //         (Since they will refer to a new point location)
  // Step 2: Delete the vertex (this retriangulates).
  // Step 3: As in point insertion, find the set of Delaunay-conflicting cells
  //         for the moved point, and add their FS constraints to the unioned sets.
  //         (Again same division between sets)
  // Step 4: Insert the point into the triangulation (this retriangulates).
  // Step 5: Repeat steps 1-4 for each vertex to be moved.
  // Step 6: Iterate over all cells in the DT and remove any FS constraints containing
  //         the deleted vertices.  Meanwhile determine the set of new cells.
  // Step 7: Process the FS constraints in the unioned sets.  Normal constraints only mark new cells.
  //         Special constraints containing the moved
  //         points mark all crossed cells (as they were explicitly deleted in step 6).  Mark new cells as old.
#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif
  Delaunay3::Vertex_handle hndlQ;
  set<pair<int, int>, Delaunay3CellInfo::LtConstraint> setUnionedStationaryConstraints;
  set<pair<int, int>, Delaunay3CellInfo::LtConstraint> setUnionedMovedConstraints;
  set<pair<int, int>, Delaunay3CellInfo::LtConstraint> setUnionedConstraints;
  set<Delaunay3::Cell_handle> setNewCells;

  int vertexIndex = m_mapPoint_VertexHandle[pointIndex];
  /* std::cout<<vertexIndex<<std::endl;
   std::cout<<pointIndex<<std::endl;*/
  hndlQ = vecVertexHandles[vertexIndex];
  PointD3 initialPosition = hndlQ->point();
  PointD3 pd3NewPoint(getPoint(pointIndex)(0), getPoint(pointIndex)(1), getPoint(pointIndex)(2));

  /*check if the move will potentially destroys the manifoldness*/

  PointD3 camPosition = PointD3(getCamCenter(curCam_)(0), getCamCenter(curCam_)(1), getCamCenter(curCam_)(2));

  double distanceInitialPosition;
  double distanceEndingPosition;
  double distx = initialPosition.x() - camPosition.x();
  double disty = initialPosition.y() - camPosition.y();
  double distz = initialPosition.z() - camPosition.z();
  double distxEnding = pd3NewPoint.x() - camPosition.x();
  double distyEnding = pd3NewPoint.y() - camPosition.y();
  double distzEnding = pd3NewPoint.z() - camPosition.z();

  //std::cout<<initialPosition<<std::endl;
  //std::cout<<pd3NewPoint<<std::endl;
  distanceInitialPosition = sqrt(distx * distx + disty * disty + distz * distz);
  distanceEndingPosition = sqrt(distxEnding * distxEnding + distyEnding * distyEnding + distzEnding * distzEnding);

  if (distanceInitialPosition < r_ && distanceEndingPosition < r_) {

    // TODO: Questo era:
    // DEBUG: PTAM has minor data corruption bugs, and this hack handles the bad data being passed to our code.  Should fix PTAM instead.
    // forse conviene toglierlo!
    if (!dt.is_vertex(hndlQ))
      return false;

    // Step 1:
    set<Delaunay3::Cell_handle> setIncidentCells;
    dt.incident_cells(hndlQ, std::inserter(setIncidentCells, setIncidentCells.begin()));

    for (set<Delaunay3::Cell_handle>::iterator itCell = setIncidentCells.begin(); itCell != setIncidentCells.end(); itCell++) {

      setUnionedConstraints.insert((*itCell)->info().getIntersections().begin(), (*itCell)->info().getIntersections().end());
      /*for (set<Delaunay3CellInfo::FSConstraint, Delaunay3CellInfo::LtFSConstraint>::const_iterator itConstraint = (*itCell)->info().getIntersections().begin();
       itConstraint != (*itCell)->info().getIntersections().end(); itConstraint++) {
       if (itConstraint->second == vertexIndex)
       setUnionedMovedConstraints.insert(*itConstraint);
       else
       setUnionedStationaryConstraints.insert(*itConstraint);
       }*/
    }

    // Step 2:
    dt.remove(hndlQ);

    // Step 3:
    // Locate the point
    Delaunay3::Locate_type lt;
    int li, lj;
    Delaunay3::Cell_handle c = dt.locate(pd3NewPoint, lt, li, lj);
    if (lt == Delaunay3::VERTEX) {
      // TODO: handle better than just returning here!
      cerr << "Error in FreespaceDelaunayAlgorithm::moveVertex(): Attempted to move a vertex to an already existing vertex location" << endl;
      return false;
    }

    // Get the cells that conflict in a vector vecConflictCells, and a facet on the boundary of this hole in f.
    vector<Delaunay3::Cell_handle> vecConflictCells;
    Delaunay3::Facet f;
    dt.find_conflicts(pd3NewPoint, c, CGAL::Oneset_iterator<Delaunay3::Facet>(f), std::back_inserter(vecConflictCells));

    // Get the partitioned unioned constraint sets of all the cells in vecConflictCells.
    for (vector<Delaunay3::Cell_handle>::const_iterator it = vecConflictCells.begin(); it != vecConflictCells.end(); it++) {
      setUnionedConstraints.insert((*it)->info().getIntersections().begin(), (*it)->info().getIntersections().end());

      /*
       for (set<Delaunay3CellInfo::FSConstraint, Delaunay3CellInfo::LtFSConstraint>::const_iterator itConstraint = (*it)->info().getIntersections().begin();
       itConstraint != (*it)->info().getIntersections().end(); itConstraint++) {
       if (itConstraint->second == vertexIndex)
       setUnionedMovedConstraints.insert(*itConstraint);
       else
       setUnionedStationaryConstraints.insert(*itConstraint);
       }*/
    }

    // Step 4
    hndlQ = dt.insert_in_hole(pd3NewPoint, vecConflictCells.begin(), vecConflictCells.end(), f.first, f.second);
    vecVertexHandles[vertexIndex] = hndlQ;

    // Step 6
    for (Delaunay3::Finite_cells_iterator itCell = dt.finite_cells_begin(); itCell != dt.finite_cells_end(); itCell++) {
      if (itCell->info().isNew())
        setNewCells.insert(itCell);
      // Linear search:
      for (auto itDelete = itCell->info().getIntersections().begin(); itDelete != itCell->info().getIntersections().end();) {
        if (itDelete->second == vertexIndex) {
          // invalidates iterator, so careful about incrementing it:
          set<Delaunay3CellInfo::FSConstraint, Delaunay3CellInfo::LtFSConstraint>::const_iterator itNext = itDelete;
          itNext++;
          itCell->info().removeIntersection(itDelete->first, itDelete->second, itDelete->vote, vecVertexHandles, getCamCenters());
          itCell->info().decrementVoteCount(num_);
          itCell->info().decrementVoteCountProb(itDelete->vote);
          itDelete = itNext;
        } else
          itDelete++;
      }
    }

    // Step 7
    for (set<pair<int, int>, Delaunay3CellInfo::LtConstraint>::iterator itConstraint = setUnionedConstraints.begin();
        itConstraint != setUnionedConstraints.end(); itConstraint++) {
      Segment QO = Segment(vecVertexHandles[itConstraint->second]->point(),
          PointD3(getCamCenter(itConstraint->first)(0), getCamCenter(itConstraint->first)(1), getCamCenter(itConstraint->first)(2)));

      if (itConstraint->second == vertexIndex) {
        rayTracing(vecVertexHandles, vecVertexHandles[itConstraint->second], QO, itConstraint->first, itConstraint->second, false);
      } else {
        rayTracing(vecVertexHandles, vecVertexHandles[itConstraint->second], QO, itConstraint->first, itConstraint->second, true);
      }

    }
    /*
     for (set<pair<int, int>, Delaunay3CellInfo::LtConstraint>::iterator itConstraint = setUnionedStationaryConstraints.begin();
     itConstraint != setUnionedStationaryConstraints.end(); itConstraint++) {
     Segment QO = Segment(vecVertexHandles[itConstraint->second]->point(),
     PointD3(getCamCenter(itConstraint->first)(0), getCamCenter(itConstraint->first)(1), getCamCenter(itConstraint->first)(2)));

     rayTracing(vecVertexHandles, vecVertexHandles[itConstraint->second], QO, itConstraint->first, itConstraint->second, true);
     }
     for (set<pair<int, int>, Delaunay3CellInfo::LtConstraint>::iterator itConstraint = setUnionedMovedConstraints.begin();
     itConstraint != setUnionedMovedConstraints.end(); itConstraint++) {
     Segment QO = Segment(vecVertexHandles[itConstraint->second]->point(),
     PointD3(getCamCenter(itConstraint->first)(0), getCamCenter(itConstraint->first)(1), getCamCenter(itConstraint->first)(2)));

     rayTracing(vecVertexHandles, vecVertexHandles[itConstraint->second], QO, itConstraint->first, itConstraint->second, false);
     }*/
    for (set<Delaunay3::Cell_handle>::iterator itCell = setNewCells.begin(); itCell != setNewCells.end(); itCell++)
      (*itCell)->info().markOld();

#ifdef LOGGING
    gettimeofday(&endCarv, NULL);
    double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
    fileStats_ << "PointsMoving " << delta << std::endl;
#endif
    return true;
  } else {
#ifdef LOGGING
    gettimeofday(&endCarv, NULL);
    double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
    fileStats_ << "PointsMoving " << delta << std::endl;
#endif
    return false;
  }
}

bool FreespaceDelaunayManifold::moveVertex_suboptimal(vector<Delaunay3::Vertex_handle> & vecVertexHandles, const int pointIndex) {
#ifdef LOGGING
  struct timeval startCarv, endCarv;
  gettimeofday(&startCarv, NULL);
#endif

  Delaunay3::Vertex_handle hndlQ;
  set<pair<int, int>, Delaunay3CellInfo::LtConstraint> setUnionedConstraints;
  set<Delaunay3::Cell_handle> setNewCells;

  int vertexIndex = m_mapPoint_VertexHandle[pointIndex];
  /* std::cout<<vertexIndex<<std::endl;
   std::cout<<pointIndex<<std::endl;*/
  hndlQ = vecVertexHandles[vertexIndex];
  PointD3 initialPosition = hndlQ->point();
  PointD3 pd3NewPoint(getPoint(pointIndex)(0), getPoint(pointIndex)(1), getPoint(pointIndex)(2));

  /*check if the move will potentially destroys the manifoldness*/

  PointD3 camPosition = PointD3(getCamCenter(curCam_)(0), getCamCenter(curCam_)(1), getCamCenter(curCam_)(2));
  //std::cout<<initialPosition<<std::endl;
  //std::cout<<pd3NewPoint<<std::endl;

  double distx = initialPosition.x() - camPosition.x();
  double disty = initialPosition.y() - camPosition.y();
  double distz = initialPosition.z() - camPosition.z();
  double distxEnding = pd3NewPoint.x() - camPosition.x();
  double distyEnding = pd3NewPoint.y() - camPosition.y();
  double distzEnding = pd3NewPoint.z() - camPosition.z();

  //std::cout<<initialPosition<<std::endl;
  //std::cout<<pd3NewPoint<<std::endl;
  double distanceInitialPosition = sqrt(distx * distx + disty * disty + distz * distz);
  double distanceEndingPosition = sqrt(distxEnding * distxEnding + distyEnding * distyEnding + distzEnding * distzEnding);

  if (distanceInitialPosition < r_ && distanceEndingPosition < r_) {

    /********************** Step 1: find the cells incident to the vertex to be removed*****************/
    vector<Delaunay3::Cell_handle> setIncidentCells;
    dt.incident_cells(hndlQ, std::back_inserter(setIncidentCells));

    //store their weights
    vector<pair<PointD3, double> > vecDistanceWeight, vecDistanceWeight2;
    for (auto it : setIncidentCells) {
      PointD3 temp = CGAL::barycenter(it->vertex(0)->point(), 1.0, it->vertex(1)->point(), 1.0, it->vertex(2)->point(), 1.0, it->vertex(3)->point(), 1.0);
      vecDistanceWeight.push_back(pair<PointD3, double>(temp, (double) it->info().getVoteCountProb()));
    }

    /**********************Step 2: raytracing to update the weights before the  point removal****************/
    for (int curCam : m_visibilityListTransp[pointIndex]) {
      Segment QO = Segment(hndlQ->point(), PointD3(getCamCenter(curCam)(0), getCamCenter(curCam)(1), getCamCenter(curCam)(2)));
      rayTracing_suboptimal(vecVertexHandles, hndlQ, QO, curCam, pointIndex, false, false);
    }

    /************Step 3: remove the point and update the new cells weights according to the information on the
     * weights collected in the previous step****************/
    // stored previously
    vector<Delaunay3::Cell_handle> newCells;
    dt.remove_and_give_new_cells(hndlQ, std::back_inserter(newCells));

    updateDistanceAndWeights(newCells, vecDistanceWeight);

    /***********Step 4: Locate the point and remove conflicting tetrahedra****************/
    Delaunay3::Locate_type lt;
    int li, lj;
    Delaunay3::Cell_handle c = dt.locate(pd3NewPoint, lt, li, lj);
    if (lt == Delaunay3::VERTEX) {
      cerr << "Error in FreespaceDelaunayManifold::moveVertex(): Attempted to move a vertex to an already existing vertex location" << endl;
      return false;
    }

    // Get the cells that conflict in a vector vecConflictCells, and a facet on the boundary of this hole in f.
    vector<Delaunay3::Cell_handle> vecConflictCells;
    Delaunay3::Facet f;
    dt.find_conflicts(pd3NewPoint, c, CGAL::Oneset_iterator<Delaunay3::Facet>(f), std::back_inserter(vecConflictCells));

    for (auto it : vecConflictCells) {
      PointD3 temp = CGAL::barycenter(it->vertex(0)->point(), 1.0, it->vertex(1)->point(), 1.0, it->vertex(2)->point(), 1.0, it->vertex(3)->point(), 1.0);
      vecDistanceWeight2.push_back(pair<PointD3, double>(temp, (double) it->info().getVoteCountProb()));
    }

    /**********Step 5: Add the new (moved) tets****************/
    hndlQ = dt.insert_in_hole(pd3NewPoint, vecConflictCells.begin(), vecConflictCells.end(), f.first, f.second);
    vecVertexHandles[vertexIndex] = hndlQ;

    /**********Step 6 update the weights of the new tetrahedra****************/
    updateDistanceAndWeights(newCells, vecDistanceWeight2);

    /**********Step 7 raytracing to update the weights after point insertion****************/
    for (int curCam : m_visibilityListTransp[pointIndex]) {
      Segment QO = Segment(hndlQ->point(), PointD3(getCamCenter(curCam)(0), getCamCenter(curCam)(1), getCamCenter(curCam)(2)));
      rayTracing_suboptimal(vecVertexHandles, hndlQ, QO, curCam, pointIndex, false, true);
    }

#ifdef LOGGING
    gettimeofday(&endCarv, NULL);
    double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
    fileStats_ << "PointsMoving " << delta << std::endl;
#endif
    return true;
  } else {
#ifdef LOGGING
    gettimeofday(&endCarv, NULL);
    double delta = ((endCarv.tv_sec - startCarv.tv_sec) * 1000000u + endCarv.tv_usec - startCarv.tv_usec) / 1.e6;
    fileStats_ << "PointsMoving " << delta << std::endl;
#endif
    return false;
  }
}

}

