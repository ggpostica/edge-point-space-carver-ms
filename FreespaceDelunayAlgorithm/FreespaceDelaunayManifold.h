//  Copyright 2014 Andrea Romanoni
//
//  This file is part of edgePointSpaceCarver.
//
//  edgePointSpaceCarver is free software: you can redistribute it
//  and/or modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version see
//  <http://www.gnu.org/licenses/>..
//
//  edgePointSpaceCarver is distributed in the hope that it will be
//  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

#ifndef FREESPACEDELAUNAYMANIFOLD_H_
#define FREESPACEDELAUNAYMANIFOLD_H_

//#ifndef OPEN_MP_ENABLED
//#define OPEN_MP_ENABLED
//#endif

#ifndef LOGGING
#define LOGGING
#endif
//#define NO_HEURISTIC_K
//#define HEURISTIC_K 5
#define HEURISTIC_K 1

#include <vector>
#include <fstream>
#include <list>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <utility>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <iostream>
#include <limits>
#include <ext/hash_map>//TODO rempove this header and use unordered_map instead
#include "Matrix.h"
// #include <maxflow/graph.h>
#include "GraphWrapper_Boost.h"

#include "../src/types.hpp"

// CGAL-related includes
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_hierarchy_3.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Projection_traits_xy_3.h>
#include <CGAL/intersections.h>

#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/convex_hull_3.h>

using namespace std;
using namespace __gnu_cxx;

//msc=manifoldSpaceCarving
namespace msc {

class FreespaceDelaunayManifold;
class FreespaceDelaunayManifold {

  public:

    // CGAL-related typedefs
    typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
    typedef K::Point_3 Point;
    typedef K::Triangle_3 Triangle;
    typedef K::Segment_3 Segment;
    typedef CGAL::Polyhedron_3<K> Polyhedron;

    // Our inner class for holding custom info for each cell in a 3D Delaunay tetrahedrization
    class Delaunay3CellInfo {
      public:
        // Sorted-Set related struct
        struct FSConstraint;
        struct LtConstraint {
            bool operator()(const pair<int, int> x, const pair<int, int> y) const {
              if (x.first < y.first)
                return true;
              else
                return (x.first == y.first) && (x.second < y.second);
            }
        };
        struct LtFSConstraint {
            bool operator()(const FSConstraint x, const FSConstraint y) const {
              if (x.first < y.first)
                return true;
              else
                return (x.first == y.first) && (x.second < y.second);
            }
        };
        struct FSConstraint {
            FSConstraint() {
              first = -1;
              second = -1;
              vote = -1;
              fNearestNeighborDist = numeric_limits<float>::infinity();
            }
            FSConstraint(int camIndex, int featureIndex) {
              first = camIndex;
              second = featureIndex;
              vote = 1.0;
              fNearestNeighborDist = numeric_limits<float>::infinity();
            }
            FSConstraint(int camIndex, int featureIndex, float voteVal) {
              first = camIndex;
              second = featureIndex;
              vote = voteVal;
              fNearestNeighborDist = numeric_limits<float>::infinity();
            }
            FSConstraint(const FSConstraint & ref) {
              first = ref.first;
              second = ref.second;
              vote = ref.vote;
              pNearestNeighbor = ref.pNearestNeighbor;
              fNearestNeighborDist = ref.fNearestNeighborDist;
            }
            FSConstraint & operator=(const FSConstraint & ref) {
              if (&ref != this) {
                first = ref.first;
                second = ref.second;
                vote = ref.vote;
                pNearestNeighbor = ref.pNearestNeighbor;
                fNearestNeighborDist = ref.fNearestNeighborDist;
              }
              return *this;
            }
            operator std::pair<int, int>() const {
              return std::make_pair(first, second);
            }

            int first;
            int second;
            float vote;
            mutable set<FSConstraint, LtFSConstraint>::iterator pNearestNeighbor;
            mutable float fNearestNeighborDist;

            // Trick c++ "const"-ness so that we can change the nearest neighbor information in the set from an iterator:
            void setNearestNeighbor(const set<FSConstraint, LtFSConstraint>::iterator itNearest, const float dist) const {
              pNearestNeighbor = itNearest;
              fNearestNeighborDist = dist;
            }
            void resetNearestNeighborDist() const {
              fNearestNeighborDist = numeric_limits<float>::infinity();
            }
        };

        // Constructors (it must be default-constructable)
        Delaunay3CellInfo() {
          m_voteCount = 0;
          m_voteCountProb = 0;
          m_bNew = true;
          boundary = false;
          keepManifold = false;
          toBeTested_ = false;
          shrinked_ = true;
          Galpha_ = false;
          temporary_Inside_ = false;
        }
        Delaunay3CellInfo(const Delaunay3CellInfo & ref) {
          setVoteCount(ref.getVoteCount());
          setBoundary(ref.isBoundary());
          setIntersections(ref.getIntersections());
          if (!ref.isNew())
            markOld();
        }

        // Getters
        int getVoteCount() const {
          return m_voteCount;
        }
        float getVoteCountProb() const {
          return m_voteCountProb;
        }

        bool isBoundary() const {
          return boundary;
        }
        bool iskeptManifold() const {
          return keepManifold;
        }

        bool isTemporaryInside() const {
          return temporary_Inside_;
        }
        bool isToBeTested() const {
          return toBeTested_;
        }
        bool isShrinked() const {
          return shrinked_;
        }
        bool isGalpha() const {
          return Galpha_;
        }
        const set<FSConstraint, LtFSConstraint> & getIntersections() const {
          return m_setIntersections;
        }
        bool isNew() const {
          return m_bNew;
        }

        // Setters
        void setVoteCount(const int voteCount) {
          m_voteCount = voteCount;
        }

        void setBoundary(bool value) {
          boundary = value;
        }
        void setKeptManifold(bool value) {
          keepManifold = value;
        }
        void setShrinked(bool value) {
          shrinked_ = value;
        }
        void setGalpha(bool value) {
          Galpha_ = value;
        }
        void setTemporaryInside(bool value) {
          temporary_Inside_ = value;
        }
        void setToBeTested(bool value) {
          toBeTested_ = value;
        }
        void setVoteCountProb(const float voteCountProb) {
          m_voteCountProb = voteCountProb;
        }
        void setIntersections(const set<FSConstraint, LtFSConstraint> & ref) {
          m_setIntersections = ref;
        }

        void setIdxManifold(bool value, int i) {
          idxFacetNotManifold[i] = value;
        }

        bool getIdxManifold(int i) {
          return idxFacetNotManifold[i];
        }

        // Public Methods
        void incrementVoteCount() {
          m_voteCount++;
        }

        void incrementVoteCount(int num) {
          m_voteCount+= num;
        }

        void incrementVoteCountProb(float incr) {
          m_voteCountProb += incr;
        }
        void decrementVoteCount() {
          if (m_voteCount > 0)
            m_voteCount--;
        }
        void decrementVoteCount(int num) {
          if (m_voteCount -num> 0)
            m_voteCount-= num;
          else
            m_voteCount = 0;
        }
        void decrementVoteCountProb(float incr) {
          if (m_voteCountProb > incr)
            m_voteCountProb -= incr;
        }
        bool isKeptByVoteCount(const int nVoteThresh = 1) const {
          if (getVoteCount() < nVoteThresh)
            return true;
          return false;
        }
        bool isKeptByVoteCountProb(const float nVoteThresh = 1.0) const {
          if (getVoteCountProb() < nVoteThresh)
            return true;
          return false;
        }

        /* template<class T>
         void addIntersection(int camIndex, int featureIndex, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
         m_setIntersections.insert(m_setIntersections.end(), FSConstraint(camIndex, featureIndex));
         }
         template<class T>
         void addIntersection(int camIndex, int featureIndex, float vote, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
         m_setIntersections.insert(m_setIntersections.end(), FSConstraint(camIndex, featureIndex,vote));
         }*/
#ifdef NO_HEURISTIC_K
        template<class T>
        void addIntersection(int camIndex, int featureIndex, float vote, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
            m_setIntersections.insert(m_setIntersections.end(), FSConstraint(camIndex, featureIndex, vote));
        }

        template<class T>
        void addIntersection(int camIndex, int featureIndex, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
            m_setIntersections.insert(m_setIntersections.end(), FSConstraint(camIndex, featureIndex));
        }
#else
        template <class T>
        void addIntersection(int camIndex, int featureIndex, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
            FSConstraint incoming(camIndex, featureIndex);
            if ((int)m_setIntersections.size() < HEURISTIC_K) {
              // The constraint set is not full, so insert the incoming free-space constraint and update nearest neighbor info.
              set<FSConstraint, LtFSConstraint>::iterator it, itIncoming;
              itIncoming = m_setIntersections.insert(m_setIntersections.end(), incoming);
              for (it = m_setIntersections.begin(); it != m_setIntersections.end(); it++) {
                if (it == itIncoming) continue;
                // Asymmetric metric:
                float curDist = distFSConstraint(*itIncoming, *it, vecVertexHandles, vecCamCenters);
                float curDist2 = distFSConstraint(*it, *itIncoming, vecVertexHandles, vecCamCenters);
                // Update incoming
                if (curDist < itIncoming->fNearestNeighborDist)
                  itIncoming->setNearestNeighbor(it, curDist);
                // Update *it:
                if (curDist2 < it->fNearestNeighborDist)
                  it->setNearestNeighbor(itIncoming, curDist2);
              }
            }
            else {
              // The constraint set is full, so apply the spatial cover heuristic to determine whether or not to insert the incoming free-space constraint

              // Quick return / rejection on the case that m_nMaxConstraintsKept == 1.
              if (HEURISTIC_K == 1) return;

              float minDist = numeric_limits<float>::infinity();
              set<FSConstraint, LtFSConstraint>::iterator it, it2, itEject;
              for (it = m_setIntersections.begin(); it != m_setIntersections.end(); it++) {
                float curDist = distFSConstraint(incoming, *it, vecVertexHandles, vecCamCenters);
                if (curDist < it->fNearestNeighborDist)
                  break; // REJECT
                float curDist2 = distFSConstraint(*it, incoming, vecVertexHandles, vecCamCenters);
                if (curDist2 < it->fNearestNeighborDist)
                  break;// REJECT
                // Update incoming
                if (curDist < incoming.fNearestNeighborDist)
                  incoming.setNearestNeighbor(it, curDist);
                // Update minDist & itEject
                if (it->fNearestNeighborDist < minDist) {
                  minDist = it->fNearestNeighborDist;
                  itEject = it;
                }
              }

              if (it == m_setIntersections.end()) {
                // No rejection, so insert incoming and evict itEject.

                // For an asymmetric metric, incoming might have its nearest neighbor ejected.  If so, compute the 2nd nearest neighbor
                if (incoming.pNearestNeighbor == itEject) {
                  incoming.fNearestNeighborDist = numeric_limits<float>::infinity();
                  for (it2 = m_setIntersections.begin(); it2 != m_setIntersections.end(); it2++) {
                    if (it2 == itEject) continue;
                    float curDist = distFSConstraint(incoming, *it2, vecVertexHandles, vecCamCenters);
                    if (curDist < incoming.fNearestNeighborDist)
                      incoming.setNearestNeighbor(it2, curDist);
                  }
                }

                // Recompute nearest neighbors that previously pointed to itEject.
                for (it2 = m_setIntersections.begin(); it2 != m_setIntersections.end(); it2++) {
                  if (it2->pNearestNeighbor == itEject) { // implicity "continue;"'s if it2 == itEject
                    // Recompute the nearest neighbor for it2:
                    it2->resetNearestNeighborDist();
                    for (it = m_setIntersections.begin(); it != m_setIntersections.end(); it++) {
                      if (it == itEject || it == it2) continue;
                      float curDist = distFSConstraint(*it2, *it, vecVertexHandles, vecCamCenters);
                      if (curDist < it2->fNearestNeighborDist)
                        it2->setNearestNeighbor(it, curDist);
                    }
                  }
                }

                // Finally erase itEject and insert incoming
                m_setIntersections.erase(itEject);
                m_setIntersections.insert(m_setIntersections.end(), incoming);
              }
            }
        }

        template <class T>
        void addIntersection(int camIndex, int featureIndex, float vote, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
            FSConstraint incoming(camIndex, featureIndex, vote);
            if ((int)m_setIntersections.size() < HEURISTIC_K) {
              // The constraint set is not full, so insert the incoming free-space constraint and update nearest neighbor info.
              set<FSConstraint, LtFSConstraint>::iterator it, itIncoming;
              itIncoming = m_setIntersections.insert(m_setIntersections.end(), incoming);
              for (it = m_setIntersections.begin(); it != m_setIntersections.end(); it++) {
                if (it == itIncoming) continue;
                // Asymmetric metric:
                float curDist = distFSConstraint(*itIncoming, *it, vecVertexHandles, vecCamCenters);
                float curDist2 = distFSConstraint(*it, *itIncoming, vecVertexHandles, vecCamCenters);
                // Update incoming
                if (curDist < itIncoming->fNearestNeighborDist)
                  itIncoming->setNearestNeighbor(it, curDist);
                // Update *it:
                if (curDist2 < it->fNearestNeighborDist)
                  it->setNearestNeighbor(itIncoming, curDist2);
              }
            }
            else {
              // The constraint set is full, so apply the spatial cover heuristic to determine whether or not to insert the incoming free-space constraint

              // Quick return / rejection on the case that m_nMaxConstraintsKept == 1.
              if (HEURISTIC_K == 1) return;

              float minDist = numeric_limits<float>::infinity();
              set<FSConstraint, LtFSConstraint>::iterator it, it2, itEject;
              for (it = m_setIntersections.begin(); it != m_setIntersections.end(); it++) {
                float curDist = distFSConstraint(incoming, *it, vecVertexHandles, vecCamCenters);
                if (curDist < it->fNearestNeighborDist)
                  break; // REJECT
                float curDist2 = distFSConstraint(*it, incoming, vecVertexHandles, vecCamCenters);
                if (curDist2 < it->fNearestNeighborDist)
                  break;// REJECT
                // Update incoming
                if (curDist < incoming.fNearestNeighborDist)
                  incoming.setNearestNeighbor(it, curDist);
                // Update minDist & itEject
                if (it->fNearestNeighborDist < minDist) {
                  minDist = it->fNearestNeighborDist;
                  itEject = it;
                }
              }

              if (it == m_setIntersections.end()) {
                // No rejection, so insert incoming and evict itEject.

                // For an asymmetric metric, incoming might have its nearest neighbor ejected.  If so, compute the 2nd nearest neighbor
                if (incoming.pNearestNeighbor == itEject) {
                  incoming.fNearestNeighborDist = numeric_limits<float>::infinity();
                  for (it2 = m_setIntersections.begin(); it2 != m_setIntersections.end(); it2++) {
                    if (it2 == itEject) continue;
                    float curDist = distFSConstraint(incoming, *it2, vecVertexHandles, vecCamCenters);
                    if (curDist < incoming.fNearestNeighborDist)
                      incoming.setNearestNeighbor(it2, curDist);
                  }
                }

                // Recompute nearest neighbors that previously pointed to itEject.
                for (it2 = m_setIntersections.begin(); it2 != m_setIntersections.end(); it2++) {
                  if (it2->pNearestNeighbor == itEject) { // implicity "continue;"'s if it2 == itEject
                    // Recompute the nearest neighbor for it2:
                    it2->resetNearestNeighborDist();
                    for (it = m_setIntersections.begin(); it != m_setIntersections.end(); it++) {
                      if (it == itEject || it == it2) continue;
                      float curDist = distFSConstraint(*it2, *it, vecVertexHandles, vecCamCenters);
                      if (curDist < it2->fNearestNeighborDist)
                        it2->setNearestNeighbor(it, curDist);
                    }
                  }
                }

                // Finally erase itEject and insert incoming
                m_setIntersections.erase(itEject);
                m_setIntersections.insert(m_setIntersections.end(), incoming);
              }
            }
        }
#endif
#ifdef NO_HEURISTIC_K
        template<class T>
        void removeIntersection(int camIndex, int featureIndex, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
            m_setIntersections.erase(FSConstraint(camIndex, featureIndex));
        }

        template<class T>
        void removeIntersection(int camIndex, int featureIndex, float vote, const vector<T> & vecVertexHandles,
            const vector<Matrix> & vecCamCenters) {
            m_setIntersections.erase(FSConstraint(camIndex, featureIndex, vote));
        }
#else
        template <class T>
        void removeIntersection(int camIndex, int featureIndex, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
            if ((int)m_setIntersections.size() <= 1) {
              // No nearest neighbor info needs to be updated
              m_setIntersections.erase(FSConstraint(camIndex, featureIndex));
            }
            else {
              // The nearest neighbor info needs to be updated
              set<FSConstraint, LtFSConstraint>::iterator it, it2, itEject;

              itEject = m_setIntersections.find(FSConstraint(camIndex, featureIndex));
              if (itEject == m_setIntersections.end())
                return;// wasn't in the set to begin with

              for (it = m_setIntersections.begin(); it != m_setIntersections.end(); it++) {
                if (it == itEject) continue;
                if (it->pNearestNeighbor == itEject) {
                  // Then recompute the nearest neighbor for it:
                  it->resetNearestNeighborDist();
                  for (it2 = m_setIntersections.begin(); it2 != m_setIntersections.end(); it2++) {
                    if (it2 == itEject || it2 == it) continue;
                    float curDist = distFSConstraint(*it, *it2, vecVertexHandles, vecCamCenters);
                    if (curDist < it->fNearestNeighborDist)
                      it->setNearestNeighbor(it2, curDist);
                  }
                }
              }

              // Finally, erase it.
              m_setIntersections.erase(itEject);
            }
        }
        template <class T>
        void removeIntersection(int camIndex, int featureIndex, float vote, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
            if ((int)m_setIntersections.size() <= 1) {
              // No nearest neighbor info needs to be updated
              m_setIntersections.erase(FSConstraint(camIndex, featureIndex,vote));
            }
            else {
              // The nearest neighbor info needs to be updated
              set<FSConstraint, LtFSConstraint>::iterator it, it2, itEject;

              itEject = m_setIntersections.find(FSConstraint(camIndex, featureIndex,vote));
              if (itEject == m_setIntersections.end())
                return;// wasn't in the set to begin with

              for (it = m_setIntersections.begin(); it != m_setIntersections.end(); it++) {
                if (it == itEject) continue;
                if (it->pNearestNeighbor == itEject) {
                  // Then recompute the nearest neighbor for it:
                  it->resetNearestNeighborDist();
                  for (it2 = m_setIntersections.begin(); it2 != m_setIntersections.end(); it2++) {
                    if (it2 == itEject || it2 == it) continue;
                    float curDist = distFSConstraint(*it, *it2, vecVertexHandles, vecCamCenters);
                    if (curDist < it->fNearestNeighborDist)
                      it->setNearestNeighbor(it2, curDist);
                  }
                }
              }

              // Finally, erase it.
              m_setIntersections.erase(itEject);
            }
        }
#endif

        int getNumIntersection() {
          return m_setIntersections.size();
        }

        /*  template<class T>
        void removeIntersection(int camIndex, int featureIndex, const vector<T> & vecVertexHandles, const vector<Matrix> & vecCamCenters) {
          m_setIntersections.erase(FSConstraint(camIndex, featureIndex));
        }

        template<class T>
        void removeIntersection(int camIndex, int featureIndex, float vote, const vector<T> & vecVertexHandles,
            const vector<Matrix> & vecCamCenters) {
          m_setIntersections.erase(FSConstraint(camIndex, featureIndex, vote));
        }*/

        template<class T>
        float distFSConstraint(const FSConstraint & x, const FSConstraint & y, const vector<T> & vecVertexHandles,
            const vector<Matrix> & vecCamCenters) {
            return distFSConstraintTriangleAreaAaron(x, y, vecVertexHandles, vecCamCenters);
        }
        void clearIntersections() {
          m_setIntersections.clear();
        }
        void markOld() {
          m_bNew = false;
        }
        void markNew() {
          m_bNew = true;
        }

        // Operators (It must be assignable)
        Delaunay3CellInfo & operator=(const Delaunay3CellInfo & rhs) {
          if (this != &rhs) {
            setVoteCount(rhs.getVoteCount());
            setIntersections(rhs.getIntersections());
            if (!rhs.isNew())
              markOld();
          }
          return *this;
        }

      private:
        // Private Methods
        template<class T>
        float distFSConstraintTriangleAreaAaron(const FSConstraint & x, const FSConstraint & y, const vector<T> & vecVertexHandles,
            const vector<Matrix> & vecCamCenters) {
          // Asymmetric distance heuristic.
          // Sum of two triangle areas, use the base segment PQ as constraint x, and the two points from y as R1 and R2.
          // Note: For efficiency, to avoid unnecessary division by 2 and square-roots, use the sum of twice-the-areas squared = squared area of parallelograms.
          const Matrix & P = vecCamCenters[x.first];
          Matrix Q(3, 1);
          Q(0) = vecVertexHandles[x.second]->point().x();
          Q(1) = vecVertexHandles[x.second]->point().y();
          Q(2) = vecVertexHandles[x.second]->point().z();
          const Matrix & R1 = vecCamCenters[y.first];
          Matrix R2(3, 1);
          R2(0) = vecVertexHandles[y.second]->point().x();
          R2(1) = vecVertexHandles[y.second]->point().y();
          R2(2) = vecVertexHandles[y.second]->point().z();

          // Vector distances
          Matrix PQ(Q - P);
          Matrix PR1(R1 - P);
          Matrix PR2(R2 - P);

          // Sum of squared areas of parallelograms
          Matrix PQxPR1(PQ.cross(PR1));
          Matrix PQxPR2(PQ.cross(PR2));
          return PQxPR1.dot(PQxPR1) + PQxPR2.dot(PQxPR2);
        }

        // Private Members
        int m_voteCount;
        bool boundary;
        bool keepManifold;
        bool toBeTested_;
        float m_voteCountProb;
        set<FSConstraint, LtFSConstraint> m_setIntersections;

        bool m_bNew;
        bool idxFacetNotManifold[4];
        bool shrinked_;
        bool Galpha_;
        bool temporary_Inside_;
    };

    // Our inner class for holding custom info for each vertex in a 3D Delaunay tetrahedrization
    class Delaunay3VertexInfo {
      public:

        // Constructors (it must be default-constructable)
        Delaunay3VertexInfo() {
          used_ = 0;
          notUsed_ = true;
        }

        // Getters
        int isUsed() const {
          return used_;
        }
        bool isNotUsed() const {
          return notUsed_;
        }

        // Setters
        void setUsed(int value) {
          used_ = value;
        }

        void setNotUsed(bool value) {
          notUsed_ = value;
        }

        void incrUsed() {
          used_++;
        }
        void decrUsed() {
          used_--;
        }

      private:
        int used_;
        bool notUsed_;
    };

    // CGAL-related typedefs for Delaunay triangulation, 3-D
    //typedef CGAL::Triangulation_vertex_base_3<K> Vb;
    typedef CGAL::Triangulation_vertex_base_with_info_3<Delaunay3VertexInfo, K> Vb;

    typedef CGAL::Triangulation_hierarchy_vertex_base_3<Vb> Vbh;
    typedef CGAL::Triangulation_cell_base_with_info_3<Delaunay3CellInfo, K> Cb;
    typedef CGAL::Triangulation_data_structure_3<Vbh, Cb> Tds;
    typedef CGAL::Delaunay_triangulation_3<K, Tds> Dt;
    typedef CGAL::Triangulation_hierarchy_3<Dt> Delaunay3;
    typedef Delaunay3::Point PointD3;

    // Graph-cuts related typedefs
    typedef GraphWrapper_Boost Graph_t; // Boykov & Kolmogorov's Code

    // Hashing-related structs:
    struct HashVertHandle {
        size_t operator()(const Delaunay3::Vertex_handle x) const {
          return (size_t) (&(*x));
        } // use pointer to create hash
    };
    struct EqVertHandle {
        bool operator()(const Delaunay3::Vertex_handle x, const Delaunay3::Vertex_handle y) const {
          return x == y;
        }
    };

    // Constructors
    FreespaceDelaunayManifold();

    // Getters
    const vector<Matrix> & getPoints() const;
    Matrix getPoint(const int index) const;
    int numPoints() const;
    const vector<Matrix> & getCams() const;
    const int & getNumCam() const;
    const int & getCurCam() const;
    Matrix getCam(const int index) const;
    const vector<Matrix> & getCamCenters() const;
    Matrix getCamCenter(const int index) const;
    const vector<Matrix> & getPrincipleRays() const;
    Matrix getPrincipleRay(const int index) const;
    const vector<vector<int> > & getVisibilityList() const;
    const vector<vector<bool> > & getVisibilityNewPoint() const;
    const vector<int> & getVisibilityList(const int index) const;
    const vector<bool> & getVisibilityNewPoint(const int index) const;
    void setVisibilityNewPoint(const int index,const int point, bool val);
    int numCams() const;
    double getBoundsMin() const;
    double getBoundsMax() const;

    // Setters
    void setConfig(Configuration configuration);

    void setPoints(const vector<Matrix> & ref);
    void setCams(const vector<Matrix> & ref);
    void setCurCam(const int & ref);
    void setCamCenters(const vector<Matrix> & ref);
    void setPrincipleRays(const vector<Matrix> & ref);
    void setVisibilityList(const vector<vector<int> > & ref);

    void addPoint(const Matrix & ref);
    int addPointWhere(const Matrix & ref);
    void movePoint(const Matrix &ref, int idxPoint);
    Matrix movePointGetOld(const Matrix &ref, int idxPoint);

    void addCamCenter(const Matrix & ref);
    void addVisibilityPair(const int camIndex, const int pointIndex);
    void addVisibilityPair(const std::pair<int, int> visibilityPair);

    bool moveVertex(vector<Delaunay3::Vertex_handle> & vecVertexHandles, const int pointIndex);
    bool moveVertex_suboptimal(vector<Delaunay3::Vertex_handle> & vecVertexHandles, const int pointIndex);
    int moveVertex(vector<Delaunay3::Vertex_handle> & vecVertexHandles, const vector<int> & arrPointIndices);

    // Operators
    FreespaceDelaunayManifold & operator=(const FreespaceDelaunayManifold & rhs);

    // Public Methods
    bool isVisible(const int pointIndex, const int viewIndex) const;
    /*void generateVisibilityFromNormals(const vector<Matrix> & normals, const double nFrontFacingAngleThreshold = pi / 3.0,
     const double nFov = pi / 2.0);*/

    void IterateTetrahedronMethod(const int frameIndex);
    void IterateTetrahedronMethod_suboptimal(const int frameIndex);

    void regionGrowingIterative(int idxCam);
    void regionGrowingBatch();

    void shrinkBall(int idxCam);

    void regionGrowingBatch(int idxCam);
    bool additionTest(Delaunay3::Cell_handle i);
    bool subtractionTest(Delaunay3::Cell_handle i);

    bool isInBoundary(Delaunay3::Cell_handle cellToTest);
    bool isInBoundary(Delaunay3::Cell_handle cellToTest, std::vector<int> &neighNotManifold);
    bool insertInBoundary(Delaunay3::Cell_handle cellToTest);
    bool removeFromBoundary(Delaunay3::Cell_handle cellToTest);

    void addTetAndUpdateBoundary(Delaunay3::Cell_handle i);
    void subTetAndUpdateBoundary(Delaunay3::Cell_handle i);

    static bool sortTetByIntersection(Delaunay3::Cell_handle i, Delaunay3::Cell_handle j);

    bool regionGrowingSeveralTetAtOnce(Delaunay3::Vertex_handle vertexToGrow, std::vector<Delaunay3::Cell_handle> &cellsAdded);
    bool isRegular(Delaunay3::Vertex_handle vertexToCheck, bool classical);

    void tetsToTris(vector<Matrix> & points, vector<Matrix> & tris, const int nVoteThresh);
    void tetsToTrisIsBound(vector<Matrix> & points, vector<Matrix> & tris);
    void tetsToTrisSure(vector<Matrix> & points, vector<Matrix> & tris);
    void tetsToTris(vector<Matrix> & points, vector<Matrix> & tris, const float nVoteThresh);
    void tetsToTris(vector<Matrix> & points, vector<Matrix> & tris);

    int writeObj(const string filename, const vector<Matrix> & points, const vector<Matrix> & tris) const;
    void writeObj(ostream & outfile, const vector<Matrix> & points, const vector<Matrix> & tris) const;
    void writeBoundaryToObj(const string outfile);
    void writeBoundaryToObj2(const string outfile);
    void writeShrinkedToObj(const string outfile);

    void calculateBoundsValues(); // TODO: Refactor.  E.g. move back to private, declare friend classes that need access, e.g. SFMTranscriptInterface_Delaunay

    bool isShrinkEnabled() const {
      return shrinkEnabled_;
    }

    void setShrinkEnabled(bool shrinkEnabled) {
      shrinkEnabled_ = shrinkEnabled;
    }
    void resetReconstruction();
    int findVisuallyCriticalEdges(vector<pair<int, int> > &visualEdges, int idCam);
    bool isVisuallyCriticalEdge(Matrix p1, Matrix p2, Matrix center);
    bool isVisuallyCriticalEdge(PointD3 p1, PointD3 p2, Matrix center);
    int findCriticalArtifacts(int idCam);

  void setR(double r) {
    r_ = r;
  }

  void set1(float _1) {
    w_1 = _1;
  }

  void set2(float _2) {
    w_2 = _2;
  }

  void set3(float _3) {
    w_3 = _3;
  }

  void setNum(int num) {
    num_ = num;
  }

  void setInfiniteValue(float infiniteValue) {
    infiniteValue_ = infiniteValue;
  }

  private:
    // Private Methods
    void copy(const vector<Matrix> & points, const vector<Matrix> & cams, const vector<Matrix> & camCenters, const vector<Matrix> & principleRays,
        const vector<vector<int> > & visibilityList);
    void createBounds();
    void createSteinerPointGridAndBound();
    //void markTetrahedraCrossingConstraint(const Delaunay3::Vertex_handle hndlQ, const Segment & constraint);
    void rayTracing(const vector<Delaunay3::Vertex_handle> & vecVertexHandles, const Delaunay3::Vertex_handle hndlQ, const Segment & constraint,
        const int camIndex, const int featureIndex, const bool bOnlyMarkNew = false, const bool incrementCount = true);
    void rayTracing_suboptimal(const vector<Delaunay3::Vertex_handle> & vecVertexHandles, const Delaunay3::Vertex_handle hndlQ,
        const Segment & constraint, const int camIndex, const int featureIndex, const bool bOnlyMarkNew = false, const bool incrementCount = true);
    /*void markTetrahedraCrossingConstraintWithBookKeepingOrig(const vector<Delaunay3::Vertex_handle> & vecVertexHandles,
     const Delaunay3::Vertex_handle hndlQ, const Segment & constraint, const int camIndex, const int featureIndex,
     const bool bOnlyMarkNew = false);*/

    void updateDistanceAndWeights(vector<Delaunay3::Cell_handle> &cellsToBeUpdated, const vector<pair<PointD3, double> > &vecDistanceWeight);

    void addNewlyObservedFeatures(vector<Delaunay3::Vertex_handle> & vecVertexHandles, vector<int> & localVisList,
        const vector<int> & originalLocalVisList);
    bool addNewlyObservedFeature(vector<Delaunay3::Vertex_handle> & vecVertexHandles,
        set<pair<int, int>, Delaunay3CellInfo::LtConstraint> & setUnionedConstraints, const PointD3 & Q, const int nPointIndex);

    void addNewlyObservedFeatures_suboptimal(vector<Delaunay3::Vertex_handle> & vecVertexHandles, vector<int> & localVisList,
        const vector<int> & originalLocalVisList);
    bool addNewlyObservedFeature_suboptimal(vector<Delaunay3::Vertex_handle> & vecVertexHandles, vector<pair<PointD3, double> > &vecDistanceWeight,
        const PointD3 & Q, const int nPointIndex);

    void regionGrowingProcedure();
    void regionGrowingProcedureForHandle(Delaunay3::Cell_handle &cell, std::vector<Delaunay3::Cell_handle> &ladd);
    void computeVisibilityTransposed();
    bool triangleConstraintIntersectionTest(const Delaunay3::Facet & tri, const Matrix & segSrc, const Matrix & segDest);
    bool triangleConstraintIntersectionTest(bool & bCrossesInteriorOfConstraint, const vector<Matrix> & points, const Matrix & tri,
        const pair<Matrix, Matrix> & constraint);
    bool cellTraversalExitTest(int & f, const Delaunay3::Cell_handle tetCur, const Delaunay3::Cell_handle tetPrev, const Matrix & matQ,
        const Matrix & matO);
    bool cellTraversalExitTest(int & f, int & fOld, const Delaunay3::Cell_handle tetCur, const Delaunay3::Cell_handle tetPrev, const Matrix & matQ,
        const Matrix & matO);
    void markTetraedron(Delaunay3::Cell_handle cell, const vector<Delaunay3::Vertex_handle> & vecVertexHandles, const int camIndex,
        const int featureIndex, const bool incrementCount = true);
    void markTetraedron_suboptimal(Delaunay3::Cell_handle cell, const vector<Delaunay3::Vertex_handle> & vecVertexHandles, const int camIndex,
        const int featureIndex, const bool incrementCount = true);

    void facetToTri(const Delaunay3::Facet & f, vector<Delaunay3::Vertex_handle> & vecTri) const;
    double timestamp() const;

    bool isFreespace(Delaunay3::Cell_handle cell);

    void tetsToTris_Basic(Delaunay3 temp, vector<Matrix> & points, vector<Matrix> & tris) const;
    void tetsToTris_naiveOrig(vector<Matrix> & points, vector<Matrix> & tris, const int nVoteThresh);
    void tetsToTris_naive(vector<Matrix> & points, vector<Matrix> & tris, const int nVoteThresh);
    void tetsToTris_naive(vector<Matrix> & points, vector<Matrix> & tris, const float nVoteThresh);
    void tetsToTris_IsBound(vector<Matrix> & points, vector<Matrix> & tris);

    void tetsToTrisSure_naive(vector<Matrix> & points, vector<Matrix> & tris) const;
    void tetsToTris_maxFlowSimple(vector<Matrix> & points, vector<Matrix> & tris, const int nVoteThresh) const;

    // Private Members
    vector<Matrix> m_points;
    vector<Matrix> m_cams;
    vector<Matrix> m_camCenters;
    vector<Matrix> m_principleRays;
    vector<vector<int> > m_visibilityList;
    vector<int> points_moves_;
    vector<vector<int> > m_visibilityListTransp;
    vector<vector<bool> > visibilityListNewPoint_;
    vector<Delaunay3::Cell_handle> freeSpaceTets_;
    //vector<Delaunay3> convexHulls_;
    //vector<Delaunay3::Cell_handle> shrinkedTets_;
    //vector<Delaunay3::Cell_handle> shrinkedTetsToTest_;
    vector<Delaunay3::Cell_handle> boundaryCells_;
    //vector<Delaunay3::Cell_handle> boundaryCellsNotManifold_;
    vector<Delaunay3::Vertex_handle> vecVertexHandles_;
    int curCam_;

    vector<Delaunay3::Cell_handle> vecConflictCellsTot_;
    Configuration configuration_;
    std::ofstream fileStats_;

    float w_1, w_2, w_3;
    int num_;
    double l_;
    double r_;
    double deltatot;

    Delaunay3 dt;
    bool shrinkEnabled_;
    double m_nBoundsMin;
    double m_nBoundsMax;
    double stepX_;
    double stepY_;
    double stepZ_;


    float infiniteValue_;


    mutable map<int, int> m_mapPoint_VertexHandle; // TODO: Refactor
};
}

#endif /* FREESPACEDELAUNAYMANIFOLD_H_ */
