#version 420

in vec2 tex1Coord;   

layout(location = 0) out vec4 cumulativeSum;

uniform float idCam2;
uniform sampler2D imageCumulative;
uniform sampler2D imageCur;

void main(){

  vec4 resCur = texture(imageCur, tex1Coord);
  vec4 cumSum = texture(imageCumulative, tex1Coord);

  float curNCC = resCur.w - float(trunc(resCur.w));
  float oldNCC = cumSum.w - float(trunc(cumSum.w));
  /*float curNCC = resCur.w ;
  float oldNCC = cumSum.w ;*/

  if(curNCC > oldNCC){
    // code the ncc and the id cam together
    float idAndNcc = idCam2 + curNCC;
   // float idAndNcc = curNCC;
    cumulativeSum = vec4(resCur.xyz, idAndNcc);
  }else{
    cumulativeSum = cumSum;
  }
 
}
