#version 420

in vec3 position;               /*3D vertex position*/

out vec3 positionPointV;
void main(){
    gl_Position = vec4(position, 1.0);
    positionPointV = position;
}

