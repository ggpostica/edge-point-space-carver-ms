#version 420

in uint position;               /*3D vertex position*/

out uint positionPointV;        /*3D point position*/


void main(){

  gl_Position = vec4(position, 1.0);
  positionPointV = position;

}

