#version 420

layout (triangles) in;
layout (triangle_strip, max_vertices=3) out;

in vec4 positionPointV[];       /*3D point position*/

out vec3 laplacianValue;

float angleBetweenVec(vec3 x, vec3 y){

  return 2 * atan(length(x * length(y) - length(x) * y), length(x * length(y) + length(x) * y));
}

void main(){
  /*cotangent */
  vec3 v01 = positionPointV[0].xyz -  positionPointV[1].xyz;
  vec3 v02 = positionPointV[0].xyz -  positionPointV[2].xyz;
  vec3 v12 = positionPointV[1].xyz -  positionPointV[2].xyz;

  float angle01_02 = angleBetweenVec(v01,v02);
  float angle10_12 = angleBetweenVec(-v01,v12);
  float angle21_20 = angleBetweenVec(-v02,-v12);

  mat3 w = mat3(0.0f);

  /*Vertex 0*/
  laplacianValue = vec3(0.0f);
  /*........with vertex 1*/
  w[0][1] = 0.5f * (1.0f/tan(angle21_20));
  /*........with vertex 2*/
  w[0][2] = 0.5f * (1.0f/tan(angle10_12));

  /*Vertex 1*/
  laplacianValue = vec3(0.0);
  /*........with vertex 1*/
  w[1][0] = 0.5f * (1.0f/tan(angle21_20));
  /*........with vertex 2*/
  w[1][2] = 0.5f * (1.0f/tan(angle01_02));

  /*Vertex 2*/
  laplacianValue = vec3(0.0);
  /*........with vertex 1*/
  w[2][0] = 0.5f * (1.0f/tan(angle10_12));
  /*........with vertex 2*/
  w[2][1] = 0.5f * (1.0f/tan(angle01_02));



/*umbrella*/
  for(int i = 0; i < gl_in.length(); i++) {
    laplacianValue = vec3(0.0f);
    for(int j = 0; j < gl_in.length(); j++) {
      if(i!=j){
        //laplacian operator computation
        laplacianValue += w[i][j] * (positionPointV[i].xyz -  positionPointV[j].xyz);
      }
    }
    
    gl_Position = gl_in[i].gl_Position;
    EmitVertex();
  }
}