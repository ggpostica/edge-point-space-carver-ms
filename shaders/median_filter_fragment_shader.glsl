#version 420
#define W_MED 3

in vec2 tex1Coord;   /*2D coordinate in camera 1*/
in vec4 shadowCoord1;

layout(location = 0) out float filtered;

uniform float imW;
uniform float imH;
uniform float wMed;
uniform sampler2D imageNCC;

void main(){

  float curRow, curCol, maxVal;
  vec4 res, resInit;
  int wM = W_MED;

  float values[W_MED*W_MED];

/*store all elements into a single array*/
  int curI = 0;
  for(curRow = -wM/imH; curRow < wM/imH; curRow += 1.0/imH){
    for(curCol = -wM/imW; curCol < wM/imW; curCol += 1.0/imW){
      values[curI] = texture(imageNCC, tex1Coord + vec2(curCol, curRow)).x;
      curI++;
    }
  }
  /*order half of the vector*/
  float lastV=0.0;
  float min,temp;
  int imin;
  for (curI = 0; curI < floor(wM*wM/2)+1; curI++){
    imin = curI;
    for(int curOr = curI; curOr<wM*wM; curOr++){
      if(values[curOr] < values[imin]){
        imin = curOr;
      }
    }

    temp = values[imin];
    values[imin] = values[curI];
    values[curI] = temp;

    lastV = values[imin];
  }
  /*pick the median value*/

  filtered = lastV;
}
