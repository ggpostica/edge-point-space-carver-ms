#version 420
#define M_PI 3.1415926535897932384626433832795

in vec4 projector1TexCoord;
in vec2 tex2Coord;   /*2D coordinate in camera 1*/
in vec2 tex1Coord;   /*2D coordinate in camera 1*/
in vec4 shadowCoord1;

/*output texture 0.0...255.0*/
layout(location = 0) out float ncc;

//uniform sampler2DShadow shadowMap1;
uniform float imW;
uniform float imH;
uniform int window;
uniform sampler2D image1;
uniform sampler2D image2Repr;

void main(){

  vec4 img1;
  vec4 image2Reproj;
  float W = float(window);
  float sumWeight=0.0;
  float beta2 = 10.0;
  float sum1 = 0.0, sum2 = 0.0,sum1temp = 0.0, sum2temp = 0.0, sqsum1 = 0.0, sqsum2 = 0.0, prod12 = 0.0;
  
  float curRow, curCol;
  float sigma = W;
/*
  for(curRow = -W/imH; curRow < W/imH; curRow += 1.0/imH){

    if(tex1Coord.y + curRow > W/imH && tex1Coord.y + curRow < (imH-W)/imH 
      && tex1Coord.y + curRow > W/imH && tex1Coord.y + curRow < (imH-W)/imH){

      for(curCol = -W/imW; curCol < W/imW; curCol += 1.0/imW){

        if(tex1Coord.x + curCol > W/imW && tex1Coord.x + curCol < (imW-W)/imW 
          && tex1Coord.x + curCol > W/imW && tex1Coord.x + curCol < (imW-W)/imW){

          float curWeight = 1.0;
          img1 = 255.0*texture(image1, tex1Coord + vec2(curCol, curRow));
          image2Reproj = 255.0*texture(image2Repr, tex2Coord + vec2(curCol, curRow));
          sum1temp += curWeight * img1.x;
          sum2temp += curWeight * image2Reproj.x;




          sumWeight += curWeight;
        }
      }
    }
  }

  
float var12temp = 0;
  float var1temp = 0.0;
  float  var2temp = 0.0;
  float mean1temp = sum1temp/sumWeight;
  float mean2temp = sum2temp/sumWeight;*/
  
  sumWeight = 0.0;
  for(curRow = -W/imH; curRow < W/imH; curRow += 1.0/imH){

    if(tex1Coord.y + curRow > W/imH && tex1Coord.y + curRow < (imH-W)/imH 
      && tex1Coord.y + curRow > W/imH && tex1Coord.y + curRow < (imH-W)/imH){

      for(curCol = -W/imW; curCol < W/imW; curCol += 1.0/imW){

        if(tex1Coord.x + curCol > W/imW && tex1Coord.x + curCol < (imW-W)/imW 
          && tex1Coord.x + curCol > W/imW && tex1Coord.x + curCol < (imW-W)/imW){

          float curC = curCol * imW;
          float curR = curRow *imH;
          float gaussianWeight = (1/(sigma * 2 * M_PI)) * exp (-(curCol * curCol + curR * curR)/(2 * sigma * sigma));
          float curWeight = gaussianWeight;
          img1 = 255.0*texture(image1, tex1Coord + vec2(curCol, curRow));
          image2Reproj = 255.0*texture(image2Repr, tex2Coord + vec2(curCol, curRow));
          sum1 += curWeight * img1.x;
          sum2 += curWeight * image2Reproj.x;
          sqsum1 += curWeight * img1.x * img1.x;
          sqsum2 += curWeight * image2Reproj.x * image2Reproj.x;
          prod12 += curWeight * img1.x * image2Reproj.x;
/*
var12temp += curWeight* ((img1.x-mean1temp) * (image2Reproj.x - mean2temp));
var1temp += curWeight *((img1.x-mean1temp) * (img1.x-mean1temp));
          var2temp+= curWeight* ((image2Reproj.x - mean2temp) * (image2Reproj.x - mean2temp));

*/
          sumWeight += curWeight;
        }
      }
    }
  }

  float mean1 = sum1/sumWeight;
  float mean2 = sum2/sumWeight;
  float var1 = sqsum1/sumWeight - mean1*mean1 + beta2;
  float var2 = sqsum2/sumWeight - mean2*mean2 + beta2;

  float var12 = prod12/sumWeight - mean1*mean2;

   ncc = (var12/(sqrt(var1 * var2)));
/*
  float var12tmp = var12temp/sumWeight;
   var1temp = var1temp/sumWeight;
   var2temp = var2temp/sumWeight;
   ncc = (var12tmp/(sqrt(var1temp * var2temp)));*/

          img1 = texture(image1, tex1Coord );
          image2Reproj = texture(image2Repr, tex2Coord );
//ncc=vec2(img1.x, image2Reproj.x);
//ncc = image2Reproj.x;
}
