#version 420

in vec4 projectorTexCoordG;   /*2D coordinate in camera 2*/
in vec4 shadowCoordG;         /*coordinate for shadow mapping (camera 2)*/
in vec3 positionPointG;        /*3D point position*/
in float area; 
in float idG;

out vec4 point;

uniform mat4 MVP;
uniform float th;
uniform sampler2D imageMaxima;
uniform sampler2DShadow shadowMap;

void main(){
  vec4 imageMaximaVal =  texture2DProj(imageMaxima, projectorTexCoordG);
  float shadowCoeff = textureProj(shadowMap, shadowCoordG);
  
  point = vec4(-666666.000000f, -777777.000000f, -666666.000000f, -500000.0f);

  if(shadowCoeff * imageMaximaVal.x > th && area > 2.0f){
    // code the ncc and the id together
    point = vec4(positionPointG, idG + 0.5f + 0.5f * 0.9f * imageMaximaVal.x);
  }else{
    //point = vec4(1.0);
  }
}