#version 420

layout (triangles) in;
layout (points, max_vertices=1) out;
 
in vec3 positionPointV[];       /*3D point position*/
in vec4 projectorTexCoord[];   /*2D coordinate in camera 2*/
in float idV[];        /*id facet*/

out vec4 point;

uniform mat4 MVP;
uniform sampler2D imageMaxima;
uniform sampler2D imageId;
uniform float imW;
uniform float imH;

float orientPoint(vec2 v0, vec2 v1, vec2 p){
  mat2 m;
  m[0][0] = (v1.x - v0.x);  m[0][1] = ( p.x - v0.x);
  m[1][0] = (v1.y - v0.y);  m[1][1] = ( p.y - v0.y);

  return m[0][0] * m[1][1] - m[0][1] * m[1][0];
}

vec3 barycentricCoordMine(vec2 p, vec2 p0, vec2 p1, vec2 p2){
  vec2 v0, v1, v2;
  //First Check if triangle is counter-clockwise
  if(orientPoint(p0, p1, p2) > 0){
    v0 = p0;
    v1 = p1;
    v2 = p2;
  }else{
    v0 = p0;
    v1 = p2;
    v2 = p1;
  }
  // Compute barycentric coordinates w.r.t pt1
  vec3 barycentricCoordinates;

  float areaTrtwice = orientPoint(v0, v1, v2);

  if(areaTrtwice!=0){
    barycentricCoordinates.x = orientPoint(v0, v1, p)/areaTrtwice;
    barycentricCoordinates.y = orientPoint(v1, v2, p)/areaTrtwice;
    barycentricCoordinates.z = orientPoint(v2, v0, p)/areaTrtwice;
  }else {
   return vec3(-1.0);
  }

  return barycentricCoordinates;
}


vec3 barycentricCoordMine(vec2 p, vec2 v0, vec2 v1, vec2 v2, float twiceSignedArea){
  // Compute barycentric coordinates w.r.t pt1
  vec3 barycentricCoordinates;
  if(twiceSignedArea!=0){
    barycentricCoordinates.x = orientPoint(v0, v1, p)/twiceSignedArea;
    barycentricCoordinates.y = orientPoint(v1, v2, p)/twiceSignedArea;
    barycentricCoordinates.z = orientPoint(v2, v0, p)/twiceSignedArea;
  }else {
   return vec3(-1.0);
  }

  return barycentricCoordinates;
}



void main(){
  vec2 pt0 = projectorTexCoord[0].xy;
  vec2 pt1 = projectorTexCoord[1].xy;
  vec2 pt2 = projectorTexCoord[2].xy;

  float maxNcc = -10.0f;

  bool switchedVertices;

  vec2 maxT,minT;
  maxT.x = max(max(pt1.x,pt2.x),pt0.x);
  maxT.y = max(max(pt1.y,pt2.y),pt0.y);
  minT.x = min(min(pt1.x,pt2.x),pt0.x);
  minT.y = min(min(pt1.y,pt2.y),pt0.y);

  //check boundary
  maxT.x = max(maxT.x,0.0);
  maxT.y = max(maxT.y,0.0);
  minT.x = min(minT.x, imW);
  minT.y = min(minT.y, imH);

  float twiceSignedArea = orientPoint(pt0, pt1, pt2);
  vec2 v0, v1, v2;

  switchedVertices = twiceSignedArea > 0;

  if (switchedVertices){
    v0 = pt0;
    v1 = pt1;
    v2 = pt2;
  }else{
    v0 = pt0;
    v1 = pt2;
    v2 = pt1;
  }


  point.x = -666666.000000f;
  point.y = -777777.000000f;
  point.z = -666666.000000f;
  point.w = -5.0f;

  vec3 area = vec3(0.0);

  float stepX =1.0/imW;
  float stepY = 1.0/imH;
  float curX = (minT.x);
  while(curX <= (maxT.x)){
    float curY = (minT.y);
    while(curY <= (maxT.y)){
      vec2 P = vec2(curX, curY);
      vec3 barycCoord = barycentricCoordMine(P, v0, v1, v2,twiceSignedArea);
      // Check if point is in triangle
      //vec3 barycCoord = vec3(1.0);
      if(barycCoord.x >=0 && barycCoord.y >=0 && barycCoord.z >=0 ){


        vec4 imageMaximaVal = texture2DProj(imageMaxima,  vec4(curX, curY, 0.0, 1.0));
        vec4 idOK = textureProj(imageId, vec4(curX, curY, 0.0, 1.0));

        if (maxNcc < imageMaximaVal.x && round(idOK.x) == round(idV[0])){
          maxNcc = imageMaximaVal.x;

          float clampedNcc = clamp(imageMaximaVal.x, 0.0f, 0.9f);

          // code the ncc and the id together
          float idAndNcc = idV[0] + clampedNcc;
          point = vec4(positionPointV[0], idAndNcc); //TO CHANGE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          
        }
      }
      curY = curY + stepY;
    }
    curX = curX + stepX;
  }

  EmitVertex();
}