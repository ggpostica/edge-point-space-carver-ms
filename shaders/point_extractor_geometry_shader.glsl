#version 420

layout (triangles) in;
layout (triangle_strip, max_vertices=3) out;
 
in vec3 positionPointV[];       /*3D point position*/
in vec4 projectorTexCoord[];   /*2D coordinate in camera 2*/
in vec4 shadowCoord[];         /*coordinate for shadow mapping (camera 2)*/
in float idV[];        /*id facet*/

out float area;
out vec4 projectorTexCoordG;  
out vec4 shadowCoordG;      
out vec3 positionPointG;        
out float idG;        

uniform float imW;
uniform float imH;
vec4 normalizeH(vec4 pt){
  vec4 ptr;
  ptr.x = pt.x /pt.w;
  ptr.y = pt.y /pt.w;
  ptr.z = pt.z /pt.w;
  ptr.w = pt.w /pt.w;
  return ptr;
}

void main(){
  vec2 pt0 = normalizeH(projectorTexCoord[0]).xy * vec2(imW, imH);
  vec2 pt1 = normalizeH(projectorTexCoord[1]).xy * vec2(imW, imH);
  vec2 pt2 = normalizeH(projectorTexCoord[2]).xy * vec2(imW, imH);

  area = 0.5f * abs(pt0.x * ( pt1.y - pt2.y) + pt1.x * (pt2.y - pt0.y) + pt2.x * (pt0.y - pt1.y) );
  
  for(int i = 0; i < gl_in.length(); i++){
    // copy attributes
    gl_Position = gl_in[i].gl_Position;
    shadowCoordG = shadowCoord[i];
    projectorTexCoordG = projectorTexCoord[i];
    positionPointG = positionPointV[i];
    idG = idV[i];
    // done with the vertex
    EmitVertex();
  }

}