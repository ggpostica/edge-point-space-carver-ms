#version 420

in vec4 projectorTexCoord;   /*2D coordinate in camera 2*/
in vec4 shadowCoord;         /*coordinate for shadow mapping (camera 2)*/
in vec3 positionPointV;        /*3D point position*/

  
out vec4 point;

uniform mat4 MVP;
uniform float th;
uniform sampler2D imageMaxima;
uniform sampler2DShadow shadowMap;

void main(){
  vec4 imageMaximaVal =  texture2DProj(imageMaxima, projectorTexCoord);
  float shadowCoeff = textureProj(shadowMap, shadowCoord);
  
  point.x = -666666.000000f;
  point.y = -777777.000000f;
  point.z = -666666.000000f;
  point.w = -500000.0f;//NCC

  if(shadowCoeff * abs(imageMaximaVal.x) > th){
    point = vec4(positionPointV, 0.5f * 0.9f * float(imageMaximaVal.x));
  }
}