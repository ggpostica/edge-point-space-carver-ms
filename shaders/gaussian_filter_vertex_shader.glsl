#version 420

in vec2 position;
in vec2 texcoord;

out vec2 tex1Coord;   /*2D coordinate in camera 1*/

//uniform mat4 MVP1;

void main(){
  tex1Coord=texcoord;
  tex1Coord.y=1.0-tex1Coord.y;
  gl_Position =  vec4(position,0.0,1.0);
}

