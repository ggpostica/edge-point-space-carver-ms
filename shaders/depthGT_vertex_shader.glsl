#version 420

in vec3 position;

out vec3 posC;  
out vec4 shadowCoord; 

uniform mat4 MVP;

void main(){
  mat4 biasMatrix = mat4(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0
    );

  shadowCoord =  MVP * vec4(position, 1.0);
  shadowCoord = biasMatrix * shadowCoord;
  shadowCoord.z -= 0.000500;

  gl_Position =  MVP * vec4(position,1.0);
  posC = position;
}

