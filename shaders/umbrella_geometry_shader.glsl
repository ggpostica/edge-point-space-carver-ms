#version 420

layout (triangles) in;
layout (triangle_strip, max_vertices=3) out;

in vec4 positionPointV[];       /*3D point position*/

out vec3 laplacianValue;

float angleBetweenVec(vec3 x, vec3 y){

  return 2 * atan(length(x * length(y) - length(x) * y), length(x * length(y) + length(x) * y));
}

void main(){
  
  for(int i = 0; i < gl_in.length(); i++) {
    laplacianValue = vec3(0.0);
    for(int j = 0; j < gl_in.length(); j++) {
      if(i!=j){
        //laplacian operator computation
        laplacianValue += positionPointV[j].xyz -  positionPointV[i].xyz;
      }
    }
    
    gl_Position = gl_in[i].gl_Position;
    EmitVertex();
  }
}