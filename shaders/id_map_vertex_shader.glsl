#version 420

in vec3 position;               /*3D vertex position*/
in vec3 normal;
in float id;

out vec4 projectorTexCoord;   /*2D coordinate in camera 2*/
out vec4 shadowCoord;         /*coordinate for shadow mapping (camera 2)*/
out vec3 positionPointV;        /*3D point position*/
out float idV;        /*id facet*/

uniform mat4 MVP;
uniform float alpha;

void main(){

  vec3 position_new = position + alpha * normal;
  mat4 biasMatrix = mat4(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0
    );
  shadowCoord =  MVP * vec4(position_new, 1.0);
  shadowCoord = biasMatrix * shadowCoord;
  shadowCoord.z -= 0.000500;
  
  projectorTexCoord = MVP * vec4(position_new, 1.0);
  projectorTexCoord = biasMatrix * projectorTexCoord;

  gl_Position = MVP * vec4(position_new, 1.0);
  positionPointV = position_new;
  idV = id;

}

