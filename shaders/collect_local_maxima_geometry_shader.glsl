#version 420

layout (points) in;
layout (points) out;
layout (max_vertices = 1) out;
 
in vec2 positionPoint[];       /*3D point position*/

out vec4 pointOK;

uniform sampler2D imageLocalMaxima;
void main(){

  vec4 imageVal = texture(imageLocalMaxima, positionPoint[0]);
  float curX = imageVal.x;
  float curY = imageVal.y;
  float curZ = imageVal.z;

  if (curX != -666666.0f && curX != -777777.0f && curX != -666666.0f && 
    (curX != 0.1666666666f || curX != 0.1666666666f || curX != 0.1666666666f)) {
     
   /* pointOK = imageVal;

    EmitVertex();*/
  }
    pointOK = imageVal;

    EmitVertex();
}