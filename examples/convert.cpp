#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>

std::string getFrameNumber(int curFrame, int digitIdxLength) {
  std::ostringstream curNumber;
  if (digitIdxLength > 0) {
    int n = curFrame;
    int curNumOfDigit = curFrame == 0 ? 1 : 0;
    while (n > 0) {
      n /= 10;
      ++curNumOfDigit;
    }
    while (curNumOfDigit < digitIdxLength) {
      curNumber << "0";
      curNumOfDigit++;
    }
  }
  curNumber << curFrame;
  return curNumber.str();
}

int main(int argc, char const *argv[])
{
  int curNum = 268;
  for (int i = 0; i < curNum; ++i)
  {
    std::stringstream base, s1, s2;

    base <<"/home/andrea/Scrivania/KITTI/2011_09_26/2011_09_26_drive_0095_sync/velodyne_points/data/" << getFrameNumber(i, 10);
    s1 << base.str() << ".bin";
    s2 << base.str() << ".txt";
    std::cout<< i <<std::endl;

    std::cout<< s1.str() <<std::endl;

    std::cout<< s2.str() <<std::endl;

    std::ofstream currFilenameTxt(s2.str().c_str());
  // allocate 4 MB buffer (only ~130*4*4 KB are needed)
    int32_t num = 1000000;
    float *data = (float*)malloc(num*sizeof(float));

  // pointers
    float *px = data+0;
    float *py = data+1;
    float *pz = data+2;
    float *pr = data+3;

  // load point cloud
    FILE *stream;
    stream = fopen (s1.str().c_str(),"rb");
    num = fread(data,sizeof(float),num,stream)/4;
    for (int32_t i=0; i<num; i++) {
      currFilenameTxt << *px << ", " << *py << ", " << *pz << ", " << *pr <<std::endl; 
      px+=4; py+=4; pz+=4; pr+=4;
    }
    fclose(stream);
    currFilenameTxt.close();

  }

  return 0;
}