/*
 * ManifoldReconstructionAndRefinement.cpp
 *
 *  Created on: 10/apr/2015
 *      Author: andrea
 */

#include "ManifoldReconstructionAndRefinement.h"

#include <glm.hpp>

#include "Matrix.h"
#include "utilities/conversionUtilities.hpp"
#include <Mesh.h>
#include "./utilities/Logger.h"

ManifoldReconstructionAndRefinement::ManifoldReconstructionAndRefinement(SweepConfiguration mySweepConf, ManifoldConfig myConf, Configuration conf) {

  conf_ = conf;
  myConf_ = myConf;
  mySweepConf_ = mySweepConf;
  manifolReconstructor_ = new ManifoldReconstructor(myConf_, conf_);
  //   meshSweeper_ = new MeshSweeperDense(mySweepConf_, false);
  meshSweeper_ = new MeshSweeper(mySweepConf_, false);

  std::ifstream s;
//  s.open("/home/andrea/workspaceC/edgePointSpaceCarver/depthGT.txt", std::ios::in);
  s.open("/home/andrea/workspaceC/edgePointSpaceCarver/FountainDepthGT.txt", std::ios::in);

  depthMapGT = cv::Mat(conf_.videoConfig.imageH, conf_.videoConfig.imageW, CV_32F);

  float mean = 0.0, tot = 0.0;
  for (int curR = 0; curR < conf_.videoConfig.imageH; ++curR) {

    std::string line;
    std::getline(s, line);
    std::istringstream iss(line);
    for (int curC = 0; curC < conf_.videoConfig.imageW; ++curC) {
      char c;
      iss >> depthMapGT.at<float>(curR, curC);
      iss >> c;
      if (depthMapGT.at<float>(curR, curC) != 0.0) {
        mean = mean + depthMapGT.at<float>(curR, curC);
        tot = tot + 1.0;
      }
    }
  }

  mean = mean / tot;
  float sumSq = 0.0;
  for (int curR = 0; curR < conf_.videoConfig.imageH; ++curR) {
    for (int curC = 0; curC < conf_.videoConfig.imageW; ++curC) {
      if (depthMapGT.at<float>(curR, curC) != 0.0) {
        sumSq = sumSq + (depthMapGT.at<float>(curR, curC) - mean) * (depthMapGT.at<float>(curR, curC) - mean);
      }
    }
  }

  sigmaGT_ = sqrt(sumSq / (tot - 1));

  meshSweeper_->setSigmaGt(sigmaGT_);
  meshSweeper_->setSigmaGt(0.05);

  std::cout << "Sigma: " << sigmaGT_ << "m" << std::endl;
  /*cv::Mat temp, temp2;
   double max,min;
   cv::minMaxLoc(depthMapGT, &min, &max);
   temp2 = 255.0 * (depthMapGT-min)/(max-min);
   temp2.convertTo(temp,CV_8UC3);
   cv::imshow("depthMapGT",temp);
   cv::waitKey();*/

}

ManifoldReconstructionAndRefinement::~ManifoldReconstructionAndRefinement() {
}

void ManifoldReconstructionAndRefinement::run() {

  manifolReconstructor_->runFirstIteration();
  manifolReconstructor_->computeMesh(true);
  std::stringstream sout, fileout;
  sout << mySweepConf_.outputSweep.pathFirstMesh << ".off";
  fileout << mySweepConf_.outputSweep.pathMesh << "Log.txt";
  std::ofstream logFile(fileout.str());
  manifolReconstructor_->getMesh().saveFormat(sout.str().c_str());

  std::vector<msc::Matrix> p;
  std::vector<std::vector<int> > vV;
  for (int curIt = 0; curIt < 500; ++curIt) {
    utilities::Logger log;

    log.startEvent();

    std::cout << "***************************************************";
    std::cout << "ManifoldReconstructionAndRefinement iteration Num.: " << curIt << std::endl;
    meshSweeper_->restartWithNewMesh(manifolReconstructor_->getMesh());

    if (curIt % 10 == 0) {
      meshSweeper_->setCleanMesh(true);
      meshSweeper_->saveCurMesh("lastCleanedMes.off");
    } else {
      meshSweeper_->setCleanMesh(false);
    }

    meshSweeper_->setCurIter(curIt);
    //meshSweeper_->createDepthMap(5);
    meshSweeper_->createDepthMap(3);
    meshSweeper_->compareDepthMap(depthMapGT, "",logFile);

    //meshSweeper_->smoothMesh();

    meshSweeper_->createDepthMap(3);
    meshSweeper_->compareDepthMap(depthMapGT, "Smooth",logFile);
    meshSweeper_->run(curIt);

    std::vector<msc::Matrix> tempMatrix;


    /*Read points and visibility to perform batch manifold reconstruction*/
    for (auto v : meshSweeper_->getVis()) {
      vV.push_back(v);
    }
    for (auto curP : meshSweeper_->getFilteredPoints()) {
      p.push_back(conversionUtilities::toLoviMatrix(curP));
    }

    log.endEventAndPrint("pointExtractionDone ", true);

    std::stringstream s;
    manifolReconstructor_->setWeightsSpaceCarver(3.0, 0.00, 0.0000);
    manifolReconstructor_->run(p, vV, true);
    manifolReconstructor_->computeMesh(true);

  }
  logFile.close();

}
