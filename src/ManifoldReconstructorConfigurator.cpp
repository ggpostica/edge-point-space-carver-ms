/*
 * ManifoldReconstructorConfigurator.cpp
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#include "ManifoldReconstructorConfigurator.h"

#include "./utilities/utilities.hpp"
ManifoldReconstructorConfigurator::ManifoldReconstructorConfigurator(const std::string &path) {
  file_.open(path.c_str());

}

ManifoldReconstructorConfigurator::~ManifoldReconstructorConfigurator() {
}

ManifoldConfig ManifoldReconstructorConfigurator::parseConfigFile() {
  ManifoldConfig c;
  utils::readLineAndStore(file_, c.videoConfig.baseNameImage);
  utils::readLineAndStore(file_, c.videoConfig.imageExtension);
  utils::readLineAndStore(file_, c.videoConfig.idxFirstFrame);
  utils::readLineAndStore(file_, c.videoConfig.digitIdxLength);
  utils::readLineAndStore(file_, c.videoConfig.idxLastFrame);
  utils::readLineAndStore(file_, c.videoConfig.imageH);
  utils::readLineAndStore(file_, c.videoConfig.imageW);
  utils::readLineAndStore(file_, c.manifConfig.pathCamsPose);
  utils::readLineAndStore(file_, c.manifConfig.pathInitPoints);
  return c;
}
