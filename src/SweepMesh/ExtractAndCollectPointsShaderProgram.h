/*
 * ExtractAndCollectPointsShaderProgram.h
 *
 *  Created on: 14/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_EXTRACTANDCOLLECTPOINTSSHADERPROGRAM_H_
#define SWEEPMESH_EXTRACTANDCOLLECTPOINTSSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include "../utilities/Logger.h"
#include <glm.hpp>
#include <vector>

class ExtractAndCollectPointsShaderProgram : public ShaderProgram {
public:
  ExtractAndCollectPointsShaderProgram(int imageWidth, int imageHeight);
  virtual ~ExtractAndCollectPointsShaderProgram();
  void createTransformFeedback(int length);

  void compute(bool renderFrameBuf = false);


  void setImagePointsKept(GLuint imagePointsKept) {
    imagePointsKept_ = imagePointsKept;
  }

  void setIdMap(GLuint idMap) {
    idMap_ = idMap;
  }

  void setMvp(const glm::mat4& mvp) {
    mvp_ = mvp;
  }


  void setNormalBuffer(GLuint normalBuffer) {
    normalBuffer_ = normalBuffer;
  }

  void setAlpha(float alpha) {
    alpha_ = alpha;
  }

  void setIdBuffer(GLuint idBuffer) {
    idBuffer_ = idBuffer;
  }

  const std::vector<glm::vec4>& getFeedbackTrPoints() const {
    return feedbackTrPoints_;
  }

private:

  void init();

  void createAttributes();
  void createUniforms();

  GLuint query_;

  GLuint framebuffer_;
  /*uniforms id*/
  GLuint imageTexId_, mvpId_, idMapId_;
  /*tex id*/
  GLuint pointsExtractedTexId_, imagePointsKept_;
  /*attributes id*/
  GLuint posId_;
  std::vector<glm::vec3> points_;

  GLuint normAttrib_, idAttrib_, imWid_, imHid_;
  GLuint normalBuffer_;
  GLuint idBuffer_;
  GLuint alphaId_, nccTex_;
  float alpha_;

  GLuint idMap_;
  glm::mat4 mvp_;

  utilities::Logger logger;
  GLuint feedbackBuffer_, feedbackLen_;
  std::vector<glm::vec4> feedbackTrPoints_;
};

#endif /* SWEEPMESH_EXTRACTANDCOLLECTPOINTSSHADERPROGRAM_H_ */
