/*
 * MeshSweeper.h
 *
 *  Created on: 01/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_MESHSWEEPER_H_
#define SWEEPMESH_MESHSWEEPER_H_

#include "../OpenGLWrapper/OpenGLProgram.h"
#include "../types.hpp"
#include "Mesh.h"
#include "../cam_parsers/CamParser.h"
#include "SweepShaderProgram.h"
#include "DepthShaderProgram.h"
#include "NormalShaderProgram.h"
#include "ReprojectionShaderProgram.h"
#include "NccShaderProgram.h"
#include "LocalMaximaShaderProgram.h"
#include "FilterImageShaderProgram.h"
#include "PointExtractorShaderProgram.h"
#include "LaplacianSmoothShaderProgram.h"
#include "ShaderProgram.h"
#include "../OpenGLWrapper/TransformationController.h"
#include "PointsCollector.h"
#include "../utilities/Logger.h"

#include <opencv2/core/core.hpp>
#include <iostream>
#include <fstream>


class MeshSweeper : public OpenGLProgram {
public:
  MeshSweeper(SweepConfiguration myConf, const bool loadMesh = true);
  virtual ~MeshSweeper();

  void run();  // implements code
  void run(int i);  // implements code
  void restartWithNewMesh(const Mesh& mesh);
  void updateWithCurPoints();
  void saveCurMesh(std::string pathName);
  void saveCurPoints(std::string pathName);
  void smoothMesh();
  void createDepthMap(int idxCam);
  void compareDepthMap(cv::Mat &gtDepth);
  void compareDepthMap(cv::Mat &gtDepth, const std::string postfix , std::ofstream &fileLog);

  //void simpleMeshUpdate();

  const std::vector<glm::vec3>& getNewPoints() const {
    return newPoints_;
  }

  void setMesh(const Mesh& mesh) {
    mesh_ = mesh;
  }

  const std::vector<std::vector<int> >& getVis() const {
    return vis_;
  }

  const std::vector<glm::vec3>& getFilteredPoints() const {
    return filteredPoints_;
  }

  void setCleanMesh(bool cleanMesh) {
    cleanMesh_ = cleanMesh;
  }

  void setSigmaGt(float sigmaGt) {
    sigmaGT_ = sigmaGt;
  }

  void setCurIter(int curIter) {
    curIter_ = curIter;
  }

private:
  void printPoint(std::string message, Vertex_handle v);
  void initialize();
  void initShaders();
  void reshapeFeedbackShaders();

  void createVertexArrayBuffer();
  void resetVertexArrayBuffer();
  void refreshVertexArrayBuffer();
  void createNormalArrayBuffer();
  void createIdArrayBuffer();
  void resetIdArrayBuffer();
  void createPixelImageArrayBuffer();
  void createIdUniqueArrayBuffer();
  void createImageVerticesBuffer();
  glm::mat4 setCameraParamAndGetMvp(const CameraType &cam);
  void loadImages();
  void resetMeshInfo();

  utilities::Logger logger;
  bool loadMesh_;
  Mesh mesh_;
  std::vector<cv::Mat> images_;
  CamParser *camParser_;
  TransformationController *cameraTransformationController_;
  SweepConfiguration myConf_;
  bool cleanMesh_;
  //***************shaders*******************************************
  ShaderProgram *depthProgram_;
  ShaderProgram *reprojProgram_;
  ShaderProgram *sweepProgram_;
  NormalShaderProgram *normalProgram_;
  NccShaderProgram *nccProgram_;
  ShaderProgram *localMaxProgram_;
  ShaderProgram *medianImageProgram_;
  ShaderProgram *gaussFilterImageProgram_;
  ShaderProgram *pointExtractorProgram_;
  ShaderProgram *collectPointsProgram_;
  ShaderProgram *collectLocalMaximaProgram_;
  ShaderProgram *cumulateProgram_;
  ShaderProgram *cumulateBestProgram_;
  ShaderProgram *depthMapForGT_;
  LaplacianSmoothShaderProgram *laplacianSmootherProgram_;

  PointsCollector *pColl;

  //***************shaders variables*********************************
  GLuint vertexBufferObj_, normalBufferObj_, imageArrayBufferObj_, imageElemBufferObj_, iDArrayBufferObj_, iDUniqueArrayBufferObj_, pixelImageArrayBufferObj_;
  GLuint framebufferDepth_, framebufferDepth2_;
  GLuint depthTexture_, depthTexture2_;
  GLuint reprojTex_, nccTex_, imageFiltered2id_, imageFilteredId_, localMaxTex_, pointsTex_, nccPointsTex_, idTex_, end1DTex_, compareDepthTex_;

  //*****************New Points vars********************************
  std::vector<glm::vec3> newPoints_;
  std::vector<glm::vec3> filteredPoints_;
  std::vector<bool> curKept_;
  std::vector<std::vector<int> > vis_;
  std::vector<glm::vec4> curPandNccId_;
  std::vector<GLfloat> ncc_;
  std::vector<GLuint> ids_;

  //*****************Sweeping vars********************************
  float coeff_;
  cv::Mat depthMap_;
  float sigmaGT_;

  float numPlanes_;

  GLfloat *pixels_;
  int curIter_;
};

#endif /* SWEEPMESH_MESHSWEEPER_H_ */
