/*
 * FilterImageShaderProgram.cpp
 *
 *  Created on: 08/apr/2015
 *      Author: andrea
 */

#include "FilterImageShaderProgram.h"

FilterImageShaderProgram::FilterImageShaderProgram(int imageWidth, int imageHeight, std::string pathVert, std::string pathFrag) :
    ShaderProgram(imageWidth, imageHeight) {
  pathVert_ = pathVert;
  pathFrag_ = pathFrag;
  imWid_ = imHid_ = filterTexId_ = -1;
  framebuffer_ = -1;
  filterTexId_ = imageNCCTexId_ = imageNCCTex_ = -1;

  posId_ = texCoordId_ = imageTex_ = -1;
}

FilterImageShaderProgram::~FilterImageShaderProgram() {
}

void FilterImageShaderProgram::initializeFramebufAndTex(GLuint& texId) {
  glGenFramebuffers(1, &framebuffer_);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  glGenTextures(1, &texId);
  glBindTexture(GL_TEXTURE_2D, texId);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, imageWidth_, imageHeight_, 0, GL_RED, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texId, 0);

  filterTexId_ = texId;
  checkFrameBuffer("FilterImageShaderProgram::initializeFramebufAndTex");
}

void FilterImageShaderProgram::compute(bool renderFrameBuf) {
  if (renderFrameBuf)
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  else
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

  GLuint attachments[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, attachments);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  shaderManager_.enable();

  glUniform1f(imWid_, static_cast<float>(imageWidth_));
  glUniform1f(imHid_, static_cast<float>(imageHeight_));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, imageNCCTex_);
  glUniform1i(imageNCCTexId_, 0);

  glEnableVertexAttribArray(posId_);
  glEnableVertexAttribArray(texCoordId_);

  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), 0);
  glVertexAttribPointer(texCoordId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), (const GLvoid*) (2 * sizeof(GLfloat)));

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
  glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);

  glDisableVertexAttribArray(posId_);
  glDisableVertexAttribArray(texCoordId_);
}

void FilterImageShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, pathVert_);
  shaderManager_.addShader(GL_FRAGMENT_SHADER, pathFrag_);
  shaderManager_.finalize();
}

void FilterImageShaderProgram::createAttributes() {
  posId_ = shaderManager_.getAttribLocation("position");
  texCoordId_ = shaderManager_.getAttribLocation("texcoord");
}

void FilterImageShaderProgram::createUniforms() {
  imageNCCTexId_ = shaderManager_.getUniformLocation("imageNCC");
  imWid_ = shaderManager_.getUniformLocation("imW");
  imHid_ = shaderManager_.getUniformLocation("imH");
}
