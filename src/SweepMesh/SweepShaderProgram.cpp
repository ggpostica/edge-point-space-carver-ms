/*
 * SweepShaderProgram.cpp
 *
 *  Created on: 01/apr/2015
 *      Author: andrea
 */

#include "SweepShaderProgram.h"

SweepShaderProgram::SweepShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {

  posAttrib_ = normAttrib_ = normalBuffer_ = -1;
  mvpID_ = alphaId_ = camCenterID_ = -1;
  alpha_ = feedbackLength_ = feedbackBuffer_ = -1;
}

SweepShaderProgram::~SweepShaderProgram() {
}

void SweepShaderProgram::compute(glm::mat4 mvp, bool renderFrameBuf) {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  shaderManager_.enable();

  glUniformMatrix4fv(mvpID_, 1, GL_FALSE, &mvp[0][0]);
  glUniform3fv(camCenterID_, 1, &camCenter_[0]);
  glUniform1f(alphaId_, alpha_);

  glEnableVertexAttribArray(posAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posAttrib_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(normAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, normalBuffer_);
  glVertexAttribPointer(normAttrib_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnable(GL_RASTERIZER_DISCARD);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, arrayBufferObj_);

  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackBuffer_);/*addd*/
  glBeginTransformFeedback(GL_TRIANGLES);

  if (useElements_Indices) {
    feedbackTr.resize(numElements_, glm::vec3(0.0));
    feedbackLength_ = numElements_;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
    glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);

  } else {
    feedbackTr.resize(sizeArray_, glm::vec3(0.0));
    feedbackLength_ = sizeArray_;
    glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
    glDrawArrays(GL_TRIANGLES, 0, sizeArray_);

  }
  glEndTransformFeedback();

  glDisableVertexAttribArray(posAttrib_);
  glDisableVertexAttribArray(normAttrib_);
  glFinish();
  glDisable(GL_RASTERIZER_DISCARD);
  glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackTr.size() * sizeof(glm::vec3), &feedbackTr[0]);

//  for (auto f : feedbackTr) {
//    std::cout << "F x: " << f.x << ", y: " << f.y << ", z: " << f.z << std::endl;
//  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void SweepShaderProgram::createTransformFeedback(int length) {
  glGenBuffers(1, &feedbackBuffer_);
  glBindBuffer(GL_ARRAY_BUFFER, feedbackBuffer_);
  glBufferData(GL_ARRAY_BUFFER, length * sizeof(glm::vec3), nullptr, GL_DYNAMIC_READ);

}

void SweepShaderProgram::updateTransformFeedback(int length) {
  glBindBuffer(GL_ARRAY_BUFFER, feedbackBuffer_);
  glBufferData(GL_ARRAY_BUFFER, length * sizeof(glm::vec3), nullptr, GL_DYNAMIC_READ);

}
void SweepShaderProgram::updateVertexOnGPU() {

  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glBufferData(GL_ARRAY_BUFFER, feedbackTr.size() * sizeof(glm::vec3), &feedbackTr[0], GL_STATIC_DRAW);
}

void SweepShaderProgram::init() {

  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/sweep_vertex_shader.glsl");
  shaderManager_.addFeedbackTransform("positionPointV");
  shaderManager_.finalize();
}

void SweepShaderProgram::createAttributes() {
  posAttrib_ = shaderManager_.getAttribLocation("position");
  normAttrib_ = shaderManager_.getAttribLocation("normal");
}

void SweepShaderProgram::createUniforms() {
  mvpID_ = shaderManager_.getUniformLocation("MVP");
  camCenterID_ = shaderManager_.getUniformLocation("camCenter");
  alphaId_ = shaderManager_.getUniformLocation("alpha");
}

