/*
 * PointsCollector.h
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_POINTSCOLLECTOR_H_
#define SWEEPMESH_POINTSCOLLECTOR_H_

#include <glm.hpp>
#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>

class PointsCollector {
public:
  PointsCollector(int numTriangles);
  virtual ~PointsCollector();

  void resetPoints();
  void resetPoints(int numTriangles);

  void addPointsNccIds(const std::vector<glm::vec4>& pointsNccIds, const int id1 = -1, const int id2 = -1);


  std::vector<glm::vec3>& getActivefilteredPoints();
  const std::vector<std::vector<int> >& getActiveVisibility();

  const std::vector<bool>& getKept() const {
    return kept_;
  }

  const std::vector<glm::vec3>& getFilteredPoints() const {
    return filteredPoints_;
  }

private:

  int numTriangles_;
  std::vector<glm::vec3> points_;
  std::vector<glm::vec3> filteredPoints_;
  std::vector<std::vector<int> > visibility_;
  std::vector<std::vector<int> > activeVisibility_;
  std::vector<glm::vec3> activefilteredPoints_;
  std::vector<GLfloat> maxNcc_;
  std::vector<bool> kept_;

};

#endif /* SWEEPMESH_POINTSCOLLECTOR_H_ */
