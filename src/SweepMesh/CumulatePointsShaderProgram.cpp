/*
 * CumulatePointsShaderProgram.cpp
 *
 *  Created on: 16/apr/2015
 *      Author: andrea
 */

#include "CumulatePointsShaderProgram.h"

CumulatePointsShaderProgram::CumulatePointsShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  textureEven_ = false;
  first_ = true;
  ch_ = 4;

   framebufferEven_ = framebufferOdd_ = cumulativeId_ = curPointsImageTex_ =-1 ;
  curPointsImageId_ = posId_ = texCoordId_ = textureEvenTex_ = textureOddTex_=-1;
  pixels_ = new GLfloat[imageWidth * imageHeight * ch_];

}

CumulatePointsShaderProgram::~CumulatePointsShaderProgram() {
  delete (pixels_);
}

void CumulatePointsShaderProgram::initializeFramebufAndTex(GLuint& texId) {
  initializeFramebufAndTexEven();
  initializeFramebufAndTexOdd();
}

void CumulatePointsShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/cumulate_vertex_shader.glsl");
  shaderManager_.addShader(GL_FRAGMENT_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/cumulate_fragment_shader.glsl");
  shaderManager_.finalize();
}

void CumulatePointsShaderProgram::createAttributes() {
}

void CumulatePointsShaderProgram::compute(bool renderFrameBuf) {

  if (textureEven_)
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferEven_);
  else
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferOdd_);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  GLuint attachments[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, attachments);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  shaderManager_.enable();

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, curPointsImageTex_);
  glUniform1i(curPointsImageId_, 0);

  if(first_){
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, curPointsImageTex_);
    glUniform1i(cumulativeId_, 1);
    first_ = false;
  }else if (textureEven_){
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textureOddTex_);
    glUniform1i(cumulativeId_, 1);
  }else{
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textureEvenTex_);
    glUniform1i(cumulativeId_, 1);
  }

  glEnableVertexAttribArray(posId_);
  glEnableVertexAttribArray(texCoordId_);

  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), 0);
  glVertexAttribPointer(texCoordId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), (const GLvoid*) (2 * sizeof(GLfloat)));

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
  glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);

  glDisableVertexAttribArray(posId_);
  glDisableVertexAttribArray(texCoordId_);
  textureEven_ = !textureEven_;
}

void CumulatePointsShaderProgram::initializeFramebufAndTexEven() {

  glGenFramebuffers(1, &framebufferEven_);
  glBindFramebuffer(GL_FRAMEBUFFER, framebufferEven_);
  glGenTextures(1, &textureEvenTex_);
  glBindTexture(GL_TEXTURE_2D, textureEvenTex_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, imageWidth_, imageHeight_, 0, GL_RGBA, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureEvenTex_, 0);

  checkFrameBuffer("LocalMaximaShaderProgram::initializeFramebufAndTexEven");
}

void CumulatePointsShaderProgram::initializeFramebufAndTexOdd() {

  glGenFramebuffers(1, &framebufferOdd_);
  glBindFramebuffer(GL_FRAMEBUFFER, framebufferOdd_);
  glGenTextures(1, &textureOddTex_);
  glBindTexture(GL_TEXTURE_2D, textureOddTex_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, imageWidth_, imageHeight_, 0, GL_RGBA, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureOddTex_, 0);

  checkFrameBuffer("LocalMaximaShaderProgram::initializeFramebufAndTexOdd");
}

void CumulatePointsShaderProgram::createUniforms() {

  curPointsImageId_ = shaderManager_.getUniformLocation("imageCur");
  cumulativeId_ = shaderManager_.getUniformLocation("imageCumulative");
}

GLuint CumulatePointsShaderProgram::getLastTex() {
  if(!textureEven_){
     return textureEvenTex_;
   }else{
     return textureOddTex_;
   }
}

void CumulatePointsShaderProgram::collectPointsFromPixels() {

  float curX, curY, curZ, idAndNcc;
  for (int i = 0; i < imageHeight_; ++i) {
    for (int j = 0; j < imageWidth_; ++j) {

      curX = (float) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 0]);
      curY = (float) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 1]);
      curZ = (float) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 2]);
      idAndNcc = (float) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 3]);

      if (curX != -666666.0f && curX != -777777.0f && curX != -666666.0f && (curX != 0.1666666666f || curX != 0.1666666666f || curX != 0.1666666666f)) {
        pointsAndIdNcc_.push_back(glm::vec4(curX, curY, curZ,idAndNcc));

        //DEBUG
//        std::cout << "IN ___ curX: " << curX;
//        std::cout << " ___ curY: " << curY;
//        std::cout << " ___ curZ:" << curZ << std::endl;
      }
    }
  }


}

int CumulatePointsShaderProgram::collectNewPoints(std::vector<glm::vec4> &points) {
  dumpTextureOnCPU();
  collectPointsFromPixels();
  points = pointsAndIdNcc_;
  return points.size();
}
void CumulatePointsShaderProgram::dumpTextureOnCPU() {

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  //glReadBuffer(GL_COLOR_ATTACHMENT0);
  glEnable(GL_TEXTURE_2D);
  if(!textureEven_){
    glBindTexture(GL_TEXTURE_2D, textureEvenTex_);
  }else{
    glBindTexture(GL_TEXTURE_2D, textureOddTex_);
  }

  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ROW_LENGTH, 0);
  glPixelStorei(GL_PACK_SKIP_ROWS, 0);
  glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
  if(!textureEven_){
     glBindTexture(GL_TEXTURE_2D, textureEvenTex_);
   }else{
     glBindTexture(GL_TEXTURE_2D, textureOddTex_);
   }
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, pixels_);
  glDisable(GL_TEXTURE_2D);
}

void CumulatePointsShaderProgram::resetTex() {

  glBindFramebuffer(GL_FRAMEBUFFER, framebufferOdd_);
  glBindTexture(GL_TEXTURE_2D, textureOddTex_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, imageWidth_, imageHeight_, 0, GL_RGBA, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureOddTex_, 0);
  checkFrameBuffer("LocalMaximaShaderProgram::initializeFramebufAndTexOdd");


  glBindFramebuffer(GL_FRAMEBUFFER, framebufferOdd_);
  glBindTexture(GL_TEXTURE_2D, textureOddTex_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, imageWidth_, imageHeight_, 0, GL_RGBA, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureOddTex_, 0);
  checkFrameBuffer("LocalMaximaShaderProgram::initializeFramebufAndTexOdd");
}
