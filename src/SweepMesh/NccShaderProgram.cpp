/*
 * NccShaderProgram.cpp
 *
 *  Created on: 08/apr/2015
 *      Author: andrea
 */

#include "NccShaderProgram.h"
#include <opencv2/imgproc/imgproc.hpp>

NccShaderProgram::NccShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  imWid_ = imHid_ = nccTexId_ = wId_= -1;
  framebuffer_ = image2ReprojTex_ = imageTex_ = -1;
  nccTexId_ = image2ReprojTexId_ = imageTexId_ = -1;

  posId_ = texCoordId_ = -1;

  window_ = 10;

}

NccShaderProgram::NccShaderProgram(int imageWidth, int imageHeight, int window) :
    ShaderProgram(imageWidth, imageHeight) {
  imWid_ = imHid_ = nccTexId_ = wId_= -1;
  framebuffer_ = image2ReprojTex_ = imageTex_ = -1;
  nccTexId_ = image2ReprojTexId_ = imageTexId_ = -1;

  posId_ = texCoordId_ = -1;
  window_ = window;

}
NccShaderProgram::~NccShaderProgram() {
}

void NccShaderProgram::initTex() {
  glGenTextures(1, &imageTex_);
  glBindTexture(GL_TEXTURE_2D, imageTex_);
  defaultTextureParameters();
}

void NccShaderProgram::populateTex(const cv::Mat& image) {

  cv::Mat image2Gray;
  cv::cvtColor(image, image2Gray, CV_RGB2GRAY);
  glBindTexture(GL_TEXTURE_2D, imageTex_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, imageWidth_, imageHeight_, 0, GL_RED, GL_UNSIGNED_BYTE, image2Gray.data);

  image2Gray.release();
}

void NccShaderProgram::initializeFramebufAndTex(GLuint& nccTexId) {
  glGenFramebuffers(1, &framebuffer_);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  glGenTextures(1, &nccTexId);
  glBindTexture(GL_TEXTURE_2D, nccTexId);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, imageWidth_, imageHeight_, 0, GL_RED, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, nccTexId, 0);

  nccTexId_ = nccTexId;
  checkFrameBuffer("NccShaderProgram::initializeFramebufAndTex");

}

void NccShaderProgram::compute(bool renderFrameBuf) {
  if (renderFrameBuf)
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  else
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

  GLuint attachments[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, attachments);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  shaderManager_.enable();

  glUniform1f(imWid_, static_cast<float>(imageWidth_));
  glUniform1f(imHid_, static_cast<float>(imageHeight_));
  glUniform1i(wId_, static_cast<int>(window_));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, imageTex_);
  glUniform1i(imageTexId_, 0);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, image2ReprojTex_);
  glUniform1i(image2ReprojTexId_, 1);

  glEnableVertexAttribArray(posId_);
  glEnableVertexAttribArray(texCoordId_);

  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), 0);
  glVertexAttribPointer(texCoordId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), (const GLvoid*) (2 * sizeof(GLfloat)));

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
  glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);

  glDisableVertexAttribArray(posId_);
  glDisableVertexAttribArray(texCoordId_);

}

void NccShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/ncc_vertex_shader.glsl");
  shaderManager_.addShader(GL_FRAGMENT_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/ncc_fragment_shader.glsl");
  shaderManager_.finalize();
}

void NccShaderProgram::createAttributes() {
  posId_ = shaderManager_.getAttribLocation("position");
  texCoordId_ = shaderManager_.getAttribLocation("texcoord");
}

void NccShaderProgram::createUniforms() {
  imageTexId_ = shaderManager_.getUniformLocation("image1");
  image2ReprojTexId_ = shaderManager_.getUniformLocation("image2Repr");
  imWid_ = shaderManager_.getUniformLocation("imW");
  imHid_ = shaderManager_.getUniformLocation("imH");
  wId_ = shaderManager_.getUniformLocation("window");
}

