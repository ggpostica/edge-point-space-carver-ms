/*
 * LaplacianSmoothShaderProgram.cpp
 *
 *  Created on: 16/apr/2015
 *      Author: andrea
 */

#include "LaplacianSmoothShaderProgram.h"

LaplacianSmoothShaderProgram::LaplacianSmoothShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  posAttrib_ = -1;
  feedbackBuffer_ = feedbackLength_ = -1;

}

LaplacianSmoothShaderProgram::~LaplacianSmoothShaderProgram() {
}

void LaplacianSmoothShaderProgram::compute() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  shaderManager_.enable();

  glEnableVertexAttribArray(posAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posAttrib_, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnable(GL_RASTERIZER_DISCARD);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, arrayBufferObj_);

  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackBuffer_);
  glBeginTransformFeedback(GL_TRIANGLES);

  if (useElements_Indices) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
    glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);

  } else {

    glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
    glDrawArrays(GL_TRIANGLES, 0, sizeArray_);

  }
  glEndTransformFeedback();glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
  glDisableVertexAttribArray(posAttrib_);

  glFinish();
  glDisable(GL_RASTERIZER_DISCARD);

  if (useElements_Indices) {
    feedbackTr.resize(numElements_, glm::vec3(0.0));
    feedbackLength_ = numElements_;

  } else {
    feedbackTr.resize(sizeArray_, glm::vec3(0.0));
    feedbackLength_ = sizeArray_;

  }


  glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackLength_ * sizeof(glm::vec3), &feedbackTr[0]);

  /*for(auto f : feedbackTr){
    std::cout<<"feeeeeed" <<f.x<< " "<<f.y<<" "<<f.z<<std::endl;
  }*/

}

void LaplacianSmoothShaderProgram::createTransformFeedback(int length) {
  glGenBuffers(1, &feedbackBuffer_);
  glBindBuffer(GL_ARRAY_BUFFER, feedbackBuffer_);
  glBufferData(GL_ARRAY_BUFFER, length * sizeof(glm::vec3), nullptr, GL_DYNAMIC_READ);
}

void LaplacianSmoothShaderProgram::init() {

  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/umbrella_vertex_shader.glsl");
  shaderManager_.addShader(GL_GEOMETRY_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/cotangent_geometry_shader.glsl");
  shaderManager_.addFeedbackTransform("laplacianValue");
  shaderManager_.finalize();
}

void LaplacianSmoothShaderProgram::createAttributes() {
  posAttrib_ = shaderManager_.getAttribLocation("position");
}

void LaplacianSmoothShaderProgram::createUniforms() {
}
