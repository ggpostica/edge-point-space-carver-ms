/*
 * CollectPointsShaderProgram.cpp
 *
 *  Created on: 13/apr/2015
 *      Author: andrea
 */

#include "CollectPointsShaderProgram.h"

CollectPointsShaderProgram::CollectPointsShaderProgram(int imageWidth, int imageHeight) :
ShaderProgram(imageWidth, imageHeight) {
  imWid_ = imHid_ = imageTexId_ = thId_ = wId_ = nccTexId_ = idTexId_ = -1;
  /*tex id*/
  imageTex_ = -1;
  /*attributes id*/
  posId_ = texCoordId_ = nccTex_ = idTex_ = endIdTex_ = framebuffer_ =  -1;

  feedbackBuffer_ = feedbackLen_ = -1;
}

CollectPointsShaderProgram::~CollectPointsShaderProgram() {
}

void CollectPointsShaderProgram::createTransformFeedback(int length) {
  glGenBuffers(1, &feedbackBuffer_);
  glBindBuffer(GL_ARRAY_BUFFER, feedbackBuffer_);
  glBufferData(GL_ARRAY_BUFFER, length * sizeof(glm::vec3), nullptr, GL_DYNAMIC_READ);
  feedbackLen_ = length;
}

void CollectPointsShaderProgram::compute(bool renderFrameBuf) {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  feedbackTrPoints_.assign(feedbackLen_, glm::vec3(0.0));

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  shaderManager_.enable();

  glUniform1f(imWid_, static_cast<float>(imageWidth_));
  glUniform1f(imHid_, static_cast<float>(imageHeight_));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, imageTex_);
  glUniform1i(imageTexId_, 0);

  glEnableVertexAttribArray(posId_);
  glEnableVertexAttribArray(texCoordId_);

  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), 0);
  glEnable(GL_RASTERIZER_DISCARD);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, arrayBufferObj_);

  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackBuffer_);/*addd*/

  glBeginTransformFeedback(GL_POINTS);

  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glDrawArrays(GL_POINTS, 0, sizeArray_);

  glEndTransformFeedback();

  glDisableVertexAttribArray(posId_);
  glFinish();
  glDisable(GL_RASTERIZER_DISCARD);
  glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackLen_ * sizeof(glm::vec3), &feedbackTrPoints_[0]);


}

void CollectPointsShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/collect_points_vertex_shader.glsl");
  shaderManager_.addShader(GL_GEOMETRY_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/collect_points_geometry_shader.glsl");
  shaderManager_.addFeedbackTransform("point");
  shaderManager_.finalize();
}

void CollectPointsShaderProgram::createAttributes() {
  posId_ = shaderManager_.getAttribLocation("position");
}

void CollectPointsShaderProgram::initializeFramebufAndTex(GLuint& texId) {
glGenFramebuffers(1, &framebuffer_);
glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
glGenTextures(1, &texId);
glBindTexture(GL_TEXTURE_2D, texId);
glTexImage2D(GL_TEXTURE_1D, 0, GL_RGB32F, imageWidth_, imageHeight_, 0, GL_RGB, GL_FLOAT, nullptr);
defaultTextureParameters();
checkFrameBuffer("CollectPointsShaderProgram::initializeFramebufAndTex");

endIdTex_ = texId;
}

void CollectPointsShaderProgram::createUniforms() {
  imageTexId_ = shaderManager_.getUniformLocation("image");
  imWid_ = shaderManager_.getUniformLocation("imW");
  imHid_ = shaderManager_.getUniformLocation("imH");
}
