/*
 * LocalMaximaShaderProgram.cpp
 *
 *  Created on: 08/apr/2015
 *      Author: andrea
 */

#include "LocalMaximaShaderProgram.h"
#include <opencv2/imgproc/imgproc.hpp>

LocalMaximaShaderProgram::LocalMaximaShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  imWid_ = imHid_ = nonMaximaTexId_ = thId_ = wId_ = -1;
  framebuffer_ = -1;
  nonMaximaTexId_ = imageNCCTexId_ = imageNCCTex_ = -1;

  posId_ = texCoordId_ = imageTex_ = -1;

  threshold_ = 0.9;
  window_ = 10;

}
LocalMaximaShaderProgram::LocalMaximaShaderProgram(int imageWidth, int imageHeight, float threshold) :
    ShaderProgram(imageWidth, imageHeight) {
  imWid_ = imHid_ = nonMaximaTexId_ = thId_ = wId_ = -1;
  framebuffer_ = -1;
  nonMaximaTexId_ = imageNCCTexId_ = imageNCCTex_ = -1;

  posId_ = texCoordId_ = imageTex_ = -1;

  threshold_ = threshold;
  window_ = 10;
}

LocalMaximaShaderProgram::LocalMaximaShaderProgram(int imageWidth, int imageHeight, float threshold, int window) :
    ShaderProgram(imageWidth, imageHeight) {
  imWid_ = imHid_ = nonMaximaTexId_ = thId_ = wId_ = -1;
  framebuffer_ = -1;
  nonMaximaTexId_ = imageNCCTexId_ = imageNCCTex_ = -1;

  posId_ = texCoordId_ = imageTex_ = -1;

  threshold_ = threshold;
  window_ = window;

}

LocalMaximaShaderProgram::~LocalMaximaShaderProgram() {
}

void LocalMaximaShaderProgram::resetTex() {
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  glBindTexture(GL_TEXTURE_2D, nonMaximaTexId_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, imageWidth_, imageHeight_, 0, GL_RED, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, nonMaximaTexId_, 0);

}
void LocalMaximaShaderProgram::initializeFramebufAndTex(GLuint& texId) {
  glGenFramebuffers(1, &framebuffer_);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  glGenTextures(1, &texId);
  glBindTexture(GL_TEXTURE_2D, texId);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, imageWidth_, imageHeight_, 0, GL_RED, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texId, 0);

  nonMaximaTexId_ = texId;
  checkFrameBuffer("LocalMaximaShaderProgram::initializeFramebufAndTex");
}

void LocalMaximaShaderProgram::compute(bool renderFrameBuf) {
  if (renderFrameBuf)
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  else
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

  GLuint attachments[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, attachments);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  shaderManager_.enable();

  glUniform1f(imWid_, static_cast<float>(imageWidth_));
  glUniform1f(imHid_, static_cast<float>(imageHeight_));
  glUniform1i(wId_, static_cast<int>(window_));
  glUniform1f(thId_, static_cast<float>(threshold_));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, imageNCCTex_);
  glUniform1i(imageNCCTexId_, 0);

  glEnableVertexAttribArray(posId_);
  glEnableVertexAttribArray(texCoordId_);

  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), 0);
  glVertexAttribPointer(texCoordId_, 2, GL_FLOAT, GL_FALSE, sizeArray_ * sizeof(GLfloat), (const GLvoid*) (2 * sizeof(GLfloat)));

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
  glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);

  glDisableVertexAttribArray(posId_);
  glDisableVertexAttribArray(texCoordId_);
}

void LocalMaximaShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/local_maxima_vertex_shader.glsl");
  shaderManager_.addShader(GL_FRAGMENT_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/local_maxima_fragment_shader.glsl");
  shaderManager_.finalize();
}

void LocalMaximaShaderProgram::createAttributes() {
  posId_ = shaderManager_.getAttribLocation("position");
  texCoordId_ = shaderManager_.getAttribLocation("texcoord");
}

void LocalMaximaShaderProgram::createUniforms() {
  imageNCCTexId_ = shaderManager_.getUniformLocation("imageNCC");
  imWid_ = shaderManager_.getUniformLocation("imW");
  imHid_ = shaderManager_.getUniformLocation("imH");
  wId_ = shaderManager_.getUniformLocation("W");
  thId_ = shaderManager_.getUniformLocation("th");
}

