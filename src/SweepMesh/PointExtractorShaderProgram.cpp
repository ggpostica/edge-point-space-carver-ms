/*
 * PointExtractorShaderProgram.cpp
 *
 *  Created on: 08/apr/2015
 *      Author: andrea
 */

#include "PointExtractorShaderProgram.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

PointExtractorShaderProgram::PointExtractorShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  pointsExtractedTexId_ = imagePointsKept_ = -1;
  framebuffer_ = imageTexId_ = depthTexture_ = 1;
  pointsExtractedTexId_ = shadowMapId_ = mvpId_ = -1;

  normAttrib_ = normalBuffer_ = idBuffer_ = -1;
  alphaId_ = -1;
  alpha_ = idAttrib_ = thId_ = -1;
  posId_ = nccTex_ = -1;
  camCenterID_ = tmpdeptht = -1;
  ch_ = 4;
  imWId_ = -1;
  imHId_ = -1;
  imH_ = static_cast<float>(imageHeight);
  imW_ = static_cast<float>(imageWidth);
  th_ = -1.0f;
  pixels_ = new GLfloat[imageWidth * imageHeight * ch_];
  frag_sh_ = "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/point_best_extractor_fragment_shader.glsl";
  vert_sh_ = "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/point_best_extractor_vertex_shader.glsl";

}

PointExtractorShaderProgram::PointExtractorShaderProgram(int imageWidth, int imageHeight, std::string vert_sh, std::string frag_sh) :
    ShaderProgram(imageWidth, imageHeight) {
  pointsExtractedTexId_ = imagePointsKept_ = -1;
  framebuffer_ = imageTexId_ = depthTexture_ = 1;
  pointsExtractedTexId_ = shadowMapId_ = mvpId_ = -1;


  imH_ = static_cast<float>(imageHeight);
  imW_ = static_cast<float>(imageWidth);
  normAttrib_ = normalBuffer_ = idBuffer_ = -1;
  alphaId_ = -1;
  alpha_ = idAttrib_ = thId_ = -1;
  posId_ = nccTex_ = -1;
  imWId_ = -1;
  imHId_ = -1;
  camCenterID_ = tmpdeptht = -1;
  ch_ = 4;
  th_ = -1.0f;
  pixels_ = new GLfloat[imageWidth * imageHeight * ch_];
  vert_sh_ = vert_sh;
  frag_sh_ = frag_sh;

}
PointExtractorShaderProgram::~PointExtractorShaderProgram() {
  delete (pixels_);
}

void PointExtractorShaderProgram::resetTex() {
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  glBindTexture(GL_TEXTURE_2D, pointsExtractedTexId_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, imageWidth_, imageHeight_, 0, GL_RGBA, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, pointsExtractedTexId_, 0);
  /*glGenTextures(1, &texId2);
   glBindTexture(GL_TEXTURE_2D, texId2);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, imageWidth_, imageHeight_, 0, GL_RGBA, GL_FLOAT, nullptr);
   defaultTextureParameters();
   glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, texId2, 0);*/
  glBindTexture(GL_TEXTURE_2D, tmpdeptht);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, imageWidth_, imageHeight_, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tmpdeptht, 0);
  checkFrameBuffer("PointExtractorShaderProgram::initializeFramebufAndTex");

}

void PointExtractorShaderProgram::initializeFramebufAndTex(GLuint& texId) {
  //GLuint texId2;
  glGenFramebuffers(1, &framebuffer_);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  glGenTextures(1, &texId);
  glBindTexture(GL_TEXTURE_2D, texId);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, imageWidth_, imageHeight_, 0, GL_RGBA, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texId, 0);
  /*glGenTextures(1, &texId2);
   glBindTexture(GL_TEXTURE_2D, texId2);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, imageWidth_, imageHeight_, 0, GL_RGBA, GL_FLOAT, nullptr);
   defaultTextureParameters();
   glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, texId2, 0);*/
  initDepthTex(tmpdeptht);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tmpdeptht, 0);
  checkFrameBuffer("PointExtractorShaderProgram::initializeFramebufAndTex");

  pointsExtractedTexId_ = texId;
  //nccTex_ = texId2;
}

void PointExtractorShaderProgram::compute(bool renderFrameBuf) {
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  if (renderFrameBuf) {
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  } else {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT };
  glDrawBuffers(2, attachments);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  shaderManager_.enable();

  glUniformMatrix4fv(mvpId_, 1, GL_FALSE, &mvp_[0][0]);
  glUniform3fv(camCenterID_, 1, &camCenter_[0]);
  glUniform1f(alphaId_, alpha_);
  glUniform1f(thId_, th_);
  glUniform1f(imWId_, imW_);
  glUniform1f(imHId_, imH_);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, depthTexture_);
  glUniform1i(shadowMapId_, 0);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, imagePointsKept_);
  glUniform1i(imageTexId_, 1);

  glEnableVertexAttribArray(posId_);
  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(normAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, normalBuffer_);
  glVertexAttribPointer(normAttrib_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(idAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, idBuffer_);
  glVertexAttribPointer(idAttrib_, 1, GL_FLOAT, GL_FALSE, 0, 0);

  if (useElements_Indices) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
    glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);
  } else {
    glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
    glDrawArrays(GL_TRIANGLES, 0, sizeArray_);
  }

  glDisableVertexAttribArray(posId_);
  glDisableVertexAttribArray(normAttrib_);
  glDisableVertexAttribArray(idAttrib_);

}

int PointExtractorShaderProgram::collectNewPoints(std::vector<glm::vec4> &points) {
  dumpTextureOnCPU();
  collectPointsFromPixels();
  points = pointsAndIdNcc_;
  return points.size();
}
void PointExtractorShaderProgram::collectNewPoints() {
  dumpTextureOnCPU();
  collectPointsFromPixels();
}

void PointExtractorShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, vert_sh_);
  shaderManager_.addShader(GL_GEOMETRY_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/point_extractor_geometry_shader.glsl");
  shaderManager_.addShader(GL_FRAGMENT_SHADER, frag_sh_);
  shaderManager_.finalize();
}

void PointExtractorShaderProgram::createAttributes() {
  posId_ = shaderManager_.getAttribLocation("position");
  normAttrib_ = shaderManager_.getAttribLocation("normal");
  idAttrib_ = shaderManager_.getAttribLocation("id");
}

void PointExtractorShaderProgram::createUniforms() {
  mvpId_ = shaderManager_.getUniformLocation("MVP");
  imageTexId_ = shaderManager_.getUniformLocation("imageMaxima");
  shadowMapId_ = shaderManager_.getUniformLocation("shadowMap");
  alphaId_ = shaderManager_.getUniformLocation("alpha");
  thId_ = shaderManager_.getUniformLocation("th");
  imHId_ = shaderManager_.getUniformLocation("imH");
  imWId_ = shaderManager_.getUniformLocation("imW");
  camCenterID_ = shaderManager_.getUniformLocation("camCenter");
}

void PointExtractorShaderProgram::dumpTextureOnCPU() {
  GLint width, height, depth;

  logger.disable();
  logger.startEvent("from GPU to CPU");
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  //glReadBuffer(GL_COLOR_ATTACHMENT0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, pointsExtractedTexId_);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_RED_SIZE, &depth);

  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ROW_LENGTH, 0);
  glPixelStorei(GL_PACK_SKIP_ROWS, 0);
  glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
  glBindTexture(GL_TEXTURE_2D, pointsExtractedTexId_);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, pixels_);
  glDisable(GL_TEXTURE_2D);
  logger.endEventAndPrint();
}

void PointExtractorShaderProgram::collectPointsFromPixels() {
  pointsAndIdNcc_.clear();
  logger.disable();
  logger.startEvent("Collect from images");
  float curX, curY, curZ, idAndNcc;
  for (int i = 0; i < imageHeight_; ++i) {
    for (int j = 0; j < imageWidth_; ++j) {

      curX = (float) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 0]);
      curY = (float) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 1]);
      curZ = (float) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 2]);
      idAndNcc = (float) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 3]);

      if (curX != -666666.0f && curX != -777777.0f && curX != -666666.0f && (curX != 0.0f || curY != 0.0f || curZ != 0.0f)) {
        pointsAndIdNcc_.push_back(glm::vec4(curX, curY, curZ, idAndNcc));

        //DEBUG
//        std::cout << "IN ___ curX: " << curX;
//        std::cout << " ___ curY: " << curY;
//        std::cout << " ___ curZ:" << curZ ;
//        std::cout << " ___ nccAndId:" << idAndNcc << std::endl;
      }
    }
  }

  logger.endEventAndPrint();

}

void PointExtractorShaderProgram::collectPointsNccIdFromPixels() {

  points_.clear();
  curNcc_.clear();
  curIds_.clear();

  logger.startEvent("Collect from images");
  GLfloat curX, curY, curZ, nccAndId, ncc;
  GLuint id;
  for (int i = 0; i < imageHeight_; ++i) {
    for (int j = 0; j < imageWidth_; ++j) {

      curX = (GLfloat) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 0]);
      curY = (GLfloat) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 1]);
      curZ = (GLfloat) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 2]);
      nccAndId = (GLfloat) (pixels_[(imageHeight_ - i - 1) * ch_ * imageWidth_ + j * ch_ + 3]);

      if (curX != -666666.0f && curX != -777777.0f && curX != -666666.0f && (curX != 0.1666666666f || curX != 0.1666666666f || curX != 0.1666666666f)) {
        points_.push_back(glm::vec3(curX, curY, curZ));
        ncc = nccAndId - glm::trunc(nccAndId);
        id = GLuint(glm::trunc(nccAndId));

        curNcc_.push_back(ncc);
        curIds_.push_back(static_cast<GLuint>(id));
        //DEBUG
       /* std::cout << "IN ___ curX: " << curX;
         std::cout << " ___ curY: " << curY;
         std::cout << " ___ curZ:" << curZ;
         std::cout << " ___ nccAndId:" << nccAndId;
         std::cout << " ___ ncc:" << ncc;
         std::cout << " ___ ID:" << id << std::endl;*/
      }
    }
  }

  logger.endEventAndPrint();
}
