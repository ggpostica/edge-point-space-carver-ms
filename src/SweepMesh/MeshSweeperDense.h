/*
 * MeshSweeper.h
 *
 *  Created on: 01/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESHDENSE_MESHSWEEPER_H_
#define SWEEPMESHDENSE_MESHSWEEPER_H_

#include "../OpenGLWrapper/OpenGLProgram.h"
#include "../types.hpp"
#include "Mesh.h"
#include "../cam_parsers/CamParser.h"
#include "SweepShaderProgram.h"
#include "DepthShaderProgram.h"
#include "NormalShaderProgram.h"
#include "ReprojectionShaderProgram.h"
#include "NccShaderProgram.h"
#include "LocalMaximaShaderProgram.h"
#include "FilterImageShaderProgram.h"
#include "PointExtractorShaderProgram.h"
#include "LaplacianSmoothShaderProgram.h"
#include "ShaderProgram.h"
#include "../OpenGLWrapper/TransformationController.h"
#include "PointsCollectorPerPixel.h"
#include "../utilities/Logger.h"

class MeshSweeperDense : public OpenGLProgram {
public:
  MeshSweeperDense(SweepConfiguration myConf, const bool loadMesh = true);
  virtual ~MeshSweeperDense();

  void computeStainerPoints(std::vector<glm::vec3> &points, std::vector<std::vector<int> >  &vis);
  void run();
  void run(int curIter);  // implements code
  void restartWithNewMesh(const Mesh& mesh);

  //void simpleMeshUpdate();

  const std::vector<glm::vec3>& getNewPoints() const {
    return newPoints_;
  }

  void setMesh(const Mesh& mesh) {
    mesh_ = mesh;
  }

  const std::vector<std::vector<int> >& getVis() const {
    return vis_;
  }

  const std::vector<glm::vec3>& getFilteredPoints() const {
    return filteredPoints_;
  }

private:
  void initialize();
  void initShaders();
  void createVertexArrayBuffer();
  void resetVertexArrayBuffer();
  void refreshVertexArrayBuffer();
  void createNormalArrayBuffer();
  void createIdArrayBuffer();
  void resetIdArrayBuffer();
  void createPixelImageArrayBuffer();
  void createIdUniqueArrayBuffer();
  void createImageVerticesBuffer();
  glm::mat4 setCameraParamAndGetMvp(const CameraType &cam);
  void loadImages();

  void showLastImagesWithPoints(cv::Mat &image1, cv::Mat &image2, std::vector<glm::vec4> &points, const glm::mat4 &cam1, const glm::mat4 &cam2);
  void saveLastImagesWithPoints(cv::Mat &image1, std::vector<glm::vec4> &points, const glm::mat4 &cam2, std::string path);


  utilities::Logger logger;
  bool loadMesh_;
  Mesh mesh_;
  std::vector<cv::Mat> images_;
  CamParser *camParser_;
  TransformationController *cameraTransformationController_;
  SweepConfiguration myConf_;
  //***************shaders*******************************************
  ShaderProgram *depthProgram_;
  ShaderProgram *reprojProgram_;
  ShaderProgram *sweepProgram_;
  NormalShaderProgram *normalProgram_;
  NccShaderProgram *nccProgram_;
  ShaderProgram *localMaxProgram_;
  ShaderProgram *medianImageProgram_;
  ShaderProgram *gaussFilterImageProgram_;
  ShaderProgram *pointExtractorProgram_;
  ShaderProgram *collectPointsProgram_;
  ShaderProgram *collectLocalMaximaProgram_;
  ShaderProgram *cumulateProgram_;
  ShaderProgram *cumulateBestProgram_;
  LaplacianSmoothShaderProgram *laplacianSmootherProgram_;

  PointsCollectorPerPixel *pColl;

  //***************shaders variables*********************************
  GLuint vertexBufferObj_, normalBufferObj_, imageArrayBufferObj_, imageElemBufferObj_, iDArrayBufferObj_, iDUniqueArrayBufferObj_, pixelImageArrayBufferObj_;
  GLuint framebufferDepth_, framebufferDepth2_;
  GLuint depthTexture_, depthTexture2_;
  GLuint reprojTex_, nccTex_, imageFiltered2id_, imageFilteredId_, localMaxTex_, pointsTex_, nccPointsTex_, idTex_, end1DTex_;

  //*****************New Points vars********************************
  std::vector<glm::vec3> newPoints_;
  std::vector<glm::vec3> filteredPoints_;
  std::vector<std::vector<int> > vis_;
  std::vector<glm::vec4> curPandNccId_;
  std::vector<GLfloat> ncc_;
  std::vector<GLuint> ids_;

  //*****************Sweeping vars********************************
  float coeff_;

  float numPlanes_;

};

#endif /* SWEEPMESHDENSE_MESHSWEEPER_H_ */
