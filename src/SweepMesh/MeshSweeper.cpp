/*
 * MeshSweeper.cpp
 *
 *  Created on: 01/apr/2015
 *      Author: andrea
 */

#include "MeshSweeper.h"

#include "../utilities/utilities.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <fstream>
#include <iostream>
#include "PointExtractorShaderProgram.h"
#include "CollectPointsShaderProgram.h"
#include "CollectLocalMaximaShaderProgram.h"
#include "LaplacianSmoothShaderProgram.h"
#include "CumulatePointsShaderProgram.h"
#include "DepthMapFromMeshShaderProgram.h"
#include <glm.hpp>
#include <random>
#include <bits/random.h>

MeshSweeper::MeshSweeper(SweepConfiguration myConf, const bool loadMesh) {
  myConf_ = myConf;
  loadMesh_ = loadMesh;
  cleanMesh_ = false;
  if (loadMesh) {
    mesh_.loadFormat(myConf_.sweepConfig.pathInitMesh.c_str(), false);
    mesh_.removeSelfIntersections();
  }
  initialize();

  std::size_t i = 0;
  for (Vertex_iterator it = mesh_.p.vertices_begin(); it != mesh_.p.vertices_end(); ++it) {
    it->id = i++;
  }
}

MeshSweeper::~MeshSweeper() {
  delete (depthProgram_);
  delete (reprojProgram_);
  delete (normalProgram_);
  delete (sweepProgram_);
  delete (camParser_);
  delete (cameraTransformationController_);
}

void MeshSweeper::run() {
  run(0);
}
void MeshSweeper::run(int curIter) {
  cv::Mat image1, image2;
  glm::mat4 mvp1, mvp2;

  logger.disable();
  logger.resetOutputPercision();
  std::stringstream s1, s2;
  logger.startEvent();
  if (cleanMesh_ == true) {
    // mesh_.removeSelfIntersections();
    //mesh_.ensureEdgeSizes();
    // mesh_.ensureEdgeSizes(0.01, mesh_.edge_max, 0.3, 160, 10);
    //mesh_.ensureEdgeSizes(0.005, 0.30, 0.3, 160, 10);
  }

  s2 << myConf_.outputSweep.pathMesh << "NotSmoothBeforeIter" << curIter << ".off";
  mesh_.saveFormat(s2.str().c_str());
  mesh_.loadFormat(s2.str().c_str(), false);  //workaround to remove unref vertices
  resetMeshInfo();

  if (curIter % 3 == 0) {
    //myConf_.sweepConfig.windowNCC -= 1;
  }
  pColl->resetPoints(mesh_.p.size_of_facets());

  logger.endEventAndPrint("Smooth done  ", false);

  std::random_device rd;
  std::mt19937 gen(rd());
  float curCoeff;
  do {
    std::normal_distribution<float> d(coeff_, 0.25 * coeff_);
    curCoeff =  d(gen);
  } while (curCoeff < 0);

  std::cout<<"coeff_"<<curCoeff<<std::endl;

  for (int idx1 = 0; idx1 < camParser_->getNumCameras(); ++idx1) {
  //int idx1 = 3;
  //int idx2 = 5;
  //if (true) {
    std::cout << "**Compare Image " << idx1 << " with: " << std::flush;

    mvp1 = setCameraParamAndGetMvp(camParser_->getCamerasList()[idx1]);
    glm::vec3 curCenter = cameraTransformationController_->getCameraCenter();
   // std::vector<int> bufferIdx;

    //int myints[] = { 2, 5, 3, 4, 1, 6, 7 };
    //int myints[] = { 2, 7, 3, 4, 1, 6, 7 };
    int myints[11][2] = { {2,6}, {5,7}, {3,0}, {2,4}, {3,5}, {4,1}, {0,9}, {8,1}, {7,1}, {10,6}, {9,6} };
//    for (int idx = 0; idx < (int) camParser_->getCamerasList().size(); ++idx) {
//      if (abs(idx1 - idx) < 4 || abs(camParser_->getCamerasList().size() - idx + idx1) < 4) {
//        bufferIdx.push_back(idx); } }
//    for (int idx2 : bufferIdx) {

    for (int idx2 = 0; idx2 < camParser_->getNumCameras(); ++idx2) {
    //for (int i = 0; i<2;i++){
    //int idx2 = myints[idx1][i];
//    if (true) {

      if (idx1 != idx2) {
        mvp2 = setCameraParamAndGetMvp(camParser_->getCamerasList()[idx2]);
        image1 = images_[idx1];
        image2 = images_[idx2];

        cv::GaussianBlur(images_[idx1], image1, cv::Size(9, 9), 7.0);
        cv::GaussianBlur(images_[idx2], image2, cv::Size(9, 9), 7.0);

        //************************facet normals*************************
        logger.startEvent();
        normalProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);

        normalProgram_->setUseElementsIndices(false);
        normalProgram_->compute();
        logger.endEventAndPrint("facet normals computation ", false);

        createNormalArrayBuffer();

        for (float curAl = -curCoeff * (numPlanes_); curAl < +curCoeff * (numPlanes_ + 1); curAl += curCoeff * 1.0f) {
          logger.startEvent();
          // std::cout<<"Cur Plane distance from base mesh: "<<curAl<<std::endl;
          //************************depth************************
          logger.startEvent();
          depthProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
          depthProgram_->setUseElementsIndices(false);
          static_cast<DepthShaderProgram *>(depthProgram_)->setAlpha(curAl);
          static_cast<DepthShaderProgram *>(depthProgram_)->setCamCenter(curCenter);
          static_cast<DepthShaderProgram *>(depthProgram_)->setNormalBuffer(normalBufferObj_);
          static_cast<DepthShaderProgram *>(depthProgram_)->computeDepthMap(framebufferDepth_, mvp1);
          static_cast<DepthShaderProgram *>(depthProgram_)->setAlpha(curAl);
          static_cast<DepthShaderProgram *>(depthProgram_)->computeDepthMap(framebufferDepth2_, mvp2);
          glFinish();
          logger.endEventAndPrint("(1) Depth computation  ", false);

          //************************reprojection**************************
          logger.startEvent();
          reprojProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
          reprojProgram_->setUseElementsIndices(false);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setAlpha(curAl);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setCamCenter(curCenter);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setNormalBuffer(normalBufferObj_);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setDepthTexture(depthTexture_, depthTexture2_);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setMvp(mvp1, mvp2);
          reprojProgram_->populateTex(image2);
          reprojProgram_->compute(true);
          glFinish();
          logger.endEventAndPrint(" (2) Reprojection  ", false);

          //************************NCC***********************************
          logger.startEvent();
          nccProgram_->setArrayBufferObj(imageArrayBufferObj_, 4);
          nccProgram_->setElementsBufferObj(imageElemBufferObj_, 6);
          nccProgram_->setUseElementsIndices(true);
          static_cast<NccShaderProgram *>(nccProgram_)->setImage2ReprojTex(reprojTex_);
          nccProgram_->populateTex(image1);
          static_cast<NccShaderProgram *>(nccProgram_)->setWindow(myConf_.sweepConfig.windowNCC);

          nccProgram_->compute(true);
          glFinish();
          logger.endEventAndPrint(" (3) NCC ", true);

          //************************localMaxima*****************************
          logger.startEvent();
          localMaxProgram_->setArrayBufferObj(imageArrayBufferObj_, 4);
          localMaxProgram_->setElementsBufferObj(imageElemBufferObj_, 6);
          localMaxProgram_->setUseElementsIndices(true);
          static_cast<LocalMaximaShaderProgram *>(localMaxProgram_)->resetTex();
          static_cast<LocalMaximaShaderProgram *>(localMaxProgram_)->setImageNccTex(nccTex_);
          static_cast<LocalMaximaShaderProgram *>(localMaxProgram_)->setThreshold(myConf_.sweepConfig.thresholdNCC);
          static_cast<LocalMaximaShaderProgram *>(localMaxProgram_)->setWindow(myConf_.sweepConfig.windowLocalMaxima);
          localMaxProgram_->compute(true);
          glFinish();
          logger.endEventAndPrint(" (4) localMaxima ", false);

          //************************pointsExtract***************************
          logger.startEvent();
          pointExtractorProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
          pointExtractorProgram_->setUseElementsIndices(false);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->resetTex();
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setAlpha(curAl);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setCamCenter(curCenter);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setNormalBuffer(normalBufferObj_);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setIdBuffer(iDArrayBufferObj_);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setDepthTexture(depthTexture_);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setMvp(mvp1);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setImagePointsKept(localMaxTex_);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setTh(myConf_.sweepConfig.thresholdNCC);
          pointExtractorProgram_->compute(true);
          glFinish();
          logger.endEventAndPrint(" (5) ExtractPoints ", false);

          logger.startEvent();
          curPandNccId_.clear();
          vector<glm::vec4>().swap(curPandNccId_);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->collectNewPoints(curPandNccId_);
          pColl->addPointsNccIds(curPandNccId_, idx1, idx2);

          logger.endEventAndPrint(" (6) pointsExtract->collectNewPoints ", false);
          //
          SwapBuffers();
          logger.endEventAndPrint(" (7) sweep iteration  ", true);
          //sleep(3.0);

        }  //iterate over planes

      }  //if(idx1 != idx2) end
      std::cout << " image " << idx2 << " done (active points " << pColl->getActivefilteredPoints().size() << "); " << std::flush;
    }  //iterate on (idx2) end
    std::cout << std::endl;
  }  //iterate on (idx1) end

  std::stringstream spt;

  spt << myConf_.outputSweep.pathMesh << "NewPointsAfterIter" << curIter << ".obj";
  std::ofstream newpointsObj(spt.str());
  for (auto p : pColl->getActivefilteredPoints()) {
    newpointsObj << "v " << p.x << " " << p.y << " " << p.z << std::endl;
  }
  newpointsObj.close();

  filteredPoints_.clear();
  vis_.clear();
  curKept_.clear();

  vis_ = pColl->getActiveVisibility();
  curKept_ = pColl->getKept();
  filteredPoints_ = pColl->getActivefilteredPoints();

  std::cout << "Num. Points found: " << filteredPoints_.size() << std::endl;

  image1.release();
  image2.release();
}

void MeshSweeper::smoothMesh() {

  //mesh_.smooth(0.5, 2);
  mesh_.smooth(0.4, 0);
  mesh_.smooth(0.4, 0);
  mesh_.smooth(0.4, 0);
  //mesh_.smooth(0.4, 0);
  resetMeshInfo();
}
void MeshSweeper::printPoint(std::string message, Vertex_handle v) {
  std::cout << message << ": x = " << v->point().x();
  std::cout << ": y = " << v->point().y();
  std::cout << ": z = " << v->point().z() << std::endl;
}
void MeshSweeper::updateWithCurPoints() {
  int curId = 0;
  std::vector<Facet_handle> facetstoBeRemoved;
  std::vector<glm::vec3> pointsToBeAdded;

  mesh_.saveFormat("before.off");
  mesh_.loadFormat("before.off", false);
  filteredPoints_ = pColl->getFilteredPoints();
  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
    if (curKept_[curId] == true) {
      facetstoBeRemoved.push_back(itFac);
      pointsToBeAdded.push_back(filteredPoints_[curId]);
    }
    curId++;
  }

  for (int curIdx = 0; curIdx < facetstoBeRemoved.size(); ++curIdx) {
    Halfedge_handle h0, h1, h2;
    Vertex_handle v0;
    h0 = facetstoBeRemoved[curIdx]->halfedge();
    h1 = h0->next();
    h2 = h1->next();
    mesh_.p.make_hole(h0);

    Halfedge_handle h = mesh_.p.add_vertex_and_facet_to_border(h0, h1);

    v0 = h->vertex();
    v0->point() = Point_d(pointsToBeAdded[curIdx].x, pointsToBeAdded[curIdx].y, pointsToBeAdded[curIdx].z);
    Halfedge_handle hb = mesh_.p.add_facet_to_border(h->opposite()->next(), h->opposite()->prev());
    Halfedge_handle hc = mesh_.p.add_facet_to_border(hb->opposite()->next(), hb->opposite());
  }

  //mesh_.saveFormat("aftera.off");
  //mesh_.removeSelfIntersections();
  /* mesh_.loadFormat("aftera.off", false);
   std::cout<<"loadFormat performed"<<std::endl;*/
  mesh_.smooth(0.2, 2);
  mesh_.smooth(0.1, 0);
  resetMeshInfo();
}

void MeshSweeper::restartWithNewMesh(const Mesh& mesh) {

  mesh_.clear();
  mesh_ = mesh;
  // mesh_.removeSelfIntersections();

  resetMeshInfo();
}

void MeshSweeper::createDepthMap(int idxCam) {

  glm::mat4 mvp1 = setCameraParamAndGetMvp(camParser_->getCamerasList()[idxCam]);
  glm::vec3 curCenter = cameraTransformationController_->getCameraCenter();

  //************************depth************************
  depthProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
  depthProgram_->setUseElementsIndices(false);
  static_cast<DepthShaderProgram *>(depthProgram_)->setAlpha(0.0);
  static_cast<DepthShaderProgram *>(depthProgram_)->setCamCenter(curCenter);
  static_cast<DepthShaderProgram *>(depthProgram_)->computeDepthMap(framebufferDepth_, mvp1);
  //************************depthForGT**************************
  depthMapForGT_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
  depthMapForGT_->setUseElementsIndices(false);
  static_cast<DepthMapFromMeshShaderProgram *>(depthMapForGT_)->setCameraCenter(curCenter);
  static_cast<DepthMapFromMeshShaderProgram *>(depthMapForGT_)->setDepthTexture(depthTexture_);
  static_cast<DepthMapFromMeshShaderProgram *>(depthMapForGT_)->setMvp(mvp1);
  static_cast<DepthMapFromMeshShaderProgram *>(depthMapForGT_)->compute(true);
  SwapBuffers();
}

void MeshSweeper::compareDepthMap(cv::Mat &gtDepth) {
  std::ofstream ss;
  compareDepthMap(gtDepth, "", ss);
}

void MeshSweeper::compareDepthMap(cv::Mat &gtDepth, const std::string postfix, std::ofstream &fileLog) {

  cv::Mat errImage = cv::Mat(imageHeight_, imageWidth_, CV_8UC3);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, compareDepthTex_);

  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ROW_LENGTH, 0);
  glPixelStorei(GL_PACK_SKIP_ROWS, 0);
  glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
  glBindTexture(GL_TEXTURE_2D, compareDepthTex_);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, pixels_);
  glDisable(GL_TEXTURE_2D);

  std::vector<int> histError(11, 0);
  depthMap_ = cv::Mat(imageHeight_, imageWidth_, CV_32F);
  float totNum = 0.0;
  float sumsq = 0.0;
  float sumT = 0.0;
  delete (pixels_);
  pixels_ = new GLfloat[imageWidth_ * imageHeight_ * 1];
  for (int i = 0; i < imageHeight_; ++i) {
    for (int j = 0; j < imageWidth_; ++j) {

      depthMap_.at<float>(i, j) = (float) (pixels_[(imageHeight_ - i - 1) * 1 * imageWidth_ + j * 1 + 0]);

      errImage.at<cv::Vec3b>(i, j)[0] = 0;
      errImage.at<cv::Vec3b>(i, j)[1] = 0;
      errImage.at<cv::Vec3b>(i, j)[2] = 0;
      if (gtDepth.at<float>(i, j) != 0.0) {
        float curErr = (depthMap_.at<float>(i, j) - gtDepth.at<float>(i, j));
        if (abs(curErr) > 3 * sigmaGT_) {
          errImage.at<cv::Vec3b>(i, j)[0] = 255;
          errImage.at<cv::Vec3b>(i, j)[1] = 0;
          errImage.at<cv::Vec3b>(i, j)[2] = 0;
        } else {
          errImage.at<cv::Vec3b>(i, j)[0] = static_cast<unsigned char>(abs(curErr) / (3 * sigmaGT_) * 255);
          errImage.at<cv::Vec3b>(i, j)[1] = static_cast<unsigned char>(abs(curErr) / (3 * sigmaGT_) * 255);
          errImage.at<cv::Vec3b>(i, j)[2] = static_cast<unsigned char>(abs(curErr) / (3 * sigmaGT_) * 255);
        }

        totNum = totNum + 1.0;
        sumsq = sumsq + (depthMap_.at<float>(i, j) - gtDepth.at<float>(i, j)) * (depthMap_.at<float>(i, j) - gtDepth.at<float>(i, j));

        sumT += abs(curErr);
        //std::cout<<" "<<depthMap_.at<float>(i, j) - gtDepth.at<float>(i, j)<<std::endl;
        if (sigmaGT_ > 0) {
          bool found = false;
          int lastId = 11;

          int curId = 1;
          do {
            if (abs(curErr) < static_cast<float>(curId) * 3 * sigmaGT_) {
              found = true;
              lastId = curId;
            }
            curId++;

          } while (!found && curId < 11);

          histError[lastId - 1]++;
        }
      }
      //DEBUG
      //std::cout << " " << depthMap_.at<float>(i, j);
      //std::cout << "IN ___ gtDepth.at<float>(i, j): " << gtDepth.at<float>(i, j)<< std::endl;
    }
    //DEBUG
    //std::cout << std::endl;
  }

  std::stringstream s;
  s << myConf_.outputSweep.pathMesh << curIter_ << postfix << ".png";
  cv::imwrite(s.str(), errImage);
  float resFin = sqrt(sumsq / (totNum));
  std::cout << setfill(' ') << setw(15) << std::fixed << std::setprecision(5);
  std::cout << "-----------GT COMPARISON------------- " << postfix;
  std::cout << "Total number of points: " << totNum << " " << std::endl;
  std::cout << "-----------GT COMPARISON------------- " << postfix;
  std::cout << "Root Mean Square error: " << resFin << " m" << std::endl;
  std::cout << "-----------GT COMPARISON------------- " << postfix;
  std::cout << "Mean of absolute error: " << sumT / totNum << " m" << std::endl;


  fileLog << setfill(' ') << setw(15) << std::fixed << std::setprecision(5);
  fileLog << "-----------GT COMPARISON------------- " << postfix;
  fileLog << "Total number of points: " << totNum << " " << std::endl;
  fileLog << "-----------GT COMPARISON------------- " << postfix;
  fileLog << "Root Mean Square error: " << resFin << " m" << std::endl;
  fileLog << "-----------GT COMPARISON------------- " << postfix;
  fileLog << "Mean of absolute error: " << sumT / totNum << " m" << std::endl;
  fileLog<<std::flush;
  if (sigmaGT_ > 0) {
    std::cout << "-----------GT COMPARISON------------- " << postfix;
    std::cout << "Errors histogram (%): " << std::endl;
    std::cout << "-----------GT COMPARISON------------- " << postfix;
    for (auto c : histError) {
      std::cout << setfill(' ') << setw(10) << std::fixed << std::setprecision(1);
      std::cout << 100.0 * static_cast<float>(c) / totNum << " ";
    }
    std::cout << std::endl;
    std::cout << "-----------GT COMPARISON------------- " << postfix;
    std::cout << "Errors histogram: (num)" << std::endl;
    std::cout << "-----------GT COMPARISON------------- " << postfix;
    for (auto c : histError) {
      std::cout << setfill(' ') << setw(10) << std::fixed << std::setprecision(1);
      std::cout << c << " ";
    }
    std::cout << std::endl;
  } else {
    std::cout << "-----------GT COMPARISON------------- ";
    std::cerr << "sigmaGT_<0: errors histogram cannot be computed " << std::endl;
  }
/*
   cv::Mat temp, temp2;
   double max, min;
   cv::minMaxLoc(depthMap_, &min, &max);
   temp2 = 255.0 * (depthMap_ - min) / (max - min);
   temp2.convertTo(temp, CV_8UC3);
   cv::imshow("depthMap_", temp);
   std::cout << "IN ___ max: " << max;
   std::cout << "IN ___ min: " << min << std::endl;
   cv::waitKey();*/

}

void MeshSweeper::reshapeFeedbackShaders() {

  normalProgram_->resetTransformFeedback(mesh_.p.size_of_facets() * 3);

}

void MeshSweeper::initShaders() {
  //************************depth********************************
  std::cout << "DepthShaderProgram init...";
  depthProgram_->initializeProgram();
  static_cast<DepthShaderProgram *>(depthProgram_)->initializeFramebufAndTex(framebufferDepth_, depthTexture_);
  static_cast<DepthShaderProgram *>(depthProgram_)->initializeFramebufAndTex(framebufferDepth2_, depthTexture2_);
  std::cout << "DONE" << std::endl;

  //************************reprojection**************************
  std::cout << "ReprojectionShaderProgram init...";
  reprojProgram_->initializeProgram();
  reprojProgram_->initTex();
  static_cast<ReprojectionShaderProgram *>(reprojProgram_)->initializeFramebufAndTex(reprojTex_);
  std::cout << "DONE" << std::endl;

  //************************facet normals*************************
  std::cout << "normalProgram_ init...";
  normalProgram_->initializeProgram();
  normalProgram_->createTransformFeedback(mesh_.p.size_of_facets() * 3);
  std::cout << "DONE" << std::endl;

  //**********************ncc*************************************
  std::cout << "NccShaderProgram init...";
  nccProgram_->initializeProgram();
  nccProgram_->initTex();
  static_cast<NccShaderProgram*>(nccProgram_)->initializeFramebufAndTex(nccTex_);
  std::cout << "DONE" << std::endl;

  //**********************gauss filter****************************
  std::cout << "FilterImageShaderProgram init...";
  gaussFilterImageProgram_->initializeProgram();
  static_cast<FilterImageShaderProgram*>(gaussFilterImageProgram_)->initializeFramebufAndTex(imageFiltered2id_);
  std::cout << "DONE" << std::endl;

  //**********************localMaxima*****************************
  std::cout << "LocalMaximaShaderProgram init...";
  localMaxProgram_->initializeProgram();
  static_cast<LocalMaximaShaderProgram*>(localMaxProgram_)->initializeFramebufAndTex(localMaxTex_);
  std::cout << "DONE" << std::endl;

  //**********************pointextractor**************************
  std::cout << "PointExtractorShaderProgram init...";
  pointExtractorProgram_->initializeProgram();
  static_cast<PointExtractorShaderProgram*>(pointExtractorProgram_)->initializeFramebufAndTex(pointsTex_);
  std::cout << "DONE" << std::endl;

  //**********************laplacian smoother**************************
  std::cout << "LaplacianSmoothShaderProgram init...";
  laplacianSmootherProgram_->initializeProgram();
  static_cast<LaplacianSmoothShaderProgram*>(laplacianSmootherProgram_)->createTransformFeedback(mesh_.p.size_of_facets() * 3);
  std::cout << "DONE" << std::endl;

  //**********************cumulateProgram_**************************
  std::cout << "CumulatePointsShaderProgram init...";
  GLuint temp;
  cumulateProgram_->initializeProgram();
  static_cast<CumulatePointsShaderProgram*>(cumulateProgram_)->initializeFramebufAndTex(temp);
  std::cout << "DONE" << std::endl;

  //**********************depthMapForGT_**************************
  std::cout << "depthMapForGT_ init...";
  depthMapForGT_->initializeProgram();
  static_cast<DepthMapFromMeshShaderProgram*>(depthMapForGT_)->initializeFramebufAndTex(compareDepthTex_);
  std::cout << "DONE" << std::endl;

}

void MeshSweeper::createVertexArrayBuffer() {
  glGenBuffers(1, &vertexBufferObj_);
  resetVertexArrayBuffer();

}

void MeshSweeper::resetMeshInfo() {

  mesh_.updateMeshData(false, false);
  mesh_.resetSimplexIndices();

  resetVertexArrayBuffer();
  resetIdArrayBuffer();
  reshapeFeedbackShaders();
}

void MeshSweeper::resetVertexArrayBuffer() {
  glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObj_);
  std::vector<glm::vec3> verticesUnwrapped;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
    Halfedge_handle h0, h1, h2;
    h0 = itFac->halfedge();
    h1 = h0->next();
    h2 = h1->next();

    Vertex_handle v0, v1, v2;
    v0 = h0->vertex();
    v1 = h1->vertex();
    v2 = h2->vertex();

    verticesUnwrapped.push_back(glm::vec3(v0->point().x(), v0->point().y(), v0->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v1->point().x(), v1->point().y(), v1->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v2->point().x(), v2->point().y(), v2->point().z()));
  }

  glBufferData(GL_ARRAY_BUFFER, 3 * mesh_.p.size_of_facets() * sizeof(glm::vec3), &verticesUnwrapped[0], GL_STATIC_DRAW);
}

void MeshSweeper::createNormalArrayBuffer() {
  if (normalBufferObj_ == static_cast<GLuint>(-1)) {
    glGenBuffers(1, &normalBufferObj_);
  }

  /*for(auto f : static_cast<NormalShaderProgram *>(normalProgram_)->getFeedbackTr()){
   std::cout<< f.x <<", "<< f.y<<", "<<f.z<<std::endl;
   }*/
  glBindBuffer(GL_ARRAY_BUFFER, normalBufferObj_);
  glBufferData(GL_ARRAY_BUFFER, static_cast<NormalShaderProgram *>(normalProgram_)->getFeedbackTr().size() * sizeof(glm::vec3),
      &static_cast<NormalShaderProgram *>(normalProgram_)->getFeedbackTr()[0], GL_STATIC_DRAW);

}

glm::mat4 MeshSweeper::setCameraParamAndGetMvp(const CameraType &cam) {

  cameraTransformationController_->setIntrinsicParameters(cam.intrinsics[0][0], cam.intrinsics[1][1], cam.intrinsics[0][2], cam.intrinsics[1][2]);
  cameraTransformationController_->setExtrinsicParameters(cam.rotation, cam.translation);
  glm::mat4 mvp = cameraTransformationController_->getMvpMatrix();
  return mvp;
}

void MeshSweeper::createImageVerticesBuffer() {
  // Create a Vertex Buffer Object and copy the vertex data to it
  glGenBuffers(1, &imageArrayBufferObj_);

  GLfloat vertices[] = {
  //  Position   Texcoords
      -1.0f, 1.0f, 0.0f, 0.0f, // Top-left
      1.0f, 1.0f, 1.0f, 0.0f, // Top-right
      1.0f, -1.0f, 1.0f, 1.0f, // Bottom-right
      -1.0f, -1.0f, 0.0f, 1.0f  // Bottom-left
      };

  glBindBuffer(GL_ARRAY_BUFFER, imageArrayBufferObj_);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // Create an element array
  glGenBuffers(1, &imageElemBufferObj_);

  GLuint elements[] = { 0, 1, 2, 2, 3, 0 };

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, imageElemBufferObj_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

}

void MeshSweeper::createIdArrayBuffer() {
  glGenBuffers(1, &iDArrayBufferObj_);
  resetIdArrayBuffer();
}

void MeshSweeper::resetIdArrayBuffer() {
  glBindBuffer(GL_ARRAY_BUFFER, iDArrayBufferObj_);
  std::vector<GLfloat> idVec;
  GLfloat curId = 0;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {

    idVec.push_back(curId);/*vertex 1*/
    idVec.push_back(curId);/*vertex 2*/
    idVec.push_back(curId);/*vertex 3*/
    //std::cout << GLfloat(curId) <<", ";
    curId++;
  }
  //std::cout<<std::endl;

  glBufferData(GL_ARRAY_BUFFER, idVec.size() * sizeof(GLfloat), &idVec[0], GL_STATIC_DRAW);
}

void MeshSweeper::createIdUniqueArrayBuffer() {
  glGenBuffers(1, &iDUniqueArrayBufferObj_);
  glBindBuffer(GL_ARRAY_BUFFER, iDUniqueArrayBufferObj_);
  std::vector<GLuint> idVec;
  GLuint curId;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {

    idVec.push_back(curId);
    curId++;
  }

  glBufferData(GL_ARRAY_BUFFER, idVec.size() * sizeof(GLuint), &idVec[0], GL_STATIC_DRAW);
}

void MeshSweeper::createPixelImageArrayBuffer() {
  // Create a Vertex Buffer Object and copy the vertex data to it
  glGenBuffers(1, &pixelImageArrayBufferObj_);

  std::vector<glm::vec2> pixels;
  for (int curX = 0; curX < imageWidth_; ++curX) {
    for (int curY = 0; curY < imageHeight_; ++curY) {
      pixels.push_back(glm::vec2(curX / static_cast<float>(imageWidth_), curY / static_cast<float>(imageHeight_)));
    }
  }
  glBindBuffer(GL_ARRAY_BUFFER, pixelImageArrayBufferObj_);
  glBufferData(GL_ARRAY_BUFFER, pixels.size() * sizeof(glm::vec2), &pixels[0], GL_STATIC_DRAW);
}

void MeshSweeper::initialize() {

  vertexBufferObj_ = normalBufferObj_ = imageArrayBufferObj_ = imageElemBufferObj_ = -1;
  framebufferDepth_ = framebufferDepth2_ = depthTexture_ = -1;
  depthTexture2_ = reprojTex_ = nccTex_ = localMaxTex_ = imageFilteredId_ = imageFiltered2id_ = -1;
  imageHeight_ = myConf_.videoConfig.imageH;
  imageWidth_ = myConf_.videoConfig.imageW;

  camParser_ = new CamParser(myConf_.sweepConfig.pathCamsPose);
  std::cout<<myConf_.sweepConfig.pathCamsPose<<std::endl;
  camParser_->parseFile();

  vertexBufferObj_ = -1;

  depthProgram_ = new DepthShaderProgram(imageWidth_, imageHeight_);
  reprojProgram_ = new ReprojectionShaderProgram(imageWidth_, imageHeight_);
  sweepProgram_ = new SweepShaderProgram(imageWidth_, imageHeight_);
  normalProgram_ = new NormalShaderProgram(imageWidth_, imageHeight_);
  nccProgram_ = new NccShaderProgram(imageWidth_, imageHeight_);
  medianImageProgram_ = new FilterImageShaderProgram(imageWidth_, imageHeight_,
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/median_filter_vertex_shader.glsl",
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/median_filter_fragment_shader.glsl");
  gaussFilterImageProgram_ = new FilterImageShaderProgram(imageWidth_, imageHeight_,
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/gaussian_filter_vertex_shader.glsl",
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/gaussian_filter_fragment_shader.glsl");
  localMaxProgram_ = new LocalMaximaShaderProgram(imageWidth_, imageHeight_);

  pointExtractorProgram_ = new PointExtractorShaderProgram(imageWidth_, imageHeight_,
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/point_extractor_vertex_shader.glsl",
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/point_extractor_fragment_shader.glsl");
  laplacianSmootherProgram_ = new LaplacianSmoothShaderProgram(imageWidth_, imageHeight_);

  collectPointsProgram_ = new CollectPointsShaderProgram(imageWidth_, imageHeight_);
  collectLocalMaximaProgram_ = new CollectLocalMaximaShaderProgram(imageWidth_, imageHeight_);
  depthMapForGT_ = new DepthMapFromMeshShaderProgram(imageWidth_, imageHeight_);
  cumulateProgram_ = new CumulatePointsShaderProgram(imageWidth_, imageHeight_);

  cameraTransformationController_ = new TransformationController(static_cast<float>(imageWidth_), static_cast<float>(imageHeight_));

  pColl = new PointsCollector(mesh_.p.size_of_facets());

  if (myConf_.videoConfig.idxLastFrame == 0) {
    myConf_.videoConfig.idxLastFrame = camParser_->getNumCameras();
  }

  if (myConf_.sweepConfig.kSweepingDistance != 0) {
    coeff_ = myConf_.sweepConfig.kSweepingDistance;
  } else {
    coeff_ = 0.0005;
  }

  if (myConf_.sweepConfig.numPlanes != 0) {
    numPlanes_ = myConf_.sweepConfig.numPlanes;
  } else {
    numPlanes_ = 10;
  }

  loadImages();

  init();
  createVertexArrayBuffer();
  createImageVerticesBuffer();
  createIdArrayBuffer();
  // createPixelImageArrayBuffer();
  //createIdUniqueArrayBuffer();
  initShaders();
  pixels_ = new GLfloat[imageWidth_ * imageHeight_ * 1];
  sigmaGT_ = -1;
}

void MeshSweeper::refreshVertexArrayBuffer() {
  std::vector<glm::vec3> incr = static_cast<LaplacianSmoothShaderProgram *>(laplacianSmootherProgram_)->getFeedbackTr();
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> gradientVectorsField_(mesh_.p.size_of_vertices(), glm::vec3(0.0));
  std::vector<int> numGradientContribField(mesh_.p.size_of_vertices(), 0);

  int countFeedback = 0;
  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
    Halfedge_handle h[3];
    h[0] = itFac->halfedge();
    h[1] = h[0]->next();
    h[2] = h[1]->next();

    for (int curI = 0; curI < 3; ++curI) {
      int idxCurV = h[curI]->vertex()->id;
      //      std::cout<<" ID "<<idxCurV;
      glm::vec3 curContribution = static_cast<float>(0.2) * incr[countFeedback + curI];
      gradientVectorsField_[idxCurV] += static_cast<float>(0.5) * curContribution;

      numGradientContribField[idxCurV]++;

    }
    countFeedback += 3;
  }

  int curV = 0;
  for (Vertex_iterator v = mesh_.p.vertices_begin(); v != mesh_.p.vertices_end(); v++) {
    if (numGradientContribField[curV] != 0) {
      float num = static_cast<float>(numGradientContribField[curV]);
      Vector the_shift = Vector(-gradientVectorsField_[curV].x / num, -gradientVectorsField_[curV].y / num, -gradientVectorsField_[curV].z / num);
      v->move(the_shift);
    }
    curV++;
  }

  glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObj_);
  std::vector<glm::vec3> verticesUnwrapped;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
    Halfedge_handle h0, h1, h2;
    h0 = itFac->halfedge();
    h1 = h0->next();
    h2 = h1->next();

    Vertex_handle v0, v1, v2;
    v0 = h0->vertex();
    v1 = h1->vertex();
    v2 = h2->vertex();

    verticesUnwrapped.push_back(glm::vec3(v0->point().x(), v0->point().y(), v0->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v1->point().x(), v1->point().y(), v1->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v2->point().x(), v2->point().y(), v2->point().z()));
  }

  glBufferData(GL_ARRAY_BUFFER, 3 * mesh_.p.size_of_facets() * sizeof(glm::vec3), &verticesUnwrapped[0], GL_STATIC_DRAW);

}

void MeshSweeper::saveCurMesh(std::string pathName) {

  mesh_.saveFormat(pathName.c_str());

}

void MeshSweeper::saveCurPoints(std::string pathName) {

  std::ofstream newpointsObj(pathName);
  for (auto p : filteredPoints_) {
    newpointsObj << "v " << p.x << " " << p.y << " " << p.z << std::endl;
  }
  newpointsObj.close();
}

void MeshSweeper::loadImages() {
  for (auto path : camParser_->getCamerasPaths()) {

    std::cout<<"path "<<path<<std::endl;
    images_.push_back(cv::imread(path));
    //cv::imshow("CAM",cv::imread(path)); cv::waitKey();
  }

  /*
   images_.resize(myConf_.videoConfig.idxLastFrame + 1);

   for (int curIm = myConf_.videoConfig.idxFirstFrame; curIm <= myConf_.videoConfig.idxLastFrame; curIm++) {
   std::stringstream pathSave;
   pathSave << myConf_.videoConfig.baseNameImage << utils::getFrameNumber(curIm, myConf_.videoConfig.digitIdxLength) << "."
   << myConf_.videoConfig.imageExtension;

   cv::Mat tmpMat = cv::imread(pathSave.str());
   images_[curIm] = tmpMat;
   }*/
}

