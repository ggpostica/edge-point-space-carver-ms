/*
 * DepthMapFromMeshShaderProgram.h
 *
 *  Created on: 30/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_DEPTHMAPFROMMESHSHADERPROGRAM_H_
#define SWEEPMESH_DEPTHMAPFROMMESHSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <glm.hpp>

class DepthMapFromMeshShaderProgram : public ShaderProgram  {
public:
  DepthMapFromMeshShaderProgram(int imageWidth, int imageHeigh);
  virtual ~DepthMapFromMeshShaderProgram();
  void initializeFramebufAndTex(GLuint &resultTex);

  void compute(bool renderFrameBuf = false);

  void setCameraCenter(const glm::vec3& cameraCenter) {
    cameraCenter_ = cameraCenter;
  }

  void setMvp(const glm::mat4& mvp) {
    mvp_ = mvp;
  }

  void setDepthTexture(GLuint depthTexture) {
    depthTexture_ = depthTexture;
  }

private:
  void init();

   void createAttributes();
   void createUniforms();

   GLuint framebuffer_;
   glm::mat4 mvp_;
   glm::vec3 cameraCenter_;
   /*uniforms id*/
   GLuint mvpId_, camCenterID_, depthTexture_, depthTextureId_;
   /*attributes id*/
   GLuint posId_;
   GLuint imageTex_;
};

#endif /* SWEEPMESH_DEPTHMAPFROMMESHSHADERPROGRAM_H_ */
