/*
 * CumulatePointsShaderProgram.h
 *
 *  Created on: 16/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_CUMULATEPOINTSSHADERPROGRAM_H_
#define SWEEPMESH_CUMULATEPOINTSSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <glm.hpp>
class CumulatePointsShaderProgram : public ShaderProgram {
public:
  CumulatePointsShaderProgram(int imageWidth, int imageHeight);
  virtual ~CumulatePointsShaderProgram();
  void initializeFramebufAndTex(GLuint &texId);
  void resetTex();

  void compute(bool renderFrameBuf = false);
  int collectNewPoints(std::vector<glm::vec4> &points);

  void setCurPointsImageTex(GLuint curPointsImageTex) {
    curPointsImageTex_ = curPointsImageTex;
  }
  GLuint getLastTex();

private:
  void init();
  void initializeFramebufAndTexEven();
  void initializeFramebufAndTexOdd();

  void createAttributes();
  void createUniforms();
  void dumpTextureOnCPU();
  void collectPointsFromPixels();

  GLuint framebufferEven_, framebufferOdd_, cumulativeId_, curPointsImageTex_, curPointsImageId_;
  /*attributes id*/
  GLuint posId_, texCoordId_;

  GLuint textureEvenTex_, textureOddTex_;
  GLboolean textureEven_, first_;

  GLfloat *pixels_;
  int ch_;
  std::vector<glm::vec4> pointsAndIdNcc_;
};

#endif /* SWEEPMESH_CUMULATEPOINTSSHADERPROGRAM_H_ */
