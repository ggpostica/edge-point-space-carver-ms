/*
 * DepthMapFromMeshShaderProgram.cpp
 *
 *  Created on: 30/apr/2015
 *      Author: andrea
 */

#include "DepthMapFromMeshShaderProgram.h"

DepthMapFromMeshShaderProgram::DepthMapFromMeshShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {

  framebuffer_ = -1;
  posId_ = -1;
  camCenterID_ = -1;
  mvpId_ = -1;
  imageTex_ = -1;
  depthTexture_ = depthTextureId_ = -1;
}
DepthMapFromMeshShaderProgram::~DepthMapFromMeshShaderProgram() {
}

void DepthMapFromMeshShaderProgram::initializeFramebufAndTex(GLuint &resultTex) {
  glGenFramebuffers(1, &framebuffer_);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);

  glGenTextures(1, &resultTex);
  glBindTexture(GL_TEXTURE_2D, resultTex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, imageWidth_, imageHeight_, 0, GL_RED, GL_FLOAT, nullptr);
  defaultTextureParameters();
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, resultTex, 0);

  GLuint tmpdeptht;
  initDepthTex(tmpdeptht);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tmpdeptht, 0);

  checkFrameBuffer("DepthMapFromMeshShaderProgram::initializeFramebufAndTex");
  imageTex_= resultTex;
}

void DepthMapFromMeshShaderProgram::compute(bool renderFrameBuf) {
  if (renderFrameBuf) {
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  } else {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT };
  glDrawBuffers(2, attachments);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  shaderManager_.enable();

  glUniformMatrix4fv(mvpId_, 1, GL_FALSE, &mvp_[0][0]);
  glUniform3f(camCenterID_, cameraCenter_[0], cameraCenter_[1], cameraCenter_[2]);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, depthTexture_);
  glUniform1i(depthTextureId_, 0);

  glEnableVertexAttribArray(posId_);
  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  if (useElements_Indices) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
    glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);

  } else {
    glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
    glDrawArrays(GL_TRIANGLES, 0, sizeArray_);

  }

  glDisableVertexAttribArray(posId_);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DepthMapFromMeshShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/depthGT_vertex_shader.glsl");
  shaderManager_.addShader(GL_FRAGMENT_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/depthGT_fragment_shader.glsl");
  shaderManager_.finalize();
}

void DepthMapFromMeshShaderProgram::createAttributes() {
  posId_ = shaderManager_.getAttribLocation("position");
}

void DepthMapFromMeshShaderProgram::createUniforms() {
  mvpId_ = shaderManager_.getUniformLocation("MVP");
  camCenterID_ = shaderManager_.getUniformLocation("cameraCenter");
  depthTextureId_ = shaderManager_.getUniformLocation("shadowMap");
}
