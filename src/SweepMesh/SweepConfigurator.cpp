/*
 * SweepConfigurator.cpp
 *
 *  Created on: 01/apr/2015
 *      Author: andrea
 */

#include "SweepConfigurator.h"

#include "../utilities/utilities.hpp"
SweepConfigurator::SweepConfigurator(const std::string &path) {
	  file_.open(path.c_str());

}

SweepConfigurator::~SweepConfigurator() {
}

SweepConfiguration SweepConfigurator::parseConfigFile() {
	SweepConfiguration c;
	utils::readLineAndStore(file_, c.videoConfig.baseNameImage);
	utils::readLineAndStore(file_, c.videoConfig.imageExtension);
	utils::readLineAndStore(file_, c.videoConfig.idxFirstFrame);
	utils::readLineAndStore(file_, c.videoConfig.digitIdxLength);
	utils::readLineAndStore(file_, c.videoConfig.idxLastFrame);
	utils::readLineAndStore(file_, c.videoConfig.imageH);
	utils::readLineAndStore(file_, c.videoConfig.imageW);
  utils::readLineAndStore(file_, c.sweepConfig.pathInitMesh);
  utils::readLineAndStore(file_, c.sweepConfig.pathCamsPose);
  utils::readLineAndStore(file_, c.sweepConfig.thresholdNCC);
  utils::readLineAndStore(file_, c.sweepConfig.kSweepingDistance);
  utils::readLineAndStore(file_, c.sweepConfig.numPlanes);
  utils::readLineAndStore(file_, c.sweepConfig.windowNCC);
  utils::readLineAndStore(file_, c.sweepConfig.windowLocalMaxima);
  utils::readLineAndStore(file_, c.outputSweep.nameDataset);
  utils::readLineAndStore(file_, c.outputSweep.nameFirstMesh);
  utils::readLineAndStore(file_, c.outputSweep.nameMesh);

	return c;
}
