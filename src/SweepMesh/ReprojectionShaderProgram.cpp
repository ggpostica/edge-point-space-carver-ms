/*
 * ReprojectionShaderProgram.cpp
 *
 *  Created on: 03/apr/2015
 *      Author: andrea
 */

#include "ReprojectionShaderProgram.h"
#include <opencv2/imgproc/imgproc.hpp>
#include "../utilities/utilities.hpp"

ReprojectionShaderProgram::ReprojectionShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  framebufferReproj_ = imageReprojTex_ = imageTex_ = -1;
  posAttribReprojId_ = mvp1IDReproj_ = mvp2IDReproj_ = image2TexId_ = shadowMap1IdReproj_ = shadowMap2IdReproj_ = -1;
  depthTexture_ = depthTexture2_ = -1;
  normAttrib_ = normalBuffer_ = -1;
  alphaId_ = -1;
  alpha_ = -1;
  camCenterID_ = -1;
}

ReprojectionShaderProgram::~ReprojectionShaderProgram() {
}

void ReprojectionShaderProgram::initTex() {
  glGenTextures(1, &imageTex_);
  glBindTexture(GL_TEXTURE_2D, imageTex_);
  defaultTextureParameters();
}

void ReprojectionShaderProgram::populateTex(const cv::Mat& image) {

  cv::Mat image2Gray;
  cv::cvtColor(image, image2Gray, CV_RGB2GRAY);
  glBindTexture(GL_TEXTURE_2D, imageTex_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, imageWidth_, imageHeight_, 0, GL_RED, GL_UNSIGNED_BYTE, image2Gray.data);

  image2Gray.release();
}

void ReprojectionShaderProgram::compute(bool renderFrameBuf) {
  if (renderFrameBuf) {
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferReproj_);
  } else {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT };
  glDrawBuffers(2, attachments);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  shaderManager_.enable();

  glUniformMatrix4fv(mvp1IDReproj_, 1, GL_FALSE, &mvp1_[0][0]);
  glUniformMatrix4fv(mvp2IDReproj_, 1, GL_FALSE, &mvp2_[0][0]);
  glUniform3fv(camCenterID_, 1,&camCenter_[0]);
  glUniform1f(alphaId_, alpha_);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, depthTexture_);
  glUniform1i(shadowMap1IdReproj_, 0);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, depthTexture2_);
  glUniform1i(shadowMap2IdReproj_, 1);

  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, imageTex_);
  glUniform1i(image2TexId_, 2);

  glEnableVertexAttribArray(posAttribReprojId_);
  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posAttribReprojId_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(normAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, normalBuffer_);
  glVertexAttribPointer(normAttrib_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  if (useElements_Indices) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
    glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);

  } else {
    glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
    glDrawArrays(GL_TRIANGLES, 0, sizeArray_);

  }

  glDisableVertexAttribArray(posAttribReprojId_);
  glDisableVertexAttribArray(normAttrib_);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ReprojectionShaderProgram::initializeFramebufAndTex(GLuint &imageReprojTex) {
  glGenFramebuffers(1, &framebufferReproj_);
  glBindFramebuffer(GL_FRAMEBUFFER, framebufferReproj_);
  initRedTex(imageReprojTex);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, imageReprojTex, 0);
  GLuint tmpdeptht;
  initDepthTex(tmpdeptht);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tmpdeptht, 0);
  checkFrameBuffer("ReprojectionShaderProgram::initializeFramebufAndTex");
  imageReprojTex_ = imageReprojTex;
}

void ReprojectionShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/reprojection_vertex_shader.glsl");
  shaderManager_.addShader(GL_FRAGMENT_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/reprojection_fragment_shader.glsl");
  shaderManager_.finalize();
}

void ReprojectionShaderProgram::createAttributes() {
  posAttribReprojId_ = shaderManager_.getAttribLocation("position");
  normAttrib_ = shaderManager_.getAttribLocation("normal");
}

void ReprojectionShaderProgram::createUniforms() {
  mvp1IDReproj_ = shaderManager_.getUniformLocation("MVP1");
  mvp2IDReproj_ = shaderManager_.getUniformLocation("MVP2");
  image2TexId_ = shaderManager_.getUniformLocation("image2");
  alphaId_ = shaderManager_.getUniformLocation("alpha");
  camCenterID_ = shaderManager_.getUniformLocation("camCenter");


  shadowMap1IdReproj_ = shaderManager_.getUniformLocation("shadowMap1");
  shadowMap2IdReproj_ = shaderManager_.getUniformLocation("shadowMap2");
}

