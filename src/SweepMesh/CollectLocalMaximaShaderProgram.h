/*
 * CollectLocalMaximaShaderProgram.h
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_COLLECTLOCALMAXIMASHADERPROGRAM_H_
#define SWEEPMESH_COLLECTLOCALMAXIMASHADERPROGRAM_H_

#include "ShaderProgram.h"

#include <glm.hpp>

#include <vector>
class CollectLocalMaximaShaderProgram : public ShaderProgram {
public:
  CollectLocalMaximaShaderProgram(int imageWidth, int imageHeight);
  virtual ~CollectLocalMaximaShaderProgram();

  void createTransformFeedback(int length);
  void compute(bool renderFrameBuf = false);

  const std::vector<glm::vec4>& getFeedbackTr() const {
    return feedbackTr;
  }

  void setImageMaximaTex(GLuint imageMaximaTex) {
    imageMaximaTex_ = imageMaximaTex;
  }

private:

  void init();

  void createAttributes();
  void createUniforms();

  /*uniforms id*/
  GLuint imWid_, imHid_, imageMaximaTexId_;
  /*tex id*/
  GLuint imageMaximaTex_;
  /*attributes id*/
  GLuint pointsId_;

  GLuint feedbackBuffer_;
  GLuint feedbackLength_;
  std::vector<glm::vec4> feedbackTr;

  GLuint query_;
};

#endif /* SWEEPMESH_COLLECTLOCALMAXIMASHADERPROGRAM_H_ */
