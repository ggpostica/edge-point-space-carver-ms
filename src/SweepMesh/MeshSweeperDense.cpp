/*
 * MeshSweeper.cpp
 *
 *  Created on: 01/apr/2015
 *      Author: andrea
 */

#include "MeshSweeperDense.h"

#include "../utilities/utilities.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <fstream>
#include <iostream>
#include "PointExtractorShaderProgram.h"
#include "CollectPointsShaderProgram.h"
#include "CollectLocalMaximaShaderProgram.h"
#include "LaplacianSmoothShaderProgram.h"
#include "CumulatePointsShaderProgram.h"
#include "CumulateBestPointsShaderProgram.h"
#include <glm.hpp>
#include <random>

MeshSweeperDense::MeshSweeperDense(SweepConfiguration myConf, const bool loadMesh) {
  myConf_ = myConf;
  loadMesh_ = loadMesh;
  if (loadMesh) {
    mesh_.loadFormat(myConf_.sweepConfig.pathInitMesh.c_str(), false);
    //mesh_.removeSelfIntersections();
  }
  initialize();

  std::size_t i = 0;
  for (Vertex_iterator it = mesh_.p.vertices_begin(); it != mesh_.p.vertices_end(); ++it) {
    it->id = i++;
  }
}

MeshSweeperDense::~MeshSweeperDense() {
  delete (depthProgram_);
  delete (reprojProgram_);
  delete (normalProgram_);
  delete (sweepProgram_);
  delete (camParser_);
  delete (cameraTransformationController_);
}

void MeshSweeperDense::run() {
  run(0);
}
void MeshSweeperDense::run(int curIter) {
  cv::Mat image1, image2;
  glm::mat4 mvp1, mvp2;

  logger.disable();
  std::stringstream s1, s2;
  logger.startEvent("laplacianSmoother");
//  //************************laplacian smoother*************************
//  /*for (int curIt = 0; curIt < 3; ++curIt) {
//    logger.startEvent("laplacianSmoother");
//    laplacianSmootherProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
//    laplacianSmootherProgram_->setUseElementsIndices(false);
//    static_cast<LaplacianSmoothShaderProgram*>(laplacianSmootherProgram_)->compute();
//    refreshVertexArrayBuffer();
//
//  }*/
  //mesh_.removeSelfIntersections();

  /*std::vector<double> vals;
   //median area of all the facets
   for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
   vals.push_back(itFac->area());
   }
   double medianVal;

   std::sort(vals.begin(), vals.end());
   if (vals.size() % 2 == 0) {
   medianVal = (vals[vals.size() / 2 - 1] + vals[vals.size() / 2]) / 2;
   } else {
   medianVal = vals[vals.size() / 2];
   }

   for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
   vals.push_back(itFac->area());
   if (itFac->area() > 1000 * medianVal)
   mesh_.p.erase_facet(itFac->halfedge());
   }*/

  s1 << myConf_.outputSweep.pathMesh << "BeforeSmoothBeforeIter" << curIter << ".off";

  mesh_.saveFormat(s1.str().c_str());
  mesh_.loadFormat(s1.str().c_str(), false);  //workaround to remove unref vertices
  mesh_.smooth(0.9, 2);
  mesh_.smooth(0.9, 0);
  s2 << myConf_.outputSweep.pathMesh << "AfterSmoothBeforeIter" << curIter << ".off";
  mesh_.saveFormat(s2.str().c_str());
  resetVertexArrayBuffer();
  logger.endEventAndPrint(true);

  /*choose a coeff every time slight different in order to avoid to get stack in no evolution cases*/

  std::random_device rd;
  std::mt19937 gen(rd());

  filteredPoints_.clear();
  // values near the mean are the most likely
  // standard deviation affects the dispersion of generated values from the mean
  float curCoeff;
  do {
    std::normal_distribution<> d(0, 0.015 * coeff_);
    curCoeff = coeff_ - abs(d(gen));
  } while (curCoeff < 0);

  int idx1 = 1;
  int idx2 = 6;
if(true){
//  for (int idx1 = 0; idx1 < camParser_->getNumCameras(); ++idx1) {
    logger.startEvent(); //***********************************************************"IterationCamExtBegin"
    std::vector<int> bufferIdx;
    mvp1 = setCameraParamAndGetMvp(camParser_->getCamerasList()[idx1]);

    //image1 = images_[idx1];
    cv::GaussianBlur(images_[idx1], image1, cv::Size(3, 3), 0.5);
    glm::vec3 curCenter = camParser_->getCamerasList()[idx1].center; //cameraTransformationController_->getCameraCenter();
    std::cout << "Centro " << idx1 << " c: x= " << curCenter.x << ", y=" << curCenter.y << ",z= " << curCenter.z << std::endl;

    static_cast<CumulateBestPointsShaderProgram *>(cumulateBestProgram_)->resetTex();

//    for (int idx = 0; idx < (int) camParser_->getCamerasList().size(); ++idx) {
//
//      if (abs(idx1 - idx) < 4 || abs(camParser_->getCamerasList().size() - idx + idx1) < 4) {
//        bufferIdx.push_back(idx);
//      }
//    }
//
//    for (int idx2 : bufferIdx) {


    if(true){
    //for (int idx2 = idx1; idx2 < camParser_->getNumCameras(); ++idx2) {

      logger.startEvent(); //*********************************************************IterationCamIntBegin

      if (idx1 != idx2) {

        mvp2 = setCameraParamAndGetMvp(camParser_->getCamerasList()[idx2]);

        cv::GaussianBlur(images_[idx2], image2, cv::Size(3, 3), 0.5);
        //image2 = images_[idx2];

        //************************facet normals*************************
        logger.startEvent(); //********************************************************facet normals computation
        normalProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
        normalProgram_->setUseElementsIndices(false);
        normalProgram_->resetTransformFeedback(mesh_.p.size_of_facets() * 3);
        normalProgram_->compute();
        createNormalArrayBuffer();
        logger.endEventAndPrint("facet normals computation ", false);

        for (float curAl = -curCoeff * (numPlanes_); curAl < curCoeff * (numPlanes_ + 1); curAl += curCoeff * 1.0f) {
          //if (curAl != 0.0f) {

          //************************depth************************
          logger.startEvent(); //********************************************************depth
          depthProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
          depthProgram_->setUseElementsIndices(false);
          static_cast<DepthShaderProgram *>(depthProgram_)->setAlpha(curAl);
          static_cast<DepthShaderProgram *>(depthProgram_)->setCamCenter(curCenter);
          static_cast<DepthShaderProgram *>(depthProgram_)->setNormalBuffer(normalBufferObj_);
          static_cast<DepthShaderProgram *>(depthProgram_)->computeDepthMap(framebufferDepth_, mvp1);
          static_cast<DepthShaderProgram *>(depthProgram_)->setAlpha(curAl);
          static_cast<DepthShaderProgram *>(depthProgram_)->computeDepthMap(framebufferDepth2_, mvp2);
          logger.endEventAndPrint("depth", false);

          //************************reprojection**************************
          logger.startEvent(); //********************************************************"reprojection"
          reprojProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
          reprojProgram_->setUseElementsIndices(false);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setAlpha(curAl);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setCamCenter(curCenter);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setNormalBuffer(normalBufferObj_);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setDepthTexture(depthTexture_, depthTexture2_);
          static_cast<ReprojectionShaderProgram *>(reprojProgram_)->setMvp(mvp1, mvp2);
          reprojProgram_->populateTex(image2);
          reprojProgram_->compute(true);
          logger.endEventAndPrint("reprojection", false);


          //************************NCC***********************************
          logger.startEvent(); //********************************************************"NCC"
          nccProgram_->setArrayBufferObj(imageArrayBufferObj_, 4);
          nccProgram_->setElementsBufferObj(imageElemBufferObj_, 6);
          nccProgram_->setUseElementsIndices(true);
          static_cast<NccShaderProgram *>(nccProgram_)->setImage2ReprojTex(reprojTex_);
          nccProgram_->populateTex(image1);
          static_cast<NccShaderProgram *>(nccProgram_)->setWindow(myConf_.sweepConfig.windowNCC);
          nccProgram_->compute(true);
          logger.endEventAndPrint("NCC", true);

          //************************localMaxima*****************************
          logger.startEvent();
          localMaxProgram_->setArrayBufferObj(imageArrayBufferObj_, 4);
          localMaxProgram_->setElementsBufferObj(imageElemBufferObj_, 6);
          localMaxProgram_->setUseElementsIndices(true);
          static_cast<LocalMaximaShaderProgram *>(localMaxProgram_)->setImageNccTex(nccTex_);
          static_cast<LocalMaximaShaderProgram *>(localMaxProgram_)->setThreshold(-1.0);
          static_cast<LocalMaximaShaderProgram *>(localMaxProgram_)->setWindow(myConf_.sweepConfig.windowLocalMaxima);
          localMaxProgram_->compute(true);
          logger.endEventAndPrint("localMaxima", false);

          //************************gaussian*******************************
          //            logger.startEvent(); //********************************************************gaussian
          //            gaussFilterImageProgram_->setArrayBufferObj(imageArrayBufferObj_, 4);
          //            gaussFilterImageProgram_->setElementsBufferObj(imageElemBufferObj_, 6);
          //            gaussFilterImageProgram_->setUseElementsIndices(true);
          //            static_cast<FilterImageShaderProgram *>(gaussFilterImageProgram_)->setImageNccTex(nccTex_);
          //            gaussFilterImageProgram_->compute(false);
          //            logger.endEventAndPrint("gaussian", false);

          // ************************pointsExtract***************************
          logger.startEvent(); //********************************************************pointsExtract
          pointExtractorProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
          pointExtractorProgram_->setUseElementsIndices(false);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setAlpha(curAl);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setCamCenter(curCenter);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setNormalBuffer(normalBufferObj_);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setIdBuffer(iDArrayBufferObj_);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setDepthTexture(depthTexture_);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setMvp(mvp1);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setTh(myConf_.sweepConfig.thresholdNCC);
          static_cast<PointExtractorShaderProgram *>(pointExtractorProgram_)->setImagePointsKept(localMaxTex_);
          pointExtractorProgram_->compute(true);
          logger.endEventAndPrint("pointsExtract", false);

          //************************cumulate***************************
          logger.startEvent(); //********************************************************cumulate
          cumulateBestProgram_->setArrayBufferObj(imageArrayBufferObj_, 4);
          cumulateBestProgram_->setElementsBufferObj(imageElemBufferObj_, 6);
          cumulateBestProgram_->setUseElementsIndices(true);
          static_cast<CumulateBestPointsShaderProgram *>(cumulateBestProgram_)->setCurPointsImageTex(pointsTex_);
          static_cast<CumulateBestPointsShaderProgram *>(cumulateBestProgram_)->setCurIdCam2(idx2);
          cumulateBestProgram_->compute(true);
          logger.endEventAndPrint("cumulate", false);

          //*/
          logger.startEvent(); //********************************************************SwapBuffers
          SwapBuffers();

          logger.endEventAndPrint("SwapBuffers", true);
/*
          cv::imshow("sdvws",image1);
          cv::waitKey();
          sleep(10.0);*/

          //} //if (curAl  !=0)end
        } //iterate over planes

      } //if(idx1 != idx2) end

      logger.endEventAndPrint("IterationCamIntEnd", true);
      std::cout << "Compared idx1 = " << idx1 << ", idx2 = " << idx2 << std::endl;

      std::cout << "*************** "
          << 100 * (camParser_->getCamerasList().size() * (idx1 + 1) + (idx2 + 1))
              / (camParser_->getCamerasList().size() * camParser_->getCamerasList().size() + camParser_->getCamerasList().size()) << "%*******" << std::endl;

    } //iterate on (idx2) end

    curPandNccId_.clear();
    int pt;
    pt = static_cast<CumulateBestPointsShaderProgram *>(cumulateBestProgram_)->collectNewPoints(curPandNccId_);
    pColl->addPointsAndIdCam(curPandNccId_, idx1);

    logger.endEventAndPrint("IterationCamExtEnd", true);
    std::stringstream spt1, spt2, sp3;

    float min = 10000.0;
    float max = -10000.0;
    float last;
    spt2 << myConf_.outputSweep.pathMesh << "ImageIdx" << idx1 << ".png";
    saveLastImagesWithPoints(image1, curPandNccId_, camParser_->getCamerasList()[idx1].cameraMatrix, spt2.str());
    for (int curIdx2 = 0; curIdx2 < 7; ++curIdx2) {

      if (idx1 != curIdx2) {
        std::vector<glm::vec4> temp;

        for (auto p : curPandNccId_) {
          if (static_cast<int>( glm::trunc(p.w)) == curIdx2) {
            temp.push_back(p);
            if (p.w - glm::trunc(p.w) < min)
              min = p.w - glm::trunc(p.w);
            if (p.w - glm::trunc(p.w) > max)
              max = p.w - glm::trunc(p.w);

          }
          last = glm::trunc(p.w);
        }
        std::cout << "Min: " << min << ", max: " << max << std::endl;
        std::cout << "p.z - glm::trunc(p.z): " << last << std::endl;

        cv::GaussianBlur(images_[curIdx2], image2, cv::Size(3, 3), 0.5);
        sp3.clear();
        sp3.str("");
        sp3 << myConf_.outputSweep.pathMesh << "ImageIdx" << idx1 << "Idx" << curIdx2 << ".png";

        saveLastImagesWithPoints(image2, temp, camParser_->getCamerasList()[curIdx2].cameraMatrix, sp3.str());
      }
    }

    spt1 << myConf_.outputSweep.pathMesh << "NewPointsAfterIter" << curIter << "idx" << idx1 << ".obj";
    std::ofstream newpointsObj(spt1.str());
    for (auto p : curPandNccId_) {
      newpointsObj << "v " << p.x << " " << p.y << " " << p.z << std::endl;
    }
    newpointsObj.close();
    exit(0);

    std::cout << "Num. new Points found: " << pt << std::endl;
  } //iterate on (idx1) end
  filteredPoints_ = pColl->getActivefilteredPoints();
  vis_ = pColl->getActiveVisibility();
  std::cout << "Num. new Points found: " << filteredPoints_.size() << std::endl;
  pColl->resetPoints();
  std::stringstream spt;

  spt << myConf_.outputSweep.pathMesh << "NewPointsAfterIter" << curIter << ".obj";
  std::ofstream newpointsObj(spt.str());
  for (auto p : filteredPoints_) {
    newpointsObj << "v " << p.x << " " << p.y << " " << p.z << std::endl;
  }
  newpointsObj.close();
  image1.release();
  image2.release();
}

void MeshSweeperDense::saveLastImagesWithPoints(cv::Mat &image1, std::vector<glm::vec4> &points, const glm::mat4 &cam, std::string path) {
  cv::Mat outIm1;

  image1.copyTo(outIm1);

  for (auto p : points) {

    glm::vec4 point3D = glm::vec4(p.x, p.y, p.z, 1.0);
    glm::vec4 point2D = glm::transpose(cam) * point3D;

    point2D[0] = point2D[0] / point2D[2];
    point2D[1] = point2D[1] / point2D[2];
    point2D[2] = point2D[2] / point2D[2];

    cv::Point2f Pfinal;
    Pfinal.x = point2D[0];
    Pfinal.y = point2D[1];
    cv::circle(outIm1, Pfinal, 2, cv::Scalar(255.0, 0.0, 255.0 * p.w), -1, 8);
  }
  std::cout << "Saving image in " << path << std::endl;
  cv::imwrite(path.c_str(), outIm1);
  outIm1.release();
}
void MeshSweeperDense::showLastImagesWithPoints(cv::Mat &image1, cv::Mat &image2, std::vector<glm::vec4> &points, const glm::mat4 &cam1,
    const glm::mat4 &cam2) {

  cv::Mat outIm1, outIm2;

  image1.copyTo(outIm1);
  image2.copyTo(outIm2);

  for (auto p : points) {

    glm::vec4 point3D = glm::vec4(p.x, p.y, p.z, 1.0);
    glm::vec4 point2D = glm::transpose(cam1) * point3D;

    point2D[0] = point2D[0] / point2D[2];
    point2D[1] = point2D[1] / point2D[2];
    point2D[2] = point2D[2] / point2D[2];
    glm::vec4 point2Db = glm::transpose(cam2) * point3D;
    point2Db[0] = point2Db[0] / point2Db[2];
    point2Db[1] = point2Db[1] / point2Db[2];
    point2Db[2] = point2Db[2] / point2Db[2];

    /* std::cout<<"point3D"<<std::endl;
     utils::printMatrix(point3D);
     std::cout<<"point2D"<<std::endl;
     utils::printMatrix(point2D);
     std::cout<<"point2Db"<<std::endl;
     utils::printMatrix(point2Db);*/

    cv::Point2f Pfinal, Pfinalb;
    Pfinal.x = point2D[0];
    Pfinal.y = point2D[1];
    Pfinalb.x = point2Db[0];
    Pfinalb.y = point2Db[1];
    cv::circle(outIm1, Pfinal, 2, cv::Scalar(0, 0, 255), -1, 8);
    cv::circle(outIm2, Pfinalb, 2, cv::Scalar(0, 0, 255), -1, 8);
  }
  cv::imshow("image1", outIm1);
  cv::imshow("image2", outIm2);
  cv::imwrite("image1.png", outIm1);
  cv::imwrite("image2.png", outIm2);
  cv::waitKey();

}

/*old stuff*/
void MeshSweeperDense::computeStainerPoints(std::vector<glm::vec3> &points, std::vector<std::vector<int> > &vis) {

  sweepProgram_->setArrayBufferObj(vertexBufferObj_, mesh_.p.size_of_facets() * 3);
  sweepProgram_->setUseElementsIndices(false);
  static_cast<SweepShaderProgram *>(sweepProgram_)->updateTransformFeedback(mesh_.p.size_of_facets() * 3);

  for (int idx1 = 0; idx1 < camParser_->getNumCameras(); ++idx1) {
    logger.startEvent();
    std::vector<int> bufferIdx;
    glm::mat4 mvp1 = setCameraParamAndGetMvp(camParser_->getCamerasList()[idx1]);
    glm::vec3 curCenter = camParser_->getCamerasList()[idx1].center; //cameraTransformationController_->getCameraCenter();
    std::cout << "Centro " << idx1 << " c: x= " << curCenter.x << ", y=" << curCenter.y << ",z= " << curCenter.z << std::endl;

    static_cast<SweepShaderProgram *>(sweepProgram_)->setCamCenter(curCenter);
    static_cast<SweepShaderProgram *>(sweepProgram_)->setAlpha(-coeff_ * 500.0f);
    static_cast<SweepShaderProgram *>(sweepProgram_)->setNormalBuffer(normalBufferObj_);
    static_cast<SweepShaderProgram *>(sweepProgram_)->compute(mvp1);

    //DEBUG write the output of the triangle sweeping
    std::vector<glm::vec3> temp = static_cast<SweepShaderProgram *>(sweepProgram_)->getFeedbackTr();
    std::vector<int> tempVis;
    tempVis.push_back(idx1);
    std::vector<std::vector<int> > tempV;
    tempV.assign(temp.size(), tempVis);

    points.insert(points.end(), temp.begin(), temp.end());
    vis.insert(vis.end(), tempV.begin(), tempV.end());

    logger.endEventAndPrint("endCollectSteiner"); //***********************************************************"IterationCamExtBegin"
    std::stringstream s;
    /*s << "/home/andrea/workspaceC/edgePointSpaceCarver/prova_" << idx1 << ".obj";
     utilities::writeObj(temp, s.str().c_str());*/
    std::cout << "DONE" << std::endl;
    temp.clear();
    tempVis.clear();
    tempV.clear();
    SwapBuffers();
  }

}

void MeshSweeperDense::restartWithNewMesh(const Mesh& mesh) {

  mesh_ = mesh;
  // mesh_.removeSelfIntersections();

  mesh_.smooth(0.2, 2);
  std::size_t i = 0;
  for (Vertex_iterator it = mesh_.p.vertices_begin(); it != mesh_.p.vertices_end(); ++it) {
    it->id = i++;
  }

  resetVertexArrayBuffer();
  resetIdArrayBuffer();
}

void MeshSweeperDense::initShaders() {

  //************************sweep facets**************************
  sweepProgram_->initializeProgram();
  sweepProgram_->createTransformFeedback(mesh_.p.size_of_facets() * 3);

  //************************depth********************************
  depthProgram_->initializeProgram();
  static_cast<DepthShaderProgram *>(depthProgram_)->initializeFramebufAndTex(framebufferDepth_, depthTexture_);
  static_cast<DepthShaderProgram *>(depthProgram_)->initializeFramebufAndTex(framebufferDepth2_, depthTexture2_);

  //************************reprojection**************************
  reprojProgram_->initializeProgram();
  reprojProgram_->initTex();
  static_cast<ReprojectionShaderProgram *>(reprojProgram_)->initializeFramebufAndTex(reprojTex_);

  //************************facet normals*************************
  normalProgram_->initializeProgram();
  normalProgram_->createTransformFeedback(mesh_.p.size_of_facets() * 3);

  //**********************ncc*************************************
  nccProgram_->initializeProgram();
  nccProgram_->initTex();
  static_cast<NccShaderProgram*>(nccProgram_)->initializeFramebufAndTex(nccTex_);

  //**********************gauss filter****************************
  gaussFilterImageProgram_->initializeProgram();
  static_cast<FilterImageShaderProgram*>(gaussFilterImageProgram_)->initializeFramebufAndTex(imageFiltered2id_);

  //**********************localMaxima*****************************
  localMaxProgram_->initializeProgram();
  static_cast<LocalMaximaShaderProgram*>(localMaxProgram_)->initializeFramebufAndTex(localMaxTex_);

  //**********************pointextractor**************************
  pointExtractorProgram_->initializeProgram();
  static_cast<PointExtractorShaderProgram*>(pointExtractorProgram_)->initializeFramebufAndTex(pointsTex_);

  //**********************laplacian smoother**************************
  laplacianSmootherProgram_->initializeProgram();
  static_cast<LaplacianSmoothShaderProgram*>(laplacianSmootherProgram_)->createTransformFeedback(mesh_.p.size_of_facets() * 3);

  //**********************cumulateProgram_**************************
  GLuint temp;
  cumulateProgram_->initializeProgram();
  static_cast<CumulatePointsShaderProgram*>(cumulateProgram_)->initializeFramebufAndTex(temp);

  //**********************cumulateBestProgram_**************************
  GLuint temp2;
  cumulateBestProgram_->initializeProgram();
  static_cast<CumulateBestPointsShaderProgram*>(cumulateBestProgram_)->initializeFramebufAndTex(temp2);

}

void MeshSweeperDense::createVertexArrayBuffer() {
  glGenBuffers(1, &vertexBufferObj_);
  resetVertexArrayBuffer();

}
void MeshSweeperDense::resetVertexArrayBuffer() {
  glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObj_);
  std::vector<glm::vec3> verticesUnwrapped;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
    Halfedge_handle h0, h1, h2;
    h0 = itFac->halfedge();
    h1 = h0->next();
    h2 = h1->next();

    Vertex_handle v0, v1, v2;
    v0 = h0->vertex();
    v1 = h1->vertex();
    v2 = h2->vertex();

    verticesUnwrapped.push_back(glm::vec3(v0->point().x(), v0->point().y(), v0->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v1->point().x(), v1->point().y(), v1->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v2->point().x(), v2->point().y(), v2->point().z()));
  }

  glBufferData(GL_ARRAY_BUFFER, 3 * mesh_.p.size_of_facets() * sizeof(glm::vec3), &verticesUnwrapped[0], GL_STATIC_DRAW);
}

void MeshSweeperDense::createNormalArrayBuffer() {
  if (normalBufferObj_ == static_cast<GLuint>(-1)) {
    glGenBuffers(1, &normalBufferObj_);
  }

  /*for(auto f : static_cast<NormalShaderProgram *>(normalProgram_)->getFeedbackTr()){
   std::cout<< f.x <<", "<< f.y<<", "<<f.z<<std::endl;
   }*/
  glBindBuffer(GL_ARRAY_BUFFER, normalBufferObj_);
  glBufferData(GL_ARRAY_BUFFER, static_cast<NormalShaderProgram *>(normalProgram_)->getFeedbackTr().size() * sizeof(glm::vec3),
      &static_cast<NormalShaderProgram *>(normalProgram_)->getFeedbackTr()[0], GL_STATIC_DRAW);

}

glm::mat4 MeshSweeperDense::setCameraParamAndGetMvp(const CameraType &cam) {

  cameraTransformationController_->setIntrinsicParameters(cam.intrinsics[0][0], cam.intrinsics[1][1], cam.intrinsics[0][2], cam.intrinsics[1][2]);
  cameraTransformationController_->setExtrinsicParameters(cam.rotation, cam.translation);
  glm::mat4 mvp = cameraTransformationController_->getMvpMatrix();
  return mvp;
}

void MeshSweeperDense::createImageVerticesBuffer() {
  // Create a Vertex Buffer Object and copy the vertex data to it
  glGenBuffers(1, &imageArrayBufferObj_);

  GLfloat vertices[] = {
  //  Position   Texcoords
      -1.0f, 1.0f, 0.0f, 0.0f, // Top-left
      1.0f, 1.0f, 1.0f, 0.0f, // Top-right
      1.0f, -1.0f, 1.0f, 1.0f, // Bottom-right
      -1.0f, -1.0f, 0.0f, 1.0f  // Bottom-left
      };

  glBindBuffer(GL_ARRAY_BUFFER, imageArrayBufferObj_);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // Create an element array
  glGenBuffers(1, &imageElemBufferObj_);

  GLuint elements[] = { 0, 1, 2, 2, 3, 0 };

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, imageElemBufferObj_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

}

void MeshSweeperDense::createIdArrayBuffer() {
  glGenBuffers(1, &iDArrayBufferObj_);
  resetIdArrayBuffer();
}

void MeshSweeperDense::resetIdArrayBuffer() {
  glBindBuffer(GL_ARRAY_BUFFER, iDArrayBufferObj_);
  std::vector<GLfloat> idVec;
  GLfloat curId = 0;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {

    idVec.push_back(curId);/*vertex 1*/
    idVec.push_back(curId);/*vertex 2*/
    idVec.push_back(curId);/*vertex 3*/
    //std::cout << GLfloat(curId) <<", ";
    curId++;
  }
  //std::cout<<std::endl;

  glBufferData(GL_ARRAY_BUFFER, idVec.size() * sizeof(GLfloat), &idVec[0], GL_STATIC_DRAW);
}
void MeshSweeperDense::createIdUniqueArrayBuffer() {
  glGenBuffers(1, &iDUniqueArrayBufferObj_);
  glBindBuffer(GL_ARRAY_BUFFER, iDUniqueArrayBufferObj_);
  std::vector<GLuint> idVec;
  GLuint curId;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {

    idVec.push_back(curId);
    curId++;
  }

  glBufferData(GL_ARRAY_BUFFER, idVec.size() * sizeof(GLuint), &idVec[0], GL_STATIC_DRAW);
}

void MeshSweeperDense::createPixelImageArrayBuffer() {
  // Create a Vertex Buffer Object and copy the vertex data to it
  glGenBuffers(1, &pixelImageArrayBufferObj_);

  std::vector<glm::vec2> pixels;
  for (int curX = 0; curX < imageWidth_; ++curX) {
    for (int curY = 0; curY < imageHeight_; ++curY) {
      pixels.push_back(glm::vec2(curX / static_cast<float>(imageWidth_), curY / static_cast<float>(imageHeight_)));
    }
  }
  glBindBuffer(GL_ARRAY_BUFFER, pixelImageArrayBufferObj_);
  glBufferData(GL_ARRAY_BUFFER, pixels.size() * sizeof(glm::vec2), &pixels[0], GL_STATIC_DRAW);
}

void MeshSweeperDense::initialize() {
  vertexBufferObj_ = normalBufferObj_ = imageArrayBufferObj_ = imageElemBufferObj_ = -1;
  framebufferDepth_ = framebufferDepth2_ = depthTexture_ = -1;
  depthTexture2_ = reprojTex_ = nccTex_ = localMaxTex_ = imageFilteredId_ = imageFiltered2id_ = -1;
  imageHeight_ = myConf_.videoConfig.imageH;
  imageWidth_ = myConf_.videoConfig.imageW;

  camParser_ = new CamParser(myConf_.sweepConfig.pathCamsPose);
  camParser_->parseFile();

  vertexBufferObj_ = -1;

  depthProgram_ = new DepthShaderProgram(imageWidth_, imageHeight_);
  reprojProgram_ = new ReprojectionShaderProgram(imageWidth_, imageHeight_);
  sweepProgram_ = new SweepShaderProgram(imageWidth_, imageHeight_);
  normalProgram_ = new NormalShaderProgram(imageWidth_, imageHeight_);
  nccProgram_ = new NccShaderProgram(imageWidth_, imageHeight_);
  medianImageProgram_ = new FilterImageShaderProgram(imageWidth_, imageHeight_,
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/median_filter_vertex_shader.glsl",
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/median_filter_fragment_shader.glsl");
  gaussFilterImageProgram_ = new FilterImageShaderProgram(imageWidth_, imageHeight_,
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/gaussian_filter_vertex_shader.glsl",
      "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/gaussian_filter_fragment_shader.glsl");
  localMaxProgram_ = new LocalMaximaShaderProgram(imageWidth_, imageHeight_);

  pointExtractorProgram_ = new PointExtractorShaderProgram(imageWidth_, imageHeight_);
  laplacianSmootherProgram_ = new LaplacianSmoothShaderProgram(imageWidth_, imageHeight_);

  collectPointsProgram_ = new CollectPointsShaderProgram(imageWidth_, imageHeight_);
  collectLocalMaximaProgram_ = new CollectLocalMaximaShaderProgram(imageWidth_, imageHeight_);

  cumulateProgram_ = new CumulatePointsShaderProgram(imageWidth_, imageHeight_);
  cumulateBestProgram_ = new CumulateBestPointsShaderProgram(imageWidth_, imageHeight_);

  cameraTransformationController_ = new TransformationController(static_cast<float>(imageWidth_), static_cast<float>(imageHeight_));

  pColl = new PointsCollectorPerPixel();

  if (myConf_.videoConfig.idxLastFrame == 0) {
    myConf_.videoConfig.idxLastFrame = camParser_->getNumCameras();
  }

  if (myConf_.sweepConfig.kSweepingDistance != 0) {
    coeff_ = myConf_.sweepConfig.kSweepingDistance;
  } else {
    coeff_ = 0.0005;
  }

  if (myConf_.sweepConfig.numPlanes != 0) {
    numPlanes_ = myConf_.sweepConfig.numPlanes;
  } else {
    numPlanes_ = 10;
  }

  loadImages();

  init();
  createVertexArrayBuffer();
  createImageVerticesBuffer();
  createIdArrayBuffer();
  // createPixelImageArrayBuffer();
  //createIdUniqueArrayBuffer();
  initShaders();
}

void MeshSweeperDense::refreshVertexArrayBuffer() {
  std::vector<glm::vec3> incr = static_cast<LaplacianSmoothShaderProgram *>(laplacianSmootherProgram_)->getFeedbackTr();
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> gradientVectorsField_(mesh_.p.size_of_vertices(), glm::vec3(0.0));
  std::vector<int> numGradientContribField(mesh_.p.size_of_vertices(), 0);

  int countFeedback = 0;
  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
    Halfedge_handle h[3];
    h[0] = itFac->halfedge();
    h[1] = h[0]->next();
    h[2] = h[1]->next();

    for (int curI = 0; curI < 3; ++curI) {
      int idxCurV = h[curI]->vertex()->id;
      //      std::cout<<" ID "<<idxCurV;
      //std::cout<<" ID "<<incr[countFeedback + curI].x<< " y "<< incr[countFeedback + curI].y<<std::endl;
      glm::vec3 curContribution = static_cast<float>(2.0) * incr[countFeedback + curI];
      gradientVectorsField_[idxCurV] += static_cast<float>(0.5) * curContribution;

      numGradientContribField[idxCurV]++;

    }
    countFeedback += 3;
  }

  int curV = 0;
  for (Vertex_iterator v = mesh_.p.vertices_begin(); v != mesh_.p.vertices_end(); v++) {
    if (numGradientContribField[curV] != 0) {
      float num = static_cast<float>(numGradientContribField[curV]);
      Vector the_shift = Vector(-gradientVectorsField_[curV].x / num, -gradientVectorsField_[curV].y / num, -gradientVectorsField_[curV].z / num);
      v->move(the_shift);
    }
    curV++;
  }

  glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObj_);
  std::vector<glm::vec3> verticesUnwrapped;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
    Halfedge_handle h0, h1, h2;
    h0 = itFac->halfedge();
    h1 = h0->next();
    h2 = h1->next();

    Vertex_handle v0, v1, v2;
    v0 = h0->vertex();
    v1 = h1->vertex();
    v2 = h2->vertex();

    verticesUnwrapped.push_back(glm::vec3(v0->point().x(), v0->point().y(), v0->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v1->point().x(), v1->point().y(), v1->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v2->point().x(), v2->point().y(), v2->point().z()));
  }

  glBufferData(GL_ARRAY_BUFFER, 3 * mesh_.p.size_of_facets() * sizeof(glm::vec3), &verticesUnwrapped[0], GL_STATIC_DRAW);

}

void MeshSweeperDense::loadImages() {
  for (auto path : camParser_->getCamerasPaths()) {

    images_.push_back(cv::imread(path));
    std::cout << path << std::endl;
    //cv::imshow("CAM",cv::imread(path)); cv::waitKey();
  }

  /*
   images_.resize(myConf_.videoConfig.idxLastFrame + 1);

   for (int curIm = myConf_.videoConfig.idxFirstFrame; curIm <= myConf_.videoConfig.idxLastFrame; curIm++) {
   std::stringstream pathSave;
   pathSave << myConf_.videoConfig.baseNameImage << utils::getFrameNumber(curIm, myConf_.videoConfig.digitIdxLength) << "."
   << myConf_.videoConfig.imageExtension;

   cv::Mat tmpMat = cv::imread(pathSave.str());
   images_[curIm] = tmpMat;
   }*/
}

