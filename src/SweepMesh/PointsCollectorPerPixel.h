/*
 * PointsCollector.h
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_POINTSCOLLECTORPERPIXWL_H_
#define SWEEPMESH_POINTSCOLLECTORPERPIXWL_H_

#include <glm.hpp>
#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>

class PointsCollectorPerPixel {
public:
  PointsCollectorPerPixel(int numTriangles);
  PointsCollectorPerPixel();
  virtual ~PointsCollectorPerPixel();

  void resetPoints();

  void addPointsAndIdCam(const std::vector<glm::vec4>& pointsNccIds, const int id1);

  const std::vector<glm::vec3>& getActivefilteredPoints();
  const std::vector<std::vector<int> >& getActiveVisibility();

private:

  std::vector<glm::vec3> points_;
  std::vector<std::vector<int> > visibility_;

};

#endif /* SWEEPMESH_POINTSCOLLECTOR_H_ */
