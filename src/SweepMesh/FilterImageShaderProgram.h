/*
 * FilterImageShaderProgram.h
 *
 *  Created on: 08/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_FILTERIMAGESHADERPROGRAM_H_
#define SWEEPMESH_FILTERIMAGESHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <string>

class FilterImageShaderProgram : public ShaderProgram {
public:

  FilterImageShaderProgram(int imageWidth, int imageHeight, std::string pathVert, std::string pathFrag);
  virtual ~FilterImageShaderProgram();
  void initializeFramebufAndTex(GLuint &texId);

  void compute(bool renderFrameBuf = false);

  void setImageNccTex(GLuint imageNccTex) {
    imageNCCTex_ = imageNccTex;
  }
private:

  void init();

  void createAttributes();
  void createUniforms();

  GLuint framebuffer_;
  /*uniforms id*/
  GLuint imWid_, imHid_, imageNCCTexId_;
  /*tex id*/
  GLuint filterTexId_, imageNCCTex_, imageTex_;
  /*attributes id*/
  GLuint posId_, texCoordId_;

  std::string pathVert_, pathFrag_;
};

#endif /* SWEEPMESH_FILTERIMAGESHADERPROGRAM_H_ */
