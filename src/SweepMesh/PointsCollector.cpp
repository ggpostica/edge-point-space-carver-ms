/*
 * PointsCollector.cpp
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#include "PointsCollector.h"
#include <iostream>

PointsCollector::PointsCollector(int numTriangles) {
  numTriangles_ = numTriangles;
  resetPoints();
}

PointsCollector::~PointsCollector() {
}

void PointsCollector::resetPoints() {
  resetPoints(numTriangles_);
}

void PointsCollector::resetPoints(int numTriangles) {
  numTriangles_ = numTriangles;
  points_.assign(numTriangles, glm::vec3(0.0));
  filteredPoints_.assign(numTriangles, glm::vec3(0.0));
  visibility_.assign(numTriangles, std::vector<int>());
  activefilteredPoints_.assign(numTriangles, glm::vec3(0.0));
  maxNcc_.assign(numTriangles, -10.0);
  kept_.assign(numTriangles, false);
}
void PointsCollector::addPointsNccIds(const std::vector<glm::vec4>& pointsNccIds, const int id1, const int id2) {
  glm::vec3 curPt;
  GLfloat curNcc, nccAndId;
  GLuint curId;
  for (auto curPNI : pointsNccIds) {
    curPt = glm::vec3(curPNI.x, curPNI.y, curPNI.z);

    nccAndId = curPNI.w;
    curNcc = nccAndId - glm::trunc(nccAndId);
    curId = GLuint(glm::trunc(nccAndId));
//std::cout<<" nccAndId " <<nccAndId<<" curNcc " <<curNcc<<" curId " <<curId<<std::endl;
    if (curNcc > maxNcc_[curId]) {
      filteredPoints_[curId] = curPt;
      maxNcc_[curId] = curNcc;
      kept_[curId] = true;
      visibility_[curId].clear();
      visibility_[curId].push_back(id1);
      visibility_[curId].push_back(id2);
    }
  }

}

std::vector<glm::vec3>& PointsCollector::getActivefilteredPoints() {
  activefilteredPoints_.assign(0,glm::vec3(0.0));

  for (int curP = 0; curP < numTriangles_; ++curP) {
    if (kept_[curP]) {
      activefilteredPoints_.push_back(filteredPoints_[curP]);
    }
  }

  return activefilteredPoints_;
}

const std::vector<std::vector<int> >& PointsCollector::getActiveVisibility() {
  activeVisibility_.clear();

  for (int curP = 0; curP < numTriangles_; ++curP) {
    if (kept_[curP]) {
      activeVisibility_.push_back(visibility_[curP]);
    }
  }

  return activeVisibility_;
}
