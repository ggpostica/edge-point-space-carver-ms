/*
 * CollectPointsShaderProgram.h
 *
 *  Created on: 13/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_COLLECTPOINTSSHADERPROGRAM_H_
#define SWEEPMESH_COLLECTPOINTSSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <glm.hpp>
#include <vector>

class CollectPointsShaderProgram : public ShaderProgram {
public:
  CollectPointsShaderProgram(int imageWidth, int imageHeight);
  virtual ~CollectPointsShaderProgram();
  void createTransformFeedback(int length);
  void initializeFramebufAndTex(GLuint &texId);

  void compute(bool renderFrameBuf = false);

  const std::vector<glm::vec3>& getFeedbackTr() const {
    return feedbackTrPoints_;
  }

  void setImageTex(GLuint imageTex) {
    imageTex_ = imageTex;
  }

  void setFeedbackLen(GLuint feedbackLen) {
    feedbackLen_ = feedbackLen;
  }

  void setIdTex(GLuint idTex) {
    idTex_ = idTex;
  }

  void setNccTex(GLuint nccTex) {
    nccTex_ = nccTex;
  }

private:
  void init();

  void createAttributes();
  void createUniforms();

  /*uniforms id*/
  GLuint imWid_, imHid_, imageTexId_, thId_, wId_,  nccTexId_, idTexId_;
  /*tex id*/
  GLuint imageTex_, nccTex_, idTex_;
  /*attributes id*/
  GLuint posId_, texCoordId_, endIdTex_, framebuffer_;

  GLuint feedbackBuffer_, feedbackLen_;
  std::vector<glm::vec3> feedbackTrPoints_;
};

#endif /* SWEEPMESH_COLLECTPOINTSSHADERPROGRAM_H_ */
