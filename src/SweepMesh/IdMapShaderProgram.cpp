/*
 * IdMapShaderProgram.cpp
 *
 *  Created on: 14/apr/2015
 *      Author: andrea
 */

#include "IdMapShaderProgram.h"

IdMapShaderProgram::IdMapShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  framebuffer_ = imageTexId_ = depthTexture_ = 1;
  mapIdid_ = shadowMapId_ = mvpId_ = -1;
  normAttrib_ = normalBuffer_ = idBuffer_ = -1;

  alphaId_ = -1;
  alpha_ = idAttrib_ = -1;
  posId_  = -1;


}

IdMapShaderProgram::~IdMapShaderProgram() {
}

void IdMapShaderProgram::initializeFramebufAndTex(GLuint& texId) {
}

void IdMapShaderProgram::compute(bool renderFrameBuf) {
  glClearColor(0.1666666666f, 0.1666666666f, 0.1666666666f, 0.0f);
  if (renderFrameBuf) {
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  } else {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT };
  glDrawBuffers(2, attachments);

  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  shaderManager_.enable();

  glUniformMatrix4fv(mvpId_, 1, GL_FALSE, &mvp_[0][0]);
  glUniform1f(alphaId_, alpha_);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, depthTexture_);
  glUniform1i(shadowMapId_, 0);

  glEnableVertexAttribArray(posId_);
  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(normAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, normalBuffer_);
  glVertexAttribPointer(normAttrib_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(idAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, idBuffer_);
  glVertexAttribPointer(idAttrib_, 1, GL_FLOAT, GL_FALSE, 0, 0);

  if (useElements_Indices) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
    glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);
  } else {
    glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
    glDrawArrays(GL_TRIANGLES, 0, sizeArray_);
  }

  glDisableVertexAttribArray(posId_);
  glDisableVertexAttribArray(normAttrib_);
  glDisableVertexAttribArray(idAttrib_);
}

void IdMapShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/id_map_vertex_shader.glsl");
  shaderManager_.addShader(GL_FRAGMENT_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/id_map_fragment_shader.glsl");
  shaderManager_.finalize();
}

void IdMapShaderProgram::createAttributes() {
  posId_ = shaderManager_.getAttribLocation("position");
  normAttrib_ = shaderManager_.getAttribLocation("normal");
  idAttrib_ = shaderManager_.getAttribLocation("id");
}

void IdMapShaderProgram::createUniforms() {
  mvpId_ = shaderManager_.getUniformLocation("MVP");
  imageTexId_ = shaderManager_.getUniformLocation("imageMaxima");
  shadowMapId_ = shaderManager_.getUniformLocation("shadowMap");
  alphaId_ = shaderManager_.getUniformLocation("alpha");
}
