/*
 * LaplacianSmoothShaderProgram.h
 *
 *  Created on: 16/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_LAPLACIANSMOOTHSHADERPROGRAM_H_
#define SWEEPMESH_LAPLACIANSMOOTHSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <glm.hpp>
class LaplacianSmoothShaderProgram : public ShaderProgram {
public:
  LaplacianSmoothShaderProgram(int imageWidth, int imageHeight);
  virtual ~LaplacianSmoothShaderProgram();
  void compute();
  void createTransformFeedback(int length);

  void setFeedbackLength(GLuint feedbackLength) {
    feedbackLength_ = feedbackLength;
  }

  const std::vector<glm::vec3>& getFeedbackTr() const {
    return feedbackTr;
  }
private:
  void init();

  void createAttributes();
  void createUniforms();

  GLuint posAttrib_;

  GLuint feedbackBuffer_;
  GLuint feedbackLength_;
  std::vector<glm::vec3> feedbackTr;
};

#endif /* SWEEPMESH_LAPLACIANSMOOTHSHADERPROGRAM_H_ */
