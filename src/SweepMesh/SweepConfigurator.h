/*
 * SweepConfigurator.h
 *
 *  Created on: 01/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_SWEEPCONFIGURATOR_H_
#define SWEEPMESH_SWEEPCONFIGURATOR_H_

#include <string>
#include <fstream>
#include <iostream>
#include "../types.hpp"

class SweepConfigurator {
public:
	SweepConfigurator(const std::string &path);
	virtual ~SweepConfigurator();
	SweepConfiguration parseConfigFile();
private:

    Configuration config_;
    std::ifstream file_;
};

#endif /* SWEEPMESH_SWEEPCONFIGURATOR_H_ */
