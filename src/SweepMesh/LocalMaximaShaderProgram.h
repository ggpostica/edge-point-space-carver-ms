/*
 * LocalMaximaShaderProgram.h
 *
 *  Created on: 08/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_LOCALMAXIMASHADERPROGRAM_H_
#define SWEEPMESH_LOCALMAXIMASHADERPROGRAM_H_

#include "ShaderProgram.h"

class LocalMaximaShaderProgram : public ShaderProgram {
public:
  LocalMaximaShaderProgram(int imageWidth, int imageHeight);
  LocalMaximaShaderProgram(int imageWidth, int imageHeight, float threshold);
  LocalMaximaShaderProgram(int imageWidth, int imageHeight, float threshold, int window);
  virtual ~LocalMaximaShaderProgram();
  void initializeFramebufAndTex(GLuint &texId);

  void compute(bool renderFrameBuf = false);
  void resetTex();

  void setImageNccTex(GLuint imageNccTex) {
    imageNCCTex_ = imageNccTex;
  }

  void setThreshold(float threshold) {
    threshold_ = threshold;
  }

  void setWindow(float window) {
    window_ = window;
  }

private:
  void init();

  void createAttributes();
  void createUniforms();

  GLuint framebuffer_;
  /*uniforms id*/
  GLuint imWid_, imHid_, imageNCCTexId_, thId_, wId_;
  /*tex id*/
  GLuint nonMaximaTexId_, imageNCCTex_, imageTex_;
  /*attributes id*/
  GLuint posId_, texCoordId_;

  float threshold_;
  float window_;
};

#endif /* SWEEPMESH_LOCALMAXIMASHADERPROGRAM_H_ */
