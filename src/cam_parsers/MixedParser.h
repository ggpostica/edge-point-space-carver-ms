/*
 * MIxedParser.h
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#ifndef MIXEDPARSER_H_
#define MIXEDPARSER_H_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "../types.hpp"
class MIxedParser {
  public:
    MIxedParser();
    virtual ~MIxedParser();
    void parse();
    CameraRect &getNextCam();
    std::vector<cv::Point3d> &getCurPoints();


    const std::vector<std::string>& getCamPaths() const {
      return camPaths_;
    }

    const std::vector<CameraRect>& getCams() const {
      return cams_;
    }

    const std::vector<std::vector<int> >& getCamViewingPointN() const {
      return camViewingPointN;
    }

    const std::vector<int> & getCamViewingPointN(int idxPoint) const {
      return camViewingPointN[idxPoint];
    }

    int getNumCameras() const {
      return numCameras_;
    }

    int getNumPoints() const {
      return numPoints_;
    }

    const std::vector<cv::Point3d>& getPoints() const {
      return points_;
    }

    const std::vector<std::vector<int> >& getPointsVisibleFromCamN() const {
      return pointsVisibleFromCamN;
    }

    const std::vector<int> & getPointsVisibleFromCamN(int idxCam) const {
      return pointsVisibleFromCamN[idxCam];
    }

  private:
    int curCam_;
    std::vector<std::string> camPaths_;
    std::vector<CameraRect> cams_;
    std::vector<cv::Point3d> points_;
    std::vector<std::vector<int> > pointsVisibleFromCamN;
    std::vector<std::vector<int> > camViewingPointN;
    std::ifstream fileStream_;
    std::ifstream fileStream2_;
    std::ofstream camOutFile_;
    int numCameras_;
    int numPoints_;
    std::vector<cv::Point3d> curPoints_;
};

#endif /* MIXEDPARSER_H_ */
