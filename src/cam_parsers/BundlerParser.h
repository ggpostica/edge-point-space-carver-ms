//  Copyright 2014 Andrea Romanoni
//
//  This file is part of edgePointSpaceCarver.
//
//  edgePointSpaceCarver is free software: you can redistribute it
//  and/or modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version see
//  <http://www.gnu.org/licenses/>..
//
//  edgePointSpaceCarver is distributed in the hope that it will be
//  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

#ifndef BUNDLERPARSER_H_
#define BUNDLERPARSER_H_

// This class parses the output of Bundler or VisualSfM software and stores the
// camera calibration parameters and the 3D point positions
#include <iostream>
#include <fstream>
#include <vector>

#include <Eigen/Core>

#include "Parser.h"

class BundlerParser: public Parser {
  public:
    BundlerParser(std::string fileInput);
    virtual ~BundlerParser();
    bool parseFile();
};

#endif /* BUNDLERPARSER_H_ */
