//  Copyright 2014 Andrea Romanoni
//
//  This file is part of edgePointSpaceCarver.
//
//  edgePointSpaceCarver is free software: you can redistribute it
//  and/or modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version see
//  <http://www.gnu.org/licenses/>..
//
//  edgePointSpaceCarver is distributed in the hope that it will be
//  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

#ifndef PCLPARSER_H_
#define PCLPARSER_H_

#include "Parser.h"
#include <iostream>
#include <fstream>
#include <vector>

#include <Eigen/Core>
class PclParser: public Parser {
  public:
    PclParser(std::string fileInput);
    virtual ~PclParser();
    bool parseFile();
};

#endif /* PCLPARSER_H_ */
