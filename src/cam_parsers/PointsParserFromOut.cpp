/*
 * PointsParserFromOut.cpp
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#include "PointsParserFromOut.h"
#include "../types.hpp"

PointsParserFromOut::PointsParserFromOut(std::string path, int imageW, int imageH) {
  numPoints_ = -1;

  fileStream_.open(path.c_str(), std::ios::in);
  imageWidth_ = imageW;
  imageHeight_ = imageH;

}

PointsParserFromOut::~PointsParserFromOut() {
}

void PointsParserFromOut::parse() {
  std::string line;
  int numCameras;

  std::istringstream iss;
  //read and discard the first line (# Bundle file v0.3)
  std::getline(fileStream_, line);
  iss.str(line);

  //read num cam
  std::getline(fileStream_, line);
  iss.str(line);
  iss >> numCameras >> numPoints_;

  camViewingPointN_.assign(numPoints_, std::vector<int>());
  point2DoncamViewingPoint_.assign(numPoints_, std::vector<cv::Point2f>());
  pointsVisibleFromCamN_.assign(numCameras, std::vector<int>());

  for (int curPoint = 0; curPoint < numPoints_; ++curPoint) {
    int lengthList, tempKey, tempIdx;
    float tempX, tempY;
    PointParser tempPoint;

    //point's position <x, y, z>
    std::getline(fileStream_, line);
    iss.clear();
    iss.str(line);
    iss >> tempPoint.x >> tempPoint.y >> tempPoint.z;

    std::getline(fileStream_, line);
    iss.clear();
    iss.str(line);
    //view list
    std::getline(fileStream_, line);
    iss.clear();
    iss.str(line);
    iss >> lengthList;
    for (int curViewingCamera = 0; curViewingCamera < lengthList; ++curViewingCamera) {
      iss >> tempIdx;

      camViewingPointN_[curPoint].push_back(tempIdx);
      pointsVisibleFromCamN_[tempIdx].push_back(curPoint);

      iss >> tempKey;
      iss >> tempX;
      tempPoint.viewingCamerasX.push_back(static_cast<float>(imageWidth_) / 2 + tempX);
      iss >> tempY;
      tempPoint.viewingCamerasY.push_back(static_cast<float>(imageHeight_) / 2 - tempY);
      cv::Point temp;
      temp.x = tempX;
      temp.y = tempY;
      point2DoncamViewingPoint_[curPoint].push_back(temp);
    }
    cv::Point3d temp;
    temp.x = tempPoint.x;
    temp.y = tempPoint.y;
    temp.z = tempPoint.z;
    points_.push_back(temp);

  }
}
