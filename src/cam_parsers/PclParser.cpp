//  Copyright 2014 Andrea Romanoni
//
//  This file is part of edgePointSpaceCarver.
//
//  edgePointSpaceCarver is free software: you can redistribute it
//  and/or modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version see
//  <http://www.gnu.org/licenses/>..
//
//  edgePointSpaceCarver is distributed in the hope that it will be
//  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

#include "PclParser.h"

PclParser::PclParser(std::string fileName) :
    Parser(fileName) {
  fileStream_.open(fileName_.c_str(), std::ios::in);
}

PclParser::~PclParser() {

}

bool PclParser::parseFile() {

  std::string line;

  //read and discard the first line (# .PCD v0.7 - Point Cloud Data file format)
  std::getline(fileStream_, line);

  //hypothesize single-scan -> numCam = 1
  numCameras_ = 1;
  //skip VERSION field
  std::getline(fileStream_, line);
  //skip FIELDS field
  std::getline(fileStream_, line);
  //skip SIZE field
  std::getline(fileStream_, line);
  //skip TYPE field
  std::getline(fileStream_, line);
  //skip COUNT field
  std::getline(fileStream_, line);
  //skip WIDTH field
  std::getline(fileStream_, line);
  //skip HEIGHT field
  std::getline(fileStream_, line);
  //skip VIEWPOINT field
  std::getline(fileStream_, line);
  //read POINTS field
  std::getline(fileStream_, line);
  std::istringstream iss(line);
  std::string buf;
  iss >> buf >> numPoints_;
  //skip DATA ascii
  std::getline(fileStream_, line);

  for (int curCam = 0; curCam < numCameras_; ++curCam) {
    SensorParser tempCamera;
    //laser no camera
    tempCamera.f  =  1.0;
    tempCamera.k1 =  0.0;
    tempCamera.k2 =  0.0;
    //SIMPLIFICATION: No rototraslation, only one laser
    //rotation matrix <R>
    tempCamera.R(0, 0) = 1;
    tempCamera.R(0, 1) = 0;
    tempCamera.R(0, 2) = 0;
    tempCamera.R(1, 0) = 0;
    tempCamera.R(1, 1) = 1;
    tempCamera.R(1, 2) = 0;
    tempCamera.R(2, 0) = 0;
    tempCamera.R(2, 1) = 0;
    tempCamera.R(2, 2) = 1;

    //translation vector <t>
    tempCamera.t(0) = 0;
    tempCamera.t(1) = 0;
    tempCamera.t(2) = 0;

    //camera center calc
    tempCamera.center(0, 0) = 0;
    tempCamera.center(1, 0) = 0;
    tempCamera.center(2, 0) = 0;
    camerasList_.push_back(tempCamera);
  }

  for (int curPoint = 0; curPoint < numPoints_; ++curPoint) {
    PointParser tempPoint;

    //point's position <x, y, z>
    std::getline(fileStream_, line);
    iss.str(line);
    sscanf(iss.str().c_str(), "%f %f %f", &tempPoint.x,&tempPoint.y,&tempPoint.z);
    std::cout << iss.str() << std::endl;
    std::cout << tempPoint.x << tempPoint.y<< tempPoint.z<< std::endl;

    tempPoint.viewingCamerasIndices.push_back(0);

    pointsList_.push_back(tempPoint);
  }
  return true;
}
