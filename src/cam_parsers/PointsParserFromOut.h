/*
 * PointsParserFromOut.h
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#ifndef CAM_PARSERS_POINTSPARSERFROMOUT_H_
#define CAM_PARSERS_POINTSPARSERFROMOUT_H_
#include <string>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>


class PointsParserFromOut {
public:
  PointsParserFromOut(const std::string path, int imageW, int imageH);
  virtual ~PointsParserFromOut();
  void parse();

  const std::vector<cv::Point3d>& getPoints() const {
    return points_;
  }

  const std::vector<std::vector<int> >& getCamViewingPointN() const {
    return camViewingPointN_;
  }

  const std::vector<std::vector<int> >& getPointsVisibleFromCamN() const {
    return pointsVisibleFromCamN_;
  }

  const std::vector<int> & getPointsVisibleFromCamN(int idxCam) const {
    return pointsVisibleFromCamN_[idxCam];
  }

  std::vector<std::vector<cv::Point2f> >& getPoint2DoncamViewingPoint()  {
    return point2DoncamViewingPoint_;
  }

private:

  std::vector<cv::Point3d> points_;
  int numPoints_;
  std::ifstream fileStream_;
  std::vector<std::vector<int> > camViewingPointN_;
  std::vector<std::vector<int> > pointsVisibleFromCamN_;
  std::vector<std::vector<cv::Point2f> > point2DoncamViewingPoint_;

  int imageWidth_, imageHeight_;
};

#endif /* CAM_PARSERS_POINTSPARSERFROMOUT_H_ */
