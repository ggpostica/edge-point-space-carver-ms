/*
 * PathCreatorSweep.cpp
 *
 *  Created on: 28/apr/2015
 *      Author: andrea
 */

#include "PathCreatorSweep.h"
#include <boost/filesystem.hpp>

PathCreatorSweep::PathCreatorSweep(SpaceCarvingConfig &spaceCarvingConfig, SweepConfiguration &sweepConf, std::string prefix, std::string prefixFolder) {
  spaceCarvingConfig_ = spaceCarvingConfig;
  sweepConf_ = sweepConf;
  prefix_ = prefix;
  prefix_Folder_ = prefixFolder;
  pathWithSpaceCarving_ = true;
  createDir();
}

PathCreatorSweep::PathCreatorSweep(SweepConfiguration &sweepConf, std::string prefix, std::string prefixFolder) {
  pathWithSpaceCarving_ = false;
  sweepConf_ = sweepConf;
  prefix_ = prefix;
  prefix_Folder_ = prefixFolder;
  createDir();
}
PathCreatorSweep::~PathCreatorSweep() {

}

void PathCreatorSweep::createDir() {

  std::stringstream pathSave, pathFile, pathEnd;
  pathSave << prefix_ << sweepConf_.outputSweep.nameDataset << "_" << sweepConf_.sweepConfig.kSweepingDistance << "_" << sweepConf_.sweepConfig.numPlanes << "_"
      << sweepConf_.sweepConfig.thresholdNCC << "_" << sweepConf_.sweepConfig.windowNCC << "_" << sweepConf_.sweepConfig.windowLocalMaxima;
  if (pathWithSpaceCarving_) {
    if (spaceCarvingConfig_.inverseConicEnabled) {
      pathSave << "_inverseConicEnabled";
      pathSave << "_" << spaceCarvingConfig_.probOrVoteThreshold;
    } else {
      pathSave << "_NotinverseConicEnabled";
    }
    if (spaceCarvingConfig_.enableSuboptimalPolicy) {
      pathSave << "_withSuboptimal" << "_" << spaceCarvingConfig_.suboptimalMethod;
    } else {
      pathSave << "_withoutSuboptimal";
    }
  }
  pathRoot_ = pathSave.str();

  pathEnd << prefix_Folder_ << "/" << pathSave.str() << "/";

  pathRootDir_ = pathEnd.str();
  boost::filesystem::path dir(pathRootDir_.c_str());
  boost::filesystem::create_directories(dir);
}

std::string PathCreatorSweep::getPathFirstMesh() {

  std::stringstream pathFile;
  pathFile << pathRootDir_ << pathRoot_ << sweepConf_.outputSweep.nameFirstMesh;

  return pathFile.str();
}

std::string PathCreatorSweep::getPathMesh() {

  std::stringstream pathFile;
  pathFile << pathRootDir_ << pathRoot_ << sweepConf_.outputSweep.nameMesh;

  return pathFile.str();
}
