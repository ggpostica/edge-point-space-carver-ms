#include "conversionUtilities.hpp"

namespace conversionUtilities {
msc::Matrix toLoviMatrix(Eigen::Vector3f mat) {
  msc::Matrix tmpMatrix(3, 1);
  tmpMatrix.set(0, 0, (double) mat(0));
  tmpMatrix.set(1, 0, (double) mat(1));
  tmpMatrix.set(2, 0, (double) mat(2));
  return tmpMatrix;
}

msc::Matrix toLoviMatrix(cv::Point3d triangulated3DPoint) {
  msc::Matrix tmpMatrix(3, 1);
  double curX = (double) triangulated3DPoint.x;
  double curY = (double) triangulated3DPoint.y;
  double curZ = (double) triangulated3DPoint.z;
  tmpMatrix.set(0, 0, curX);
  tmpMatrix.set(1, 0, curY);
  tmpMatrix.set(2, 0, curZ);
  return tmpMatrix;
}
msc::Matrix toLoviMatrix(cv::Point3f triangulated3DPoint) {
  msc::Matrix tmpMatrix(3, 1);
  double curX = (double) triangulated3DPoint.x;
  double curY = (double) triangulated3DPoint.y;
  double curZ = (double) triangulated3DPoint.z;
  tmpMatrix.set(0, 0, curX);
  tmpMatrix.set(1, 0, curY);
  tmpMatrix.set(2, 0, curZ);
  return tmpMatrix;
}
}

msc::Matrix conversionUtilities::toLoviMatrix(glm::vec3 point) {
  msc::Matrix tmpMatrix(3, 1);
  tmpMatrix.set(0, 0, point.x);
  tmpMatrix.set(1, 0, point.y);
  tmpMatrix.set(2, 0, point.z);
  return tmpMatrix;
}

Eigen::Matrix4f conversionUtilities::toEigenMatrix(glm::mat4 mat) {
  Eigen::Matrix4f matOut;

  for (int curR = 0; curR < 4; ++curR) {
    for (int curC = 0; curC < 4; ++curC) {
      matOut(curR, curC) = mat[curR][curC];
    }
  }

  return matOut;

}

cv::Mat conversionUtilities::toOpenCVMatrix(glm::mat4 mat, bool cameraMat4x4) {
  int maxR;
  if (cameraMat4x4)
    maxR = 4;
  else
    maxR = 3;

  cv::Mat matOut = cv::Mat(maxR, 4, CV_32F);

  for (int curR = 0; curR < maxR; ++curR) {
    for (int curC = 0; curC < 4; ++curC) {
      matOut.at<float>(curR, curC) = mat[curR][curC];
    }
  }

  return matOut;

}
