/*
 * Logger.h
 *
 *  Created on: 10/apr/2015
 *      Author: andrea
 */

#ifndef UTILITIES_LOGGER_H_
#define UTILITIES_LOGGER_H_

#include <sys/time.h>
#include <vector>
#include <string>

namespace utilities {

class Logger {
public:
  Logger();
  virtual ~Logger();
  void resetOutputPercision();
  void enable();
  void disable();
  void startEvent();
  void startEvent(std::string message, bool newParagraph = false);
  void endEvent();
  void endEvent(std::string message, bool newParagraph = false);
  void printLastTime(bool newParagraph = false);
  void printLastTime(std::string message, bool newParagraph = false);
  void endEventAndPrint(bool newParagraph = false);
  void endEventAndPrint(std::string message, bool newParagraph = false);
private:
  void computeTime();
  std::vector<struct timeval> eventBeginnings, eventEnds;
  float lastDelta_;
  bool enable_;

};

} /* namespace utilities */

#endif /* UTILITIES_LOGGER_H_ */
