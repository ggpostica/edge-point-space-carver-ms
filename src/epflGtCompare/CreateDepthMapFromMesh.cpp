/*
 * CreateDepthMapFromMesh.cpp
 *
 *  Created on: 29/apr/2015
 *      Author: andrea
 */

#include "CreateDepthMapFromMesh.h"
#include <iostream>
#include <sstream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

#include "../utilities/utilities.hpp"

CreateDepthMapFromMesh::CreateDepthMapFromMesh(std::string fileMesh, int imageWidth, int imageHeight, glm::mat4 &cameraM, glm::vec3 &cameraCenter) {
  isMesh_ = false;
  loadFromVertices(fileMesh);

  imageWidth_ = imageWidth;
  imageHeight_ = imageHeight;
  cameraM_ = cameraM;
  cameraCenter_ = cameraCenter;
}

CreateDepthMapFromMesh::CreateDepthMapFromMesh(std::string fileMesh, int imageWidth, int imageHeight, CameraType cam, int cutPart) {
  isMesh_ = true;
  //mesh_.loadFormat(fileMesh.c_str(), false);

  loadMesh(fileMesh, cutPart);
  imageWidth_ = imageWidth;
  imageHeight_ = imageHeight;
  cam_ = cam;
  cameraCenter_ = cam_.center;
  pixels_ = new GLfloat[imageWidth * imageHeight * 1];
  depthMap_ = 10000000.0 * cv::Mat::ones(imageHeight_, imageWidth_, CV_32F);

  initialize();
}
CreateDepthMapFromMesh::~CreateDepthMapFromMesh() {
}

void CreateDepthMapFromMesh::loadMesh(std::string path, int curPart) {

  std::string line;

  std::istringstream iss;

  //read num cam

  std::ifstream file(path);

  int numV, numF;
  float x, y, z;
  int a, b, c;
  std::string temp, temp2;
  do {
    temp.clear();
    temp2.clear();
    std::getline(file, line);
    iss.clear();
    iss.str(line);
    iss >> temp;
    if (temp.compare(std::string("element")) == 0) {
      iss >> temp2;
      if (temp2.compare(std::string("vertex")) == 0) {
        iss >> numV;
      } else if (temp2.compare(std::string("face")) == 0) {
        iss >> numF;
      }
    }
  } while (temp.compare(std::string("end_header")) != 0);

  for (int curV = 0; curV < numV; ++curV) {
    std::getline(file, line);
    iss.str(line);

    sscanf(iss.str().c_str(), "%f %f %f ", &x, &y, &z);

    vert_.push_back(glm::vec3(x, y, z));
  }

  int first = static_cast<float>(static_cast<float>(curPart) / 20.0) * numF;
  int last = static_cast<float>(static_cast<float>(curPart + 1) / 20.0) * numF;
  for (int curV = 0; curV < numF; ++curV) {
    std::getline(file, line);
    if (first <= curV && curV < last) {
      iss.str(line);
      int n;
      iss >> n >> a >> b >> c;
      sscanf(iss.str().c_str(), "%d %d %d %d ", &n, &a, &b, &c);
      /*glm::vec3 boundingBoxUL = glm::vec3(4.47677, -14.2158, -9.03138);
      glm::vec3 boundingBoxDR = glm::vec3(16.2159, -1.63118, 1.32302);*/
      glm::vec3 boundingBoxUL = glm::vec3(-20.9042, -21.68, -6.68267);
      glm::vec3 boundingBoxDR = glm::vec3(-9.85972, -8.77828, 1.71626 );

      if ((vert_[a].x >= boundingBoxUL.x && vert_[a].x <= boundingBoxDR.x && //
          vert_[a].y >= boundingBoxUL.y && vert_[a].y <= boundingBoxDR.y && //
          vert_[a].z >= boundingBoxUL.z && vert_[a].z <= boundingBoxDR.z) || //
          (vert_[b].x >= boundingBoxUL.x && vert_[b].x <= boundingBoxDR.x && //
              vert_[b].y >= boundingBoxUL.y && vert_[b].y <= boundingBoxDR.y && //
              vert_[b].z >= boundingBoxUL.z && vert_[b].z <= boundingBoxDR.z) || //
          (vert_[c].x >= boundingBoxUL.x && vert_[c].x <= boundingBoxDR.x && //
              vert_[c].y >= boundingBoxUL.y && vert_[c].y <= boundingBoxDR.y && //
              vert_[c].z >= boundingBoxUL.z && vert_[c].z <= boundingBoxDR.z)) {
        fac_.push_back(glm::vec3(a, b, c));
      } else {

      }
    }
  }
}

void CreateDepthMapFromMesh::compute() {
  std::cout << "InitCompute" << std::endl;
  if (isMesh_) {
    computeFromMesh();
  } else {
    computeFromVertices();
  }
  std::cout << "EndCompute" << std::endl;
}

void CreateDepthMapFromMesh::store(std::string fileName) {
  std::cout << "Init Store" << std::endl;
  if (!isMesh_) {
    std::ofstream f(fileName);
    for (int curR = 0; curR < imageHeight_; ++curR) {
      for (int curC = 0; curC < imageWidth_; ++curC) {
        f << depthMap_.at<float>(curR, curC) << " ";
      }
      f << std::endl;
    }
    f.close();
  } else {

    dumpTextureOnCPU();
    collectPointsFromPixels();

    std::ofstream f(fileName);
    for (int curR = 0; curR < imageHeight_; ++curR) {
      for (int curC = 0; curC < imageWidth_; ++curC) {
        f << depthMap_.at<float>(curR, curC) << " ";
      }
      f << std::endl;
    }
    f.close();
  }
  std::cout << "End Store" << std::endl;
}

void CreateDepthMapFromMesh::computeFromMesh() {
  /*
   int curX, curY, curW;
   int count = 0;
   glm::vec3 vecCamPoint;
   float dist;
   depthMap_ = 10000000.0 * cv::Mat::ones(imageHeight_, imageWidth_, CV_32F);
   for (Vertex_iterator v = mesh_.p.vertices_begin(); v != mesh_.p.vertices_end(); ++v) {
   curX = static_cast<int>(cameraM_[0][0] * v->point().x() + cameraM_[0][1] * v->point().y() + cameraM_[0][2] * v->point().z() + cameraM_[0][3] * 1);
   curY = static_cast<int>(cameraM_[1][0] * v->point().x() + cameraM_[1][1] * v->point().y() + cameraM_[1][2] * v->point().z() + cameraM_[1][3] * 1);
   curW = static_cast<int>(cameraM_[2][0] * v->point().x() + cameraM_[2][1] * v->point().y() + cameraM_[2][2] * v->point().z() + cameraM_[2][3] * 1);

   curX = static_cast<int>(curX / curW);
   curY = static_cast<int>(curY / curW);

   if (0 < curX && curX < imageHeight_ && 0 < curY && curY < imageWidth_) {
   dist = sqrt(
   (v->point().x() - cameraCenter_.x) * (v->point().x() - cameraCenter_.x) + (v->point().y() - cameraCenter_.y) * (v->point().y() - cameraCenter_.y)
   + (v->point().z() - cameraCenter_.z) * (v->point().z() - cameraCenter_.z));

   if (dist < depthMap_.at<float>(curY, curX)) {
   depthMap_.at<float>(curY, curX) = dist;
   }
   }

   if (count % 10000) {
   std::cout << count << " points processed (tot points: " << mesh_.p.size_of_vertices() << ")" << std::endl;
   }
   ++count;
   }*/
  run();
}

void CreateDepthMapFromMesh::computeFromVertices() {
  int curXF, curYF, curWF;
  int curX, curY;
  int count = 0;
  glm::vec3 vecCamPoint;
  float dist;
  depthMap_ = 10000000.0 * cv::Mat::ones(imageHeight_, imageWidth_, CV_32F);
  for (auto v : vertices_) {
    curXF = static_cast<float>(cameraM_[0][0] * v.x + cameraM_[0][1] * v.y + cameraM_[0][2] * v.z + cameraM_[0][3] * 1);
    curYF = static_cast<float>(cameraM_[1][0] * v.x + cameraM_[1][1] * v.y + cameraM_[1][2] * v.z + cameraM_[1][3] * 1);
    curWF = static_cast<float>(cameraM_[2][0] * v.x + cameraM_[2][1] * v.y + cameraM_[2][2] * v.z + cameraM_[2][3] * 1);

    curX = static_cast<int>(curXF / curWF);
    curY = static_cast<int>(curYF / curWF);

    if (0 < curX && curX < imageWidth_ && 0 < curY && curY < imageHeight_) {
      dist = sqrt(
          (v.x - cameraCenter_.x) * (v.x - cameraCenter_.x) + (v.y - cameraCenter_.y) * (v.y - cameraCenter_.y)
              + (v.z - cameraCenter_.z) * (v.z - cameraCenter_.z));

      if (dist < depthMap_.at<float>(curY, curX)) {
        depthMap_.at<float>(curY, curX) = dist;
      }
    }

    if (count % 100000 == 0) {
      std::cout << count << " points processed (tot points: " << numVert_ << ")" << std::endl;
    }
    ++count;
  }
}

void CreateDepthMapFromMesh::loadFromVertices(std::string &fileMesh) {
  std::ifstream newpointsObj(fileMesh);

  std::string line;
  char tmp[2];

  std::istringstream iss;
  float x, y, z;

  while (std::getline(newpointsObj, line)) {
    iss.str(line);
    sscanf(iss.str().c_str(), "%s %f %f %f", tmp, &x, &y, &z);
    vertices_.push_back(glm::vec3(x, y, z));
    if (vertices_.size() % 100000 == 0) {
      std::cout << vertices_.size() << "----loading----points read" << std::endl;
    }
  }

  std::cout << vertices_.size() << " points read" << std::endl;
  newpointsObj.close();

}

void CreateDepthMapFromMesh::run() {
  glm::mat4 mvp = setCameraParamAndGetMvp(cam_);

  depthMapComp_->setArrayBufferObj(vertexBufferObj_, numVert_);
  depthMapComp_->setUseElementsIndices(false);
  static_cast<DepthShaderProgram *>(depthMapComp_)->setAlpha(0.0);
  static_cast<DepthShaderProgram *>(depthMapComp_)->setCamCenter(cameraCenter_);
  static_cast<DepthShaderProgram *>(depthMapComp_)->computeDepthMap(framebufferDepth_, mvp);

  std::cout << "end depth";
  depthProgram_->setArrayBufferObj(vertexBufferObj_, numVert_);
  depthProgram_->setUseElementsIndices(false);
  static_cast<DepthMapFromMeshShaderProgram *>(depthProgram_)->setDepthTexture(depthTexture_);
  static_cast<DepthMapFromMeshShaderProgram *>(depthProgram_)->setCameraCenter(cameraCenter_);
  static_cast<DepthMapFromMeshShaderProgram *>(depthProgram_)->setMvp(mvp);
  static_cast<DepthMapFromMeshShaderProgram *>(depthProgram_)->compute(true);
  std::cout << "end run";
  glFinish();
  std::cout << "end finish";
  //SwapBuffers();
}

void CreateDepthMapFromMesh::initialize() {
  vertexBufferObj_ = -1;

  depthProgram_ = new DepthMapFromMeshShaderProgram(imageWidth_, imageHeight_);
  depthMapComp_ = new DepthShaderProgram(imageWidth_, imageHeight_);

  cameraTransformationController_ = new TransformationController(static_cast<float>(imageWidth_), static_cast<float>(imageHeight_));
  init();
  if (isMesh_) {
    createVertexArrayBufferMesh();
  } else {
    createVertexArrayBuffer();
  }
  initShaders();
}

void CreateDepthMapFromMesh::initShaders() {
  depthMapComp_->initializeProgram();
  static_cast<DepthShaderProgram *>(depthMapComp_)->initializeFramebufAndTex(framebufferDepth_, depthTexture_);

  depthProgram_->initializeProgram();
  static_cast<DepthMapFromMeshShaderProgram *>(depthProgram_)->initializeFramebufAndTex(resultTex_);

}

void CreateDepthMapFromMesh::createVertexArrayBuffer() {
  glGenBuffers(1, &vertexBufferObj_);

  glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObj_);
  std::vector<glm::vec3> verticesUnwrapped;

  for (Facet_iterator itFac = mesh_.p.facets_begin(); itFac != mesh_.p.facets_end(); itFac++) {
    Halfedge_handle h0, h1, h2;
    h0 = itFac->halfedge();
    h1 = h0->next();
    h2 = h1->next();

    Vertex_handle v0, v1, v2;
    v0 = h0->vertex();
    v1 = h1->vertex();
    v2 = h2->vertex();

    verticesUnwrapped.push_back(glm::vec3(v0->point().x(), v0->point().y(), v0->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v1->point().x(), v1->point().y(), v1->point().z()));
    verticesUnwrapped.push_back(glm::vec3(v2->point().x(), v2->point().y(), v2->point().z()));
  }
  numVert_ = 3 * mesh_.p.size_of_facets();

  glBufferData(GL_ARRAY_BUFFER, 3 * mesh_.p.size_of_facets() * sizeof(glm::vec3), &verticesUnwrapped[0], GL_STATIC_DRAW);
}

void CreateDepthMapFromMesh::createVertexArrayBufferMesh() {
  glGenBuffers(1, &vertexBufferObj_);

  glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObj_);
  std::vector<glm::vec3> verticesUnwrapped;

  for (auto f : fac_) {
    verticesUnwrapped.push_back(vert_[static_cast<int>(f.x)]);
    verticesUnwrapped.push_back(vert_[static_cast<int>(f.y)]);
    verticesUnwrapped.push_back(vert_[static_cast<int>(f.z)]);
  }
  numVert_ = verticesUnwrapped.size();

  glBufferData(GL_ARRAY_BUFFER, verticesUnwrapped.size() * sizeof(glm::vec3), &verticesUnwrapped[0], GL_STATIC_DRAW);
}

glm::mat4 CreateDepthMapFromMesh::setCameraParamAndGetMvp(const CameraType& cam) {
  cameraTransformationController_->setIntrinsicParameters(cam.intrinsics[0][0], cam.intrinsics[1][1], cam.intrinsics[0][2], cam.intrinsics[1][2]);
  cameraTransformationController_->setExtrinsicParameters(cam.rotation, cam.translation);
  glm::mat4 mvp = cameraTransformationController_->getMvpMatrix();
  return mvp;
}

void CreateDepthMapFromMesh::dumpTextureOnCPU() {
  GLint width, height, depth;

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
//glReadBuffer(GL_COLOR_ATTACHMENT0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, resultTex_);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_RED_SIZE, &depth);

  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ROW_LENGTH, 0);
  glPixelStorei(GL_PACK_SKIP_ROWS, 0);
  glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
  glBindTexture(GL_TEXTURE_2D, resultTex_);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, pixels_);
  glDisable(GL_TEXTURE_2D);
}

void CreateDepthMapFromMesh::collectPointsFromPixels() {
  for (int i = 0; i < imageHeight_; ++i) {
    for (int j = 0; j < imageWidth_; ++j) {

      depthMap_.at<float>(i, j) = (float) (pixels_[(imageHeight_ - i - 1) * 1 * imageWidth_ + j * 1 + 0]);

      //DEBUG
      //std::cout << "IN ___ depthMap_.at<float>(i, j): " << depthMap_.at<float>(i, j)<< std::endl;
    }
  }
}

