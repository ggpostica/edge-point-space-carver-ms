/*
 * ManifoldReconstructor.h
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#ifndef MANIFOLDRECONSTRUCTOR_H_
#define MANIFOLDRECONSTRUCTOR_H_
#include "cam_parsers/CamParser.h"
#include "cam_parsers/PointsParserFromOut.h"
#include <vector>
#include "FreespaceDelaunayManifold.h"
#include "types.hpp"
#include <Mesh.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <glm.hpp>

class ManifoldReconstructor {
public:
  ManifoldReconstructor(ManifoldConfig myConf, Configuration conf);
  virtual ~ManifoldReconstructor();

  void runFirstIteration();
  void run(std::vector<msc::Matrix> points, std::vector<std::vector<int> > vis, bool resetAll = false);
  void computeMesh(bool bounding = false);

  void storeFreespaceMesh(std::string nameFile);
  void setWeightsSpaceCarver(float w_1, float w_2, float w_3);
  void setThSpaceCarver(float th);
  void setInfinityVal(float inf);

  Mesh& getMesh() {
    return mesh_;
  }

  // void run();
private:
  void addSfMPointsAndCams();
  void writeOFF(const std::string filename, const std::vector<msc::Matrix> & points, const std::vector<msc::Matrix> & tris, bool bounding = false);
  void saveCurrentManifoldC(msc::FreespaceDelaunayManifold &spaceCarver, std::string suffix);

  void storeManifoldMeshC(msc::FreespaceDelaunayManifold &spaceCarver, std::string nameFile);

  Mesh mesh_;
  CamParser *camParser_;
  PointsParserFromOut *pointParser_;
  std::vector<cv::Mat> images_;
  msc::FreespaceDelaunayManifold *spaceCarver_;
  ManifoldConfig manifConf_;
  Configuration conf_;

  glm::vec3 boundingMinVert;
  glm::vec3 boundingMaxVert;

};

#endif /* MANIFOLDRECONSTRUCTOR_H_ */
