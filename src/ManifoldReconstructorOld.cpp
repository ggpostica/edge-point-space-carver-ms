/*
 * ManifoldReconstructor.cpp
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#include "ManifoldReconstructorOld.h"
#include "utilities/conversionUtilities.hpp"
#include <sstream>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/eigen.hpp>
#include <Eigen/Core>


ManifoldReconstructorOld::ManifoldReconstructorOld(Configuration myConf) {
  parser_.parse();
  std::vector<std::string> pathImages = parser_.getCamPaths();
  for (auto curPath : pathImages) {
    images_.push_back(cv::imread(curPath, 0));
  }
  conf_ = myConf;
  spaceCarver_.setConfig(myConf);

}

ManifoldReconstructorOld::~ManifoldReconstructorOld() {
}


void storeManifoldMesh(msc::FreespaceDelaunayManifold &spaceCarver,std::string nameFile) {

      std::cout << "storeManifoldMesh...";
      std::vector<msc::Matrix> m_arrModelPoints;
      std::vector<msc::Matrix> m_lstModelTris;
      spaceCarver.tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
      spaceCarver.writeObj(nameFile.c_str(), m_arrModelPoints, m_lstModelTris);
      std::cout << "DONE." << std::endl;
}
void saveCurrentManifold(msc::FreespaceDelaunayManifold &spaceCarver,std::string suffix){

  std::stringstream namefile;
  namefile << "ProvaHerzu_ManifinverseConicEnabled_" << suffix << ".obj";
  storeManifoldMesh(spaceCarver, namefile.str());
}


void ManifoldReconstructorOld::run() {

  for (int curCamIdx = 0; curCamIdx < parser_.getNumCameras(); ++curCamIdx) {
    CameraRect curCam = parser_.getNextCam();
    spaceCarver_.addCamCenter(conversionUtilities::toLoviMatrix(curCam.center));
    /*std::cout<<"center "<<curCamIdx;
    std::cout<<" x "<<curCam.center.x();
    std::cout<<" y "<<curCam.center.y();
    std::cout<<" z "<<curCam.center.z();*/

  }

  /*std::cout<<std::endl;*/

  for (auto curPt : parser_.getPoints()) {
    spaceCarver_.addPoint(conversionUtilities::toLoviMatrix(curPt));
    std::cout<<"point ";
        std::cout<<" x "<<curPt.x;
        std::cout<<" y "<<curPt.y;
        std::cout<<" z  "<< curPt.z << std::endl;
  }

  for (int curCamIdx = 0; curCamIdx < parser_.getNumCameras(); ++curCamIdx) {

    std::cout << "spaceCarver_.addVisibilityPair cam:" << curCamIdx<< std::endl;
    cv::Mat tmp,tmpres;
    std::stringstream s1;
   /* s1<<"/home/andrea/sharedVirtualBox/herzjesu_dense/urd/0"<<curCamIdx<<".jpg";
    tmp = cv::imread(s1.str());*/
    tmp = images_[curCamIdx];
    //cv::resize(tmp,tmpres,cv::Size(tmp.cols/4,tmp.rows/4));
    for (auto curPtIdx : parser_.getPointsVisibleFromCamN(curCamIdx)) {
      spaceCarver_.addVisibilityPair(curCamIdx, curPtIdx);
      Eigen::Matrix4f cam = parser_.getCams()[curCamIdx].P;
      Eigen::Vector4f p;
      p[0] = parser_.getPoints()[curPtIdx].x;
      p[1] = parser_.getPoints()[curPtIdx].y;
      p[2] = parser_.getPoints()[curPtIdx].z;
      p[3] = 1.0;

      Eigen::Vector4f point2D = cam*p;
      point2D[0] = point2D[0] / point2D[2];
      point2D[1] = point2D[1] / point2D[2];
      point2D[2] = point2D[2] / point2D[2];

      cv::Point2f Pfinal;
      Pfinal.x = point2D[0];
      Pfinal.y = point2D[1];
      cv::circle( tmp,Pfinal,2,cv::Scalar( 0, 0, 255 ),-1,8 );
    }
    cv::imshow("nouni",tmp);
   //cv::waitKey(0);




    if (curCamIdx > 0) {
      std::cout << "spaceCarver_.setShrinkEnabled(true) cam:" << curCamIdx<< std::endl;
      spaceCarver_.setShrinkEnabled(true);

    } else {
      std::cout << "spaceCarver_.setShrinkEnabled(false) cam:" << curCamIdx<< std::endl;
      spaceCarver_.setShrinkEnabled(false);
    }

    std::cout << "spaceCarver_.IterateTetrahedronMethod cam:" << curCamIdx<< std::endl;
    spaceCarver_.IterateTetrahedronMethod(curCamIdx);

    if (curCamIdx > 0) {
      std::cout << "spaceCarver_.regionGrowingIterative cam:" << curCamIdx<< std::endl;
      spaceCarver_.regionGrowingIterative(curCamIdx);
    }

    if (curCamIdx == 0) {

      std::cout << "spaceCarver_.regionGrowingBatch cam:" << curCamIdx<< std::endl;
      spaceCarver_.regionGrowingBatch(curCamIdx);
      //spaceCarver.findCriticalArtifacts(curC);
    }
    std::stringstream s;
    s<<curCamIdx;
    std::cout << "spaceCarver_.saveCurrentManifold cam:" << curCamIdx<< std::endl;
    saveCurrentManifold(spaceCarver_,s.str());
  }
}

