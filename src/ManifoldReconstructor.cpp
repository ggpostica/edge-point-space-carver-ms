/*
 * ManifoldReconstructor.cpp
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#include "ManifoldReconstructor.h"
#include "utilities/conversionUtilities.hpp"
#include <sstream>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/eigen.hpp>
#include <Eigen/Core>

ManifoldReconstructor::ManifoldReconstructor(ManifoldConfig myConf, Configuration conf) {

  manifConf_ = myConf;
  conf_ = conf;
  spaceCarver_ = new msc::FreespaceDelaunayManifold();
  spaceCarver_->setConfig(conf_);

  camParser_ = new CamParser(manifConf_.manifConfig.pathCamsPose);
  camParser_->parseFile();

  std::vector<std::string> pathImages = camParser_->getCamerasPaths();
  for (auto curPath : pathImages) {
    images_.push_back(cv::imread(curPath, 0));
  }

  pointParser_ = new PointsParserFromOut(manifConf_.manifConfig.pathInitPoints, myConf.videoConfig.imageW, myConf.videoConfig.imageH);
  pointParser_->parse();

}

ManifoldReconstructor::~ManifoldReconstructor() {
}

void ManifoldReconstructor::storeManifoldMeshC(msc::FreespaceDelaunayManifold &spaceCarver, std::string nameFile) {

  std::cout << "storeManifoldMesh...";
  std::vector<msc::Matrix> m_arrModelPoints;
  std::vector<msc::Matrix> m_lstModelTris;
  spaceCarver.tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
  spaceCarver.writeObj(nameFile.c_str(), m_arrModelPoints, m_lstModelTris);
  std::cout << "DONE." << std::endl;
}
void ManifoldReconstructor::saveCurrentManifoldC(msc::FreespaceDelaunayManifold &spaceCarver, std::string suffix) {

  std::stringstream namefile;
  namefile << "ProvaHerzu_ManifinverseConicEnabled_" << suffix << ".obj";
  storeManifoldMeshC(spaceCarver, namefile.str());
}
void ManifoldReconstructor::addSfMPointsAndCams() {

  for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {
    CameraType curCam = camParser_->getCamerasList()[curCamIdx];
    spaceCarver_->addCamCenter(conversionUtilities::toLoviMatrix(curCam.center));
  }

  /*std::cout<<std::endl;*/

  for (auto curPt : pointParser_->getPoints()) {
    if (curPt.x > boundingMaxVert.x)
      boundingMaxVert.x = 1.1*curPt.x;
    if (curPt.y > boundingMaxVert.y)
      boundingMaxVert.y = 1.1*curPt.y;
    if (curPt.z > boundingMaxVert.z)
      boundingMaxVert.z = 1.1*curPt.z;
    if (curPt.x < boundingMinVert.x)
      boundingMinVert.x = 1.1*curPt.x;
    if (curPt.y < boundingMinVert.y)
      boundingMinVert.y = 1.1*curPt.y;
    if (curPt.z < boundingMinVert.z)
      boundingMinVert.z = 1.1*curPt.z;

    spaceCarver_->addPoint(conversionUtilities::toLoviMatrix(curPt));
  }

  for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {

    std::cout << "spaceCarver_->addVisibilityPair cam:" << curCamIdx << " ";
    for (auto curPtIdx : pointParser_->getPointsVisibleFromCamN(curCamIdx)) {
      std::cout << "visPair: (cam " << curCamIdx << ", " << curPtIdx << ") ";
      spaceCarver_->addVisibilityPair(curCamIdx, curPtIdx);
    }
    std::cout << std::endl;
  }

}

void ManifoldReconstructor::runFirstIteration() {
  boundingMaxVert = glm::vec3(-100000);
  boundingMinVert = glm::vec3(100000);
  addSfMPointsAndCams();

  for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {
    //std::cout << "spaceCarver_->setShrinkEnabled(false) cam:" << curCamIdx << std::endl;
    spaceCarver_->setShrinkEnabled(false);

    //std::cout << "spaceCarver_->IterateTetrahedronMethod cam:" << curCamIdx << std::endl;
    spaceCarver_->IterateTetrahedronMethod(curCamIdx);
  }


  //std::cout << "spaceCarver_->regionGrowingBatch" << std::endl;
  spaceCarver_->regionGrowingBatch(0);

}
void ManifoldReconstructor::storeFreespaceMesh(std::string nameFile) {

  std::cout << "Convert Tetrahedra to Triangles...";
  std::vector<msc::Matrix> m_arrModelPoints;
  std::vector<msc::Matrix> m_lstModelTris;
  spaceCarver_->tetsToTris(m_arrModelPoints, m_lstModelTris, 1);
  writeOFF(nameFile.c_str(), m_arrModelPoints, m_lstModelTris);
  std::cout << "DONE." << std::endl;

}

void ManifoldReconstructor::run(std::vector<msc::Matrix> point, std::vector<std::vector<int> > vis, bool resetAll) {

  if (!resetAll) {
    // std::cout << "spaceCarver_->setShrinkEnabled(true)" << std::endl;
    spaceCarver_->setShrinkEnabled(true);

  } else {
    spaceCarver_->setShrinkEnabled(false);
    //std::cout << "resetAll triangulation" << std::endl;

    delete (spaceCarver_);
    spaceCarver_ = new msc::FreespaceDelaunayManifold();
    spaceCarver_->setConfig(conf_);
    boundingMaxVert = glm::vec3(-100000);
    boundingMinVert = glm::vec3(100000);
    addSfMPointsAndCams();
    /*spaceCarver_->resetReconstruction();*/

  }

  if (point.size() != vis.size()) {
    std::cerr << "point.size() != vis.size() not good" << std::endl;
    return;
  }

  for (int curPt = 0; curPt < point.size(); ++curPt) {
    if (point[curPt](0) > boundingMaxVert.x)
      boundingMaxVert.x = 1.1*point[curPt](0);
    if (point[curPt](1) > boundingMaxVert.y)
      boundingMaxVert.y = 1.1*point[curPt](0);
    if (point[curPt](2) > boundingMaxVert.z)
      boundingMaxVert.z = 1.1*point[curPt](2);
    if (point[curPt](0) < boundingMinVert.x)
      boundingMinVert.x = 1.1*point[curPt](0);
    if (point[curPt](1) < boundingMinVert.y)
      boundingMinVert.y = 1.1*point[curPt](1);
    if (point[curPt](2) < boundingMinVert.z)
      boundingMinVert.z = 1.1*point[curPt](2);
    int id = spaceCarver_->addPointWhere(point[curPt]);
    std::cout << "Point" << point[curPt](0) << " " << point[curPt](1) << " " << point[curPt](2) << " ";
    for (auto curVisPair : vis[curPt]) {
      std::cout << "visPair: (cam " << curVisPair << ", " << id << ") ";
      spaceCarver_->addVisibilityPair(curVisPair, id);
    }
    std::cout << std::endl;
  }

  std::cout << "spaceCarver_->IterateTetrahedronMethod cam: 0" << std::flush;
  //spaceCarver_->IterateTetrahedronMethod_suboptimal(0);
  spaceCarver_->IterateTetrahedronMethod(0);

  for (int curCamIdx = 1; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {
    spaceCarver_->setShrinkEnabled(false);
    std::cout << " DONE, cam: " << curCamIdx << std::flush;
    spaceCarver_->IterateTetrahedronMethod(curCamIdx);
    //spaceCarver_->IterateTetrahedronMethod_suboptimal(curCamIdx);
  }
  std::cout << "DONE" << std::endl;
  if (!resetAll) {
    // std::cout << "spaceCarver_->regionGrowingBatch" << std::endl;
    spaceCarver_->regionGrowingIterative(0);
  } else {
    spaceCarver_->regionGrowingBatch();

  }
}

void ManifoldReconstructor::computeMesh(bool bounding) {

  std::string nameFile = "dump.off";
  std::cout << "storeManifoldMesh...";
  std::vector<msc::Matrix> m_arrModelPoints;
  std::vector<msc::Matrix> m_lstModelTris;
  spaceCarver_->tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
  std::cout << "DONE." << std::endl;
  writeOFF(nameFile.c_str(), m_arrModelPoints, m_lstModelTris);

  mesh_.loadFormat(nameFile.c_str(), false);

}

void ManifoldReconstructor::writeOFF(const std::string filename, const std::vector<msc::Matrix> & points, const std::vector<msc::Matrix> & tris,
    bool bounding) {

  ofstream outfile;

  // Open file
  outfile.open(filename.c_str());
  if (!outfile.is_open()) {
    cerr << "Unable to open file: " << filename << endl;
    return;
  }

  outfile << "OFF" << std::endl;
  outfile << points.size() << " " << tris.size() << " 0" << std::endl;

  // Write out lines one by one.
  for (auto itPoints = points.begin(); itPoints != points.end(); itPoints++)
    outfile << static_cast<float>(itPoints->at(0)) << " " << static_cast<float>(itPoints->at(1)) << " " << static_cast<float>(itPoints->at(2)) << " " << endl;
  for (auto itTris = tris.begin(); itTris != tris.end(); itTris++) {
    int idxTr0 = static_cast<int>(round(itTris->at(0)));
    int idxTr1 = static_cast<int>(round(itTris->at(1)));
    int idxTr2 = static_cast<int>(round(itTris->at(2)));

    if (bounding) {
      if (boundingMinVert.x < points[idxTr0] && points[idxTr0] < boundingMaxVert.x &&
          boundingMinVert.x < points[idxTr0] && points[idxTr0] < boundingMaxVert.x &&
          boundingMinVert.x < points[idxTr0] && points[idxTr0] < boundingMaxVert.x &&
          boundingMinVert.y < points[idxTr1] && points[idxTr1] < boundingMaxVert.y &&
          boundingMinVert.y < points[idxTr1] && points[idxTr1] < boundingMaxVert.y &&
          boundingMinVert.y < points[idxTr1] && points[idxTr1] < boundingMaxVert.y &&
          boundingMinVert.z < points[idxTr2] && points[idxTr2] < boundingMaxVert.z &&
          boundingMinVert.z < points[idxTr2] && points[idxTr2] < boundingMaxVert.z &&
          boundingMinVert.z < points[idxTr2] && points[idxTr2] < boundingMaxVert.z ) {
        outfile << "3 " << idxTr0 << " " << idxTr1 << " " << idxTr2 << std::endl;
      }

    } else {
      outfile << "3 " << idxTr0 << " " << idxTr1 << " " << idxTr2 << std::endl;

    }
  }
  // Close the file and return
  outfile.close();
}

void ManifoldReconstructor::setWeightsSpaceCarver(float w_1, float w_2, float w_3) {
  spaceCarver_->set1(w_1);
  spaceCarver_->set2(w_2);
  spaceCarver_->set3(w_3);
}

void ManifoldReconstructor::setThSpaceCarver(float th) {
  spaceCarver_->setNum(th);
}

void ManifoldReconstructor::setInfinityVal(float inf) {
  spaceCarver_->setInfiniteValue(inf);
}
