/*
 * ManifoldReconstructor.h
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#ifndef MANIFOLDRECONSTRUCTOR_OLD_H_
#define MANIFOLDRECONSTRUCTOR_OLD_H_
#include "cam_parsers/MixedParser.h"
#include <vector>
#include "FreespaceDelaunayManifold.h"
#include "types.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

class ManifoldReconstructorOld {
  public:
    ManifoldReconstructorOld(Configuration myConf);
    virtual ~ManifoldReconstructorOld();

    void run();
  private:
    MIxedParser parser_;
    std::vector<cv::Mat> images_;
    msc::FreespaceDelaunayManifold spaceCarver_;
    Configuration conf_;

};

#endif /* MANIFOLDRECONSTRUCTOR_H_ */
